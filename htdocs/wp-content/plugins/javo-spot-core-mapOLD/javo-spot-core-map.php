<?php
/**
 * Plugin Name: Javo Spot Core Map Addons
 * Description: This plugin is requested for javo spot wordpress theme. it loads shortcodes and some custom code for javo spot theme.
 * Version: 1.0.7
 * Author: Javo Themes
 * Author URI: http://javothemes.com/spot/
 * Text Domain: javo
 * Domain Path: /languages/
 * License: GPLv2 or later */

if( ! defined( 'ABSPATH' ) )
	die;

if( ! class_exists( 'Javo_Spot_Core_Map' ) ) :

	class Javo_Spot_Core_Map
	{

		const DEBUG = false;
		public $post_type = 'lv_listing';
		public $name = 'listing';

		private $theme_name = 'javo_spot';

		private $theme_names = Array(
			'javo_spot' => 'jvfrm_spot',
			'javo_directory' => 'jvfrm_spot',
		);

		private $theme = null;

		public static $instance;

		public function __construct( $file ) {
			$this->file							= $file;
			$this->folder						= basename( dirname( $this->file ) );
			$this->path						= dirname( $this->file );
			$this->assets_url				= esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
			$this->include_path			= trailingslashit( $this->path ) . 'includes';
			$this->template_path		= trailingslashit( $this->path ) . 'templates';

			if( $this->theme_check( $this->theme_name ) ) {
				$this->load_files();
				$this->register_hooks();
			}

			do_action( 'Javo_Spot_Core_Map_init' );
		}

		public function theme_check( $theme_name )
		{
			$this->theme = wp_get_theme();
			$this->template = $this->theme->get( 'Name' );
			if( $this->theme->get( 'Template' ) ) {
				$this->parent = wp_get_theme(  $this->theme->get( 'Template' ) );
				$this->template = $this->parent->get( 'Name' );
			}
			$this->template = str_replace( ' ', '_', strtolower( $this->template ) );
			if( array_key_exists( sanitize_key( $this->template ), $this->theme_names ) )
				return true;
			return false;
		}

		public function load_template( $template_name, $options=Array(), $args=Array() ) {

			if( !$template_name )
				return false;

			$arrOption = wp_parse_args(
				$options,
				Array(
					'prefix'	=> 'template',
					'extension'	=> 'php',
				)
			);

			if( is_array( $args ) )
				extract( $args );

			$strPath = sprintf(
				'%1$s/%2$s-%3$s.%4$s',
				self::$instance->template_path,
				$arrOption[ 'prefix' ],
				$template_name,
				$arrOption[ 'extension' ]
			);

			if( file_exists( $strPath ) ) {
				require $strPath;
				return true;
			}
			return false;
		}

		public function load_files()
		{
			$arrFIles		= Array();
			$arrFIles[]		= $this->include_path . '/class-admin.php';
			$arrFIles[]		= $this->include_path . '/class-template.php';

			if( !empty( $arrFIles ) ) foreach( $arrFIles as $filename )
				if( file_exists( $filename ) )
					require_once $filename;
		}

		public function register_hooks() {

			add_action( 'init', Array( $this, 'load_core' ), 98 );
			add_action( 'after_setup_theme', Array( $this, 'theme_init' ) );
			add_action( 'admin_enqueue_scripts', Array( $this, 'admin_enqueue' ), 99 );
			add_action( 'lava_' . $this->post_type . '_map_box_enqueue_scripts', Array( $this, 'map_enqueue' ) );
		}

		public function theme_init() {
			add_action( 'wp_enqueue_scripts', Array( $this, 'enqueue' ), 99 );
			$this->template = new Javo_Spot_Core_Map_Template;
		}

		public function load_core() {
			$this->admin = new Javo_Spot_Core_Map_Admin;
		}

		public function enqueue() {
			/*
			wp_enqueue_style(
				'javo-spot-core-map-template-style'
				, $this->assets_url . 'css/map-extend.less'
			);*/

			wp_enqueue_style(
				'javo-spot-core-map-template-style'
				, $this->assets_url . 'css/map-extend.css'
			);

			/*
			wp_enqueue_style(
				'javo-spot-core-map-module-style'
				, $this->assets_url . 'css/map-modules.less'
			); */

			wp_enqueue_style(
				'javo-spot-core-map-module-style'
				, $this->assets_url . 'css/map-modules.css'
			);

		}

		public function getOption( $strKey='', $strDefault='' ){
			global $post;

			if( ! $post instanceof WP_Post )
				return $strDefault;

			$arrOptions = (Array) get_post_meta( $post->ID, 'jvfrm_spot_map_page_opt', true );
			if( isset( $arrOptions[ $strKey ] ) )
				$strDefault = $arrOptions[ $strKey ];
			return $strDefault;
		}

		public function map_enqueue() {

			wp_register_script(
				'javo-spot-core-map-extend-script'
				, $this->assets_url . 'js/map-extend.js'
				, Array( 'jquery' )
				, '0.0.1'
				, true
			);

			wp_localize_script(
				'javo-spot-core-map-extend-script',
				'jvfrm_spot_core_map_args',
				Array(
					'marker_animation' => $this->getOption( 'marker_animation' ),
					'first_marker_animation' => $this->getOption( 'fist_marker_animation' ),
					'default_pos_lat' => $this->getOption( 'default_pos_lat', 0 ),
					'default_pos_lng' => $this->getOption( 'default_pos_lng', 0 ),
					'auto_myposition' => $this->getOption( 'auto_myposition' ),
				)
			);
			wp_enqueue_script( 'javo-spot-core-map-extend-script' );

		}

		public function admin_enqueue()
		{

			wp_register_script(
				'javo-spot-core-map-admin-metabox',
				$this->assets_url . 'js/admin-metabox.js',
				Array( 'jquery' ),
				null,
				false
			);

			wp_enqueue_style(
				'javo-spot-core-map-admin-style'
				, $this->assets_url . 'css/admin-style.css'
			);

		}

		public static function get_instance( $file=null ) {
			if( null === self::$instance )
				self::$instance = new self( $file );
			return self::$instance;
		}

	}
endif;

if( !function_exists( 'jv_core_map' ) ) :
	function jv_core_map(){
		$objInstance = Javo_Spot_Core_Map::get_instance( __FILE__ );
		$GLOBALS[ 'Javo_Spot_Core_Map' ] = $objInstance;
		return $objInstance;
	}
	jv_core_map();
endif;
