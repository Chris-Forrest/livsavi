<table class="widefat">
	<tbody>
		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Filter Setting', 'javospot'); ?></h4></td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Amenities", 'javospot');?></label>
			</th>
			<td>
				<label>
					<input type="radio" name="jvfrm_spot_map_opts[amenities_filter]" value='' <?php checked( '' == jv_core_map()->getOption( 'amenities_filter' ) ); ?>>
					<?php _e( "OR (Default)", 'javospot');?>
				</label>
				<label>
					<input type="radio" name="jvfrm_spot_map_opts[amenities_filter]" value='and' <?php checked( 'and' == jv_core_map()->getOption( 'amenities_filter' ) ); ?>>
					<?php _e( "And", 'javospot');?>
				</label>
			</td>
		</tr>
	</tbody>
</table>