<table class="widefat">
	<tr>
		<td><label><?php _e( 'Display featured listings first', 'javospot' );?></label></td>
		<td><input type="radio" name="jvfrm_spot_map_opts[panel_list_featured_first]" value="enable" <?php checked(sanitize_text_field( jv_core_map()->getOption('panel_list_featured_first', 'disable' ) ) == 'enable'); ?>><?php _e('Enable', 'javospot'); ?>&nbsp;&nbsp;&nbsp;
			<input type="radio" name="jvfrm_spot_map_opts[panel_list_featured_first]" value="disable" <?php checked(sanitize_text_field( jv_core_map()->getOption('panel_list_featured_first', 'disable' ) ) == 'disable'); ?>><?php _e('Disable', 'javospot'); ?></td>
	</tr>

	<tr>
		<td><label><?php _e( 'Random listing display', 'javospot' );?></label></td>
		<td><input type="radio" name="jvfrm_spot_map_opts[panel_list_random]" value="enable" <?php checked(sanitize_text_field( jv_core_map()->getOption('panel_list_random', 'disable' ) ) == 'enable'); ?>><?php _e('Enable', 'javospot'); ?>&nbsp;&nbsp;&nbsp;
			<input type="radio" name="jvfrm_spot_map_opts[panel_list_random]" value="disable" <?php checked(sanitize_text_field( jv_core_map()->getOption('panel_list_random', 'disable') ) == 'disable'); ?>><?php _e('Disable', 'javospot'); ?></td>
	</tr>
</table>