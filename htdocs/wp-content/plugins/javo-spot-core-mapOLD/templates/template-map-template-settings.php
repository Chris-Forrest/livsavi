<?php
$jvfrm_spot_mapOptionArgs		= Array(

	'_panel_position'			=> Array(
		'label'						=> __( "Panel Position", 'javospot' ),
		'values'						=> Array(
			'' => __( "Right ( Default )", 'javospot' ),
			'map-layout-box-right-map'	=> __( "Left", 'javospot' ),
			'map-layout-top' => __( "Top-Bottom", 'javospot' )
		),
		'cond'						=> Array(
			'_map_type'	=> Array( '', 'map-layout-box-right-map' ),
			'_filter_type'	=> Array( 'map-layout-top' ),
			'_map_display_panel_or_content'	=> Array( 'map-layout-top' ),
		)
	),

	'_map_type'				=> Array(
		'label'					=> __( "Panel Width", 'javospot' ),
		'values'					=> Array(
			'map-layout-wide' 							=> __( "Narrow (433px) - 1 col", 'javospot' ),
			''			=> __( "Middle (700px) - 2 cols", 'javospot' ),
			'panel-col3-layout-wide'	=> __( "Wide (935px) <br /> - 3 cols", 'javospot' ),
			'panel-col4-layout-wide'	=> __( "wide (1200px) <br /> - 4 cols", 'javospot' ),
		),
		'hidden' => true,
	),

	'separator',

	'_map_display_panel_or_content'		=> Array(
		'label' => __( "Display Panel or Content", 'javospot' ),
		'values' => Array(
			'' => __( "Panel (Default)", 'javospot' ),
			'map-display-content'	=> __( "Content", 'javospot' )
		),
		'hidden' => true,
		'inner_separator' => true,
	),

	'_map_filter_position'		=> Array(
		'label' => __( "Search Filter Position", 'javospot' ),
		'values' => Array(
			'' => __( "On Panel (Default)", 'javospot' ),
			'map-layout-search-top'	=> __( "Upper Map", 'javospot' )
		),
	),

	'_filter_type'				=> Array(
		'label'					=> __( "Filter Type", 'javospot' ),
		'values'					=> Array(
			''						=> __( "Default", 'javospot' ),
			'jv-map-filter-type-bottom-oneline'	=> __( "One-Line Type", 'javospot' )
		),
		'hidden'						=> true,
		'cond'						=> Array(
			'_map_display_filter'	=> Array( 'jv-map-filter-type-bottom-oneline' ),
		),
	),

	'separator',

	'_map_display_filter'		=> Array(
		'label' => __( "Display Search Form", 'javospot' ),
		'values' => Array(
			'' => __( "Show (Default)", 'javospot' ),
			'hide'	=> __( "Hide", 'javospot' )
		),
		'hidden' => true,
		'inner_separator' => true,
	),

	'_switch'					=> Array(
		'label'						=> __( "Show Map/List Switcher", 'javospot' ),
		'values'					=> Array(
			'' => __( "Enable", 'javospot' ),
			'1' => __( "Disable", 'javospot' )
		)
	),

	'separator',

	'_listing_filter'					=> Array(
		'label'						=> __( "LIST - Display Filter", 'javospot' ),
		'values'					=> Array(
			'' => __( "Enable", 'javospot' ),
			'1' => __( "Disable", 'javospot' )
		)
	),

	'separator',

	'_page_listing'				=> Array(
		'label'						=> __( "Initial display - MAP or LIST first", 'javospot' ),
		'values'					=> Array(
			''						=> __( "Map", 'javospot' )	,
			'1'						=> __( "List", 'javospot' )
		)
	),
);

if( !empty( $jvfrm_spot_mapOptionArgs ) ) : foreach( $jvfrm_spot_mapOptionArgs as $key => $meta )
{
	if( $meta == 'separator' ) {
		echo "<div class=\"separator\"><hr></div>";
		continue;
	}

	$strAddition = Array();

	$has_innerSeparator = isset( $meta[ 'inner_separator' ] ) && $meta[ 'inner_separator' ];

	if( isset( $meta[ 'cond' ] ) )
		$strAddition = $meta[ 'cond' ];

	$thisValue	= get_post_meta( $post->ID, $key, true );

		printf(
			'<div class="jv-map-options-wrap%1$s%2$s" data-option="%3$s" data-cond=\'%4$s\'>',
			( isset( $meta[ 'hidden' ] ) ? ' hidden' : '' ),
			( $has_innerSeparator ? ' inner-separator' : '' ),
			$key,
			json_encode( $strAddition )
		);

		echo "<strong>{$meta['label']}</strong>";
		echo "<ul class=\"jv-map-options {$key}\">";
		if( is_array( $meta[ 'values' ] ) ) : foreach( $meta[ 'values' ] as $value => $title ) {
			$is_active	= $value == $thisValue ? ' active' : false;
			echo "
				<li class=\"jv-map-option-item value-{$value}{$is_active}\">
					<label><i></i>
						<input type=\"radio\" name=\"lava_map_param[$key]\" value=\"{$value}\"" . checked( $value == $thisValue, true, false ) . ">
						<span>{$title}</span>
					</label>
				</li>
			";
		} endif;
		echo "</ul>";
		if( $has_innerSeparator )
			echo "<div class=\"separator\"><hr></div>";
	echo "</div>";
} endif;

if( ! $jvfrm_spot_this_map_opt = get_post_meta( $post->ID, 'jvfrm_spot_map_page_opt', true ) )
	$jvfrm_spot_this_map_opt = Array();
$jvfrm_spot_mopt		= new jvfrm_spot_array( $jvfrm_spot_this_map_opt );
?>

<table class="widefat">
	<tbody>
		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Template Layout SETTING', 'javospot'); ?></h4></td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Search form background color ( Only One-Line Type )", 'javospot');?></label>
			</th>
			<td>
				<input type="text" name="jvfrm_spot_map_opts[one_line_color]" value="<?php echo $jvfrm_spot_mopt->get( 'one_line_color', '#f4f4f4' );?>" class="wp_color_picker wp-color-picker">
			</td>
		</tr>
		<?php do_action( 'jvfrm_spot_core_map_template_layout_setting_after', $post, $jvfrm_spot_mopt ); ?>
		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Markers (Pin / Icon) SETTING', 'javospot'); ?></h4></td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Listing hover effect", 'javospot');?></label>
			</th>
			<td>
				<select name="jvfrm_spot_map_opts[link_type]">
					<?php
					foreach(
						Array(
							''			=> __( "Type 1 (Default - No movement) ", 'javospot' ),
							'type2'	=> __( "Type 2 (Auto move to the listing)", 'javospot' ),
							'type3'	=> __( "Type 3 (More button)", 'javospot' ),
						) as $value => $label
					) printf(
						"<option value=\"{$value}\"%s>{$label}</option>",
						selected( $value == $jvfrm_spot_mopt->get( 'link_type', '' ), true, false )
					); ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Map Marker (Pin/Icon) Setup", 'javospot');?></label>
			</th>
			<td>
				<select name="jvfrm_spot_map_opts[marker_type]">
					<?php
					foreach(
						Array(
							''			=> __( "Category Marker Icons", 'javospot' ),
							'default'	=> __( "Default Marker Icons", 'javospot' ),
						) as $value => $label
					) printf(
						"<option value=\"{$value}\"%s>{$label}</option>",
						selected( $value == $jvfrm_spot_mopt->get( 'marker_type', '' ), true, false )
					); ?>
				</select>
			</td>
		</tr>
		<tr>
			<th valign="top">
				<label><?php _e( "Default map marker<br /><small>(For default marker icons and Type2)</small>", 'javospot' );?></label>
			</th>
			<td>
				<div class="jv-uploader-wrap">
					<input type="text" name="jvfrm_spot_map_opts[map_marker]" value="<?php echo esc_attr( $jvfrm_spot_mopt->get('map_marker'));?>" >
					<button type="button" class="button button-primary upload" data-title="<?php _e( "Marker Selector", 'javospot' ); ?>" data-btn="<?php _e( "Select", 'javospot' ); ?>">
						<span class="dashicons dashicons-admin-appearance"></span>
						<?php _e( "Marker Select", 'javospot' ); ?>
					</button>
					<button type="button" class="button remove">
						<?php _e( "Delete", 'javospot' );?>
					</button>
					<div><small> ( <?php _e( "Blank is marker image as theme settings", 'javospot' ); ?> ) </small></div>
					<h4><?php _e("Preview","javo"); ?></h4>
					<img src="<?php echo esc_attr( $jvfrm_spot_mopt->get( 'map_marker' ) );?>">
				</div>
			</td>
		</tr>
		<tr>
			<th valign="top">
				<label><?php _e( "Hover map marker<br /><small>(For Type2 : default marker icons only)</small>", 'javospot' );?></label>
			</th>
			<td>
				<div class="jv-uploader-wrap">
					<input type="text" name="jvfrm_spot_map_opts[after_map_marker]" value="<?php echo esc_attr( $jvfrm_spot_mopt->get('after_map_marker'));?>" >
					<button type="button" class="button button-primary upload" data-title="<?php _e( "Marker Selector", 'javospot' ); ?>" data-btn="<?php _e( "Select", 'javospot' ); ?>">
						<span class="dashicons dashicons-admin-appearance"></span>
						<?php _e( "Marker Select", 'javospot' ); ?>
					</button>
					<button type="button" class="button remove">
						<?php _e( "Delete", 'javospot' );?>
					</button>
					<h4><?php _e("Preview","javo"); ?></h4>
					<img src="<?php echo esc_attr( $jvfrm_spot_mopt->get( 'after_map_marker' ) );?>">
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Zoom Level (when mouse over a list)<small>(For Type2 : default marker icons only)</small>", 'javospot' );?></label>
			</th>
			<td>
				<input type="number" class="large-text" name="jvfrm_spot_map_opts[marker_zoom_level]" value="<?php echo sanitize_text_field( $jvfrm_spot_mopt->get('marker_zoom_level', 0 ) ); ?>">
				<div><small><?php _e( "( 0 or Blank is OFF. 1 ~ 24 (Higher is closer) ", 'javospot' ); ?></small></div>
			</td>
		</tr>
		<?php
		/*
		<tr>
			<th valign="top">
				<label><?php _e( "Map Marker Shadow", 'javospot' );?></label>
			</th>
			<td>
				<div class="jv-uploader-wrap">
					<input type="text" name="jvfrm_spot_map_opts[map_marker_shadow]" value="<?php echo esc_attr( $jvfrm_spot_mopt->get('map_marker_shadow'));?>" >
					<button type="button" class="button button-primary upload" data-title="<?php _e( "Marker Selector", 'javospot' ); ?>" data-btn="<?php _e( "Select", 'javospot' ); ?>">
						<span class="dashicons dashicons-admin-appearance"></span>
						<?php _e( "Shadow Select", 'javospot' ); ?>
					</button>
					<button type="button" class="button remove">
						<?php _e( "Delete", 'javospot' );?>
					</button>
					<h4><?php _e("Preview","javo"); ?></h4>
					<img src="<?php echo esc_attr( $jvfrm_spot_mopt->get( 'map_marker_shadow' ) );?>">
				</div>
			</td>
		</tr>
		*/ ?>
		<tr>
			<th valign="top">
				<label><?php _e( "Hover Marker Animation ( Hover on lists )", 'javospot' );?></label>
			</th>
			<td>
				<select name="jvfrm_spot_map_opts[marker_animation]">
					<?php
					foreach(
						Array(
							''			=> __( "Disable", 'javospot' ),
							'drop'		=> __( "Drop", 'javospot' ),
							'bounce'	=> __( "Bounce", 'javospot' ),
						) as $value => $label
					) printf(
						"<option value=\"{$value}\"%s>{$label}</option>",
						selected( $value == $jvfrm_spot_mopt->get( 'marker_animation', '' ), true, false )
					); ?>
				</select><small><?php _e('( For Type2 : when a listing is hovered )', 'javospot' ); ?></small>
			</td>
		</tr>
		<tr>
			<th valign="top">
				<label><?php _e( "Initial Marker Animation", 'javospot' );?></label>
			</th>
			<td>
				<select name="jvfrm_spot_map_opts[fist_marker_animation]">
					<?php
					foreach(
						Array(
							''			=> __( "Disable", 'javospot' ),
							'enable'	=> __( "Enable", 'javospot' ),
						) as $value => $label
					) printf(
						"<option value=\"{$value}\"%s>{$label}</option>",
						selected( $value == $jvfrm_spot_mopt->get( 'fist_marker_animation', '' ), true, false )
					); ?>
				</select><small><?php _e('( Drop animation after filtering or loading )', 'javospot' ); ?></small>
			</td>
		</tr>
		<?php do_action( 'jvfrm_spot_core_map_markers_setting_after', $post, $jvfrm_spot_mopt ); ?>
		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Cluster / Zoom SETTING', 'javospot'); ?></h4></td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Initial Map Zoom Level", 'javospot' );?></label>
			</th>
			<td>
				<input type="number" class="large-text" name="jvfrm_spot_map_opts[init_map_zoom]" value="<?php echo sanitize_text_field( $jvfrm_spot_mopt->get('init_map_zoom', 0 ) ); ?>">
				<div><small><?php _e( "( 0 or Blank is OFF. 1 ~ 24 (Higher is closer) ", 'javospot' ); ?></small></div>
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Turn on 'My Current Location' as default", 'javospot' );?></label>
			</th>
			<td>
				<?php
				foreach(
					Array( '' => __( 'Disable (Default)', 'javospot' ), 'enable' => __( 'Enable', 'javospot' ) ) as $strValue => $strLabel
				) printf(
					'<label><input type="radio" name="jvfrm_spot_map_opts[auto_myposition]" value="%1$s" %3$s>%2$s</label>',
					$strValue, $strLabel, checked( $strValue == $jvfrm_spot_mopt->get( 'auto_myposition' ), true, false )
				); ?>
				<div><small><?php _e( "(It will get visitors current location when this page loads. This feature reqires SSL)", 'javospot' ); ?></div>
			</td>
		</tr>
		<tr>
		<tr>
			<th>
				<label><?php _e( "Marker Cluster ( Grouping )", 'javospot' );?></label>
			</th>
			<td>
				<select name="jvfrm_spot_map_opts[cluster]">
					<?php
					foreach(
						Array(
							''			=> __( "Enable", 'javospot' ),
							'disable'	=> __( "Disable", 'javospot' ),
						) as $value => $label
					) printf(
						"<option value=\"{$value}\"%s>{$label}</option>",
						selected( $value == $jvfrm_spot_mopt->get( 'cluster', '' ), true, false )
					); ?>
				</select>
				<div><small><?php _e( "For Listing hover effect Type 2, you should disalbe. ", 'javospot' ); ?></small></div>
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Marker Cluster ( Grouping ) Radius", 'javospot' );?></label>
			</th>
			<td>
				<input type="number" class="large-text" name="jvfrm_spot_map_opts[cluster_level]" value="<?php echo sanitize_text_field( $jvfrm_spot_mopt->get('cluster_level', 100 ) ); ?>">
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Search Maximum Distance Radius Range", 'javospot' );?></label>
			</th>
			<td>
				<input type="number" class="large-text" name="jvfrm_spot_map_opts[distance_max]" value="<?php echo sanitize_text_field( $jvfrm_spot_mopt->get('distance_max', 100 ) ); ?>">
				<div><small><?php _e( "It`s for search distance or geolocation radius. ", 'javospot' ); ?></small></div>
			</td>
		</tr>
		<tr>
			<th>
				<label><?php _e( "Number of posts to list ( Number of posts to load )", 'javospot' );?></label>
			</th>
			<td>
				<input type="number" class="large-text" name="jvfrm_spot_map_opts[loadmore_amount]" value="<?php echo sanitize_text_field( $jvfrm_spot_mopt->get('loadmore_amount', 10 ) ); ?>">
				<div><small><?php _e( "( 0 or Blank is 10 posts )", 'javospot' ); ?></small></div>
			</td>
		</tr>

		<tr>
			<th>
				<label><?php _e( "Map Primary Color / Background Color", 'javospot' );?></label>
			</th>
			<td>
				<input type="text" name="jvfrm_spot_map_opts[map_primary_color]" value="<?php echo esc_attr( $jvfrm_spot_mopt->get( 'map_primary_color', null ) );?>" class="wp_color_picker">
				<div>
					<small><?php _e( "Blank, Clear Color will be back to normal (default) map color", 'javospot' ); ?></small>
				</div>
			</td>
		</tr>

		<tr>
			<th>
				<label><?php _e( "Advanced Google Map Styles", 'javospot' );?></label>
			</th>
			<td>
				<textarea name="jvfrm_spot_map_opts[map_style_json]" rows="5" style="width:100%;"><?php echo esc_html( $jvfrm_spot_mopt->get( 'map_style_json', null ) );?></textarea>
				<div>
					<?php
					printf(
						__( 'Please <a href="%1$s" target="_blank">click here</a> to create your own stlye and paste json code here.', 'javospot' ),
						esc_url( 'mapstyle.withgoogle.com' )
					); ?>
				</div>
				<div>
					<small>( <?php esc_html_e( "This code will overwritten map color setting", 'javospot' ); ?> )</small>
				</div>
			</td>
		</tr>

		<tr>
			<th>
				<label><?php _e( "Distance Unit", 'javospot' );?></label>
			</th>
			<td>
				<input type="radio" name="jvfrm_spot_map_opts[distance_unit]" value='' <?php checked( '' == $jvfrm_spot_mopt->get( 'distance_unit', null ) );?>>
				<?php printf( '%1$s (%2$s)', esc_html__( 'KM', 'javospot' ), esc_html__( 'Default', 'javospot' ) );?>
				<input type="radio" name="jvfrm_spot_map_opts[distance_unit]" value='mile' <?php checked( 'mile' == $jvfrm_spot_mopt->get( 'distance_unit', null ) );?>>
				<?php printf( '%1$s', esc_html__( 'Mile', 'javospot' ) );?>
			</td>
		</tr>

		<?php do_action( 'jvfrm_spot_core_map_zoom_setting_after', $post, $jvfrm_spot_mopt ); ?>

		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Listing Module setting', 'javospot'); ?></h4></td>
		</tr>

		<?php if( class_exists( 'Lava_Directory_Review' ) ) : ?>
			<tr>
				<th>
					<label><?php _e( "Module Display Rating Type", 'javospot' );?></label>
				</th>
				<td>
					<select name="jvfrm_spot_map_opts[map_rating_type]">
						<?php
						foreach(
							Array(
								'star'				=> __( "Stars ( default )", 'javospot' ),
								'number'		=> __( "Numeric", 'javospot' ),
								'disabled'		=> __( "Disabled" ,'javospot' ),
							) as $type_key => $type_label
						) printf(
							"<option value=\"{$type_key}\" %s>{$type_label}</option>",
							selected( $type_key == $jvfrm_spot_mopt->get( 'map_rating_type' ), true, false )
						); ?>
					</select>
				</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>

<?php do_action( 'jvfrm_spot_core_map_setting_after' ); ?>

<script type="text/javascript">
jQuery( function( $ ) {
	"use strict";
	var
		container		= $( "ul.jv-map-options" )
		, option			= $( "input[name^='lava_map_param']", container );
	option.on( 'change', function() {
		var parent	= $( this ).closest( 'ul.jv-map-options' );
		$( 'li', parent ).removeClass( 'active' );
		$( this ).closest( 'li' ).addClass( 'active' );
	} );
} );
</script>