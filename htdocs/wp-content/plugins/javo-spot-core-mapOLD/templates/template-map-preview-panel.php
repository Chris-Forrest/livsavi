<div id="lava-map-box-more-panel" class="hidden">
	<div class="lava-mhome-item-info-wrap" style="position:relative; left: auto;">
		<div class="lava-mhome-item-info-loading-cover hidden"></div>
		<div class="lava-mhome-item-info-inner">
			<div class="row">
				<div class=" col-md-12 lava-mhome-item-img-wrap">
					<i class="fa fa-times lava-map-box-more-panel-close"></i>
					<div id="lava-detail-panel-thumbnail"></div>
					<div id="lava-detail-panel-title" class="lava-mhome-item-img-heading-title"></div>
					<div class="lava-map-inner-bg-overlay"></div>
				</div><!--/lava-mhome-item-img-wrap-->
			</div><!-- /.row -->
			<div class="row" id="lava-mhome-item-tabs-navi">
				<div class="col-md-12 lava-mhome-item-tabs-wrap">
					<div class="header-tabs">
						<ul class="nav tabs-container">
							<a class="detail-tab scroll-spy force-active col-md-3" data-href="#basic-card" href="#basic-card">
								<h4 class="text-center"><?php echo strtoupper( __( "condition", 'lava_fr' ) );?></h4>
							</a>
							<a class="detail-tab scroll-spy col-md-3" data-href="#amenities-card" href="#amenities-card">
								<h4 class="text-center"><?php echo strtoupper( __( "Description", 'lava_fr' ) );?></h4>
							</a>
						</ul><!-- /.tabs-container -->
					</div><!-- /.header-tabs -->
				</div><!--/lava-mhome-item-tabs-wrap-->
			</div><!-- /.row -->

			<div class="lava-mhome-item-contents-wrap">

				<div class="lava-mhome-item-basic-wrap" id="basic-card">

					<div class="row lava-mhome-item-condition">
						<div class="col-md-12 heading-wrap">
							<h2><?php _e( "Job condition", 'lava_fr' );?></h2>
						</div><!-- /.col-md-12 -->
						<div class="col-md-12 panel job-condition-panel">
							<div class="panel-body">
								<div class="row summary_items">
									<div class="col-md-12 col-xs-12">
										<div class="row">
											<div class="col-md-5 col-xs-12"><span><?php _e( "Category", 'lava_fr');?></span></div>
											<div class="col-md-7 col-xs-12"><span id="lava-detail-panel-category"></span></div>
										</div>
										<div class="row">
											<div class="col-md-5 col-xs-12"><span><?php _e( "Location", 'lava_fr'); ?></span></div>
											<div class="col-md-7 col-xs-12"><span id="lava-detail-panel-location"></span></div>
										</div>
										<div class="row">
											<div class="col-md-5 col-xs-12">
												<span class="hidden-xs"><?php _e( "Salary", 'lava_fr'); ?></span>
											</div>
											<div class="col-md-7 col-xs-12">
												<span><span class="lava-detail-panel-salary"></span></span>
											</div>
										</div>
										<div class="row">
											<div class="col-md-5 col-xs-12"><span><?php _e( "Recruit By", 'lava_fr' ); ?></span></div>
											<div class="col-md-7 col-xs-12"><span id="lava-detail-panel-startday"></span></div>
										</div>
										<div class="row">
											<div class="col-md-5 col-xs-12"><span><?php _e( "How many", 'lava_fr' ); ?></span></div>
											<div class="col-md-7 col-xs-12"><span id="lava-detail-panel-howmany"></span></div>
										</div>
										<div class="row">
											<div class="col-md-5 col-xs-12"><span><?php _e( "Contact Type", 'lava_fr' ); ?></span></div>
											<div class="col-md-7 col-xs-12"><span id="lava-detail-panel-type"></span></div>
										</div>
									</div>
								</div><!--/.summary_items-->
							</div><!--/.panel-body-->
						</div>
					</div><!-- /.lava-mhome-item-title-->

					<div class="row lava-mhome-item-description">
						<div class="col-md-12 heading-wrap">
							<h2><?php _e( "Description", 'lava_fr' );?></h2>
						</div><!-- /.col-md-12 -->
						<div class="lava-mhome-item-description-panel col-md-12">
							<div class="lava-mhome-item-description-inner" id="lava-detail-panel-describe"></div><!-- /.lava-mhome-item-description-inner -->
						</div><!--/.lava-mhome-item-description-panel-->
					</div><!-- /.lava-mhome-item-description-->

				</div><!-- /.lava-mhome-item-contents-inner -->

				<?php
				/*

				<div class="lava-mhome-item-amenities-wrap" id="amenities-card">
					<div class="row lava-mhome-item-amenities" >
						<div class="col-md-12 heading-wrap">
							<h2><?php _e( "Amenities", 'lava_fr' );?></h2>
						</div><!-- /.col-md-12 -->
						<div class="col-md-12 amenities-title" data-toggle="collapse" data-target="#collapseAmenities">
							<h3><?php echo strtoupper( __("Amenities", 'lava_fr') );?></h3>
						</div><!--/.amenities-title-->
						<div class="panel-collapse collapse in" id="collapseAmenities">
							<div id="lava-detail-panel-terms" class="col-md-12"></div>
						</div>

					</div><!-- /.row -->
				</div><!-- /.lava-mhome-item-amenities-wrap -->
				<div class="lava-mhome-item-commute-wrap" id="commute-card">
					<div class="row lava-mhome-item-commute">
						<div class="col-md-12 heading-wrap">
							<h2><?php _e( "Description", 'lava_fr' );?></h2>
						</div><!-- /.col-md-12 -->
					</div><!-- /.row -->
					<div class="row" data-toggle="collapse" data-target="#collapseBus">
						<div class="col-md-12 commute-title">
							<h3><i class="fa fa-bus"></i><?php _e( "Bus", 'lava_fr' );?></h3>
						</div>
					</div>
					<div class="panel-collapse collapse" id="collapseBus">
						<ul class="commute-inner list-unstyled lava-detail-panel-commute bus_station"></ul>
					</div><!--/#collapseBus-->

					<div class="row" data-toggle="collapse" data-target="#collapseTrain">
						<div class="col-md-12 commute-title">
							<h3><i class="fa fa-bus"></i><?php _e( "Train", 'lava_fr' );?></h3>
						</div>
					</div>
					<div class="panel-collapse collapse" id="collapseTrain">
						<ul class="commute-inner list-unstyled lava-detail-panel-commute train_station"></ul>
					</div><!--/#collapseTrain-->
				</div><!--/.lava-mhome-item-commute-wrap-->

				*/ ?>

			</div><!-- /.lava-mhome-item-contents-wrap -->
		</div><!-- /.lava-mhome-item-info-inner -->
	</div><!-- /.lava-mhome-item-info-wrap -->
	<div class="row contact-wrap">
		<div class="col-md-6">
			<i class="icon icon-contact"></i>
			<span class="text">
				<a href="#" id="lava-detail-panel-permalink"><?php _e( "See Detail Page", 'lava_fr' ); ?></a></span>
		</div>
		<div class="col-md-6">
			<i class="icon icon-shortlist"></i>
			<span class="text">
				<a id="lava-detail-panel-favorite" class="lava_favorite" data-no-swap="yes" data-post-id></a>
					<?php _e( "Save List", 'lava_fr' ); ?>
				</a>
			</span>
		</div>
	</div><!-- /.row.contact-wrap -->
</div><!-- #lava-map-box-more-panel -->