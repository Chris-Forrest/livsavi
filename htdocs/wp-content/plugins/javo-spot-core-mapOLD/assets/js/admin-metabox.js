;( function( $ ){

	var javo_spot_core_map_admin = function(){
		this.init();
	}

	javo_spot_core_map_admin.prototype = {

		constructor : javo_spot_core_map_admin,

		init : function(){

			var obj = this;

			obj.attr = javo_spot_core_map_args;
			obj.el = $( obj.attr.selector );

			obj
				.toggleOptionsTrigger()
				.toggleOptions()
				.defaultPos();

		},

		toggleOptionsTrigger : function(){

			var
				obj = this,
				element = obj.attr.el,
				options = $( '.jv-map-options-wrap', element );

			$( 'input[name^="lava_map_param"]', options ).on( 'change',
				function( e ) {
					e.preventDefault();
					obj.toggleOptions();
				}
			);

			return obj;

		},

		toggleOptions : function(){

			var
				obj = this,
				element = obj.attr.el,
				options = $( '.jv-map-options-wrap', element );

			options.each( function(){

				var
					strOption = $( this ).data( 'option' ),
					conditions = $( this ).data( 'cond' ),
					input = $( 'input[name="lava_map_param[' + strOption + ']"]:checked', this );
					currentValue = input.val();

				$.each( conditions, function( option_name, option_value ){

					var
						is_visible = false,
						division = $( 'div[data-option="' + option_name + '"]' );

					$.each( option_value, function( optIndex, optValue ) {

						if( currentValue == optValue )
							is_visible = true;
					} );

					if( is_visible ) {
						division.removeClass( 'hidden' );
					}else{
						division.addClass( 'hidden' );
					}
				});
			} );

			return obj;
		},

		defaultPos : function() {

			var
				obj = this,
				container = $( 'div.jv-page-settings-wrap' ),
				nav = $( 'li.jv-page-settings-nav-item.option-map', container ),
				wrap = $( '.jv-default-pos-wrap' ),
				map = $( '.jv-default-pos-map', wrap ),
				lat = $( 'input[name="jvfrm_spot_map_opts[default_pos_lat]"]', wrap ),
				lng = $( 'input[name="jvfrm_spot_map_opts[default_pos_lng]"]', wrap ),
				intLat = parseFloat( lat.val() || 0 ),
				intLng = parseFloat( lng.val() || 0 ),
				latLng = new google.maps.LatLng( intLat, intLng )
				inputChangeCallBack = function( e ){
					map.gmap3({
						get:{
							name: 'marker',
							callback : function( marker ) {
								var
									thisMap = map.gmap3( 'get' ),
									intLat = parseFloat( lat.val() || 0 ),
									intLng = parseFloat( lng.val() || 0 ),
									latLng = new google.maps.LatLng( intLat, intLng );

								marker.setPosition( latLng );
								thisMap.panTo( latLng );
							}
						}
					});
				};

			if( typeof $.fn.gmap3 != 'undefined' ) {

				lat.on( 'blur', inputChangeCallBack );
				lng.on( 'blur', inputChangeCallBack );

				nav.on( 'click', function() {

					if( ! obj.defaultPosMapLaoded ){
						map.css( 'height', 300 );
						map.gmap3({
							map : {
								options:{
									zoom : 5,
									center : latLng
								},
								events : {
									click : function( markerm, point ) {
										lat.val( point.latLng.lat );
										lng.val( point.latLng.lng ).trigger( 'blur' );
									}
								}
							},
							marker : {
								'latLng' : latLng,
								options:{
									draggable : true
								},
								events : {
									dragend : function( marker ) {
										lat.val( marker.getPosition().lat );
										lng.val( marker.getPosition().lng );
									}
								}
							}
						});
						obj.defaultPosMapLaoded = true;
					}
					map.gmap3({ trigger : 'resize' });
				} );
			}

			return obj;
		}
	}

	new javo_spot_core_map_admin;

} )( jQuery );