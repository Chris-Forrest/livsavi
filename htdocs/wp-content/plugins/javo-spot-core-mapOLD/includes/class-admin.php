<?php

class Javo_Spot_Core_Map_Admin extends Javo_Spot_Core_Map
{

	const MAP_MODULE	= '_map_module';
	const LIST_MODULE	= '_list_module';

	public $modules		= Array();
	public $exclude_modules = Array(
		'module2',
		'module3',
		'module4',
		'module5',
		'module6',
		'module7',
		'module9',
		'module10',
		'module11',
		'module13',
		'module14',
		'moduleSmallGrid',
		'moduleBigGrid',
		'moduleHorizontalGrid',
		'moduleWC1',
	);

	public $hide_avatar_modules	= Array(
		'module12',
	);

	public $is_hide_avatar_module	= Array(
		'map'		=> false,
		'list'			=> false
	);

	public function __construct() {
		$this->register_hooks();
		$this->map_callback_hooks();
		$this->marker_hooks();
	}

	public function register_hooks() {
		// Edit Page
		add_action( 'jvfrm_spot_admin_page_options', Array( $this, 'custom_map_add_options_panel' ) );
		add_action( 'jvfrm_spot_admin_page_options_template', Array( $this, 'custom_map_add_options_template' ), 10, 2 );
		add_action( 'jvfrm_spot_modules_loaded', Array( $this, 'parseModules' ) );
		add_action( 'jvfrm_spot_core_map_setting_after', Array( $this, 'module_selector' ) );
		add_action( 'jvfrm_spot_core_map_setting_after', Array( $this, 'order_selector' ) );
		add_action( 'jvfrm_spot_core_map_setting_after', Array( $this, 'default_position_selector' ) );
		add_action( 'jvfrm_spot_core_map_setting_after', Array( $this, 'filter_settings' ) );
	}

	public function map_callback_hooks() {
		// Module Name
		add_filter( 'jvfrm_spot_template_map_module', Array( $this, 'custom_template_map_module' ), 10, 2 );
		add_filter( 'jvfrm_spot_template_list_module', Array( $this, 'custom_template_list_module' ), 10, 2 );

		// Module Layout Template
		add_filter( 'jvfrm_spot_template_map_module_loop', Array( $this, 'custom_loop_map_module' ), 10, 3 );
		add_filter( 'jvfrm_spot_template_list_module_loop', Array( $this, 'custom_loop_list_module' ), 10, 3 );

		// Module Parameter
		add_filter( 'jvfrm_spot_template_map_module_options', Array( $this, 'custom_options_map_module' ), 10, 2 );
		add_filter( 'jvfrm_spot_template_list_module_options', Array( $this, 'custom_options_list_module' ), 10, 2 );

		// Create Shortcode Class Instance
		add_action( 'jvfrm_spot_template_all_module_loop_before', Array( $this, 'virtual_map_shortcode' ), 11 );
	}

	public function marker_hooks(){
		// Category
		add_action( "{$this->name}_category_edit_form_fields"	, Array($this,'edit_featured_term'), 10, 2);
		add_action( "{$this->name}_category_add_form_fields"	, Array($this, 'add_featured_term'));
		add_action( "created_{$this->name}_category", Array($this, 'save_featured_term'), 10, 2);
		add_action( "edited_{$this->name}_category", Array($this, 'save_featured_term'), 10, 2);
		add_action( 'deleted_term_taxonomy', Array($this, 'remove_featured_term'));
	}

	public function custom_map_add_options_panel( $panels ) {
		return wp_parse_args(
			Array(
				'option-map'	=> Array(
					'label'		=> __( "Map", 'javo' ),
					'icon'		=> 'fa fa-map-marker',
					'post_type'	=> Array( 'page' ),
					'require'	=> 'lava_' . self::$instance->post_type . '_map'
				)
			)
			, $panels
		);
	}

	public function getModuleMeta( $type='map', $post_id=0, $suffix='' ){
		global $post;

		if( ! $post instanceof WP_Post )
			$post = (object) Array(
				'ID' => 0
			);

		if( ! intVal( $post_id ) )
			$post_id = $post->ID;

		$strKey	= $type == 'map' ? self::MAP_MODULE : self::LIST_MODULE;
		return get_post_meta( $post_id, $strKey . $suffix, true );
	}

	public function getModuleName( $type='map', $post_id=0 ){
		return $this->getModuleMeta( $type,$post_id, false );
	}

	public function getModuleColumns( $type='map', $post_id=0 ){
		return intVal( $this->getModuleMeta( $type,$post_id, '_column' ) );
	}

	public function getModuleLength( $type='map', $post_id=0 ){
		return intVal( $this->getModuleMeta( $type,$post_id, '_excerpt_length' ) );
	}

	public function getMapOptionObject( $post=0 ) {

		if( is_numeric( $post ) )
			$post = get_post( $post_id );

		if( ! is_object( $post ) )
			return false;

		if( ! $arrMapOptions = get_post_meta( $post->ID, 'jvfrm_spot_map_page_opt', true ) )
			$arrMapOptions = array();

		return new jvfrm_spot_array( $arrMapOptions );
	}

	public function custom_map_add_options_template( $filePath, $optionName=null ) {

		if( $optionName === 'option-map' ) {

			wp_localize_script(
				'javo-spot-core-map-admin-metabox',
				'javo_spot_core_map_args',
				Array(
					'selector' => '.jv-page-settings-content.option-map'
				)
			);

			wp_enqueue_script( 'javo-spot-core-map-admin-metabox' );

			$filePath = self::$instance->template_path .'/template-map-template-settings.php';
		}

		return $filePath;
	}

	public function parseModules( $modules=Array() ) {
		if( is_Array( $modules ) )
			$this->modules	= Array_diff( Array_Keys( $modules ), $this->exclude_modules );
	}

	public function module_selector() {
		$this->load_template( 'map-module-selector', array(), array( 'post' => $GLOBALS[ 'post' ] ) );
	}

	public function order_selector() {
		$this->load_template( 'admin-order-selector', array(), array( 'jvfrm_spot_mopt' => $this->getMapOptionObject( $GLOBALS[ 'post' ] ) ) );
	}

	public function default_position_selector() {
		$this->load_template( 'admin-position-selector', array(), array( 'jvfrm_spot_mopt' => $this->getMapOptionObject( $GLOBALS[ 'post' ] ) ) );
	}

	public function filter_settings() {
		$this->load_template( 'admin-filter-settings' );
	}

	public function custom_template_map_module( $module_name, $post_id=0 ) {

		if( $custom_module = $this->getModuleName( 'map', $post_id ) )
			$module_name		= $custom_module;

		$this->is_hide_avatar_module[ 'map' ]	= in_Array( $module_name, $this->hide_avatar_modules );
		return $module_name;
	}

	public function custom_template_list_module( $module_name, $post_id=0 ) {

		if( $custom_module = $this->getModuleName( 'list', $post_id ) )
			$module_name		= $custom_module;

		$this->is_hide_avatar_module[ 'list' ]	= in_Array( $module_name, $this->hide_avatar_modules );
		return $module_name;
	}

	public function custom_loop_map_module( $html_template, $module_name=null, $post_id=0 ) {
		if( $column = $this->getModuleColumns( 'map', $post_id ) ) {
			if( $column == 2 ) {
				$html_template		= '<div class="col-md-6">%s</div>';
			} else if( $column == 3 ) {
				$html_template		= '<div class="col-md-4">%s</div>';
			} else if( $column == 4 ) {
				$html_template		= '<div class="col-md-3">%s</div>';
			}
		}
		return $html_template;
	}

	public function custom_loop_list_module( $html_template, $module_name=null, $post_id=0 ) {
		if( $column = $this->getModuleColumns( 'list', $post_id ) ) {
			if( $column == 2 ) {
				$html_template		= '<div class="col-md-6">%s</div>';
			} else if( $column == 3 ) {
				$html_template		= '<div class="col-md-4">%s</div>';
			}
		}
		return $html_template;
	}

	public function custom_options_map_module( $options=Array(), $post_id=0 ) {
		$options[ 'hide_avatar' ]		= $this->is_hide_avatar_module[ 'map' ];
		if( '' !== ( $length = $this->getModuleLength( 'map', $post_id ) ) )
			$options = wp_parse_args(
				Array(
					'length_content'	=> intVal( $length ),
					'rating_type'			=> 'numeric',
				), $options
			);
		return $options;
	}

	public function custom_options_list_module( $options=Array(), $post_id=0 ) {
		$options[ 'hide_avatar' ]		= $this->is_hide_avatar_module[ 'list' ];
		if( '' !== ( $length = $this->getModuleLength( 'list', $post_id ) ) )
			$options = wp_parse_args( Array( 'length_content' => intVal( $length ) ), $options );
		return $options;
	}

	public function virtual_map_shortcode( $page_id ) {

		if( !class_exists( 'Lava_Directory_Review' ) || !function_exists( 'jvfrm_spot_core' ) )
			return;

		$arrMapOpt					= (Array) get_post_meta( $page_id, 'jvfrm_spot_map_page_opt', true );
		$strMapRatingTYPE		= isset( $arrMapOpt[ 'map_rating_type' ] ) ? $arrMapOpt[ 'map_rating_type' ] : 'star';
		$objVirtualShortcode	= (object) Array(
			'post_type'				=> jvfrm_spot_core()->slug,
			'rating_type'				=> $strMapRatingTYPE,
		);

		jvfrm_spot_core()->shortcode->parsed_shortcode( $objVirtualShortcode );
	}

	public function add_featured_term( $tag ) {
		?>

		<div class="form-field">
			<label for=""><?php _e('Map / List ?', 'Lavacode');?></label>
			<select name="lava_listing_category_type_option">
				<?php
				foreach(
					Array(
						0 => __( "Map", 'javospot' ),
						1 => __( "Archive", 'javospot' ),
					) as $intValue => $strLabel
				) printf(
					'<option value="%1$s">%2$s</option>',
					$intValue,
					$strLabel
				); ?>
			</select>
		</div>
		<?php

	}
	public function edit_featured_term($tag, $taxonomy) {
		$strType = get_option( 'lava_listing_category_'.$tag->term_id.'_type_option', 0 );
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for=""><?php _e( 'Map / List ?', 'Lavacode');?></label>
			</th>
			<td>
				<select name="lava_listing_category_type_option">
					<?php
					foreach(
						Array(
							0 => __( "Map", 'javospot' ),
							1 => __( "Archive", 'javospot' ),
						) as $intValue => $strLabel
					) printf(
						'<option value="%1$s"%2$s>%3$s</option>',
						$intValue,
						selected( $intValue == $strType, true, false ),
						$strLabel
					); ?>
				</select>
			</td>
		</tr>
		<?php
	}

	public function save_featured_term($term_id, $tt_id) {
		if (!$term_id) return;
		if (isset($_POST['lava_listing_category_type_option'])){
			$name = 'lava_listing_category_' .$term_id. '_type_option';
			update_option( $name, $_POST['lava_listing_category_type_option'] );
		}
	}

	public function remove_featured_term($id) {
		delete_option( 'lava_listing_category_'.$id.'_type_option' );
	}

}