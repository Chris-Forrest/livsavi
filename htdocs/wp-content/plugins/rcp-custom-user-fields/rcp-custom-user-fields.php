<?php
/**
 * Example for adding a custom checkbox field to the
 * Restrict Content Pro registration form and profile editors.
 */
/**
 * Adds a custom checkbox field to the registration form and profile editor.
 */
function ag_rcp_add_checkbox_field() {
    $special_offers = get_user_meta( get_current_user_id(), 'rcp_special_offers', true );
    ?>
    <p>
        <input name="rcp_special_offers" id="rcp_special_offers" type="checkbox" value="1" <?php checked( $special_offers ); ?>/>
        <label for="rcp_special_offers"><?php _e( 'Check to opt in to our special offers', 'rcp' ); ?></label>
    </p>

    <?php
}
add_action( 'rcp_after_password_registration_field', 'ag_rcp_add_checkbox_field' );
add_action( 'rcp_profile_editor_after', 'ag_rcp_add_checkbox_field' );
/**
 * Adds the custom checkbox field to the member edit screen.
 */
function ag_rcp_add_checkbox_member_edit_fields( $user_id = 0 ) {
    $special_offers = get_user_meta( $user_id, 'rcp_special_offers', true );
    ?>
    <tr valign="top">
        <th scope="row" valign="top">
            <label for="rcp_special_offers"><?php _e( 'Special Offers', 'rcp' ); ?></label>
        </th>
        <td>
            <input name="rcp_special_offers" id="rcp_special_offers" type="checkbox" <?php checked( $special_offers ); ?>/>
            <span class="description"><?php _e( 'Check to opt in to our special offers', 'rcp' ); ?></span>
        </td>
    </tr>
    <?php
}
add_action( 'rcp_edit_member_after', 'ag_rcp_add_checkbox_member_edit_fields' );
/**
 * Determines if there are problems with the registration data submitted.
 * Remove this code if you want the checkbox to be optional.
 */
function ag_rcp_validate_checkbox_on_register( $posted ) {
    if ( rcp_get_subscription_id() ) {
        return;
    }
    if ( ! isset( $posted['rcp_special_offers'] ) ) {
        rcp_errors()->add( 'invalid_special_offers', __( 'Please opt in to our special offers', 'rcp' ), 'register' );
    }
}
add_action( 'rcp_form_errors', 'ag_rcp_validate_checkbox_on_register', 10 );
/**
 * Stores the information submitted during registration
 *
 * Sets the meta value to `true` to designate that the checkbox was checked on.
 */
function ag_rcp_save_checkbox_field_on_register( $posted, $user_id ) {
    if ( isset( $posted['rcp_special_offers'] ) ) {
        update_user_meta( $user_id, 'rcp_special_offers', true );
    }
}
add_action( 'rcp_form_processing', 'ag_rcp_save_checkbox_field_on_register', 10, 2 );
/**
 * Stores the information submitted profile update
 *
 * Sets the meta value to `true` to designate that the checkbox was checked on.
 */
function ag_rcp_save_checkbox_field_on_profile_save( $user_id ) {
    if ( isset( $_POST['rcp_special_offers'] ) ) {
        // Set the user meta if the box was checked on.
        update_user_meta( $user_id, 'rcp_special_offers', true );
    } else {
        // Delete the user meta if the box is unchecked.
        delete_user_meta( $user_id, 'rcp_special_offers' );
    }
}
add_action( 'rcp_user_profile_updated', 'ag_rcp_save_checkbox_field_on_profile_save', 10 );
add_action( 'rcp_edit_member', 'ag_rcp_save_checkbox_field_on_profile_save', 10 );
