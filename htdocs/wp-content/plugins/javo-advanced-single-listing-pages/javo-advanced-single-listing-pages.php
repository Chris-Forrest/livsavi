<?php
/**
 * Plugin Name: Javo Advanced Single Listing Page
 * Description: Advanced single listing pages for Javo themes. you can have dynamic options to have various single listing page styles for Javo directory themes.
 * Version: 1.0.0
 * Author: Lavacode
 * Author URI: http://lava-code.com/spot/
 * Text Domain: lavacode
 * Domain Path: /languages/
 * License: GPLv2 or later */

if( ! defined( 'ABSPATH' ) )
	die;

if( ! class_exists( 'Javo_Spot_Single_Addon' ) ) :
	class Javo_Spot_Single_Addon
	{
		const SLUG = 'lv_listing';

		public $file = null;
		public $path = null;
		public $include_path = null;
		public $template_path = null;

		public static $instance = null;
		private $theme_names = Array(
			'javo_spot' => 'javo_spot',
			'javo_directory' => 'javo_spot'
		);

		public function __construct( $file ) {
			$this->file = $file;
			$this->setVariables();
			$this->load_files();
			$this->register_hooks();
		}

		public function setVariables() {
			$this->folder = basename( dirname( $this->file ) );
			$this->path = dirname( $this->file );
			$this->include_path = trailingslashit( $this->path ) . 'includes';
			$this->template_path = trailingslashit( $this->path ) . 'templates';
		}

		public function getSlug() {
			return self::SLUG;
		}

		public function load_files() {
			require_once( $this->include_path . '/class-admin.php' );
			require_once( $this->include_path . '/class-template.php' );
		}

		public function register_hooks() {
			add_action( 'init', Array( $this, 'initialize' ), 9 );
		}

		public function initialize() {
			$this->admin = new Javo_Spot_Single_Addon_Admin;
			$this->template = new Javo_Spot_Single_Addon_Template;
		}

		public function theme_check( $theme_names=Array() ) {
			$this->theme = wp_get_theme();
			$this->template = $this->theme->get( 'Name' );
			if( $this->theme->get( 'Template' ) ) {
				$this->parent = wp_get_theme(  $this->theme->get( 'Template' ) );
				$this->template = $this->parent->get( 'Name' );
			}
			$this->template = str_replace( ' ', '_', strtolower( $this->template ) );
			return array_key_exists( sanitize_key( $this->template ), $theme_names );
		}

		public static function getInstance( $file ) {
			if( is_null( self::$instance ) )
				self::$instance = new self( $file );
			return self::$instance;
		}
	}

	if( !function_exists( 'javo_spot_single' ) ) {
		function javo_spot_single() {
			return Javo_Spot_Single_Addon::getInstance( __FILE__ );
		}
		javo_spot_single();
	}


endif;