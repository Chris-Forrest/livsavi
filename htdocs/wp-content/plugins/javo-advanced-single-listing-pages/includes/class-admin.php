<?php
class Javo_Spot_Single_Addon_Admin
{

	public $options = Array();

	public $post_type = false;
	public $post_id = 0;
	public $singleSelectorID = false;
	public $fieldName = false;

	public function __construct() {
		$this->setVariables();
		$this->register_hooks();
	}

	public function setVariables() {
		$this->post_type = javo_spot_single()->getSlug();
		$this->singleSelectorID = sprintf( 'javo_%s_single_selector', $this->post_type );
		$this->fieldName = sprintf( 'javo_%s_single_field', $this->post_type );
	}

	public function field( $name='' ) {
		return sprintf( '%1$s[%2$s]', $this->fieldName, $name );
	}

	public function register_hooks() {
		add_action( 'add_meta_boxes', Array( $this, 'singleTypeMeta' ) );
		add_action( 'save_post', Array( $this, 'saveSingleType' ) );
	}

	public function singleTypeMeta() {
		add_meta_box( $this->singleSelectorID, esc_html__( 'Single Listing Page - Layout Setting', 'javospot' ), Array( $this, 'addSingleType' ), $this->post_type, 'normal', 'default' );
	}

	public function getOption( $key='', $default='', $post_id=0 ) {
		if( ! intVal( $post_id ) > 0 )
			$post_id = isset( $GLOBALS[ 'post' ] ) ? $GLOBALS[ 'post' ]->ID : 0;
		$strOptions = (array) get_post_meta( $post_id, $this->fieldName, true );
		$strReturn = array_key_exists( $key, $strOptions ) ? $strOptions[ $key ] : $default;
		return $strReturn;
	}

	public function addSingleType( $post ) {
		$this->post_id = $post->ID;
		javo_spot_single()->template->load_template(
			'single-type-selector',
			array( 'prefix' => 'admin' ),
			array(
				'jvs_singleTypes' => Array(
					'' => esc_html__( "Theme Settings as", 'javospot' ),
					'type-a' => esc_html__( "Type A", 'javospot' ),
					'type-b' => esc_html__( "Type B", 'javospot' ),
					//'type-c' => esc_html__( "Type C", 'javospot' ),
					'type-grid' => esc_html__( "Grid Style", 'javospot' ),
					'type-half' => esc_html__( "Half Style", 'javospot' ),
					//'type-three-blocks' => esc_html__( "3 Blocks ", 'javospot' ),
					'type-left-tab' => esc_html__( "Left Tab Style", 'javospot' ),
					'type-top-tab' => esc_html__( "Top Tab Style", 'javospot' ),
				)
			)
		);

		javo_spot_single()->template->load_template(
			'single-type-additional-options',
			array( 'prefix' => 'admin' ),
			array(
				'jvs_disable_detail' => Array(
					'' => esc_html__( "Enable", 'javospot' ),
					'disable' => esc_html__( "Disable", 'javospot' ),
				),
				'jvs_background_transparent' => Array(
					'' => esc_html__( "Enable", 'javospot' ),
					'disable' => esc_html__( "Disable", 'javospot' ),
				)
			)
		);
	}

	public function saveSingleType( $post_id=0 ) {
		if( isset( $_POST[ $this->fieldName ] ) && $_POST[ $this->fieldName ] != '' )
			update_post_meta( $post_id, $this->fieldName, $_POST[ $this->fieldName ] );
	}


}