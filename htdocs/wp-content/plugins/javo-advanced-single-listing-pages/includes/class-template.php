<?php

class Javo_Spot_Single_Addon_Template
{

	public $post_type = false;
	public $template_path = false;

	public function __construct() {
		$this->setVariables();
		$this->register_hooks();
	}

	public function setVariables() {
		$this->post_type = javo_spot_single()->getSlug();
		$this->template_path = javo_spot_single()->template_path;
	}

	public function register_hooks() {
		add_action( 'init', array( $this, 'getSingleType' ) );
		add_filter('body_class', array( $this, 'class_advanced_singles'));

	}

	public function getSingleType() {
		add_filter( 'jvfrm_spot_' . $this->post_type . '_single_type', array( $this, 'custom_singleType' ), 10, 2);
	}

	public function custom_singleType( $oldType='', $obj=array() ) {
		$dynamic_options = array(
			'newType' => javo_spot_single()->admin->getOption( 'singleType', false )
			,'featured_height' => javo_spot_single()->admin->getOption( 'featured_height', false )
			,'disable_detail_section' => javo_spot_single()->admin->getOption( 'disable_detail_section', false )
			,'background_transparent' => javo_spot_single()->admin->getOption( 'background_transparent', false )
			,'title_background_color' => javo_spot_single()->admin->getOption( 'title_background_color', false )
		);
		$dynamic_options['newType'] = $dynamic_options['newType'] ? $dynamic_options['newType'] : $oldType;
		return $dynamic_options;
	}

	public function load_template( $template_name, $options=Array(), $args=Array() ) {

		if( !$template_name )
			return false;

		$arrOption = wp_parse_args(
			$options,
			Array(
				'prefix'	=> 'template',
				'extension'	=> 'php',
			)
		);

		if( is_array( $args ) )
			extract( $args );

		$strPath = sprintf(
			'%1$s/%2$s-%3$s.%4$s',
			$this->template_path,
			$arrOption[ 'prefix' ],
			$template_name,
			$arrOption[ 'extension' ]
		);

		if( file_exists( $strPath ) ) {
			require $strPath;
			return true;
		}
		return false;
	}

	
	public function class_advanced_singles($classes) {

	// Single page addon option
	if( class_exists( 'Javo_Spot_Single_Addon' ) ){
		$single_addon_options = get_single_addon_options(get_the_ID());
			if($single_addon_options['background_transparent'] == 'disable'){ 			
				$classes[] = 'extend-meta-block';
				return $classes;		
			}else{
				$classes[] = 'transparent-meta-block';
				return $classes;					
			}
		}
	}

}