<th><?php esc_html_e( "Layout Style", 'javospot' ); ?></th>
	<td>
		<select class="jvadm-field" name="<?php echo javo_spot_single()->admin->field( 'singleType' );?>" style="width:300px;">
			<?php
			if( !empty( $jvs_singleTypes ) ) : foreach( $jvs_singleTypes as $strValue => $strLabel ) {
				printf(
					'<option value="%1$s" %3$s>%2$s</option>', $strValue, $strLabel,
					selected( $strValue == javo_spot_single()->admin->getOption( 'singleType' ), true, false )
				);
			} endif; ?>
		</select>
	</td>
	<td class="des"></td>
