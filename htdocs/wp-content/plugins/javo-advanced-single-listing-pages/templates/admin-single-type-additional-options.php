<style type="text/css">

	#javo_lv_listing_single_selector th {
		font-size:13px;
	}

	#javo_lv_listing_single_selector .inside {
		padding:20px 25px 10px 25px;
	}
	#javo_lv_listing_single_selector .hndle.ui-sortable-handle {
		color: #f1f1f1;
		background: #34495e;
	}

	#javo_lv_listing_single_selector .unit, #javo_lv_listing_single_selector .des {
		color: #666666;
		line-height: 1.5;
		margin: 4px 0 0 20px;
		font-size: 12px;
		font-style: italic;
		font-weight: normal;
	}
	#javo_lv_listing_single_selector .unit {
		margin: 0 0 0 5px;
	}
	input.jvadm-field {	
		background: #f1f1f1;
		-webkit-box-shadow: none;
		box-shadow: none;
		padding: 3px 5px !important;
	}

	input[type=text].jvadm-field{
		height: 28px;
		line-height: 26px;
	}

	select.jvadm-field {
		position: relative;
		display: block;
		overflow: hidden;
		height: 28px;
		width: 100%;
		padding: 0;
		font-size: 13px;
		font-weight: 400;
		line-height: 1;
		color: #333;
		outline: 0;
		border: 1px solid #dddddd;
		background-color: #f1f1f1;
		box-sizing: border-box;
		cursor: pointer;
	}

	input[type=radio].jvadm-field {
		opacity: 1;
		filter: alpha(opacity=100);
		background: #ffffff;
		margin: -4px 4px 0 15px;
		padding: 0 !important;
		color: #34495e;
		border: 2px solid #34495e;
		width: 14px !important;
		height: 14px !important;
		max-width: 14px !important;
		min-width: 0px !important;
	}
	input[type=radio].jvadm-field:checked:before{
		margin:2px;
	}
	.jvadm-colorpicker .wp-color-result {
		vertical-align: top !important;
		height: 26px !important;
		line-height: 26px !important;
		-webkit-border-radius: 0 !important;
		border-radius: 0 !important;
		-webkit-box-shadow: none !important;
		box-shadow: none !important;
		margin: 0 0 2px 0 !important;
	}
	
	.jvadm-colorpicker .wp-color-result:after {
		vertical-align: top !important;
		height: 26px !important;
		line-height: 26px !important;
		-webkit-border-radius: 0 !important;
		border-radius: 0 !important;
		-webkit-box-shadow: none !important;
		box-shadow: none !important;
		margin: 0 0 2px 0 !important;
	}

	.jvadm-colorpicker .wp-picker-open+.wp-picker-input-wrap input.wp-color-picker {
		min-width: 98px !important;
		margin: 0 0 0 6px !important;
	}

</style>

<table class="form-table">
	<tbody>
		<tr>
			<th><?php esc_html_e( "Featured Image Custom Height", 'javospot' ); ?></th>
			<td>
				<input type="text" class='jvadm-field' name="<?php echo javo_spot_single()->admin->field( 'featured_height' ); ?>" value="<?php echo javo_spot_single()->admin->getOption( 'featured_height' ); ?>"><span class="unit">px</span>
				<span class="des"><?php esc_html_e( "( Default 550px. exclude half style )", 'javospot' ); ?></span>
			</td>
		</tr>
		<tr>
			<th><?php esc_html_e( "Display Item Detail Section", 'javospot' ); ?></th>
			<td>
				<?php
					if( !empty( $jvs_disable_detail ) ) : foreach( $jvs_disable_detail as $strValue => $strLabel ) {
						printf(
							'<input type="radio" class="jvadm-field" name="%4$s" value="%2$s" %3$s>%1$s ', $strLabel, $strValue,
							checked( $strValue == javo_spot_single()->admin->getOption( 'disable_detail_section' ), true, false )
							, javo_spot_single()->admin->field( 'disable_detail_section' )
						);
					} endif; ?>
					<span class="des"><?php esc_html_e( "( If disabled, Item detail will be hidden. )", 'javospot' ); ?></span>
			</td>
		</tr>
		<tr>
			<th><?php esc_html_e( "Post Meta (Title) Background Transparent", 'javospot' ); ?></th>
			<td>
				<?php
					if( !empty( $jvs_background_transparent ) ) : foreach( $jvs_background_transparent as $strValue => $strLabel ) {
						printf(
							'<input type="radio" class="jvadm-field" name="%4$s" value="%2$s" %3$s>%1$s ', $strLabel, $strValue,
							checked( $strValue == javo_spot_single()->admin->getOption( 'background_transparent' ), true, false )
							, javo_spot_single()->admin->field( 'background_transparent' )
						);
					} endif; ?>
					<span class="des"><?php esc_html_e( "( If disabled, Post meta area will be extended as a block. )", 'javospot' ); ?></span>
			</td>
		</tr>
		<tr>
			<th><?php esc_html_e( "Spyscroll Navigation Background", 'javospot' ); ?></th>
			<td class="jvadm-colorpicker">
				<input name="<?php echo javo_spot_single()->admin->field( 'spy_nav_background' ); ?>" type="text" value="<?php echo javo_spot_single()->admin->getOption( 'spy_nav_background' ); ?>" class="wp_color_picker" data-default-color="">
			</td>
		</tr>
		<tr>
			<th><?php esc_html_e( "Spyscroll Navigation Text Color", 'javospot' ); ?></th>
			<td class="jvadm-colorpicker">
				<input name="<?php echo javo_spot_single()->admin->field( 'spy_nav_font_color' ); ?>" type="text" value="<?php echo javo_spot_single()->admin->getOption( 'spy_nav_font_color' ); ?>" class="wp_color_picker" data-default-color="">
			</td>
		</tr>
		<tr>
			<th><?php esc_html_e( "Post Meta Background", 'javospot' ); ?></th>
			<td class="jvadm-colorpicker">
				<input name="<?php echo javo_spot_single()->admin->field( 'header_background' ); ?>" type="text" value="<?php echo javo_spot_single()->admin->getOption( 'header_background' ); ?>" class="wp_color_picker" data-default-color="">
			</td>
		</tr>
	</tbody>
</table>