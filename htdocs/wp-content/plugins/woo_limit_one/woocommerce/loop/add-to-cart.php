<?php

/**

 * Loop Add to Cart

 *

 * @author 		WooThemes

 * @package 	WooCommerce/Templates

 * @version     2.1.0

 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;
date_default_timezone_set(get_option('timezone_string'));
	$current_user = wp_get_current_user();
	//Get purchaseble once values
	$purchasable_once = get_post_meta( get_the_ID(), 'woo_limit_one_select_dropdown', true );
	$woo_limit_one_based_on_ip_select_dropdown = get_post_meta( get_the_ID(), 'woo_limit_one_based_on_ip_select_dropdown', true );
	$purchasable_once_text = get_post_meta( get_the_ID(), 'woo_limit_one_purchased_text', true );
	$woo_limit_one_time_period = get_post_meta( get_the_ID(), 'woo_limit_one_time_dropdown', true );
	$woo_limit_one_time_range = get_post_meta( get_the_ID(), 'woo_limit_one_time_range', true );
	$showDaysLeft = get_post_meta( get_the_ID(), 'woo_limit_one_days_left', true );
	$beforeDays = get_post_meta( get_the_ID(), 'woo_limit_one_before_days', true );
	$afterDays = get_post_meta( get_the_ID(), 'woo_limit_one_after_days', true );
	if (empty($beforeDays)) {
		$beforeDays = "Purchasable in";
	}
	if (empty($afterDays)) {
		$afterDays = "days";
	}

	//If disable by ip enabled, check previous orders for users ip address
	if ($woo_limit_one_based_on_ip_select_dropdown == "1") {
		$user_ip = $_SERVER['REMOTE_ADDR'];

	    //Check orders for ip address
		$args = array(
		  'post_type' => 'shop_order',
		  'post_status' => 'publish',
		  'meta_key' => '_customer_ip_address',
		  'meta_value' => $user_ip,
		  'posts_per_page' => '-1'
		);
		$my_query = new WP_Query($args);

		$wc_orders = $my_query->posts;

		foreach ($wc_orders as $order) {
			//print_r($order);
			//Get order meta to get ip address of order
			$order_meta = get_post_meta($order->ID);
			$order_ip = $order_meta['_customer_ip_address'][0];

			//Compare ip addresses
			if ($user_ip == $order_ip) {

				//Get items in order
				$the_order = wc_get_order( $order->ID );
				$items = $the_order->get_items();

				foreach ($items as $item) {
					$itemid = $item['product_id'][0];
					if ( $itemid == get_the_ID() ) {
						$ip_purchased_before = 1;
					}
				}

			}
		}
	}
	
	//Get the order date for this product
	if (!function_exists('_cmk_check_ordered_product')) {
		function _cmk_check_ordered_product( $id ) {
		    // Get All order of current user
		    $orders = get_posts( array(
		        'numberposts' => -1,
		        'meta_key'    => '_customer_user',
		        'meta_value'  => get_current_user_id(),
		        'post_type'   => wc_get_order_types( 'view-orders' ),
		        'post_status' => array_keys( wc_get_order_statuses() )
		    ) );

		    if ( !$orders ) return false; // return if no order found

		    $all_ordered_product = array(); // store products ordered in an array

		    foreach ( $orders as $order => $data ) { // Loop through each order
		        $order_data = new WC_Order( $data->ID ); // create new object for each order
		        foreach ( $order_data->get_items() as $key => $item ) {  // loop through each order item
		            // store in array with product ID as key and order date a value
		            $all_ordered_product[ $item['product_id'] ] = $data->post_date; 
		        }
		    }
		    if ( isset( $all_ordered_product[ $id ] ) ) { // check if defined ID is found in array

		    	return date('Y-m-d', strtotime( $all_ordered_product[ $id ] ) );

		    }
		}
		
	}
	$ordered_date = _cmk_check_ordered_product( get_the_ID() );

	//Check if purchasable_once is enabled and customer purchased product otherwise use default purchase button
	if ( $purchasable_once == '1' and $ip_purchased_before == '1' || wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id)) {

		//Check if we are using month or year
		if ($woo_limit_one_time_period != 'all') {
			//If Month Selected
			if ($woo_limit_one_time_period == 'month') {
				$newDate = date('Y-m-d', strtotime($ordered_date. ' + '.($woo_limit_one_time_range).' months'));
			}else if($woo_limit_one_time_period == 'day') {
				$newDate = date('Y-m-d', strtotime($ordered_date. ' + '.($woo_limit_one_time_range).' days'));
			}
			if ($woo_limit_one_time_period == 'month' || $woo_limit_one_time_period == 'day') {
				$diff=date_diff(new DateTime(date("Y-m-d")), new DateTime($newDate));
				if ( 0 >= intval($diff->format("%R%a")) ) {//If purchased within same day disable purchase
					echo apply_filters( 'woocommerce_loop_add_to_cart_link',

					sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s">%s</a>',
						esc_url( $product->add_to_cart_url() ),

						esc_attr( $product->id ),

						esc_attr( $product->get_sku() ),

						$product->is_purchasable() ? 'add_to_cart_button' : '',

						esc_attr( $product->product_type ),

						esc_html( $product->add_to_cart_text() )

					),
					$product );	
				}else{//If its been longer than a day allow purchase
					if ($purchasable_once_text) {
						echo '<p class="woo-limit-one">'.$purchasable_once_text.'</p>';
					}else{
						echo '<p class="woo-limit-one">Purchased</p>'; 
					}
					if ($showDaysLeft) {
						echo $beforeDays." ".intval($diff->format("%a"))." ".$afterDays;
					}
				}	
			}
		}else{//Else can only purchase once all time
			if ($purchasable_once_text) {
				echo '<p class="woo-limit-one">'.$purchasable_once_text.'</p>';
			}else{
				echo '<p class="woo-limit-one">Purchased</p>'; 
			}		
		}
	}
	else{
	echo apply_filters( 'woocommerce_loop_add_to_cart_link',

	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),

		esc_attr( $product->id ),

		esc_attr( $product->get_sku() ),

		$product->is_purchasable() ? 'add_to_cart_button' : '',

		esc_attr( $product->product_type ),

		esc_html( $product->add_to_cart_text() )

	),
	$product );	
}