<?php
/**
 * Include Factories
 *
 * @package   restrict-content-pro
 * @copyright Copyright (c) 2020, Sandhills Development, LLC
 * @license   GPL2+
 */

require_once dirname( __FILE__ ) . '/factories/class-factory.php';
require_once dirname( __FILE__ ) . '/factories/class-customer-factory.php';
require_once dirname( __FILE__ ) . '/factories/class-level-factory.php';
