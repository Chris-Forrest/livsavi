<?php
function_exists('lava_ticket') && lava_ticket()->template->get_template('/parts/user-info.php', Array(
    'current_user' => new WP_User( bp_displayed_user_id() ),
));
if( current_user_can( 'manage_options' ) ) :
    function_exists('lava_ticket') && lava_ticket()->template->get_template('/parts/admin-memo.php', Array(
        'current_user' => new WP_User( bp_displayed_user_id() ),
    ));
endif;
function_exists('lava_ticket') && lava_ticket()->template->get_template('/parts/purchases.php', Array(
    'current_user' => new WP_User( bp_displayed_user_id() ),
));