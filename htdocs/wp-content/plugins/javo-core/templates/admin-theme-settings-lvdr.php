<?php
$arrPages = jvbpd_tso()->getPages(); ?>
<div class="jvbpd_ts_tab javo-opts-group-tab hidden" tar="lvdr">
	<h2> <?php esc_html_e( "Page Settings", 'jvfrmtd' ); ?> </h2>
	<table class="form-table">
	<tr><th>
		<?php esc_html_e( "Post pages", 'jvfrmtd' );?>
		<span class="description"></span>
	</th><td>
		<?php
        foreach( Array(
            'single_post_template' => Array(
                'label' => esc_html__( "Default Single Post Detail template", 'listopia' ),
                'description' => sprintf(
                    'To create more headers, Core >> page builder ( <a href="%1$s" target="_blank">Go page builder</a> )',
                    esc_url( add_query_arg( Array( 'post_type' => 'jvbpd-listing-elmt', ), admin_url( 'edit.php' ) ) )
                ),
                'options' => jvbpdCore()->admin->getElementorSinglePostID(),
            ),
            'archive_post_template' => Array(
                'label' => esc_html__( "Default Post Archive template", 'listopia' ),
                'options' => jvbpdCore()->admin->getElementorArchiveID('post'),
            ),
            'archive_author_template' => Array(
                'label' => esc_html__( "Default Post Author template", 'listopia' ),
                'options' => jvbpdCore()->admin->getElementorPostAuthorID(),
            ),
        ) as $elementor_single_key => $elementor_single_meta ) {
            ?>
            <h4><?php echo esc_html( $elementor_single_meta[ 'label' ] ); ?></h4>
            <fieldset class="inner">
                <select name="jvbpd_ts[<?php echo $elementor_single_key; ?>]">
                    <option value=""><?php esc_html_e("Select a template"); ?></option>
                    <?php
                    foreach( $elementor_single_meta[ 'options' ] as $template_id  ) {
                        printf(
                            '<option value="%1$s"%3$s>%2$s</option>', $template_id, get_the_title( $template_id ),
                            selected( $template_id == jvbpd_tso()->get( $elementor_single_key, '' ), true, false )
                        );
                    } ?>
                </select>
                <?php
                if( isset( $elementor_single_meta[ 'description' ] ) ) { ?>
                    <div>
                        <span class="description"><?php echo $elementor_single_meta[ 'description' ]; ?></span>
                    </div>
                    <?php
                } ?>
            </fieldset>
            <?php
		} ?>
	</td></tr><tr><th>
		<?php esc_html_e( "Portfolio pages", 'jvfrmtd' );?>
		<span class="description"></span>
	</th><td>
        <?php
        foreach( Array(
            'single_portfolio_page' => Array(
                'label' => esc_html__( "Default Single Portfolio Detail template", 'listopia' ),
                'options' => jvbpdCore()->admin->getPageBuilderID('single_portfolio_page'),
            ),
            'archive_portfolio_template' => Array(
                'label' => esc_html__( "Default Portfolio Archive template", 'listopia' ),
                'options' => jvbpdCore()->admin->getElementorArchiveID('portfolio'),
            ),
        ) as $elementor_single_key => $elementor_single_meta ) {
            ?>
            <h4><?php echo esc_html( $elementor_single_meta[ 'label' ] ); ?></h4>
            <fieldset class="inner">
                <select name="jvbpd_ts[<?php echo $elementor_single_key; ?>]">
                    <option value=""><?php esc_html_e("Select a template"); ?></option>
                    <?php
                    foreach( $elementor_single_meta[ 'options' ] as $template_id  ) {
                        printf(
                            '<option value="%1$s"%3$s>%2$s</option>', $template_id, get_the_title( $template_id ),
                            selected( $template_id == jvbpd_tso()->get( $elementor_single_key, '' ), true, false )
                        );
                    } ?>
                </select>
                <?php
                if( isset( $elementor_single_meta[ 'description' ] ) ) { ?>
                    <div>
                        <span class="description"><?php echo $elementor_single_meta[ 'description' ]; ?></span>
                    </div>
                    <?php
                } ?>
            </fieldset>
            <?php
		} ?>
    </td></tr>
        <?php
        if( function_exists( 'lava_directory' ) ) { ?>
            <tr>
                <th>
                    <?php esc_html_e("Listing pages", 'jvfrmtd'); ?>
                </th>
                <td>
                    <h4><?php esc_html_e("Add listing", 'jvfrmtd'); ?></h4>
                    <fieldset class="inner">
                        <select name="jvbpd_ts[add_listing]">
                            <option value=""><?php esc_html_e("Select a page", 'jvfrmtd'); ?></option>
                            <?php
                            foreach( $arrPages as $objPage) {
                                printf(
                                    '<option value="%1$s"%3$s>%2$s</option>',
                                    $objPage->ID, $objPage->post_title,
                                    selected(jvbpd_tso()->get('add_listing') == $objPage->ID, true, false)
                                );
                            } ?>
                        </select>
                        <div class="description"><?php esc_html_e("It's for the user submit button on user menu widget.", 'jvfrmtd'); ?></div>
                    </fieldset>
                    <?php
                    if (function_exists('jvbpdCore')) :
                        foreach( Array(
                                     'single_lv_listing_template' => Array(
                                         'label' => esc_html__("Default Single Listing template", 'listopia'),
                                         'description' => sprintf(
                                             'To create more headers, Core >> page builder ( <a href="%1$s" target="_blank">Go page builder</a> )',
                                             esc_url(add_query_arg(Array('post_type' => 'jvbpd-listing-elmt',), admin_url('edit.php')))
                                         ),
                                         'options' => jvbpdCore()->admin->getElementorSingleID(),
                                     ),
                                     'archive_lv_listing_template' => Array(
                                         'label' => esc_html__("Default Listing Archive template", 'listopia'),
                                         'options' => jvbpdCore()->admin->getElementorArchiveID('listing'),
                                     ),
                                 ) as $elementor_single_key => $elementor_single_meta) {
                            ?>
                            <h4><?php echo esc_html($elementor_single_meta['label']); ?></h4>
                            <fieldset class="inner">
                                <select name="jvbpd_ts[<?php echo $elementor_single_key; ?>]">
                                    <option value=""><?php esc_html_e("Select a template"); ?></option>
                                    <?php
                                    foreach( $elementor_single_meta['options'] as $template_id) {
                                        printf(
                                            '<option value="%1$s"%3$s>%2$s</option>', $template_id, get_the_title($template_id),
                                            selected($template_id == jvbpd_tso()->get($elementor_single_key, ''), true, false)
                                        );
                                    } ?>
                                </select>
                                <?php
                                if (isset($elementor_single_meta['description'])) { ?>
                                    <div>
                                        <span class="description"><?php echo $elementor_single_meta['description']; ?></span>
                                    </div>
                                    <?php
                                } ?>
                            </fieldset>
                            <?php
                        }
                    endif; ?>

                </td>
            </tr>
            <tr>
            <th>
                <?php esc_html_e("Shortcode", 'jvfrmtd'); ?>
                <span class="description"></span>
            </th>
            <td>
                <h4><?php esc_html_e("Header search shortcode result page", 'jvfrmtd'); ?></h4>
                <fieldset class="inner">
                    <select name="jvbpd_ts[search_sesult_page]">
                        <option value=""><?php esc_html_e("Select a page", 'jvfrmtd'); ?></option>
                        <?php
                        foreach ($arrPages as $objPage) {
                            printf(
                                '<option value="%1$s"%3$s>%2$s</option>',
                                $objPage->ID, $objPage->post_title,
                                selected(jvbpd_tso()->get('search_sesult_page') == $objPage->ID, true, false)
                            );
                        } ?>
                    </select>
                </fieldset>
            </td></tr>
            <tr>
            <th>
                <?php esc_html_e("Map Pages", 'jvfrmtd'); ?>
                <span class="description"></span>
            </th>
            <td>
                <h4><?php esc_html_e("Map Info Window Skin", 'jvfrmtd'); ?></h4>
                <fieldset class="inner">
                    <select name="jvbpd_ts[map_info_win_skin]">
                        <?php
                        foreach(Array(
                            'skin1' => esc_html__("Skin 1 : Full image", 'jvfrmtd'),
                            'skin2' => esc_html__("Skin 2 : Streetview", 'jvfrmtd'),
                        ) as $skin => $skinLabel) {
                            printf(
                                '<option value="%1$s"%3$s>%2$s</option>',
                                $skin, $skinLabel,
                                selected(jvbpd_tso()->get('map_info_win_skin') == $skin, true, false)
                            );
                        } ?>
                    </select>
                </fieldset>
            </td></tr><?php
        } // condition for lava_directory class
        ?>
	</table>
</div>