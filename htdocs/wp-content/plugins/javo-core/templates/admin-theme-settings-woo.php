<div class="jvbpd_ts_tab javo-opts-group-tab hidden" tar="woocommerce">
	<h2> <?php esc_html_e( "Woo Commerce Setting", 'jvfrmtd' ); ?> </h2>
	<table class="form-table">
	<tr><th>
		<?php esc_html_e( "Templates Settings", 'jvfrmtd' );?>
		<span class="description"></span>
	</th><td>
		<?php
        foreach( Array(
			'single_product_template' => Array(
				'label' => esc_html__("Single Product Template", 'listopia'),
				'options' => jvbpdCore()->admin->getPageBuilderID('single_product_page'),
			),
			'archive_product_template' => Array(
				'label' => esc_html__("Archive Product Template", 'listopia'),
				'options' => jvbpdCore()->admin->getPageBuilderID('archive_product_page'),
			),
            'shop_page' => Array(
                'label' => esc_html__( "Woocommerce Shop List Page", 'listopia' ),
                'description1' => sprintf(
                    'To create more headers, Core >> page builder ( <a href="%1$s" target="_blank">Go page builder</a> )',
                    esc_url( add_query_arg( Array( 'post_type' => 'jvbpd-listing-elmt', ), admin_url( 'edit.php' ) ) )
                ),
                'options' => jvbpdCore()->admin->getPageBuilderID('single_shop_page'),
			),
        ) as $elementor_single_key => $elementor_single_meta ) {
            ?>
            <h4><?php echo esc_html( $elementor_single_meta[ 'label' ] ); ?></h4>
            <fieldset class="inner">
                <select name="jvbpd_ts[<?php echo $elementor_single_key; ?>]">
					<option value=""><?php esc_html_e("Select a shop page", 'jvfrmtd'); ?></option>
                    <?php
                    foreach( $elementor_single_meta[ 'options' ] as $template_id  ) {
                        printf(
                            '<option value="%1$s"%3$s>%2$s</option>', $template_id, get_the_title( $template_id ),
                            selected( $template_id == jvbpd_tso()->get( $elementor_single_key, '' ), true, false )
                        );
                    } ?>
                </select>
                <?php
                if( isset( $elementor_single_meta[ 'description' ] ) ) { ?>
                    <div>
                        <span class="description"><?php echo $elementor_single_meta[ 'description' ]; ?></span>
                    </div>
                    <?php
                } ?>
            </fieldset>
            <?php
		} ?>
	</td></tr><tr><th>
		<?php esc_html_e( "Page Settings", 'jvfrmtd' );?>
		<span class="description"></span>
	</th><td>
		<h4><?php esc_html_e( "Wishlist Page", 'jvfrmtd' );?>: </h4>
		<fieldset  class="inner">
			<select name="jvbpd_ts[wishlist_page]">
				<option value=""><?php esc_html_e( "Select a page", 'jvfrmtd' ); ?></option>
				<?php
				foreach( $arrPages as $objPage ){
					printf(
						'<option value="%1$s"%3$s>%2$s</option>',
						$objPage->ID, $objPage->post_title,
						selected( jvbpd_tso()->get( 'wishlist_page' ) == $objPage->ID, true, false )
					);
				} ?>
			</select>
		</fieldset>
	</td></tr><tr><th>
		<?php esc_html_e( "Layout and Sidebar", 'jvfrmtd' );?>
		<span class="description"></span>
	</th><td>
		<?php
		$arrBpPages = Array(
			'woo_archive_sidebar' => esc_html__( "Woocommerce Archive Page", 'jvfrmtd' ),
			'woo_single_sidebar' => esc_html__( "Woocommerce Single Page", 'jvfrmtd' ),
		);

		if( !empty( $arrBpPages ) ) {

			foreach( $arrBpPages as $strOption => $strOptionLabel ) {
				?>
				<h4><?php echo esc_html( $strOptionLabel ); ?></h4>
				<fieldset class="inner">
					<select name="jvbpd_ts[<?php echo esc_attr( $strOption ); ?>]">
						<option value=""><?php esc_html_e( "Select one", 'jvfrmtd' ); ?></option>
						<?php
						foreach(
							Array( 'left' => esc_html__( "Left", 'jvfrmtd' ), 'right' => esc_html__( "Right", 'jvfrmtd' ), 'full' => esc_html__( "Full", 'jvfrmtd' ) )
							as $strPositionOption => $strPositionLabel
						) {
							printf(
								'<option value="%1$s"%3$s>%2$s</option>',
								$strPositionOption, $strPositionLabel,
								selected( $strPositionOption == jvbpd_tso()->get( $strOption ), true, false )
							);
						} ?>
					</select>
				</fieldset>
				<?php
			}
		} ?>
	</td></tr>
	</table>
</div>