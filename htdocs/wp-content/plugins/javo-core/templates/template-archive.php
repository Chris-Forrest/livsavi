<?php
/**
 * Template Name: Custom Archive Template
 */
get_header();


$customPageBuilderID = apply_filters('jvbpd_core/archive/render/template_id', (isset($GLOBALS['customPageBuiderID']) ? $GLOBALS['customPageBuiderID'] : 0));
if(0 < intVal($customPageBuilderID) && class_exists('\Elementor\Plugin')) {
    echo \Elementor\Plugin::instance()->frontend->get_builder_content_for_display($customPageBuilderID);
}

do_action('Javo/Footer/Render');
do_action( 'jvbpd_body_after', get_page_template_slug() );
get_footer();