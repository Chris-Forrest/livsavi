;( function( $ ){
	var jvcoreAdmin = function (){
		this.attTemplate = '<div class="upload-item{featured}"> {img} <input type="hidden" name="{name}" value="{val}" /><button type="button" class="button item-remove">{remove_label}</button></div>';
        this.init();
    }
    jvcoreAdmin.prototype.constructor = jvcoreAdmin;
    jvcoreAdmin.prototype.init = function() {
        var self = this;

        $('.upload-item-group').each(function(){
			$(this).sortable();
		});
        self.WpFileUpload();
        var instance = self.setFeaturedImage();
        $(window).on('javo:wpLibraryMultipleImageUpload', instance);
        $(window).on('load', instance);
    }

    jvcoreAdmin.prototype.WpFileUpload = function() {
		var
			obj = this,
            wndWpMedia;
        var $wrap = $('.postbox');
        var elements = $wrap.find( '.javo-portfolio-wp-media' );
		$( window ).on( 'javo:wpLibrarySingleImageUpload', function( event, preview ) {
			$( preview ).css({ width: 150, height: 150 } );
		} );

		elements.each( function() {
			var
				singleField,
				limit = $( this ).data( 'limit' ) || 0,
				selCount = 0,
				output = $( '.upload-item-group', this ),
				preview = $( '.upload-preview', this ),
				element = $( this ),
				_options = element.data(),
				values = _options.value,
				isMultipleUpload = _options.multiple || false;

			if(true == _options.attachment) {
				obj.template = obj.attTemplate;
			}else{
				obj.template = $('script#portfolio-detail-image-template').html();
			}

			_options.element = element;

			if( element.hasClass( 'has-instance' ) ) {
				return true;
			}

			element.addClass( 'has-instance' );

			if( ! isMultipleUpload ) {
				singleField = $( 'input[name="' + _options.field + '"]' );
				preview.css({
					position: 'relative',
					'width': '150px',
					'height': '150px',
					'background-size': 'cover',
					'background-repeat': 'no-repeat'
				});

				if( singleField.val() ) {
					$( window ).trigger( 'javo:wpLibrarySingleImageUpload', preview );
				}
			}else{
				$('input[data-media-input]', element).remove();
				$( values ).each( function( intIndex, _data ) {
					_data.name = _options.field;
					if( _options.attachment == true ) {
						var downloadString = "Download";
						_data.type = _data.type;
						_data.output = '<a href="' + _data.url + '" target="_blank">' + _data.filename;
						_data.output += ' ( ' +  downloadString + ' ) ';
						_data.output += '</a>';
					}
					output.append( obj.addUploadItem( obj.template, _data ) );
				} );
			}

			$( '.action-add-item', element ).on( 'click', function() {
				if( typeof wp.media === 'undefined' ) {
					obj.message( "WP media load fail" );
					return false;
				}
				wndWpMedia = wp.media.frames.file_frame = wp.media({ title : _options.modalTitle || 'Upload', multiple : isMultipleUpload });
				wndWpMedia.on( 'select', function() {
					if( ! obj.CheckLimitDetailImage( wndWpMedia.state().get( 'selection' ).length, limit ) ) {
						return false;
					}

					if( isMultipleUpload ) {
						wndWpMedia.state().get('selection').map( obj.wpUploadMultipleAction( _options, output ) );
					}else{
						obj.wpUploadSingleAction( wndWpMedia.state().get('selection').first().toJSON(), _options, preview );
					}
				} );

				if( obj.CheckLimitDetailImage( $( '.upload-item', output ).length, limit, true ) ) {
					wndWpMedia.open();
				}

			} );

			$( document ).on( 'click', output.find( '.item-remove' ).selector, function( e ) {
				$( this ).closest( '.upload-item' ).remove();
			} );

			$( '.item-clear', element ).on( 'click', function( e ) {
				preview.css( 'background-image', 'none' );
				$( 'input[name="' + _options.field + '"]', element ).val( '' );
				$( '[data-media-input]', element ).val( '' );
			} );

		} );
    }

    jvcoreAdmin.prototype.CheckLimitDetailImage = function( lenItem, limit, c ) {
        return true;
        var strLimit = this.args.strings.limitDetailImages;
		if( ( 0 < limit ) && ( ( c ? limit <= lenItem : limit < lenItem ) ) ) {
			strLimit = strLimit.replace( /{limit}/g, limit || 0 );
			strLimit = strLimit.replace( /{count}/g, lenItem || 0 );
			alert( strLimit );
			return false;
		}
		return true;
	}

	jvcoreAdmin.prototype.wpUploadSingleAction = function( data, opt, element ) {
		var obj = this;
		$( 'input[name="' + opt.field + '"]' ).val( data.id );
		$( '[data-media-input]', opt.element ).val( data.id ).data( 'background-url', data.url );
		element.css( 'background-image', 'url(' + data.url + ')' );
		$( window ).trigger( 'lava:wpLibrarySingleImageUpload', [element] );
	}

    jvcoreAdmin.prototype.wpUploadMultipleAction = function( opt, element ) {
		var
			obj = this,
			values = opt.value;
		return function( data ) {
			var
				_data = {},
				item = data.toJSON(),
				template = obj.attTemplate;

			if(true == opt.attachment) {
				template = obj.attTemplate;
			}else{
				template = $('script#portfolio-detail-image-template').html();
			}
			// var downloadString = typeof obj.args.strings.download !== 'undefined' ? obj.args.strings.download : "Download";
			var downloadString = "Download";

			if( opt.attachment == true ) {
				_data.type = item.type;
				_data.output = '<a href="' + item.url + '" target="_blank">' + item.filename;
				_data.output += ' ( ' +  downloadString + ' ) ';
				_data.output += '</a>';
			}

			_data.name = opt.field;
			_data.val = item.id;
			_data.img = '<img src="' + item.url + '" style="width:150px; height:150px;">';
			element.append( obj.addUploadItem( template, _data ) );
			$( window ).trigger( 'javo:wpLibraryMultipleImageUpload', [element] );
		}
    }
    jvcoreAdmin.prototype.addUploadItem = function( _template, data ) {
		var
			obj = this,
			type = data.type || false,
            template = _template || '';

		if( data.type ) {
			template = template.replace( /{img}/g, data.output || '' );
		}else{
			template = template.replace( /{img}/g, data.img || '' );
		}
		template = template.replace( /{name}/g, data.name || '' );
		template = template.replace( /{val}/g, data.val || '' );
		template = template.replace( /{_title}/g, data._title || '' );
		template = template.replace( /{_name}/g, data._name || '' );
		template = template.replace( /{featured}/g, data.f || '' );
		template = template.replace( /{remove_label}/g, 'Remove' || '' );
		return template;
    }

    jvcoreAdmin.prototype.setFeaturedImage = function() {
        var self = this;
        return function(event, element) {
            var FEATURED = 'featured';
            var $wrap = $('.postbox');
            $('div.upload-item:not(.event)', $wrap).each(function(){
                var $item = $(this);
                $(this).on('click', function(event){
					if($(event.target).is('.item-remove')) {
						return;
					}
					var featuredId = $('> input[type=hidden]', $item).val();
                    $('div.upload-item').removeClass(FEATURED);
                    $(this).addClass(FEATURED);
					$('[name="portfolio_featured_image"]', $wrap).val(featuredId);
					$('[data-featured-id]', $wrap).val(featuredId);
                });
                $item.prop('title', 'Set as a Featured image'); //.tooltip();
                $item.addClass('event');
            });
        }
    }

    window.javo_admin = new jvcoreAdmin;
})(jQuery);