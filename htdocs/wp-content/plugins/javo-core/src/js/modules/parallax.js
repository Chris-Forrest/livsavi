;(function ( $, window, document, undefined ) {

    "use strict";

    window._jcsspfx = 'webkit';

    // Create the defaults once
    var pluginName = "AvertaParallaxBox",
        defaults = {
            targets       : 'jvfrm-parallax', // target elements classname to move in parallax
            defaultDepth  : 0.5,            // default target parallax depth
            defaultOrigin : 'top',          // defalut target parallax origin, possible values: 'top', 'bottom', 'middle'
            forceHR       : false           // force use hardware accelerated
        },
        $window = $(window);

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = element;
        this.$element = $(element);
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            this.$targets = this.$element.find( '.' + this.settings.targets );
            this._targetsNum = this.$targets.length;
            this._prefix = window._jcsspfx || '';

            if ( this._targetsNum === 0 ) {
                return;
            }

            $window.on( 'scroll resize', this.update.bind( this ) );
            this.update();
        },

        /**
         * Updates the position of parallax element in box
         * @param  {Element} $target
         * @param  {Number} scrollValue
         */
        _setPosition: function( $target, scrollValue ) {

            var origin       = $target.data( 'parallax-origin' ) || this.settings.defaultOrigin,
                depth        = $target.data( 'parallax-depth' )  || this.settings.defaultDepth,
                disablePoint = $target.data( 'parallax-off' ),
                absDepth     = Math.abs(depth),
                dir          = depth < 0 ? 1 : -1,
                type         = $target.data( 'parallax-type' )   || 'position',
                value;

            if ( disablePoint >= window.innerWidth ) {
                if ( $target.data( 'disabled' ) ) {
                    return;
                }

                $target.data( 'disabled', true );

                if ( type === 'background' ) {
                    $target[0].style.backgroundPosition = '';
                } else {
                    $target[0].style[this._prefix + 'Transform'] = '';
                }

                return;
            } else {
                $target.data( 'disabled', false );
            }

            switch( origin ) {
                case 'top':
                    value = Math.min( 0, this._spaceFromTop * absDepth );
                    break;
                case 'bottom':
                    value = Math.max( 0, this._spaceFromBot * absDepth );
                    break;
                case 'middle':
                    value = this._spaceFromMid * absDepth;
                    break;
            }

            if ( value < 0 ) {
                value = Math.max( value, -window.innerHeight );
            } else {
                value = Math.min( value, window.innerHeight );
            }

            if ( type === 'background' ) {
                $target[0].style.backgroundPosition = '50% ' + value * dir + 'px';
            } else {
                $target[0].style[this._prefix + 'Transform'] = 'translateY(' + value * dir + 'px)' + ( this.settings.forceHR ? ' translateZ(1px)' : '' );
            }
        },

        /* ------------------------------------------------------------------------------ */
        // public methods

        /**
         * update the parallax
         */
        update: function() {
            this._boxHeight    = this.$element.height();
            this._spaceFromTop = this.$element[0].getBoundingClientRect().top;
            this._spaceFromBot = window.innerHeight - this._boxHeight - this._spaceFromTop;
            this._spaceFromMid = window.innerHeight / 2 - this._boxHeight / 2 - this._spaceFromTop;
            for( var i = 0; i !== this._targetsNum; i++ ) {
                this._setPosition( this.$targets.eq(i), $window.scrollTop() );
            }
        },

        /**
         * Enables the parallax effect in box
         */
        enable: function() {
            $window.on( 'resize scroll', this.update );
            this.update();
        },

        /**
         * Disables the parallax effect in box
         */
        disable: function() {
            $window.off( 'resize scroll', this.update );
        },

        destroy: function() {
            this.disable();
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function( options ) {
        var _arguments = arguments;
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                 $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            } else if ( typeof options === 'string' && options.indexOf(0) !== '_' )  {
                // access to public methods method
                var plugin = $.data( this, "plugin_" + pluginName);
                plugin[options].apply( plugin, Array.prototype.slice.call( _arguments, 1 ) );
            }
        });
    };

})( jQuery, window, document );