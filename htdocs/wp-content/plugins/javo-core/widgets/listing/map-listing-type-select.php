<?php
/**
 * Widget Name: Map listing type select
 * Author: Javo
 * Version: 1.0.0.1
*/

namespace jvbpdelement\Widgets;

use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Jvbpd_Map_Listing_Type_Select extends Widget_Base {

	public function get_name() { return 'jvbpd-map-listing-type-select'; }
	public function get_title() { return 'Map Listing Type Select'; }
	public function get_icon() { return 'eicon-button'; }
    public function get_categories() { return [ 'jvbpd-map-page' ]; }

    protected function _register_controls() {
        $this->start_controls_section('section_general', Array(
            'label' => esc_html__( 'General', 'jvfrmtd' ),
        ));
            $this->add_control( 'hide_label', Array(
                'label' => __( 'Hide Label & Use Tooltip', 'jvfrmtd' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
            ));
            $this->add_control( 'items', Array(
                    'label' => __( 'Items', 'jvfrmtd' ),
                    'type' => Controls_Manager::REPEATER,
                    'fields' => $this->getFormItem(),
                    'title_field' => 'Listing Type ID: {{{ listing_type }}}',
                )
            );
        $this->end_controls_section();
    }

    public function getListyTypes() {
        $listingTypesOption = Array('0' => esc_html__("All", 'jvfrmtd'));
		if(class_exists('\\LavaDirectoryManagerPro\\Base\\BaseController')) {
			foreach(\LavaDirectoryManagerPro\Base\BaseController::ListingTypesArray() as $listingID => $listingLabel) {
				$listingTypesOption[$listingID] = $listingLabel;
			}
        }
        return $listingTypesOption;
    }

    public function getFormItem() {
        $repeater = new Repeater();
        $repeater->add_control( 'icon', Array(
            'label' => __( 'Icon', 'jvfrmtd' ),
            'type' => Controls_Manager::ICONS,
            'default' => Array(
                'value' => '',
                'library' => '',
            ),
        ) );
        $repeater->add_control( 'title', Array(
            'type' => Controls_Manager::TEXT,
            'label' => esc_html__( 'Title', 'jvfrmtd' ),
			'default' => '',
        ) );
		$repeater->add_control( 'listing_type', Array(
            'type' => Controls_Manager::SELECT,
            'label' => esc_html__( 'Listing Type', 'jvfrmtd' ),
			'options' => $this->getListyTypes(),
        ) );
        return array_values( $repeater->get_controls() );
    }

    protected function render() {
        $items = (Array)$this->get_settings_for_display('items');
        foreach(array_filter($items) as $index => $item) {
            if(!$item['listing_type']) {
                continue;
            }
            $listingType = $item['listing_type'];
            $this->add_render_attribute('button-'.$listingType, Array(
                'class' => Array('listing-type-filter', 'btn', 'btn-sm', 'btn-light'),
                'href' => 'javascript:',
                'data-type' => $listingType,
            ), false, true);

            $this->add_render_attribute('button-label', 'class', 'button-label', true);
            if('yes' == $this->get_settings_for_display('hide_label')) {
                $this->add_render_attribute('button-label', 'class', 'hidden');
                $this->add_render_attribute('button-'.$listingType, Array(
                    'class' => 'javo-tooltip',
                    'title' => $item['title'],
                ));
            }


            ob_start();
            Icons_Manager::render_icon( $item['icon'], Array(
                'aria-hidden' => 'true',
            ) );
            $iconRender = ob_get_clean();
            printf(
                '<a %1$s>%3$s <span %2$s>%4$s</span></a>',
                $this->get_render_attribute_string('button-'.$listingType),
                $this->get_render_attribute_string('button-label'),
                $iconRender, $item['title']
            );
        }
    }
}