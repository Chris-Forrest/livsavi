<?php
namespace jvbpdelement\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;


if ( ! defined( 'ABSPATH' ) ) exit;

class jvbpd_single_vender extends Widget_Base {

	public function get_name() { return 'jvbpd-single-vendor'; }

	public function get_title() { return 'Single Vendor'; }

	public function get_icon() { return 'eicon-button'; }

	public function get_categories() { return [ 'jvbpd-single-listing' ]; }

	protected function _register_controls() {
		$this->start_controls_section( 'section_general', array(
			'label' => esc_html__( 'General', 'jvfrmtd' ),
		) );
		$this->add_control('columns', Array(
			'type' => Controls_Manager::SELECT,
			'label' => esc_html__( 'Columns', 'jvfrmtd' ),
			'default' => '1',
			'options' => jvbpd_elements_tools()->getColumnsOption(1, 4),
		));
		$this->end_controls_section();
	}

	protected function render() {
		if( !function_exists( 'lava_directory_vendor' ) ){
			return;
		}

		$settings = $this->get_settings();
		switch($this->get_settings('columns')) {
		case 2:
			$columnCSS = 'col-6';
		break;
		case 3:
			$columnCSS = 'col-4';
		break;
		case 4:
			$columnCSS = 'col-3';
		break;
		case 1:
		default:
			$columnCSS = 'col-12';
		}

		jvbpd_elements_tools()->switch_preview_post();
		$intVendorID = lava_directory_vendor()->core->getVendorID();
		$vendor_products = lava_directory_vendor()->core->getProducts(
			Array(
				'listing_id' => get_the_ID(),
				'vendor' => $intVendorID,
				'exclude_booking' => true,
			)
		);
		echo '<ul class="row">';
		if( !empty( $vendor_products ) ) : foreach( $vendor_products as $objProduct ) {
			$objModule = new \moduleWC1( $objProduct, Array(
				'css' => $columnCSS,
			) );
			echo $objModule->output();
		} endif;
		echo '</ul>';
		jvbpd_elements_tools()->restore_preview_post();
    }
}