<?php
/**
 * Widget Name: Map Switch
 * Author: Javo
 * Version: 1.0.0.1
*/

namespace jvbpdelement\Widgets;

use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Jvbpd_Map_Switch extends Widget_Base {

	public function get_name() { return 'jvbpd-map-switch'; }
	public function get_title() { return 'Map Switch'; }
	public function get_icon() { return 'fas fa-toggle-on'; }
    public function get_categories() { return [ 'jvbpd-map-page' ]; }

    protected function _register_controls() {
        $this->start_controls_section('section_general', Array(
            'label' => esc_html__( 'General', 'jvfrmtd' ),
        ));
            $this->add_control( 'items', Array(
                    'label' => __( 'Items', 'jvfrmtd' ),
                    'type' => Controls_Manager::REPEATER,
                    'fields' => $this->getFormItem(),
                    // 'title_field' => 'Listing Type ID: {{{ listing_type }}}',
                )
            );
        $this->end_controls_section();
    }
    public function getFormItem() {
        $repeater = new Repeater();
        $repeater->add_control( 'element', Array(
            'label' => esc_html__( 'Element Type', 'jvfrmtd' ),
            'type' => Controls_Manager::SELECT,
            'default' => '',
            'options' => Array(
                '' => esc_html__("Select one", 'jvfrmtd'),
                'listOnly' => esc_html__("Only List Switch", 'jvfrmtd'),
                'filterToggle' => esc_html__("Filter Open/Close Switch", 'jvfrmtd'),
                'filterOpener' => esc_html__("Filter Opener", 'jvfrmtd'),
            ),
        ) );
        $repeater->add_control( 'default', Array(
            'label' => esc_html__( 'Switch default', 'jvfrmtd' ),
            'type' => Controls_Manager::SWITCHER,
            'default' => '',
            'condition' => Array(
                'element!' => 'filterOpener',
            ),
        ) );
        $repeater->add_control( 'filter_section', Array(
            'label' => __( 'Filter Section ID', 'jvfrmtd' ),
            'type' => Controls_Manager::TEXT,
        ));
        $repeater->add_control( 'list_section', Array(
            'label' => __( 'List Section ID', 'jvfrmtd' ),
            'type' => Controls_Manager::TEXT,
        ));
        $repeater->add_control( 'map_section', Array(
            'label' => __( 'Map Section ID', 'jvfrmtd' ),
            'type' => Controls_Manager::TEXT,
        ));

        return array_values( $repeater->get_controls() );
    }
    protected function render() {
        $this->add_render_attribute('switch-label-off', 'class', Array('map-switch-label', 'switch-off'));
        $this->add_render_attribute('switch-label-on', 'class', Array('map-switch-label', 'switch-on'));
        foreach($this->get_settings_for_display('items') as $item) {
            if('filterOpener' == $item['element']) {
                $this->add_render_attribute('wrap', Array(
                    'class' => Array('map-switcher', 'type-'.$item['element'], 'hidden'),
                    'data-filter' => false === strpos($item['filter_section'], '#') ? '#'.$item['filter_section'] : $item['filter_section'],
                ), false, true);
            }else{
                $this->add_render_attribute('wrap', Array(
                    'class' => Array('map-switcher', 'type-'.$item['element'], 'mr-3'),
                    'data-filter' => false === strpos($item['filter_section'], '#') ? '#'.$item['filter_section'] : $item['filter_section'],
                    'data-list' => false === strpos($item['list_section'], '#') ? '#'.$item['list_section'] : $item['list_section'],
                    'data-map' => false === strpos($item['map_section'], '#') ? '#'.$item['map_section'] : $item['map_section'],
                ), false, true);
            }

            if(is_callable(Array($this, $item['element']))) {
                ?>
                <span <?php echo $this->get_render_attribute_string('wrap'); ?>>
                    <?php $callback = call_user_func_array(Array($this, $item['element']), Array($item)); ?>
                </span>
                <?php
            }
        }
    }

    public function listOnly($args=Array()) {
        printf('<span %1$s>%2$s</span>', $this->get_render_attribute_string('switch-label-off'), esc_html__("List Only", 'jvfrmtd'));
        ?>
        <label class="jv-switch small">
            <input type="checkbox" class="" <?php checked('yes' == $args['default']);?>>
            <span class="slider round"></span>
        </label>
        <?php
        printf('<span %1$s>%2$s</span>', $this->get_render_attribute_string('switch-label-on'), esc_html__("List + Map", 'jvfrmtd'));
    }

    public function filterToggle($args=Array()) {
        printf('<span %1$s>%2$s</span>', $this->get_render_attribute_string('switch-label-off'), esc_html__("Filter Close", 'jvfrmtd'));
        esc_html_e("", 'jvfrmtd'); ?>
        <label class="jv-switch small">
            <input type="checkbox" class="" <?php checked('yes' == $args['default']);?>>
            <span class="slider round"></span>
        </label>
        <?php
        printf('<span %1$s>%2$s</span>', $this->get_render_attribute_string('switch-label-on'), esc_html__("Filter Open", 'jvfrmtd'));
    }

    public function filterOpener($args=Array()) {
        return '';
    }
}