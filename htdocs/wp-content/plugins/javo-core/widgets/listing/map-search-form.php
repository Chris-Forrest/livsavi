<?php
/**
 * Widget Name: Map search form
 * Author: Javo
 * Version: 1.0.0.1
*/

namespace jvbpdelement\Widgets;

use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Icons_Manager;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Jvbpd_Map_Search_Form extends Widget_Base {

	public function get_name() { return 'jvbpd-map-search-form'; }
	public function get_title() { return 'Map Search Form'; }
	public function get_icon() { return 'fa fa-map-o'; }
    public function get_categories() { return [ 'jvbpd-map-page' ]; }

    protected function _register_controls() {
        $this->start_controls_section('section_general', Array(
            'label' => esc_html__( 'General', 'jvfrmtd' ),
        ));
            $this->add_control( 'hide_label', Array(
                'label' => __( 'Hide Label & Use Tooltip', 'jvfrmtd' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
            ));
            $this->add_control( 'items', Array(
                'label' => __( 'Items', 'jvfrmtd' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $this->getFormItem(),
                'title_field' => 'Listing Type ID: {{{ listing_type }}}',
                'separator' => 'before',
            ));
        $this->end_controls_section();
    }

    public function getListyTypes() {
        $listingTypesOption = Array('0' => esc_html__("All", 'jvfrmtd'));
		if(class_exists('\\LavaDirectoryManagerPro\\Base\\BaseController')) {
			foreach(\LavaDirectoryManagerPro\Base\BaseController::ListingTypesArray() as $listingID => $listingLabel) {
				$listingTypesOption[$listingID] = $listingLabel;
			}
        }
        return $listingTypesOption;
    }

    public function getFormItem() {
        $repeater = new Repeater();
        $repeater->add_control( 'icon', Array(
            'label' => __( 'Icon', 'jvfrmtd' ),
            'type' => Controls_Manager::ICONS,
            'default' => Array(
                'value' => '',
                'library' => '',
            ),
        ) );
        $repeater->add_control( 'title', Array(
            'type' => Controls_Manager::TEXT,
            'label' => esc_html__( 'Tab title', 'jvfrmtd' ),
			'default' => '',
        ) );
		$repeater->add_control( 'listing_type', Array(
            'type' => Controls_Manager::SELECT,
            'label' => esc_html__( 'Listing Type', 'jvfrmtd' ),
			'options' => $this->getListyTypes(),
        ) );
        return array_values( $repeater->get_controls() );
    }

    public function tab_headers() {
        $items = (Array)$this->get_settings_for_display('items');
        $defaultListingType = jvbpdCore()->template_instance->getListingTypeParametter();
        foreach(array_filter($items) as $index => $item) {
            if(!$item['listing_type']) {
                continue;
            }
            $listingType = $item['listing_type'];
            $this->add_render_attribute('tab-head-'.$listingType, Array(
                'class' => 'nav-link',
                'data-toggle' => 'tab',
                'data-type' => $listingType,
                'href' => '#tab-' . $this->get_id() .'-body-'.$listingType,
                'role' => 'tab',
            ), false, true);

            $this->add_render_attribute('nav-item-label', 'class', 'nav-item-label', true);

            if('yes' == $this->get_settings_for_display('hide_label')) {
                $this->add_render_attribute('nav-item-label', 'class', 'hidden');
                $this->add_render_attribute('tab-head-'.$listingType, Array(
                    'class' => 'javo-tooltip',
                    'title' => $item['title'],
                ));
            }

            if($defaultListingType) {
                if($defaultListingType == $listingType) {
                    $this->add_render_attribute('tab-head-'.$listingType, 'class', 'active');
                }
            }else{
                if( 0 === $index ) {
                    $this->add_render_attribute('tab-head-'.$listingType, 'class', 'active');
                }
            }

            ob_start();
            Icons_Manager::render_icon( $item['icon'], Array(
                'aria-hidden' => 'true',
            ) );
            $iconRender = ob_get_clean();
            printf(
                '<li class="nav-item"><a %1$s>%3$s <span %2$s>%4$s</span></a></li>',
                $this->get_render_attribute_string('tab-head-'.$listingType),
                $this->get_render_attribute_string('nav-item-label'),
                $iconRender, $item['title']
            );
        }
    }
    public function tab_contents() {
        $items = (Array)$this->get_settings_for_display('items');
        $defaultListingType = jvbpdCore()->template_instance->getListingTypeParametter();
        foreach(array_filter($items) as $index => $item) {
            if(!$item['listing_type']) {
                continue;
            }
            $listingType = $item['listing_type'];
            $GLOBALS['map_search_listing_type'] = $listingType;
            $searchFormID = 0;
            $output = '';
            if(class_exists('\\LavaDirectoryManagerPro\\Base\\BaseController')) {
                $baseController = new \LavaDirectoryManagerPro\Base\BaseController;
                $searchFormID = $baseController->ListingTypeValues($listingType, 'lt_search');
            }
            $output = \Elementor\Plugin::$instance->frontend->get_builder_content_for_display($searchFormID);
            $this->add_render_attribute('tab-body-'.$listingType, Array(
                'class' => Array('tab-pane', 'fade'),
                'id' => 'tab-' . $this->get_id() .'-body-'.$listingType,
                'role' => 'tabpanel',
            ));
            if($defaultListingType) {
                if($defaultListingType == $listingType) {
                    $this->add_render_attribute('tab-body-'.$listingType, 'class', 'active show');
                }
            }else{
                if( 0 === $index ) {
                    $this->add_render_attribute('tab-body-'.$listingType, 'class', 'active show');
                }
            }

            printf(
                '<div %1$s>%2$s</div>',
                $this->get_render_attribute_string('tab-body-'.$listingType),
                $output
            );
        }
        unset($GLOBALS['map_search_listing_type']);
    }

    protected function render() {
        ?>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <?php $this->tab_headers(); ?>
        </ul>
        <div class="tab-content" id="myTabContent">
            <?php $this->tab_contents(); ?>
        </div>
        <?php
    }
}