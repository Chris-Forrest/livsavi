<?php
namespace jvbpdelement\Widgets;

use Elementor\Embed;
use Elementor\Utils;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Modules\DynamicTags\Module as TagsModule;

use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit;

class Jvbpd_Single_Portfolio_Gallery extends Widget_Base {

	public function get_name() { return 'jvbpd-single-portfolio-gallery'; }
	public function get_title() { return 'Portfolio Gallery'; }
	public function get_icon() { return 'eicon-image'; }
    public function get_categories() { return [ 'jvbpd-elements' ]; }

	protected function _register_controls() {
		global $post;

        $this->start_controls_section('section_general', Array(
            'label' => esc_html__('General', "jvfrmtd"),
        ));
            $this->add_control('portfolio_slider_type', Array(
                'type' => Controls_Manager::SELECT,
                'label' => esc_html__('Type', "jvfrmtd"),
                'default' => 'type1',
                'options' => Array(
                    'type1' => esc_html__('Type 1', "jvfrmtd"),
                    'type2' => esc_html__('Type 2', "jvfrmtd"),
                ),
            ));
            $this->add_control('portfolio_slider_item_type', Array(
                'type' => Controls_Manager::SELECT,
                'label' => esc_html__('Item Type', "jvfrmtd"),
                'default' => 'repeater',
                'options' => Array(
                    'repeater' => esc_html__('Repeater Items', "jvfrmtd"),
                    'gallery' => esc_html__('Detail Images', "jvfrmtd"),
                ),
                'separator' => 'before',
			));
			$defaultRepeaterItems = Array();
			if($post instanceof \WP_Post && $post->post_type == 'portfolio') {
				$detailImageFields = get_post_meta($post->ID, 'detail_images', true);
				foreach($detailImageFields as $field) {
					$defaultRepeaterItems[] = Array(
						'title' => $field['title'],
						'name' => $field['name'],
						'image' => Array(
							'id' => $field['image_id'],
							'url' => wp_get_attachment_image_url($field['image_id'], 'full'),
						),
					);
				}
			}
            $this->add_control('portfolio_slider_repeater_items', Array(
                'type' => Controls_Manager::REPEATER,
                'show_label' => false,
				'fields' => $this->getSliderItemSettings(),
				'default' => $defaultRepeaterItems,
                'title_field' => '{{{ title }}}',
                'separator' => 'after',
            ));
        $this->end_controls_section();
    }

    public function getSliderItemSettings() {

		$repeater = new \Elementor\Repeater();
		$repeater->add_control('title', Array(
			'type' => Controls_Manager::TEXT,
			'label' => esc_html__('Title', "jvfrmtd"),
			'label_block' => true,
		));
		$repeater->add_group_control( Group_Control_Typography::get_type(), Array(
			'name' => 'title_font',
			'show_label' => false,
			'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			'selector' => '{{WRAPPER}} {{CURRENT_ITEM}} .title',
		));
		$repeater->add_control('title_color', Array(
			'type' => Controls_Manager::COLOR,
			'label' => esc_html__('Title Color', "jvfrmtd"),
			'selectors' => Array(
				'{{WRAPPER}} {{CURRENT_ITEM}} .title' => 'color:{{VALUE}};',
			),
			'separator' => 'after',
		));
		$repeater->add_control('name', Array(
			'type' => Controls_Manager::TEXT,
			'label' => esc_html__('Name', "jvfrmtd"),
		));
		$repeater->add_group_control( Group_Control_Typography::get_type(), Array(
			'name' => 'name_font',
			'show_label' => false,
			'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			'selector' => '{{WRAPPER}} {{CURRENT_ITEM}} .subtitle',
		));
		$repeater->add_control('name_color', Array(
			'type' => Controls_Manager::COLOR,
			'label' => esc_html__('Name Color', "jvfrmtd"),
			'selectors' => Array(
				'{{WRAPPER}} {{CURRENT_ITEM}} .subtitle' => 'color:{{VALUE}};',
			),
			'separator' => 'after',
		));
		$repeater->add_control('type', Array(
			'type' => Controls_Manager::SELECT,
			'label' => esc_html__('Tyoe', "jvfrmtd"),
			'default' => 'image',
			'options' => Array(
				'image' => esc_html__("Image", 'jvfrmtd'),
				'video' => esc_html__("Video", 'jvfrmtd'),
				'iframe' => esc_html__("IFrame", 'jvfrmtd'),
			),
			'separator' => 'before',
		));
		$repeater->add_control('image', Array(
			'type' => Controls_Manager::MEDIA,
			'show_label' => false,
			'condition' => Array(
				'type' => 'image',
			),
		));
		$repeater->add_control('video_type', Array(
			'label' => __( 'Source', 'jvfrmtd' ),
			'type' => Controls_Manager::SELECT,
			'default' => 'youtube',
			'options' => Array(
				'youtube' => __( 'YouTube', 'jvfrmtd' ),
				'vimeo' => __( 'Vimeo', 'jvfrmtd' ),
				'dailymotion' => __( 'Dailymotion', 'jvfrmtd' ),
				'hosted' => __( 'Self Hosted', 'jvfrmtd' ),
			),
			'condition' => Array(
				'type' => 'video',
			),
		));

		$repeater->add_control('youtube_url', Array(
			'label' => __( 'Link', 'jvfrmtd' ),
			'type' => Controls_Manager::TEXT,
			'dynamic' => Array(
				'active' => true,
				'categories' => Array(
					TagsModule::POST_META_CATEGORY,
					TagsModule::URL_CATEGORY,
				),
			),
			'placeholder' => __( 'Enter your URL', 'jvfrmtd' ) . ' (YouTube)',
			'default' => 'https://www.youtube.com/watch?v=XHOmBV4js_E',
			'label_block' => true,
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'youtube',
			),
		));

		$repeater->add_control( 'vimeo_url', Array(
			'label' => __( 'Link', 'jvfrmtd' ),
			'type' => Controls_Manager::TEXT,
			'dynamic' => Array(
				'active' => true,
				'categories' => Array(
					TagsModule::POST_META_CATEGORY,
					TagsModule::URL_CATEGORY,
				),
			),
			'placeholder' => __( 'Enter your URL', 'jvfrmtd' ) . ' (Vimeo)',
			'default' => 'https://vimeo.com/235215203',
			'label_block' => true,
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'vimeo',
			),
		));

		$repeater->add_control( 'dailymotion_url', Array(
			'label' => __( 'Link', 'jvfrmtd' ),
			'type' => Controls_Manager::TEXT,
			'dynamic' => Array(
				'active' => true,
				'categories' => Array(
					TagsModule::POST_META_CATEGORY,
					TagsModule::URL_CATEGORY,
				),
			),
			'placeholder' => __( 'Enter your URL', 'jvfrmtd' ) . ' (Dailymotion)',
			'default' => 'https://www.dailymotion.com/video/x6tqhqb',
			'label_block' => true,
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'dailymotion',
			),
		));

		$repeater->add_control('insert_url', Array(
			'label' => __( 'External URL', 'jvfrmtd' ),
			'type' => Controls_Manager::SWITCHER,
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'hosted',
			),
		));

		$repeater->add_control( 'hosted_url', Array(
			'label' => __( 'Choose File', 'jvfrmtd' ),
			'type' => Controls_Manager::MEDIA,
			'dynamic' => Array(
				'active' => true,
				'categories' => Array(
					TagsModule::MEDIA_CATEGORY,
				),
			),
			'media_type' => 'video',
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'hosted',
				'insert_url' => '',
			),
		));

		$repeater->add_control('external_url', Array(
			'label' => __( 'URL', 'jvfrmtd' ),
			'type' => Controls_Manager::URL,
			'autocomplete' => false,
			'options' => false,
			'label_block' => true,
			'show_label' => false,
			'dynamic' => Array(
				'active' => true,
				'categories' => Array(
					TagsModule::POST_META_CATEGORY,
					TagsModule::URL_CATEGORY,
				),
			),
			'media_type' => 'video',
			'placeholder' => __( 'Enter your URL', 'jvfrmtd' ),
			'condition' => Array(
				'type' => 'video',
				'video_type' => 'hosted',
				'insert_url' => 'yes',
			),
		));
		$repeater->add_control('iframe_code', Array(
			'show_label' => false,
			'type' => Controls_Manager::CODE,
			'language' => 'html',
			'condition' => Array(
				'type' => 'iframe',
			),
		));
		return $repeater->get_controls();
	}

    protected function render() {
        $displayType = $this->get_settings_for_display('portfolio_gallery_type');
		$displayType = 'slider'; // $this->get_settings_for_display('portfolio_gallery_type');
		$itemImageSize = 'medium';
		$isMasonry = 'masonry' == $displayType;
		$isSlider = 'slider' == $displayType;
		$this->add_render_attribute('portfolio-gallery-wrap', Array(
			'class' => Array(
				'portfolio-gallery-wrap',
				'type-' .  $displayType,
				($isMasonry ? 'effect-' . $this->get_settings_for_display('portfolio_masonry') . ' ' . 'hidden' : ($isSlider ? 'swiper-wrapper' : 'row'))
			),
			'id' => 'pg-wrap-' . $this->get_id(),
		), false, true );

		$images = Array();
		if($isSlider) {
			$itemImageSize = 'full';
			if('repeater' == $this->get_settings_for_display('portfolio_slider_item_type')) {
				foreach($this->get_settings_for_display('portfolio_slider_repeater_items') as $item) {
					$itemArgs = Array(
						'id' => $item['_id'],
						'type' => $item['type'],
						'title' => $item['title'],
						'name' => $item['name'],
						'settings' => $item,
					);
					if('video' == $item['type']) {
						$video_url = $item[ $item['video_type'] . '_url' ];
						if ( 'hosted' === $item['video_type'] ) {
							$video_url = $this->get_hosted_video_url($item);
						}elseif('youtube' === $item['video_type']) {
							preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $video_url, $matches);
							$itemArgs['video_id'] = $matches[0];
						}elseif('vimeo' === $item['video_type']) {
							preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $video_url, $matches);
							$itemArgs['video_id'] = $matches[3];
						}
						$itemArgs['video_type'] = $item['video_type'];
						$itemArgs['video_url'] = $video_url;
					}elseif('iframe' == $item['type']){
						$itemArgs['code'] =$item['iframe_code'];
					}else{
						$itemArgs['image'] = wp_get_attachment_image($item['image']['id'], $itemImageSize, false, Array('style' => 'width:100%;'));
						$itemArgs['image_id'] = $item['image']['id'];
					}
					$images[] = $itemArgs;
				}
			}
		}else{
			$detailImages = (Array)get_post_meta(get_the_ID(), 'detail_images', true);
			foreach(array_filter($detailImages) as $image_id) {
				$images[] = Array(
					'type' => 'image',
					'image_id' => $image_id,
					'image' => wp_get_attachment_image($image_id, $itemImageSize),
					'title' => 'Image',
				);
			}
        }
		$output = '';

		$itemContainerTag = $itemTag = 'div';

		if($isMasonry) {
			$itemContainerTag = 'ul';
			$itemTag = 'li';
		}

		if($isSlider) {
			$output .= '<div class="swiper-container portfolio-gallery-swiper-container">';
		}

		$items = Array();
		$output .= sprintf('<%1$s %2$s>', $itemContainerTag, $this->get_render_attribute_string('portfolio-gallery-wrap'));
		$renderOverlay = '<div class="swiper-overlay"></div>';
		foreach((array)$images as $imageIndex => $image) {
			$this->add_render_attribute('portfolio-gallery-item', Array(
				'class' => Array(
					'portfolio-gallery-item',
					($isMasonry ? '' : ($isSlider ? 'swiper-slide':'col-3 pb-3'))
				),
			), false, true );
			if($isSlider) {
				$this->add_render_attribute('portfolio-gallery-item', Array(
					'data-id' => $image['id'],
					'data-title' => $image['title'],
					'data-subtitle' => $image['name'],
					'data-number' => ++$imageIndex,
				), false, true);
			}
			if('video' == $image['type']) {
				if ( 'hosted' === $image['video_type'] ) {
					ob_start();
					$this->render_hosted_video($image['settings']);
					$video_html = ob_get_clean();
				} else {
					$embedParam = Array();
					if('youtube' == $image['video_type']) {
						$embedParam = Array(
							'autoplay' => 1,
							'mute' => 1,
							'loop' => 1,
							'playlist' => $image['video_id'],
							'controls' => 0,
						);
					}
					$video_html = Embed::get_embed_html($image['video_url'], $embedParam, Array(
						'lazy_load' => false,
					));
				}
				$items[] = sprintf(
					'<%1$s %2$s><div class="img-mask">%3$s</div>' . $renderOverlay . '</%1$s>', $itemTag,
					$this->get_render_attribute_string('portfolio-gallery-item'), $video_html
				);
			}elseif('iframe' ==$image['type'] ){
				$items[] = sprintf(
					'<%1$s %2$s><div class="img-mask">%3$s</div></%1$s>', $itemTag,
					$this->get_render_attribute_string('portfolio-gallery-item'), $image['code']
				);
			}else{
				$items[] = sprintf(
					'<%1$s %2$s><div class="img-mask">%3$s</div>' . $renderOverlay . '</%1$s>', $itemTag,
					$this->get_render_attribute_string('portfolio-gallery-item'), $image['image']
				);
			}
		}

		$items = apply_filters('jvbpd_core/widget/portfolio_gallery/items', $items, $this);
		$output .= join('', $items);

		$output .= sprintf('</%1$s>', $itemContainerTag);
		if($isSlider) {
			$output .= '<div class="showcase-pagination-wrap">';
			$output .= '	<div class="showcase-counter" data-total="' . sizeof($items) . '"></div>';
			$output .= '	<div class="showcase-pagination"></div>';
			$output .= '	<div class="caption-border left"></div>';
			$output .= '	<div class="caption-border right"></div>';
			$output .= '	<div class="arrows-wrap">';
			$output .= '		<div class="prev-wrap parallax-wrap"><div class="swiper-button-prev swiper-button-white parallax-element"></div></div>';
			$output .= '		<div class="next-wrap parallax-wrap"><div class="swiper-button-next swiper-button-white parallax-element"></div></div>';
			$output .= '	</div>';
			$output .= '</div>';
			$output .= '</div>';
		}
		echo $output;
	}

	private function get_hosted_video_url($settings=Array()) {
		if ( ! empty( $settings['insert_url'] ) ) {
			$video_url = $settings['external_url']['url'];
		} else {
			$video_url = $settings['hosted_url']['url'];
		}

		if ( empty( $video_url ) ) {
			return '';
		}
		return $video_url;
	}

	private function render_hosted_video($settings) {
		$video_url = $this->get_hosted_video_url($settings);
		if ( empty( $video_url ) ) {
			return;
		}
		$video_params = Array(
			'muted' => 'muted',
			'playsinline' => '',
			'controlsList' => 'nodownload',
		); ?>
		<video class="elementor-video" src="<?php echo esc_url( $video_url ); ?>" <?php echo Utils::render_html_attributes( $video_params ); ?>>></video>
		<?php
	}

}