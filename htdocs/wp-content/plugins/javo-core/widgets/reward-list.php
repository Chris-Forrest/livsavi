<?php
/**
 * Widget Name: Reward list
 * Author: Javo
 * Version: 1.0.0.0
*/

namespace jvbpdelement\Widgets;

use Elementor\Plugin;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class jvbpd_reward_list extends Widget_Base {

    private $template = false;

	public function get_name() { return 'jvbpd-reward-list'; }
	public function get_title() { return 'Reward List'; }
	public function get_icon() { return 'fa fa-map-o'; }
    public function get_categories() { return [ 'jvbpd-elements' ]; }

    public function _register_controls() {
        $this->start_controls_section( 'section_general', Array(
            'label' => esc_html__( 'General', 'jvfrmtd' ),
        ) );

            $this->add_control( 'reward_module', Array(
                'label' => esc_html__( 'Module', 'jvfrmtd' ),
                'type' => Controls_Manager::SELECT2,
                'default' => '',
                'options' => jvbpd_elements_tools()->getModuleIDs(),
            ) );
            $this->add_responsive_control('columns', Array(
				'label' => esc_html__( "Columns", 'jvfrmtd' ),
				'type' => Controls_Manager::SELECT,
				'prefix_class' => 'columns%s-',
				'options' => jvbpd_elements_tools()->getColumnsOption(1, 4),
				'devices' => Array( 'desktop', 'tablet', 'mobile' ),
				'desktop_default' => 3,
				'tablet_default' => 2,
				'mobile_default' => 1,
			));

        $this->end_controls_section();
    }

    public function getRewards() {
        $args = Array(
            'post_type' => 'lv-reward',
            'post_status' => 'publish',
        );
        $query = new \WP_Query($args);
        return $query->get_posts();
    }

    public function getRender($reward=0) {
        if(!$this->template) {
            $module = $this->get_settings_for_display('reward_module');
            $this->template = Plugin::instance()->frontend->get_builder_content_for_display($module);
            if('' == $this->template) {
				$this->template = '<div class="alert alert-warning" role="alert">';
				$this->template .= esc_html__("Please select a module", 'jvfrmtd');
				$this->template .= '</div>';
			}
        }
        return new \Jvbpd_Replace_Content($reward, $this->template);
    }

    protected function render() {
        $this->add_render_attribute('container', 'class', 'reward-list-wrap' );
        $this->add_render_attribute('wrap', 'class', 'reward-list-items' );
         ?>
        <div <?php echo $this->get_render_attribute_string('container'); ?>>
            <ul <?php echo $this->get_render_attribute_string('wrap'); ?>>
            <?php
            foreach($this->getRewards() as $reward) {
                $render = $this->getRender($reward->ID);
                echo sprintf(
                    '<li class="reward-list-item" data-reward-id="%1$s">%2$s</li>',
                    $reward->ID,
                    $render->render()
                );
            } ?>
            </ul>
        </div>
        <?php
    }
}