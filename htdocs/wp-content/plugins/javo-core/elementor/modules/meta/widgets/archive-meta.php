<?php
namespace jvbpdelement\Modules\Meta\Widgets;

use Elementor\Controls_Manager;

class Archive_Meta extends Base {

	private $post = null;

	public function get_name() { return parent::ARCHIVE_META; }
	public function get_title() { return 'Archive Module Meta'; }
	public function get_icon() { return 'eicon-sidebar'; }
	public function get_categories() { return [ 'jvbpd-page-builder-module' ]; }

	protected function _register_controls() {
		parent::_register_controls();
		$this->add_prefix_settings_control();
		$this->add_html_tag_controls();
		$this->updateArchiveMeta();
	}

	public function updateArchiveMeta() {
		$this->update_control('field_type', Array(
			'type' => Controls_Manager::HIDDEN,
			'default' => 'base',
		));
		$this->update_control('meta', Array(
			'default' => 'archive_title',
			'options' => Array(
				'archive_title' => esc_html__("Archive Title", 'jvfrmtd'),
				'archive_description' => esc_html__("Archive Description", 'jvfrmtd'),
			),
		));
	}

	protected function render() {
		jvbpd_elements_tools()->switch_preview_post();
		$this->_render();
		jvbpd_elements_tools()->restore_preview_post();
	}

}