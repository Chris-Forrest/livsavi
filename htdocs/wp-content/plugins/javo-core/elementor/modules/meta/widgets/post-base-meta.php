<?php
namespace jvbpdelement\Modules\Meta\Widgets;

use Elementor\Controls_Manager;

class Post_Base_Meta extends Base {
	public function get_name() { return parent::POST_META; }
	public function get_title() { return 'Post Meta'; }
	public function get_icon() { return 'eicon-sidebar'; }
	public function get_categories() { return [ 'jvbpd-page-builder-module' ]; }

	protected function _register_controls() {
		parent::_register_controls();
		$this->update_controls();
		$this->add_label_settings_control();
	}

	protected function render() {
		$this->_render();
	}

	public function getModuleMeta() {
		$field = $this->get_settings( 'meta' );
		return $this->getFieldMeta( $field );
	}

	protected function update_controls() {
		$this->update_control( 'field_type', Array(
			'type' => Controls_Manager::HIDDEN,
			'default' => 'base',
		) );
		$this->update_control( 'meta', Array(
			'condition' => Array(),
			'default' => 'txt_title',
			'label_block' => true,
			'options' => $this->getMetaOptions(),
		) );
		$this->start_injection(Array(
			'of' => 'meta',
		));
			$this->add_control('portfolio_gallery_type', Array(
				'type' => Controls_Manager::SELECT,
				'label' => esc_html__('Gallery Type', "jvfrmtd"),
				'default' => 'grid',
				'options' => Array(
					'grid' => esc_html__('Grid', "jvfrmtd"),
					'masonry' => esc_html__('Masonry', "jvfrmtd"),
					'slider' => esc_html__('Full Slider', "jvfrmtd"),
				),
				'condition' => Array('meta' => 'portfolio_gallery')
			));
			$aniOptions = Array();
			for($aniID=1;$aniID<=11;$aniID++){
				$aniOptions[$aniID] = sprintf(esc_html__("Effect %s", 'jvfrmtd'), $aniID);
			}
			$this->add_control('portfolio_masonry', Array(
				'type' => Controls_Manager::SELECT,
				'label' => esc_html__('Masonry Effect', "jvfrmtd"),
				'default' => '1',
				'options' => $aniOptions,
				'separator' => 'after',
				'condition' => Array('portfolio_gallery_type' => 'masonry')
			));
			/*
			$this->add_control('portfolio_slider_item_type', Array(
				'type' => Controls_Manager::SELECT,
				'label' => esc_html__('Item Type', "jvfrmtd"),
				'default' => 'repeater',
				'options' => Array(
					'repeater' => esc_html__('Repeater Items', "jvfrmtd"),
					'gallery' => esc_html__('Detail Images', "jvfrmtd"),
				),
				'separator' => 'before',
				'condition' => Array('portfolio_gallery_type' => 'slider'),
			));
			$this->add_control('portfolio_slider_repeater_items', Array(
				'type' => Controls_Manager::REPEATER,
				'show_label' => false,
				'fields' => $this->getSliderItemSettings(),
				'title_field' => '{{{ title }}}',
				'separator' => 'after',
				'condition' => Array(
					'portfolio_gallery_type' => 'slider',
					'portfolio_slider_item_type' => 'repeater',
				),
			)); */
		$this->end_injection();
	}

	public function getSliderItemSettings() {
		$repeater = new \Elementor\Repeater();
		$repeater->add_control('title', Array(
			'type' => Controls_Manager::TEXT,
			'label' => esc_html__('Slide Title', "jvfrmtd"),
		));
		$repeater->add_control('image', Array(
			'type' => Controls_Manager::MEDIA,
			'show_label' => false,
			'separator' => 'before',
		));
		return $repeater->get_controls();
	}

	public function getMetaOptions() {
		$options = Array(
			'txt_title' => esc_html__( "Title", 'jvfrmtd' ),
			'txt_content' => esc_html__( "Content", 'jvfrmtd' ),
			'_featured' => esc_html__( "Featured", 'jvfrmtd' ),
			'post_author' => esc_html__( "Author", 'jvfrmtd' ),
			'post_date' => esc_html__( "Date", 'jvfrmtd' ),
			'post_category' => esc_html__( "Category", 'jvfrmtd' ),
			'post_tag' => esc_html__( "Tags", 'jvfrmtd' ),
			'comment_count' => esc_html__( "Comment Count", 'jvfrmtd' ),
			'portfolio_gallery' => esc_html__( "Portfolio Gallery", 'jvfrmtd' ),
		);

		if(function_exists('activate_lvcd_plugin')) {
			$options['ticket_user_information'] = esc_html__( "[Ticket] User Information(Admin)", 'jvfrmtd' );
			$options['ticket_remote_status'] = esc_html__( "[Ticket] Status Switcher", 'jvfrmtd' );
			$options['ticket_remote_type'] = esc_html__( "[Ticket] Assign Switcher(Admin)", 'jvfrmtd' );
			$options['ticket_remote_priorities'] = esc_html__( "[Ticket] Priorities Switcher(Admin)", 'jvfrmtd' );
		}
		return $options;
	}
}