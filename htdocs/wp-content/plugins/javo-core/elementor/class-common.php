<?php

use Elementor\Controls_Manager;

class Jvbpd_Common_Elementor {

    public static $instance = null;

    public function __construct(){}

    public function register(){
        add_action( 'elementor/frontend/column/before_render', array( $this, 'modify_render' ) );
        add_action( "elementor/element/after_section_end", array( $this, 'add_transition_controls_section'), 18, 3 );
        add_action( "elementor/element/after_section_end", array( $this, 'add_parallax_controls_section'), 18, 3 );

        add_action( 'elementor/frontend/widget/before_render' , array( $this, 'render_attributes' ) );
        add_action( 'elementor/frontend/column/before_render' , array( $this, 'render_attributes' ) );
        add_action( 'elementor/frontend/section/before_render', array( $this, 'render_attributes' ) );
    }

    public function modify_render( $column ){
        // Add parallax initializer to all column elements
        $column->add_render_attribute( '_wrapper', 'class', 'jvfrm-parallax-section' );
    }

    public function add_transition_controls_section($widget, $section_id, $args) {
        // Anchor element sections
        $target_sections = array('section_custom_css');

        if( ! defined('ELEMENTOR_PRO_VERSION') ) {
            $target_sections[] = 'section_custom_css_pro';
        }

        if( ! in_array( $section_id, $target_sections ) ){
            // die(var_dump($target_sections));
            return;
        }

        // Adds transition options to all elements
        // ---------------------------------------------------------------------
        $widget->start_controls_section(
            'jvfrm_core_common_inview_transition',
            array(
                'label'     => __( 'Entrance Animation', 'jvfrmtd' ),
                'tab'       => Controls_Manager::TAB_ADVANCED
            )
        );

        $widget->add_control(
            'jvfrm_animation_name',
            array(
                'label'   => __( 'Animation', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''                           => 'None',

                    'jvfrm-fade-in'                => 'Fade In',
                    'jvfrm-fade-in-down'           => 'Fade In Down',
                    'jvfrm-fade-in-down-1'         => 'Fade In Down 1',
                    'jvfrm-fade-in-down-2'         => 'Fade In Down 2',
                    'jvfrm-fade-in-up'             => 'Fade In Up',
                    'jvfrm-fade-in-up-1'           => 'Fade In Up 1',
                    'jvfrm-fade-in-up-2'           => 'Fade In Up 2',
                    'jvfrm-fade-in-left'           => 'Fade In Left',
                    'jvfrm-fade-in-left-1'         => 'Fade In Left 1',
                    'jvfrm-fade-in-left-2'         => 'Fade In Left 2',
                    'jvfrm-fade-in-right'          => 'Fade In Right',
                    'jvfrm-fade-in-right-1'        => 'Fade In Right 1',
                    'jvfrm-fade-in-right-2'        => 'Fade In Right 2',

                    // Slide Animation
                    'jvfrm-slide-from-right'       => 'Slide From Right',
                    'jvfrm-slide-from-left'        => 'Slide From Left',
                    'jvfrm-slide-from-top'         => 'Slide From Top',
                    'jvfrm-slide-from-bot'         => 'Slide From Bottom',

                    // Mask Animation
                    'jvfrm-mask-from-top'          => 'Mask From Top',
                    'jvfrm-mask-from-bot'          => 'Mask From Bottom',
                    'jvfrm-mask-from-left'         => 'Mask From Left',
                    'jvfrm-mask-from-right'        => 'Mask From Right',

                    'jvfrm-rotate-in'              => 'Rotate In',
                    'jvfrm-rotate-in-down-left'    => 'Rotate In Down Left',
                    'jvfrm-rotate-in-down-left-1'  => 'Rotate In Down Left 1',
                    'jvfrm-rotate-in-down-left-2'  => 'Rotate In Down Left 2',
                    'jvfrm-rotate-in-down-right'   => 'Rotate In Down Right',
                    'jvfrm-rotate-in-down-right-1' => 'Rotate In Down Right 1',
                    'jvfrm-rotate-in-down-right-2' => 'Rotate In Down Right 2',
                    'jvfrm-rotate-in-up-left'      => 'Rotate In Up Left',
                    'jvfrm-rotate-in-up-left-1'    => 'Rotate In Up Left 1',
                    'jvfrm-rotate-in-up-left-2'    => 'Rotate In Up Left 2',
                    'jvfrm-rotate-in-up-right'     => 'Rotate In Up Right',
                    'jvfrm-rotate-in-up-right-1'   => 'Rotate In Up Right 1',
                    'jvfrm-rotate-in-up-right-2'   => 'Rotate In Up Right 2',

                    'jvfrm-zoom-in'                => 'Zoom In',
                    'jvfrm-zoom-in-1'              => 'Zoom In 1',
                    'jvfrm-zoom-in-2'              => 'Zoom In 2',
                    'jvfrm-zoom-in-3'              => 'Zoom In 3',

                    'jvfrm-scale-up'               => 'Scale Up',
                    'jvfrm-scale-up-1'             => 'Scale Up 1',
                    'jvfrm-scale-up-2'             => 'Scale Up 2',

                    'jvfrm-scale-down'             => 'Scale Down',
                    'jvfrm-scale-down-1'           => 'Scale Down 1',
                    'jvfrm-scale-down-2'           => 'Scale Down 2',

                    'jvfrm-flip-in-down'           => 'Flip In Down',
                    'jvfrm-flip-in-down-1'         => 'Flip In Down 1',
                    'jvfrm-flip-in-down-2'         => 'Flip In Down 2',
                    'jvfrm-flip-in-up'             => 'Flip In Up',
                    'jvfrm-flip-in-up-1'           => 'Flip In Up 1',
                    'jvfrm-flip-in-up-2'           => 'Flip In Up 2',
                    'jvfrm-flip-in-left'           => 'Flip In Left',
                    'jvfrm-flip-in-left-1'         => 'Flip In Left 1',
                    'jvfrm-flip-in-left-2'         => 'Flip In Left 2',
                    'jvfrm-flip-in-left-3'         => 'Flip In Left 3',
                    'jvfrm-flip-in-right'          => 'Flip In Right',
                    'jvfrm-flip-in-right-1'        => 'Flip In Right 1',
                    'jvfrm-flip-in-right-2'        => 'Flip In Right 2',
                    'jvfrm-flip-in-right-3'        => 'Flip In Right 3',

                    'jvfrm-pulse'                  => 'Pulse In 1' ,
                    'jvfrm-pulse1'                 => 'Pulse In 2',
                    'jvfrm-pulse2'                 => 'Pulse In 3',
                    'jvfrm-pulse3'                 => 'Pulse In 4',
                    'jvfrm-pulse4'                 => 'Pulse In 5',

                    'jvfrm-pulse-out-1'            => 'Pulse Out 1' ,
                    'jvfrm-pulse-out-2'            => 'Pulse Out 2' ,
                    'jvfrm-pulse-out-3'            => 'Pulse Out 3' ,
                    'jvfrm-pulse-out-4'            => 'Pulse Out 4' ,

                    // Specials
                    'jvfrm-shake'                  => 'Shake',
                    'jvfrm-bounce-in'              => 'Bounce In',
                    'jvfrm-jack-in-box'            => 'Jack In the Box',


                ),
                'default'            => '',
                'prefix_class'       => 'jvfrm-appear-watch-animation ',
                'label_block'        => false
            )
        );


        $widget->add_control(
            'jvfrm_animation_duration',
            array(
                'label'     => __( 'Duration', 'jvfrmtd' ) . ' (ms)',
                'type'      => Controls_Manager::NUMBER,
                'default'   => '',
                'min'       => 0,
                'step'      => 100,
                'selectors'    => array(
                    '{{WRAPPER}}' => 'animation-duration:{{SIZE}}ms;'
                ),
                'condition' => array(
                    'jvfrm_animation_name!' => ''
                ),
                'render_type' => 'template'
            )
        );

        $widget->add_control(
            'jvfrm_animation_delay',
            array(
                'label'     => __( 'Delay', 'jvfrmtd' ) . ' (ms)',
                'type'      => Controls_Manager::NUMBER,
                'default'   => '',
                'min'       => 0,
                'step'      => 100,
                'selectors' => array(
                    '{{WRAPPER}}' => 'animation-delay:{{SIZE}}ms;'
                ),
                'condition' => array(
                    'jvfrm_animation_name!' => ''
                )
            )
        );

        $widget->add_control(
            'jvfrm_animation_easing',
            array(
                'label'   => __( 'Easing', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''                       => 'Default',
                    'initial'                => 'Initial',

                    'linear'                 => 'Linear',
                    'ease-out'               => 'Ease Out',
                    '0.19,1,0.22,1'          => 'Ease In Out',

                    '0.47,0,0.745,0.715'     => 'Sine In',
                    '0.39,0.575,0.565,1'     => 'Sine Out',
                    '0.445,0.05,0.55,0.95'   => 'Sine In Out',

                    '0.55,0.085,0.68,0.53'   => 'Quad In',
                    '0.25,0.46,0.45,0.94'    => 'Quad Out',
                    '0.455,0.03,0.515,0.955' => 'Quad In Out',

                    '0.55,0.055,0.675,0.19'  => 'Cubic In',
                    '0.215,0.61,0.355,1'     => 'Cubic Out',
                    '0.645,0.045,0.355,1'    => 'Cubic In Out',

                    '0.895,0.03,0.685,0.22'  => 'Quart In',
                    '0.165,0.84,0.44,1'      => 'Quart Out',
                    '0.77,0,0.175,1'         => 'Quart In Out',

                    '0.895,0.03,0.685,0.22'  => 'Quint In',
                    '0.895,0.03,0.685,0.22'  => 'Quint Out',
                    '0.895,0.03,0.685,0.22'  => 'Quint In Out',

                    '0.95,0.05,0.795,0.035'  => 'Expo In',
                    '0.19,1,0.22,1'          => 'Expo Out',
                    '1,0,0,1'                => 'Expo In Out',

                    '0.6,0.04,0.98,0.335'    => 'Circ In',
                    '0.075,0.82,0.165,1'     => 'Circ Out',
                    '0.785,0.135,0.15,0.86'  => 'Circ In Out',

                    '0.6,-0.28,0.735,0.045'  => 'Back In',
                    '0.175,0.885,0.32,1.275' => 'Back Out',
                    '0.68,-0.55,0.265,1.55'  => 'Back In Out'
                ),
                'selectors' => array(
                    '{{WRAPPER}}' => 'animation-timing-function:cubic-bezier({{VALUE}});'
                ),
                'condition' => array(
                    'jvfrm_animation_name!' => ''
                ),
                'default'      => '',
                'return_value' => ''
            )
        );

        $widget->add_control(
            'jvfrm_animation_count',
            array(
                'label'   => __( 'Repeat Count', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''  => __( 'Default', 'jvfrmtd' ),
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    'infinite' => __( 'Infinite', 'jvfrmtd' )
                ),
                'selectors' => array(
                    '{{WRAPPER}}' => 'animation-iteration-count:{{VALUE}};opacity:1;' // opacity is required to prevent flick between repetitions
                ),
                'condition' => array(
                    'jvfrm_animation_name!' => ''
                ),
                'default'      => ''
            )
        );

        $widget->end_controls_section();
    }

    public function add_parallax_controls_section( $widget, $section_id, $args ){

        if( in_array( $widget->get_name(), array('section') ) ){
            return;
        }

        // Hook element section
        $target_sections = array('section_custom_css');

        if( ! defined('ELEMENTOR_PRO_VERSION') ) {
            $target_sections[] = 'section_custom_css_pro';
        }

        if( ! in_array( $section_id, $target_sections ) ){
            return;
        }

        // Adds parallax options to advanced section
        // ---------------------------------------------------------------------
        $widget->start_controls_section(
            'jvfrm_pro_common_parallax_section',
            array(
                'label'     => __( 'Parallax Pro', 'jvfrmtd' ),
                'tab'       => Controls_Manager::TAB_ADVANCED
            )
        );

        $widget->add_control(
            'jvfrm_parallax_enabled',
            array(
                'label'        => __( 'Enable Parallax', 'jvfrmtd' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => __( 'On', 'jvfrmtd' ),
                'label_off'    => __( 'Off', 'jvfrmtd' ),
                'return_value' => 'yes',
                'default'      => 'no',
                'separator'    => 'before'
            )
        );

        $widget->add_control(
            'jvfrm_parallax_el_origin',
            array(
                'label'   => __( 'Parallax Origin', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    'top'     => __( 'Top', 'jvfrmtd' ),
                    'middle'  => __( 'Middle', 'jvfrmtd' ),
                    'bottom'  => __( 'Bottom', 'jvfrmtd' )
                ),
                'default'   => 'middle',
                'condition' => array(
                    'jvfrm_parallax_enabled' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_el_depth',
            array(
                'label'      => __('Parallax Velocity','jvfrmtd' ),
                'type'       => Controls_Manager::SLIDER,
                'default'   => array(
                    'size' => 0.15,
                ),
                'range' => array(
                    'px' => array(
                        'min'  => -1,
                        'max'  => 1,
                        'step' => 0.01
                    )
                ),
                'condition' => array(
                    'jvfrm_parallax_enabled' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_disable_on',
            array(
                'label'   => __( 'Disable Parallax', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    'tablet'  => __( 'On Mobile and Tablet', 'jvfrmtd' ),
                    'phone'   => __( 'On Mobile', 'jvfrmtd' ),
                    'custom'  => __( 'Under a screen size', 'jvfrmtd' )
                ),
                'default'   => 'tablet',
                'condition' => array(
                    'jvfrm_parallax_enabled' => 'yes'
                ),
                'label_block' => true
            )
        );

        $widget->add_control(
            'jvfrm_parallax_disable_under',
            array(
                'label'      => __('Disable under size','jvfrmtd' ),
                'description'=> __('Specifies a screen width under which the parallax will be disabled automatically. (in pixels)','jvfrmtd' ),
                'type'       => Controls_Manager::SLIDER,
                'default'    => array(
                    'size' => 768,
                ),
                'range' => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1400,
                        'step' => 1
                    )
                ),
                'condition' => array(
                    'jvfrm_parallax_enabled'    => 'yes',
                    'jvfrm_parallax_disable_on' => 'custom'
                )
            )
        );

        $widget->end_controls_section();

        $widget->start_controls_section(
            'jvfrm_pro_parallax_anims_section',
            array(
                'label'     => __( 'Parallax Animations', 'jvfrmtd' ),
                'tab'       => Controls_Manager::TAB_ADVANCED
            )
        );

        $widget->add_control(
            'jvfrm_parallax_anims_enable',
            array(
                'label'        => __( 'Enable Parallax Animations', 'jvfrmtd' ),
                'type'         => Controls_Manager::SWITCHER,
                'label_on'     => __( 'On', 'jvfrmtd' ),
                'label_off'    => __( 'Off', 'jvfrmtd' ),
                'return_value' => 'yes',
                'default'      => 'no',
                'separator'    => 'before'
            )
        );

        $widget->add_control(
            'jvfrm_parallax_in_anims',
            array(
                'label'   => __( 'Move in Animations', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''               => 'None',
                    'moveVertical'   => 'Vertical Move',
                    'moveHorizontal' => 'Horizontal Move',
                    'fade'           => 'Fade',
                    'fadeTop'        => 'Fade From Top',
                    'fadeBottom'     => 'Fade From Bottom',
                    'fadeRight'      => 'Fade From Right',
                    'fadeLeft'       => 'Fade From Left',
                    'slideRight'     => 'Slide From Right',
                    'slideLeft'      => 'Slide From Left',
                    'slideTop'       => 'Slide From Top',
                    'slideBottom'    => 'Slide From Bottom',
                    'maskTop'        => 'Mask From Top',
                    'maskBottom'     => 'Mask From Bottom',
                    'maskRight'      => 'Mask From Right',
                    'maskLeft'       => 'Mask From Left',
                    'rotateIn'       => 'Rotate In',
                    'rotateOut'      => 'Rotate Out',
                    'scale'          => 'Scale',
                ),
                'default'            => '',
                'label_block'        => false,
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_out_anims',
            array(
                'label'   => __( 'Move out Animations', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''               => 'None',
                    'moveVertical'   => 'Vertical Move',
                    'moveHorizontal' => 'Horizontal Move',
                    'fade'           => 'Fade',
                    'fadeTop'        => 'Fade From Top',
                    'fadeBottom'     => 'Fade From Bottom',
                    'fadeRight'      => 'Fade From Right',
                    'fadeLeft'       => 'Fade From Left',
                    'slideRight'     => 'Slide From Right',
                    'slideLeft'      => 'Slide From Left',
                    'slideTop'       => 'Slide From Top',
                    'slideBottom'    => 'Slide From Bottom',
                    'maskTop'        => 'Mask From Top',
                    'maskBottom'     => 'Mask From Bottom',
                    'maskRight'      => 'Mask From Right',
                    'maskLeft'       => 'Mask From Left',
                    'rotateIn'       => 'Rotate In',
                    'rotateOut'      => 'Rotate Out',
                    'scale'          => 'Scale',
                ),
                'default'            => '',
                'label_block'        => false,
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_horizontal_transform',
            array(
                'label'           => __( 'Horizontal Transform', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'label_block'        => true,
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1000,
                        'step' => 50
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 200,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_vertical_transform',
            array(
                'label'           => __( 'Vertical Transform', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1000,
                        'step' => 50
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 200,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_rotate_transform',
            array(
                'label'           => __( 'Rotate', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 360,
                        'step' => 10
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 90,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );


        $widget->add_control(
            'jvfrm_parallax_scale_transform',
            array(
                'label'           => __( 'Scale', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 2.5,
                        'step' => 0.1
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 1,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_animation_easing',
            array(
                'label'   => __( 'Easing', 'jvfrmtd' ),
                'type'    => Controls_Manager::SELECT,
                'options' => array(
                    ''                       => 'Default',
                    'initial'                => 'Initial',

                    'linear'                 => 'Linear',
                    'ease-out'               => 'Ease Out',
                    '0.19,1,0.22,1'          => 'Ease In Out',

                    '0.47,0,0.745,0.715'     => 'Sine In',
                    '0.39,0.575,0.565,1'     => 'Sine Out',
                    '0.445,0.05,0.55,0.95'   => 'Sine In Out',

                    '0.55,0.085,0.68,0.53'   => 'Quad In',
                    '0.25,0.46,0.45,0.94'    => 'Quad Out',
                    '0.455,0.03,0.515,0.955' => 'Quad In Out',

                    '0.55,0.055,0.675,0.19'  => 'Cubic In',
                    '0.215,0.61,0.355,1'     => 'Cubic Out',
                    '0.645,0.045,0.355,1'    => 'Cubic In Out',

                    '0.895,0.03,0.685,0.22'  => 'Quart In',
                    '0.165,0.84,0.44,1'      => 'Quart Out',
                    '0.77,0,0.175,1'         => 'Quart In Out',

                    '0.895,0.03,0.685,0.22'  => 'Quint In',
                    '0.895,0.03,0.685,0.22'  => 'Quint Out',
                    '0.895,0.03,0.685,0.22'  => 'Quint In Out',

                    '0.95,0.05,0.795,0.035'  => 'Expo In',
                    '0.19,1,0.22,1'          => 'Expo Out',
                    '1,0,0,1'                => 'Expo In Out',

                    '0.6,0.04,0.98,0.335'    => 'Circ In',
                    '0.075,0.82,0.165,1'     => 'Circ Out',
                    '0.785,0.135,0.15,0.86'  => 'Circ In Out',

                    '0.6,-0.28,0.735,0.045'  => 'Back In',
                    '0.175,0.885,0.32,1.275' => 'Back Out',
                    '0.68,-0.55,0.265,1.55'  => 'Back In Out'
                ),
                'selectors' => array(
                    '{{WRAPPER}} > .elementor-widget-container' => 'transition-timing-function:cubic-bezier({{VALUE}});'
                ),
                'default'      => '',
                'return_value' => '',
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_animation_duration',
            array(
                'label'     => __( 'Duration', 'jvfrmtd' ) . ' (ms)',
                'type'      => Controls_Manager::NUMBER,
                'default'   => 1000,
                'min'       => 0,
                'step'      => 100,
                'selectors'    => array(
                    '{{WRAPPER}} > .elementor-widget-container' => 'transition-duration:{{SIZE}}ms; transition-property: all'
                ),
                'render_type' => 'template',
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_animation_delay',
            array(
                'label'     => __( 'Delay', 'jvfrmtd' ) . ' (ms)',
                'type'      => Controls_Manager::NUMBER,
                'default'   => '',
                'min'       => 0,
                'step'      => 100,
                'selectors' => array(
                    '{{WRAPPER}} > .elementor-widget-container' => 'transition-delay:{{SIZE}}ms;'
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_viewport_top_origin',
            array(
                'label'           => __( 'Viewport Top Origin', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1,
                        'step' => 0.1
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 0.5,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_viewport_bottom_origin',
            array(
                'label'           => __( 'Viewport Bottom Origin', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1,
                        'step' => 0.1
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 0.5,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->add_control(
            'jvfrm_parallax_element_origin',
            array(
                'label'           => __( 'Element Top Origin', 'jvfrmtd' ),
                'type'            => Controls_Manager::SLIDER,
                'size_units'      => array('px'),
                'range'      => array(
                    'px' => array(
                        'min'  => 0,
                        'max'  => 1,
                        'step' => 0.1
                    ),
                ),
                'default' => array(
					'unit' => 'px',
					'size' => 0.2,
                ),
                'condition' => array(
                    'jvfrm_parallax_anims_enable' => 'yes'
                )
            )
        );

        $widget->end_controls_section();
    }

    public function render_attributes( $widget ){
        $settings = $widget->get_settings();

        // Add parallax attributes
        if( $this->setting_value( $settings, 'jvfrm_parallax_enabled', 'yes' ) ){
            $widget->add_render_attribute( '_wrapper', 'class', 'jvfrm-parallax-piece' );

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_el_origin' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-parallax-origin', $value );
            }
            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_el_depth' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-parallax-depth', $value['size'] );
            }
            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_disable_on' ) ){
                $breakpoint = 1024;

                if( 'tablet' == $value ){
                    $breakpoint = 1024;
                } elseif( 'phone' == $value ){
                    $breakpoint = 768;
                } elseif( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_disable_under' ) ){
                    $breakpoint = $value['size'];
                }
                $widget->add_render_attribute( '_wrapper', 'data-parallax-off', $breakpoint );
            }
        }

        if( $this->setting_value( $settings, 'jvfrm_parallax_anims_enable', 'yes' ) ){
            $widget->add_render_attribute( '_wrapper', 'class', 'jvfrm-scroll-anim' );

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_in_anims' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-move-in', $value );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_out_anims' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-move-out', $value );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_horizontal_transform' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-axis-x', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_vertical_transform' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-axis-y', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_rotate_transform' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-rotate', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_scale_transform' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-scale', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_viewport_top_origin' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-vp-top', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_viewport_bottom_origin' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-vp-bot', $value['size'] );
            }

            if( null !== $value = $this->setting_value( $settings, 'jvfrm_parallax_element_origin' ) ){
                $widget->add_render_attribute( '_wrapper', 'data-el-top', $value['size'] );
            }
        }
    }

    private function setting_value( $settings, $key, $value = null ){
        if( ! isset( $settings[ $key ] ) ){
            return;
        }
        // Retrieves the setting value
        if( is_null( $value ) ){
            return $settings[ $key ];
        }
        // Validates the setting value
        return ! empty( $settings[ $key ] ) && $value == $settings[ $key ];
    }

    public static function instance() {
        if(null == self::$instance) {
            self::$instance = new self;
            self::$instance->register();
        }
        return self::$instance;
    }
}