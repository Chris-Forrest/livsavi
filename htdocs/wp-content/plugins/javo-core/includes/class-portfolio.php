<?php
if ( !class_exists('jvbpd_portpolio') ) :

    class jvbpd_portpolio {

		public static $instance = null;

		const SLUG = 'portfolio';
		const SLUGX = 'portfolio_%s';

        /**
        * @var  string  $prefix  The prefix for storing custom fields in the postmeta table
        */
        var $prefix = '_lynk_';
        /**
        * @var  array  $postTypes  An array of public custom post types, plus the standard "post" and "page" - add the custom types you want to include here
        */
        var $postTypes = array( 'portfolio' );
        /**
        * @var  array  $customFields  Defines the custom fields available
        */
        var $customFields = array(
            array(
                "name"          => "short-description",
                "title"         => "Short Description",
                "description"   => "",
                "type"          => "textarea",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_pages"
            ),
            array(
                "name"          => "creation-date",
                "title"         => "Date",
                "description"   => "Created date. ex) 01/01/2016 or 2016-01-01",
                "type"          =>   "text",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_posts"
            ),
            array(
                "name"          => "link-to",
                "title"         => "Link",
                "description"   => "Website link without 'http://' (ex: www.google.com)",
                "type"          =>   "text",
                "scope"         =>   array( "portfolio" ),
                "capability"    => "edit_posts"
            ),
            array(
                "name"          => "featured_portfolio",
                "title"         => "Featured Portfolio",
                "description"   => "Check if this portfolio is a featured one",
                "type"          => "checkbox",
                "scope"         =>   array( "portfolio" ),
                //"scope"         =>   array( "portfolio", "page" ),
                "capability"    => "manage_options"
            ),

			array(
                "name"          => "portfolio_detail_page_head_style",
                "title"         => "Head style",
                "description"   => "Select Layout Style",
                "type"          => "dropdown",
				'options' => Array(
					'Default ( Theme Setting )'		=> '',
					'Featured image with Title'		=> 'featured_image',
					'Title on the top'						=> 'title_on_top',
					'Title upper content'						=> 'title_upper_content'
				),
				"scope"         =>   array( "portfolio" ),
                "capability"    => "manage_options"
            ),

			array(
                "name"          => "portfolio_detail_page_layout",
                "title"         => "Layout Type",
                "description"   => "Select Layout Style",
                "type"          => "dropdown",
				'options' => Array(
					'Default ( Theme Setting )'		=> '',
					'Fullwidth Content After'		=> 'fullwidth-content-after',
					'Fullwidth Content Before'		=> 'fullwidth-content-before',
					'Right Sider'						=> 'quick-view',
				),
				"scope"         =>   array( "portfolio" ),
                "capability"    => "manage_options"
            ),
        );
        /**
        * PHP 4 Compatible Constructor
        */
        //function jvbpd_portpolio() { $this->__construct(); }
        /**
        * PHP 5 Constructor
        */
		public function __construct() {}

		public function register_hooks() {

			add_action( 'init', Array( $this, 'registerPortfolio' ) );
            add_action( 'init', Array( $this, 'registerPortfolioTaxonomy' ) );

            add_action('admin_enqueue_scripts', Array($this, 'enqueue'));
            add_action('admin_footer', Array($this, 'renderDetailImagesTemplate'));

            add_action('add_meta_boxes', Array($this, 'createMetaBox'));
            add_action( 'save_post', array( &$this, 'saveCustomFields' ), 1, 2 );
            add_action('save_post', array($this, 'savePortfolioMeta'), 10, 2);

			// Comment this line out if you want to keep default custom fields meta box
            add_action( 'do_meta_boxes', array( &$this, 'removeDefaultCustomFields' ), 10, 3 );

            add_action('wp_footer', Array(__CLASS__, 'render_ball'));

		}

		public function custom_pageSettings(){
			add_filter( 'jvbpd_admin_page_options', Array( $this, 'page_setting_add_portfolio' ) );

		}

		public function registerPortfolio() {
			$labels = array(
				'name' => 'Portfolio',
				'singular_name' => 'Portfolio',
				'add_new' => 'Add New Portfolio',
				'add_new_item' => 'Add New Portfolio',
				'edit_item' => 'Edit Portfolio',
				'new_item' => 'New Portfolio',
				'all_items' => 'All Portfolio',
				'view_item' => 'View Portfolio',
				'search_items' => 'Search Portfolio',
				'not_found' =>  'No Portfolio Found',
				'not_found_in_trash' => 'No Portfolio found in Trash',
				'parent_item_colon' => '',
				'menu_name' => 'Portfolio',
			);
			register_post_type(
				self::SLUG,
				array(
					'labels' => $labels,
					'has_archive' => true,
					'public' => true,
					'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
					// 'taxonomies' => array( 'portfolio_category', 'portfolio_tag' ),
					'exclude_from_search' => false,
					'capability_type' => 'post',
					'rewrite' => array( 'slug' => 'portfolios' ),
				)
			);
		}

		public function registerPortfolioTaxonomy() {
			register_taxonomy(
				sprintf( self::SLUGX, 'category' ),
				self::SLUG,
				array(
					'labels' => array(
					'name' => 'Portofolio Category',
					'add_new_item' => 'Add New Category',
					'new_item_name' => "New Category"
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
				)
			);

			register_taxonomy(
				sprintf( self::SLUGX, 'tag' ),
				self::SLUG,
				array(
					'labels' => array(
						'name' => 'Portofolio Tag',
						'add_new_item' => 'Add New Tag',
						'new_item_name' => "New Tag"
					),
					'show_ui' => true,
					'show_tagcloud' => false,
					'hierarchical' => true
				)
			);
        }

        public function enqueue() {
            if('portfolio' != get_current_screen()->id) {
                return;
            }
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.2.0/css/all.css' );
        }

        public function renderDetailImagesTemplate() {
            ?>
            <script type="text/html" id="portfolio-detail-image-template">
                <div class="upload-item{featured}">
                    <input type="text" name="{name}[name][]" value="{_name}" placeholder="<?php esc_attr_e( "Type a name", 'jvfrmtd' );?>" class="large-text" />
                    <input type="text" name="{name}[title][]" value="{_title}" placeholder="<?php esc_attr_e( "Type a title", 'jvfrmtd' );?>" class="large-text" />
                    {img}
                    <input type="hidden" name="{name}[image_id][]" value="{val}" />

                    <button type="button" class="button item-remove">{remove_label}</button>
                </div>
            </script>
            <?php
        }

		public function page_setting_add_portfolio( $settings=Array() ){
			$settings[ 'page-option-portfolio' ] = Array(
				'label'						=> esc_html__( "Portfolio Setup", 'jvfrmtd' ),
				'icon'						=> 'fas fa-camera',
				'post_type'				=> Array( self::SLUG ),
			);

			if( isset( $settings[ 'page-option-navi' ] )  && isset( $settings[ 'page-option-navi' ][ 'post_type' ] ) )
				$settings[ 'page-option-navi' ][ 'post_type' ][] = self::SLUG;
			return $settings;
		}

        /**
        * Remove the default Custom Fields meta box
        */
        function removeDefaultCustomFields( $type, $context, $post ) {
            foreach ( array( 'normal', 'advanced', 'side' ) as $context ) {
                foreach ( $this->postTypes as $postType ) {
                    remove_meta_box( 'postcustom', $postType, $context );
                }
            }
        }
        /**
        * Create the new Custom Fields meta box
        */
        function createCustomFields() {
            if ( function_exists( 'add_meta_box' ) ) {
                foreach ( $this->postTypes as $postType ) {
                    add_meta_box( 'jvbpd-post-custom-fields', 'Additional information', array( &$this, 'displayCustomFields' ), $postType, 'normal', 'high' );
                }
            }
        }

        /**
        * Display the new Custom Fields meta box
        */
        function displayCustomFields() {
            global $post;
            ?>
            <div class="form-wrap">
                <?php
                wp_nonce_field( 'jvbpd-post-custom-fields', 'jvbpd-post-custom-fields_wpnonce', false, true );
                foreach ( $this->customFields as $customField ) {
                    // Check scope
                    $scope = $customField[ 'scope' ];
                    $output = false;
                    foreach ( $scope as $scopeItem ) {
                        switch ( $scopeItem ) {
                            default: {
                                if ( $post->post_type == $scopeItem )
                                    $output = true;
                                break;
                            }
                        }
                        if ( $output ) break;
                    }
                    // Check capability
                    if ( !current_user_can( $customField['capability'], $post->ID ) )
                        $output = false;
                    // Output if allowed
                    if ( $output ) { ?>
                        <div class="form-field form-required">
                            <?php
                            switch ( $customField[ 'type' ] ) {
                                case "checkbox": {
                                    // Checkbox
                                    echo '<div class="lynk_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>';
									if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>';
									echo '</div>';
                                    echo '<div class="lynk_ctm_field"><input type="checkbox" name="' . $this->prefix . $customField['name'] . '" id="' . $this->prefix . $customField['name'] . '" value="yes"';
                                    if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == "yes" )
                                        echo ' checked="checked"';
                                    echo '" style="width: auto;" />';
									echo '</div>';
                                    break;
                                }
								case "dropdown": {
                                    // Dropdown
                                    echo '<div class="lynk_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>';
									if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>';
									echo '</div>';
									echo '<div class="lynk_ctm_field">';
									echo '<select name="' . $this->prefix . $customField[ 'name' ] .'">';
									foreach( $customField['options'] as $label => $value )
									{
										printf( "<option value='{$value}' %s>{$label}</option>", selected( $value == get_post_meta( $post->ID, $this->prefix . $customField['name'], true), true, false ) );
									}
									echo '</select>';
									echo '</div>';
                                    break;
                                }
                                case "textarea":
                                case "wysiwyg": {
                                    // Text area
                                    echo '<div class="lynk_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label></div>';
                                    echo '<div class="lynk_ctm_field"><textarea name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" columns="30" rows="3">' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '</textarea>';
                                    // WYSIWYG
                                    if ( $customField[ 'type' ] == "wysiwyg" ) { ?>
                                        <script type="text/javascript">
                                            jQuery( document ).ready( function() {
                                                jQuery( "<?php echo esc_attr( $this->prefix . $customField[ 'name' ] ); ?>" ).addClass( "mceEditor" );
                                                if ( typeof( tinyMCE ) == "object" && typeof( tinyMCE.execCommand ) == "function" ) {
                                                    tinyMCE.execCommand( "mceAddControl", false, "<?php echo esc_attr( $this->prefix . $customField[ 'name' ] ); ?>" );
                                                }
                                            });
                                        </script>
                                    <?php }
									echo '</div>';
                                    break;
                                }
                                default: {
                                    // Plain text field
                                    echo '<div class="lynk_ctm_label"><label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label></div>';
                                    echo '<div class="lynk_ctm_field"><input type="text" name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" value="' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '" />';
									echo '</div>';
                                    break;
                                }
                            }
                            ?>
                            <?php if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>'; ?>
                        </div>
                    <?php
                    }
                } ?>
            </div>
            <?php
        }

        public function createMetaBox() {
            add_meta_box(
                'moa-pf-portfolio-images',
                esc_html__("Gallery Images"),
                Array($this, 'renderGalleries'),
                self::SLUG
            );
        }

        public function renderGalleries($portfolio=null) {
            $detailImages_is_required = false;
            $arrDetailImages = get_post_meta( $portfolio->ID, 'detail_images', true );
            $arrDetailImageOutput = Array();
            if( is_array( $arrDetailImages ) ) {
                /*
                foreach( $arrDetailImages as $intImage ) {
                    $arrDetailImageOutput[] = Array(
                        'val' => $intImage,
                        'img' => wp_get_attachment_image( $intImage ),
                        'f' => $intImage == get_post_thumbnail_id() ? ' featured' : false,
                    );
                } */
                foreach($arrDetailImages as $imageIDX => $imageMeta) {
                    if(array_key_exists('image_id', $imageMeta)) {
                        $arrDetailImageOutput[] = Array(
                            'val' => $imageMeta['image_id'],
                            'img' => wp_get_attachment_image( $imageMeta['image_id'] ),
                            'f' => $imageMeta['image_id'] == get_post_thumbnail_id() ? ' featured' : false,
                            '_title' => $imageMeta['title'],
                            '_name' => $imageMeta['name'],
                        );
                    }
                }
            } ?>
            <input type="hidden" name="portfolio_featured_image" value="<?php echo get_post_thumbnail_id(); ?>">
            <div class="javo-portfolio-wp-media" data-field="portfolioImages" data-multiple="true" data-value="<?php echo htmlspecialchars( json_encode( $arrDetailImageOutput ) ); ?>" data-modal-title="<?php esc_html_e( "Detail Images", 'Lavacode' ); ?>" data-button-select="<?php esc_html_e( "Select", 'Lavacode' ); ?>" data-button-remove="<?php esc_html_e( "Remove", 'Lavacode' ); ?>">
                <input type="hidden" name="portfolioImages[]">
                <div class="upload-item-group"></div>
                <div class="upload-action">
                    <button type="button" class="action-add-item button">
                        <i class="fa fa-plus"></i>
                        <?php esc_html_e( "Select", 'Lavacode' ); ?>
                    </button>
                </div>
            </div>
            <?php
        }

        /**
        * Save the new Custom Fields values
        */
        function saveCustomFields( $post_id, $post ) {
            if ( !isset( $_POST[ 'jvbpd-post-custom-fields_wpnonce' ] ) || !wp_verify_nonce( $_POST[ 'jvbpd-post-custom-fields_wpnonce' ], 'jvbpd-post-custom-fields' ) )
                return;
            if ( !current_user_can( 'edit_post', $post_id ) )
                return;
            if ( ! in_array( $post->post_type, $this->postTypes ) )
                return;

            foreach ( $this->customFields as $customField ) {
                if ( current_user_can( $customField['capability'], $post_id ) ) {
                    if ( isset( $_POST[ $this->prefix . $customField['name'] ] ) && trim( $_POST[ $this->prefix . $customField['name'] ] ) ) {
                        $value = $_POST[ $this->prefix . $customField['name'] ];
                        // Auto-paragraphs for any WYSIWYG
                        if ( $customField['type'] == "wysiwyg" ) $value = wpautop( $value );
                        update_post_meta( $post_id, $this->prefix . $customField[ 'name' ], $value );
                    } else {
                        delete_post_meta( $post_id, $this->prefix . $customField[ 'name' ] );
                    }
                }
            }
        }

        public function savePortfolioMeta($portfolio_id=null, $portfolio=null) {
            if(!is_admin() || !in_array( $portfolio->post_type, $this->postTypes )) {
                return;
            }
            $portFolioFeaturedImage = isset($_POST['portfolio_featured_image']) ? $_POST['portfolio_featured_image'] : false;
            $defailImages = Array();
            $portFolioImages = isset($_POST['portfolioImages']) ? $_POST['portfolioImages'] : false;
            if(false !== $portFolioImages) {
                foreach($portFolioImages as $metaKey => $metaVars ) {
                    foreach($metaVars as $varIDX => $varValue) {
                        $defailImages[$varIDX][$metaKey] = $varValue;
                    }
                }
                update_post_meta($portfolio_id, 'detail_images', $defailImages);
            }
            if(false !== $portFolioFeaturedImage) {
                set_post_thumbnail($portfolio_id, $portFolioFeaturedImage);
            }
        }

		public static function getInstance() {
			if( is_null( self::$instance ) ) {
                self::$instance = new self;
                self::$instance->register_hooks();
                self::$instance->custom_pageSettings();
            }
			return self::$instance;
        }

        public static function render_ball() {
            $queried = get_queried_object();
            if($queried instanceof \WP_Post && $queried->post_type == self::SLUG) {
                $output = '<div id="ball" class="hidden">';
                $output .= '<div id="hold-event" data-hover="Hold"></div>';
                $output .= '<div id="ball-loader"></div>';
                $output .= '</div>';
                printf($output);
            }
        }

    } // End Class

endif; // End if class exists statement

// Instantiate the class
if( !function_exists( 'jvbpd_folio' ) ) : function jvbpd_folio() {
	return jvbpd_portpolio::getInstance();
} jvbpd_folio(); endif;