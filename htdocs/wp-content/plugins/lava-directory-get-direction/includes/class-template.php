<?php
class Lava_Directory_GetDirection_Template
{
	public $post_type;

	public $arrElementClass = Array(
		'travel'	=> Array(),
		'address'	=> Array(),
		'button'	=> Array(),
	);

	public function __construct(){
		$this->post_type	= lava_directory_direction()->post_type;
		$this->hooks();
	}

	public function hooks(){
		add_action( 'wp_enqueue_scripts', Array( $this, 'load_script' ), 20 );
		add_action( 'lava_' . $this->post_type . '_single_map_after', Array( $this, 'singleTemplate' ) );
	}

	public function load_file( $strFileName=false, $args=Array() ){
		if( is_Array( $args ) )
			extract( $args, EXTR_SKIP );

		if( file_exists( $strFileName ) ){
			require( $strFileName );
			return true;
		}
		return false;
	}

	public function add_class( $element='', $className='' ) {
		if( empty( $element ) || empty( $className ) )
			return;

		if( isset( $this->arrElementClass[ $element ] ) ){
			$this->arrElementClass[ $element ][] = esc_attr( $className );
		}
	}

	public function get_classes( $element ){
		if( isset( $this->arrElementClass[ $element ] ) ){
			return join( ' ', $this->arrElementClass[ $element ] );
		}
		return false;
	}

	public function getParams(){
		return Array(
			'lava_directory_direction_travel'	=> $this->getTravels(),
		);
	}

	public function getTravels(){
		return Array(
			'driving'	=> __( "Driving", 'lvdr-get-direction' ),
			'bicycling'	=> __( "Bicycling", 'lvdr-get-direction' ),
			'transit'	=> __( "Transit", 'lvdr-get-direction' ),
			'walking'	=> __( "Walking", 'lvdr-get-direction' ),
		);
	}

	public function load_template( $template_name=false, $args=Array() ) {
		$strFileName = lava_directory_direction()->template_path;
		$strFileName .= '/template-' . $template_name;
		return $this->load_file( $strFileName, $args );
	}

	public function load_script(){
		wp_localize_script(
			lava_directory_direction()->core->getHandleName( 'core-script' ),
			'lava_directory_direction_args',
			Array(
				'wrap'				=> '.lava-directory-get-direction-wrap',
				'notfoundAdress'	=> __( "Not Found Address", 'lvdr-get-direction' ),
				'notfoundDirection'	=> __( "The direction is too far or not provided by Google API", 'lvdr-get-direction' ),
			)
		);
		wp_enqueue_script( lava_directory_direction()->core->getHandleName( 'core-script' ) );
	}

	public function singleTemplate(){
		printf( "<h2 class=\"item-subtitle\">%s</h2>", __( "Get Direction", 'lvdr-get-direction' ) );
		$this->load_template( 'single-get-direction.php', $this->getParams() );
	}
}