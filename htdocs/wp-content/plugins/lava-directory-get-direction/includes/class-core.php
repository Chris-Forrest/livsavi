<?php
class Lava_Directory_GetDirection_CORE extends Lava_Directory_GetDirection
{
	public $handle_prefix = '';


	public function __construct() {

		$this->handle_prefix = $this->getScriptHandlePrefix();
		add_action( 'wp_enqueue_scripts', Array( $this, 'register_scripts' ) );
		add_action( 'lava_' . $this->post_type .'_more_meta', Array( $this, 'append_video_field' ) );
		add_action( 'lava_' . $this->post_type .'_single_description_before', Array( $this, 'append_video' ) );

	}

	public function getScriptHandlePrefix(){
		return sanitize_title( lava_directory_direction()->manager->enqueue->handle_prefix );
	}

	public function getHandleName( $filename ){
		return sanitize_title( $this->handle_prefix . '-' . $filename );
	}

	public function register_scripts(){
		wp_register_script(
			$this->getHandleName( 'core-script' ),
			lava_directory_direction()->assets_url . '/js/lava-directory-get-direction.js',
			Array( $this->getHandleName( 'lava-single.js' )  ),
			'',
			true
		);
		wp_enqueue_style(
			$this->getHandleName( 'core-style' ),
			lava_directory_direction()->assets_url . '/css/lava-directory-get-direction.css'
		);
	}
}