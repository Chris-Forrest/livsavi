<div class="lava-directory-get-direction-wrap">
	<div class="fields">
		<div class="select-travel">
			<label><?php _e( "Travels", 'lvdr-get-direction' ); ?></label>
			<select class="<?php echo lava_directory_direction()->template->get_classes( 'travel' );?>" data-travel>
				<option value=''><?php _e( "Select One", 'lvdr-get-direction' );?></option>
				<?php
				if( !empty( $lava_directory_direction_travel ) ) {
					foreach( $lava_directory_direction_travel as $travelID => $travelLabel ){
						echo "<option value=\"{$travelID}\">{$travelLabel}</option>";
					}
				} ?>
			</select>
		</div>
		<div class="input-address">
			<label><?php _e( "Location", 'lvdr-get-direction' ); ?></label>
			<input type="text" class="<?php echo lava_directory_direction()->template->get_classes( 'address' );?>" data-address>
		</div>
		<div class="button-find">
			<button type="button" class="<?php echo lava_directory_direction()->template->get_classes( 'button' );?>" data-find>
				<?php _e( "Get Direction", 'lvdr-get-direction' ); ?>
			</button>
		</div>
	</div>
	<div class="radio-result-to">
		<label>
			<input type="radio" name="result_to" value="" <?php checked( true ); ?>>
			<?php esc_html_e( "Current Map", 'lvdr-get-direction' ); ?>
		</label>
		<label>
			<input type="radio" name="result_to" value="google">
			<?php esc_html_e( "Google map", 'lvdr-get-direction' ); ?>
		</label>
	</div>
	<div class="" data-detail-output></div>
</div>