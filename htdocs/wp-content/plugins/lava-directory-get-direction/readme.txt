=== Lava Directory Manager ===
Contributors: lavacode
Requires at least: 3.2
Tested up to: 4.2.2
Stable tag: 0.1
Tags:  addresses, address book, addressbook, bio, bios, biographies, business, businesses, business directory, business-directory, business directory plugin, directory widget, chamber of commerce, church, contact, contacts, connect, connections, directory, directories, hcalendar, hcard, ical, icalendar, image, images, list, lists, listings, member directory, members directory, members directories, microformat, microformats, page, pages, people, profile, profiles, post, posts, plugin, shortcode, staff, user, users, vcard, wordpress business directory, wordpress directory, wordpress directory plugin, wordpress business directory, wordpress local directory plugin
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage Directory listings. Provided front-end form, google map support, listing, filtering.

== Description ==

Lava directory manager plugin is for preperty

This plugin enables you to use the power and flexibility of WordPress to generate optimized pages for your business listings.

Intro page : http://lava-code.com/directory/

Documentation : http://lava-code.com/directory/documentation/

Demo site : http://lava-code.com/directory-demo/


== Installation ==

Install via Search:

1. In WordPress admin, visit Plugins > Add New
2. Search for "lava directory manager"
3. Click the "Install Now" link.
4. Once installed, click the "Activate Plugin" link.
5. Visit settings to setup pages

Install via Upload:

1. In WordPress admin, visit "Plugins" > "Add New" > "Upload"
2. Upload lava-directory-manager.zip file
3. Once uploaded, click the "Activate Plugin" link
4. Visit settings to setup pages


That's it!
If you need more documentation, please visit here ( http://lava-code.com/directory/documentation/ )


== Frequently Asked Questions ==

= Can I use my existing WordPress theme? =

Yes! Lava directory manager works out-of-the-box with nearly every WordPress theme.

= Will this work on WordPress multisite? =

Yes! If your WordPress installation has multisite enabled, Lava directory manager will work on it.

= Where can I get support? =

We will open our own support site soon. or you can create tickets on wordpress plugin support page.

= Where can I find documentation? =

Our codex can be found at <a href="http://lava-code.com/directory/documentation/">http://lava-code.com/directory/documentation/</a>.

= Where can I report a bug? =

Report bugs, suggest ideas, and participate in development at wp.lava.code@gmail.com .

== Screenshots ==
1. ** Setting page ** - Lava directory manager plugin setting page.
2. ** Back-end form ** - You can manager (add/edit/delete) property information.
3. ** Front-end form ** - Your user can add properties on front-end


== Languages ==

It is currently (v0.1) available only English version. we are happy to have volunteer to assist to translate in your own languages.

https://www.transifex.com/projects/p/lava-directory-manager/



Please email us at wp.lava.code@gmail.com , if you are interested.
Thank you in advance.


== Upgrade Notice ==

= 0.1.0 =
*Release Date - 30th Jun, 2015*

== Changelog ==

= 0.1.0 =
*Release Date - 30th Jun, 2015*

* Initial release
