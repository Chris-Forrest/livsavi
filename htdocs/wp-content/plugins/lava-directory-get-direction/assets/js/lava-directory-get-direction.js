( function($){

	var lava_directory_direction = function(){
		this.args = lava_directory_direction_args;
		this.el = $( this.args.wrap );
		$( document ).on( 'lava:single-msp-setup-after', this.init() )
	}

	lava_directory_direction.prototype = {

		constructor : lava_directory_direction,
		init : function() {
			var obj = this;
			return function( e, objSingle ){
				obj.bind();
				$( "[data-find]", obj.el ).on( 'click', function(e){ obj.trigger( objSingle ); });
				$( "[data-address]", obj.el ).on( 'keypress', function(e){
					if( e.keyCode == 13 ) {
						obj.trigger( objSingle );
					}
				});
			}
		},

		trigger : function( obj ){
			var
				$this = this,
				strAddress = $( '[data-address]', this.el ).val().toString(),
				result_to = $( 'input[name="result_to"]:checked', this.el );

			if( result_to.val() == 'google' ) {
				this.submit_google( obj.latLng );
				return false;
			}

			$( obj.el ).gmap3({
				getlatlng:{
					address: strAddress,
					callback : function( response ){
						$this.find( obj, response );
					}
				}
			});
		},

		bind : function(){
			var objAddress = $( '[data-address]', this.el );
			if( objAddress.length )
				new google.maps.places.Autocomplete( objAddress.get( 0 ) );
			return false;
		},

		find : function( obj, result ) {
			var
				codeTravel,
				$this = this,
				strTravel = $( '[data-travel]', this.el ).val(),
				elOutput = $( '[data-detail-output]', this.el ),
				item_position	= obj.latLng;

			codeTravel = this.getTravelCode( strTravel );

			if( !result ) {
				alert( this.args.notfoundAdress );
				return false;
			}

			$( obj.el ).gmap3({
				getroute : {
					options : {
						origin : result[0].geometry.location,
						destination : item_position,
						travelMode : codeTravel
					},
					callback : function( results ){
						if (!results){
							alert( $this.args.notfoundDirection );
							return;
						};

						elOutput.empty();

						$( this ).gmap3({
							clear:{ name:[ 'marker', 'directionsrenderer' ] },
							directionsrenderer:{
								container: elOutput,
								options:{ directions : results }
							}
						});
					}
				}
			});
		},

		getTravelCode : function( codeName ){
			var strReturn;
			switch( codeName.toString() ){
				case 'bicycling'	: return google.maps.DirectionsTravelMode.BICYCLING; break;
				case 'transit'		: return google.maps.DirectionsTravelMode.TRANSIT; break;
				case 'walking'		: return google.maps.DirectionsTravelMode.WALKING; break;
				case 'driving'		:
				default				: return google.maps.DirectionsTravelMode.DRIVING;
			}
			return false;
		},

		submit_google : function( latLng ) {
			var
				strURL = 'https://www.google.com/maps/',
				travelMode = $( '[data-travel]', this.el ).val(),
				param = $.param({
					daddr : [ latLng.lat(), latLng.lng() ].join( ',' ),
					saddr : $( '[data-address]', this.el ).val().toString(),
					directionsmode : this.getTravelCode( travelMode )
				});
			window.open( [ strURL, param].join( '?' ) );
		}
	}

	new lava_directory_direction;

})(jQuery, window);