<?php
/**
 * Plugin Name: Lava Directory Get Direction
 * Plugin URI : http://lava-code.com/real-estate/
 * Description: Lava Directory Single Item Map GetDirection Addon.
 * Version: 1.0.2
 * Author: lavacode
 * Author URI: http://lava-code.com/
 * Text Domain: lvdr-get-direction
 * Domain Path: /languages/
 */
/*
    Copyright Automattic and many other contributors.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

if( ! defined( 'ABSPATH' ) )
	die();

if( ! class_exists( 'Lava_Directory_GetDirection' ) ) :

	class Lava_Directory_GetDirection
	{
		private $version = '1.0.2';

		public static $instance;

		public $manager;

		public $post_type;

		public $path = '';

		public function __construct( $file )
		{
			$this->file				= $file;
			$this->folder			= basename( dirname( $this->file ) );
			$this->path				= dirname( $this->file );
			$this->template_path	= trailingslashit( $this->path ) . 'templates';
			$this->includes_path	= trailingslashit( $this->path ) . 'includes';
			$this->assets_url		= esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
			$this->image_url		= esc_url( trailingslashit( $this->assets_url . 'images/' ) );

			$this->load_files();
			$this->register_hooks();
		}

		public function getVersion() { return $this->version; }

		public function getName(){ return get_class( $this ); }

		public function getPluginDir() {
			return trailingslashit( dirname( dirname( __FILE__ ) ) );
		}

		public function load_files()
		{
			$arrFiles	= Array(
				'class-core.php',
				'class-template.php',
			);

			foreach( $arrFiles as $filename ) {
				if( file_exists( $this->includes_path . '/' . $filename ) )
					require_once $this->includes_path . '/' . $filename;
			}
		}

		public function register_hooks() {
			add_action( 'lava_directory_manager_init', Array( $this, 'initialize' ) );
			load_plugin_textdomain('lvdr-get-direction', false, $this->folder . '/languages/');
		}

		public function initialize() {
			$this->manager = lava_directory();
			$this->post_type = $this->manager->core->slug;
			$this->core = new Lava_Directory_GetDirection_CORE;
			$this->template = new Lava_Directory_GetDirection_Template;
			do_action( 'lava_directory_get_direction_init' );
		}

		public static function get_instance( $file )
		{
			if( null === self::$instance )
				self::$instance = new self( $file );
			return self::$instance;
		}
	}
endif;

if( ! function_exists( 'Lava_Directory_GetDirection' ) ){
	function lava_directory_direction() {
		return Lava_Directory_GetDirection::get_instance( __FILE__ );
	}
	lava_directory_direction();
}