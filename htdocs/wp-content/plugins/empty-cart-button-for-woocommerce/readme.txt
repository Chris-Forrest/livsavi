=== Empty Cart Button for WooCommerce ===
Contributors: algoritmika,anbinder
Tags: woocommerce,empty cart button,empty cart
Requires at least: 4.4
Tested up to: 4.7
Stable tag: 1.0.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

"Empty cart" button for WooCommerce.

== Description ==

Plugin lets you add (and customize) "Empty cart" button to WooCommerce cart and checkout pages.

= Feedback =
* We are open to your suggestions and feedback. Thank you for using or trying out one of our plugins!

== Installation ==

1. Upload the entire 'empty-cart-button-for-woocommerce' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Start by visiting plugin settings at WooCommerce > Settings > Empty Cart Button.

== Changelog ==

= 1.0.0 - 22/01/2017 =
* Initial Release.

== Upgrade Notice ==

= 1.0.0 =
This is the first release of the plugin.
