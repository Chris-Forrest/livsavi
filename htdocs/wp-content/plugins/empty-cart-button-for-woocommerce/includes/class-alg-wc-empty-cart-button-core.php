<?php
/**
 * Empty Cart Button for WooCommerce - Core Class
 *
 * @version 1.0.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Empty_Cart_Button_Core' ) ) :

class Alg_WC_Empty_Cart_Button_Core {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		if ( 'yes' === get_option( 'alg_wc_empty_cart_button_enabled', 'yes' ) ) {
			add_action( 'init', array( $this, 'empty_cart' ) );
			add_action( apply_filters( 'alg_wc_empty_cart_button', 'woocommerce_after_cart', 'value_position_cart' ), array( $this, 'add_empty_cart_link' ) );
			if ( 'disable' != ( $empty_cart_checkout_position = get_option( 'alg_wc_empty_cart_checkout_position', 'disable' ) ) ) {
				add_action( $empty_cart_checkout_position, array( $this, 'add_empty_cart_link' ) );
			}
		}
	}

	/**
	 * add_empty_cart_link.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function add_empty_cart_link() {
		$confirmation_html = ( 'confirm_with_pop_up_box' == get_option( 'alg_wc_empty_cart_confirmation', 'no_confirmation' ) ) ?
			' onclick="return confirm(\'' . get_option( 'alg_wc_empty_cart_confirmation_text', __( 'Are you sure?', 'empty-cart-button-for-woocommerce' ) ) . '\')"' : '';
		$button_form_html = '<form action="" method="post"><input type="submit" style="' . get_option( 'alg_wc_empty_cart_button_style', '' ). '" class="' .
			get_option( 'alg_wc_empty_cart_button_class', 'button' ). '" name="alg_wc_empty_cart" value="' .
			apply_filters( 'alg_wc_empty_cart_button', 'CANCEL', 'value_text' ) . '"' . $confirmation_html . '></form>';
		echo str_replace( '%button_form%', $button_form_html, get_option( 'alg_wc_empty_cart_template', '<div style="float:right;">%button_form%</div>' ) );
	}

	/**
	 * empty_cart.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function empty_cart() {
		if ( isset( $_POST['alg_wc_empty_cart'] ) ) {
			WC()->cart->empty_cart();
		}
	}

}

endif;

return new Alg_WC_Empty_Cart_Button_Core();
