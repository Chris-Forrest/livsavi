<?php
/**
 * Empty Cart Button for WooCommerce - General Section Settings
 *
 * @version 1.0.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Empty_Cart_Button_Settings_General' ) ) :

class Alg_WC_Empty_Cart_Button_Settings_General extends Alg_WC_Empty_Cart_Button_Settings_Section {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		$this->id   = '';
		$this->desc = __( 'General', 'empty-cart-button-for-woocommerce' );
		parent::__construct();
	}

	/**
	 * get_section_settings.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @todo    (maybe) "do not add" to "Button Position on the Cart Page"
	 * @todo    (maybe) changeable priorities for both "Button Position on the Cart Page" and "Button Position on the Checkout Page"
	 * @todo    (maybe) all different options for Cart and Checkout (i.e. "Text and Style Options" and "Confirmation Options")
	 * @todo    (maybe) changeable redirect after empty cart action
	 * @todo    (Booster) copy all changes
	 */
	function get_section_settings() {
		$settings = array(
			array(
				'title'    => __( 'Empty Cart Button Options', 'empty-cart-button-for-woocommerce' ),
				'type'     => 'title',
				'id'       => 'alg_wc_empty_cart_button_options',
			),
			array(
				'title'    => __( 'WooCommerce Empty Cart Button', 'empty-cart-button-for-woocommerce' ),
				'desc'     => '<strong>' . __( 'Enable', 'empty-cart-button-for-woocommerce' ) . '</strong>',
				'desc_tip' => __( 'Empty Cart Button for WooCommerce.', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_button_enabled',
				'default'  => 'yes',
				'type'     => 'checkbox',
			),
			array(
				'type'     => 'sectionend',
				'id'       => 'alg_wc_empty_cart_button_options',
			),
			array(
				'title'    => __( 'Style and Label Options', 'empty-cart-button-for-woocommerce' ),
				'type'     => 'title',
				'id'       => 'alg_wc_empty_cart_button_text_and_style_options',
			),
			array(
				'title'    => __( 'Template', 'empty-cart-button-for-woocommerce' ),
				'desc_tip' => __( 'Template for wrapping the button.', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_template',
				'default'  => '<div style="float:right">%button_form%</div>',
				'type'     => 'textarea',
				'css'      => 'width:90%;min-width:300px;',
			),
			array(
				'title'    => __( 'Button Class', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_button_class',
				'default'  => 'button',
				'type'     => 'text',
				'css'      => 'width:50%;min-width:300px;',
			),
			array(
				'title'    => __( 'Button Style', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_button_style',
				'default'  => '',
				'type'     => 'text',
				'css'      => 'width:50%;min-width:300px;',
			),
			array(
				'title'    => __( 'Empty Cart Button Label', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_text',
				'default'  => 'Empty Cart',
				'type'     => 'text',
				'desc'     => apply_filters( 'alg_wc_empty_cart_button', sprintf( __( 'Get <a href="%s">Empty Cart Button for WooCommerce Pro</a> plugin to change value.', 'empty-cart-button-for-woocommerce' ), 'http://coder.fm/item/empty-cart-button-for-woocommerce/' ), 'settings' ),
				'custom_attributes' => apply_filters( 'alg_wc_empty_cart_button', array( 'readonly' => 'readonly' ), 'settings' ),
				'css'      => 'width:50%;min-width:300px;',
			),
			array(
				'type'     => 'sectionend',
				'id'       => 'alg_wc_empty_cart_button_text_and_style_options',
			),
			array(
				'title'    => __( 'Position Options', 'empty-cart-button-for-woocommerce' ),
				'type'     => 'title',
				'id'       => 'alg_wc_empty_cart_button_position_options',
			),
			array(
				'title'    => __( 'Button Position on the Cart Page', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_position',
				'default'  => 'woocommerce_after_cart',
				'type'     => 'select',
				'options'  => array(
					'woocommerce_after_cart'          => __( 'After Cart', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_before_cart'         => __( 'Before Cart', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_proceed_to_checkout' => __( 'After Proceed to Checkout button', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_after_cart_totals'   => __( 'After Cart Totals', 'empty-cart-button-for-woocommerce' ),
				),
				'desc'     => apply_filters( 'alg_wc_empty_cart_button', sprintf( __( 'Get <a href="%s">Empty Cart Button for WooCommerce Pro</a> plugin to change value.', 'empty-cart-button-for-woocommerce' ), 'http://coder.fm/item/empty-cart-button-for-woocommerce/' ), 'settings' ),
				'custom_attributes' => apply_filters( 'alg_wc_empty_cart_button', array( 'disabled' => 'disabled' ), 'settings' ),
			),
			array(
				'title'    => __( 'Button Position on the Checkout Page', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_checkout_position',
				'default'  => 'disable',
				'type'     => 'select',
				'options'  => array(
					'disable'                                       => __( 'Do not add', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_before_checkout_form'              => __( 'Before checkout form', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_before_customer_details'  => __( 'Before customer details', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_billing'                  => __( 'Billing', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_shipping'                 => __( 'Shipping', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_after_customer_details'   => __( 'After customer details', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_before_order_review'      => __( 'Before order review', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_order_review'             => __( 'Order review', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_checkout_after_order_review'       => __( 'After order review', 'empty-cart-button-for-woocommerce' ),
					'woocommerce_after_checkout_form'               => __( 'After checkout form', 'empty-cart-button-for-woocommerce' ),
				),
			),
			array(
				'type'     => 'sectionend',
				'id'       => 'alg_wc_empty_cart_button_position_options',
			),
			array(
				'title'    => __( 'Confirmation Options', 'empty-cart-button-for-woocommerce' ),
				'type'     => 'title',
				'id'       => 'alg_wc_empty_cart_button_confirmation_options',
			),
			array(
				'title'    => __( 'Confirmation', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_confirmation',
				'default'  => 'no_confirmation',
				'type'     => 'select',
				'options'  => array(
					'no_confirmation'         => __( 'No confirmation', 'empty-cart-button-for-woocommerce' ),
					'confirm_with_pop_up_box' => __( 'Confirm by pop up box', 'empty-cart-button-for-woocommerce' ),
				),
			),
			array(
				'title'    => __( 'Confirmation Text', 'empty-cart-button-for-woocommerce' ),
				'desc_tip' => __( 'Ignored if confirmation is not enabled.', 'empty-cart-button-for-woocommerce' ),
				'id'       => 'alg_wc_empty_cart_confirmation_text',
				'default'  => __( 'Are you sure?', 'empty-cart-button-for-woocommerce' ),
				'type'     => 'text',
			),
			array(
				'type'     => 'sectionend',
				'id'       => 'alg_wc_empty_cart_button_confirmation_options',
			),
		);
		return $settings;
	}

}

endif;

return new Alg_WC_Empty_Cart_Button_Settings_General();
