<?php

if( ! defined( 'ABSPATH' ) )
	die();

class Lava_Directory_Review_Core extends Lava_Directory_Review
{
	const MAX_SCORE	= 5;

	public function __construct() {
		// Enqueue Scripts
		add_action( 'admin_enqueue_scripts'				, Array( __CLASS__, 'admin_enqueue' ) );

		// Review Submit
		add_action( 'wp_ajax_nopriv_add_item_review'	, Array( __CLASS__, 'register_review' ) );
		add_action( 'wp_ajax_add_item_review'			, Array( __CLASS__, 'register_review' ) );

		// Review reply Submit
		add_action( 'wp_ajax_nopriv_add_comment_reply'	, Array( __CLASS__, 'register_review_reply' ) );
		add_action( 'wp_ajax_add_comment_reply'			, Array( __CLASS__, 'register_review_reply' ) );

		// Review Listings
		add_action( 'wp_ajax_nopriv_get_item_review'	, Array( __CLASS__, 'listings_review' ) );
		add_action( 'wp_ajax_get_item_review'			, Array( __CLASS__, 'listings_review' ) );

		// Review Listings
		add_action(
			'wp_ajax_nopriv_jvfrm_spot_update_review'
			, Array( __CLASS__, 'update_review_contents' )
		);
		add_action(
			'wp_ajax_jvfrm_spot_update_review'
			, Array( __CLASS__, 'update_review_contents' )
		);

		// Review Scripts
		add_action( 'jvfrm_spot_save_review'					, Array( __CLASS__, 'save_rating' ), 10, 2 );
		add_filter( 'jvfrm_spot_review_add_rating_meta'		, Array( __CLASS__, 'add_rating_meta' ), 10, 2 );
		add_action( 'update_rating'						, Array( __CLASS__, 'update_rating' ), 10 , 2 );

		// Trigger
		add_action( 'transition_comment_status'			, Array( __CLASS__, 'modify_hook_trig' ), 10, 3 );

		// Admin
		add_action( 'add_meta_boxes'					, Array( __CLASS__, 'comment_meta_boxes' ) );
		add_action( 'edit_comment'						, Array( __CLASS__, 'comment_meta_update' ) );

		// Display
		add_action( 'jvfrm_spot_review_scores_display'		, Array( __CLASS__, 'review_scores_display_callback' ), 10, 3 );

	}

	public static function setup_reviewData( &$review )
	{
		if( ! $review->author = new WP_User( $review->user_id ) )
		{
			$review->author				= new stdClass();
			$review->author->ID			= 0;
			$review->author->avatar		= 0;
		}
		$review->average	= get_comment_meta( $review->comment_ID, 'rating_average', true );
		$review->scores		= get_comment_meta( $review->comment_ID, 'rating_scores', true );
	}

	public static function review_scores_display_callback(
		$review
		, $show_title	= false
		, $total		= false
	){
		if( $total ){
			?>
			<div class="row">
				<div class="col-md-6 text-right">
					<?php _e('Total', 'lvdr-review');?>
				</div>
				<div class="col-md-6"><?php echo get_post_meta( $commend_id, 'rating_average', true);?></div>
			</div> <!-- row -->
			<?php
		};

		if( !empty( $review->scores ) ){
			foreach( $review->scores as $label => $value )
			{
				echo "<div class=\"row\">";

					if( $show_title )
						echo "<div class=\"col-md-6 col-sm-6 col-xs-6 text-right\">{$label}</div>";

					printf(
						"<div class=\"%s javo-tooltip\" title=\"%s\" data-direction=\"left\">"
						. "<div class=\"javo-rating-registed-score\" data-score=\"%s\"></div></div>"
						, ( $show_title ? 'col-md-6 col-sm-6 col-xs-6' : 'col-md-12 col-sm-12 col-xs-12' )
						, $label
						, $value
					);
				echo "</div>";
			}; // End Foreach
		}; // End If
	}

	public static function admin_enqueue()
	{
		wp_enqueue_script(
			'javo-wp-media-tirgger'
			, get_template_directory_uri() . "/assets/js/javo-wp-media-control.js"
			, false
			, '2.0.1'
			, true
		);
	}

	public static function comment_meta_boxes()
	{
		add_meta_box(
			'lava-directory-review-metabox'
			, __( "Review Detail", 'lvdr-review' )
			, Array( __CLASS__, 'comment_meta_box' )
			, 'comment'
			, 'normal'
			, 'high'
		);
	}

	public static function scoresCalculation( $scores_values )
	{
		global $jv_tso;

		$jv_filtered_scores	= Array();
		$jv_results			= Array();
		$jv_rating_fields		= jvfrm_spot_tso()->get( 'rating_field', Array() );


		if( empty( $jv_rating_fields ) )
			return false;

		if( empty( $scores_values ) || !is_Array( $scores_values ) )
			return false;

		foreach( $jv_rating_fields as $field )
			if( isset( $scores_values[ $field ] ) )
				$jv_filtered_scores[ $field ] = floatVal( $scores_values[ $field ] );

		$jv_results['scores']		= $jv_filtered_scores;
		$jv_results['count']		= sizeof( $jv_filtered_scores );
		$jv_results['total']		= Array_Sum( $jv_filtered_scores );
		@$jv_results['average']	= floatVal( Array_Sum( $jv_filtered_scores ) / sizeof( $jv_filtered_scores ) );
		return $jv_results;
	}

	public static function comment_meta_update( $comment_id )
	{
		$jv_query				= new jvfrm_spot_Array( $_POST );
		$jv_rating_scores		= $jv_query->get( 'jvfrm_spot_review_score' );

		if( $result = self::scoresCalculation( $jv_rating_scores ) )
		{
			update_comment_meta( $comment_id, 'rating_scores'	, $result['scores'] );
			update_comment_meta( $comment_id, 'rating_total'	, $result['total'] );
			update_comment_meta( $comment_id, 'rating_average'	, $result['average'] );
		}

		update_comment_meta( $comment_id, 'detail_images', $jv_query->get( 'jvfrm_spot_cmt_image' ) );
		do_action( 'update_rating', $comment_id );
	}

	public static function comment_meta_box( $comment )
	{

		global $jv_tso;

		$comment_id			= (int) $comment->comment_ID;
		$cmt_ratings		= get_comment_meta( $comment_id, 'rating_scores', true );
		$cmt_images			= get_comment_meta( $comment_id, 'detail_images', true );

		ob_start();
		?>
		<div id="postcustomstuff">
			<table id="list-table">
				<thead>
					<tr>
						<th class="left"><?php _e('Option Name', 'lvdr-review');?></th>
						<th><?php _e('Value', 'lvdr-review');?></th>
					</tr>
				</thead>
				<tbody id="the-list" data-wp-lists="list:meta">
					<?php if( $jv_cmt_fields	= jvfrm_spot_tso()->get( 'rating_field' ) ) : ?>
						<tr>
							<td valign="top"><p><?php _e("Rating Scores", 'lvdr-review');?></p></td>
							<td valign="top">
								<table class="javo-post-header-meta">
									<tbody>
										<?php
										foreach( $jv_cmt_fields as $field )
										{
											$jv_cmt_rating		= 0;
											if( is_Array( $cmt_ratings ) && !empty( $cmt_ratings[ $field ] ) )
												$jv_cmt_rating	= (float) $cmt_ratings[ $field ];

											echo "<tr>";
												echo "<td valign=\"middle\">{$field}</td>";
												echo "<td valign=\"middle\">";
													echo "<input" . ' ';
													echo "type	=\"text\"" . ' ';
													echo "name	=\"jvfrm_spot_review_score[{$field}]\"". ' ';
													echo "value	=\"{$jv_cmt_rating}\">";
												echo "</td>";
												echo "<td valign=\"middle\"> /".self::MAX_SCORE."</td>";

											echo "</tr>";

										} ?>
									</tbody>
								</table>
							</td>
						</tr>
					<?php endif; ?>
					<tr>
						<td valign="top"><p><?php _e("Upload Images", 'lvdr-review');?></p></td>
						<td valign="top">
							<button type="button" class="button button-primary" data-javo-wp-media-add>
								<?php _e( "Add Image", 'lvdr-review' );?>
							</button>
							<div id="javo-images-item-container">
								<?php
								if( !empty( $cmt_images ) )
								{
									foreach( $cmt_images as $image_id )
									{
										if( $jv_image_src = wp_get_attachment_image_src( $image_id , 'thumbnail' ) )
											$jv_image_src = $jv_image_src[0];

										if( false !== $jv_image_src )
										{
											echo "<div class='item'>";
												echo "<p><img src=\"{$jv_image_src}\"></p>";
												echo "<input name='jvfrm_spot_cmt_image[]' value='{$image_id}' type='hidden'>";
												echo "<button type=\"button\" class=\"button\" data-javo-wp-media-del>";
													_e( "Delete", 'lvdr-review' );
												echo "</button>";
											echo "</div>";
										}
									}
								} ?>
							</div>
						</td>
					</tr>
				</tbody>
			</table><!-- /#list-table -->
		</div><!-- /#postcustomstuff-->

		<script type="text/html" id="javo-review-image-container">
			<div class="item">
				<p><img src="{image_src}"></p>
				<input type="hidden" name="{image_input_name}" value="{image_id}">
				<button type="button" class="button" data-javo-wp-media-del>
					<?php _e("Delete", 'lvdr-review');?></button>
			</div><!-- /.item-container -->
		</script>

		<script type="text/javascript">
		jQuery( function( $ ) {
			jQuery.jvfrm_spot_wp_media({
				template			: $( "#javo-review-image-container" )
				, container			: $( "#javo-images-item-container" )
				, input_name		: "jvfrm_spot_cmt_image[]"
				/* Default
				, add_button		: $( "[data-javo-wp-media-add]" )
				, delete_button		: $( "[data-javo-wp-media-del]" )*/
			});
		});
		</script>

		<?php
		ob_end_flush();
	}

	public static function modify_hook_trig( $old, $new, $comment ) {
		do_action( 'update_rating', $comment->comment_ID );
	}

	public static function add_rating_meta( $comment_id, $args )
	{

		if( ! (int) $comment_id )
			return $comment_id;

		{
			$_comment					= get_comment( $comment_id );
			$_cmt_author				= new WP_User( $_comment->user_id );

			$jv_avatar_src			= jvfrm_spot_tso()->get( 'no_image', JVFRM_SPOT_IMG_DIR . '/no-image.png' );

			$intBlogID				= intVal( get_user_meta( $_cmt_author->ID, 'avatar_on_blog', true ) );

			if( is_multisite() && function_exists( 'switch_to_blog' ) && intVal( $intBlogID ) > 0 )
				switch_to_blog( $intBlogID );

			if( (int) $_cmt_author->avatar > 0 ) {
				if( $jv_avatar_meta = wp_get_attachment_image_src( $_cmt_author->avatar, 'jvfrm-spot-avatar' ) ) {
					if( '' !== ( $src = $jv_avatar_meta[0] ) )
						$jv_avatar_src	= $src;
				}
			}

			$args[ 'rating_sum' ]		= get_comment_meta( $comment_id, 'rating_total', true);
			$args[ 'rating_average' ]	= get_comment_meta( $comment_id, 'rating_average', true);
			$args[ 'ratings' ]			= get_comment_meta( $comment_id, 'rating_scores', true);
			$args[ 'avatar' ]			= $jv_avatar_src;
		}

		/* Images */{
			$jv_cmt_images			= get_comment_meta( $comment_id, 'detail_images', true );
			if( !empty( $jv_cmt_images ) && is_Array( $jv_cmt_images ) )
			{
				ob_start();
				?>
				<div class="row review-thumbnails-inner">
					<?php
					$jv_integer = 0;
					foreach( $jv_cmt_images as $attach_id )
					{
						if(
							( $jv_image_src = wp_get_attachment_image_src( $attach_id , Array( 170, 170 ) ) ) &&
							( $jv_full_image_src = wp_get_attachment_image_src( $attach_id , 'full' ) )
						) {
							$jv_image_src = $jv_image_src[0];
							$jv_full_image_src = $jv_full_image_src[0];
						}

						if( false !== $jv_image_src )
						{
							$jv_integer++;

							echo "<div class=\"col-md-4 col-xs-4\">";
								echo "<div class=\"javo-thb\" style=\"background-image:url({$jv_image_src}); height:120px;cursor:pointer;\" href=\"{$jv_full_image_src}\">";
								echo "</div>";
							echo "</div>";

							if( $jv_integer % 3 == 0 )
								echo "</div><div class=\"row review-thumbnails-inner\">";
						}
					} ?>
				</div><!-- /.row-->
				<?php
				$args[ 'thumbnails' ]		= ob_get_clean();
			}
		}

		return $args;
	}

	public static function register_review()
	{
		check_ajax_referer( 'javo-write-review', 'nonce' );

		global $jv_tso;

		$response					= Array();
		$jv_query					= new jvfrm_spot_Array( $_POST );
		$post_id					= $jv_query->get( 'post_id', 0 );

		if( (int) $post_id == 0 )
			die( json_encode( $response ) );

		$comment_approved			= jvfrm_spot_tso()->get( 'approve_review', false ) === 'approve';

		$jv_cmt_author			= $jv_query->get(
			'comment_author'
			, __( "Anonymous", 'lvdr-review' )
		);

		$jv_cmt_author_id			= 0;

		if( is_user_logged_in() ) {
			if( (boolean) $jv_cmt_author_id = get_current_user_id() ) {
				$jv_cmt_author		= new WP_User( $jv_cmt_author_id );
				$jv_cmt_author		= $jv_cmt_author->display_name;
			}
		}

		$args							= Array(
			'user_id'					=> $jv_cmt_author_id
			, 'comment_type'			=> $jv_cmt_author_id > 0 ? "javo-member-review" : ''
			, 'comment_post_ID'			=> $post_id
			, 'comment_author'			=> $jv_cmt_author
			, 'comment_content'			=> $jv_query->get( 'comment_content', '' )
			, 'comment_author_email'	=> $jv_query->get( 'comment_author_email', '' )
			, 'comment_author_url'		=> esc_url( $jv_query->get( 'comment_author_url', '' ) )
			, 'comment_approved'		=> intVal( $comment_approved )
		);

		if( $comment_id = wp_insert_comment( $args ) )
		{
			$_comment				= get_comment( $comment_id );
			$_data_meta				= Array(
				'id'				=> $_comment->comment_ID
				, 'author'			=> $_comment->comment_author
				, 'content'			=> $_comment->comment_content
				, 'date'			=> get_comment_date( false, $_comment )
			);

			// Register Successfully
			$response['state']		= true;
			$response['comment_id']	= $comment_id;
			$response['author']		= $jv_cmt_author;
			$response['content']	= $jv_query->get( 'comment_content', '' );
			$response['approve']	= (boolean)$comment_approved;

			update_comment_meta( $comment_id, 'detail_images', $jv_query->get( 'jvfrm_spot_dim_detail', Array() ) );

			do_action( 'jvfrm_spot_save_review', $comment_id, $jv_query );

			$response['data']		= apply_filters(
				'jvfrm_spot_review_add_rating_meta'
				, $comment_id
				, $_data_meta
			);
		}

		$response = apply_filters( 'jvfrm_spot_save_review_result', $response );

		die( json_encode( $response ) );
	}

	public static function register_review_reply()
	{
		global $jv_tso;

		$response					= Array();
		$jv_query					= new jvfrm_spot_Array( $_POST );
		$comment_id					= $jv_query->get( 'comment_id', 0 );

		$comment_approved			= jvfrm_spot_tso()->get( 'approve_review', false ) === 'approve';

		if( (int) $comment_id == 0 )
			die( json_encode( $response ) );

		$response['state']			= 'failed';

		$jv_cmt_author_id			= 0;

		$jv_cmt_author			= $jv_query->get(
			'comment_author'
			, __( "Anonymous", 'lvdr-review' )
		);

		if( is_user_logged_in() ) {
			if( (boolean) $jv_cmt_author_id = get_current_user_id() ) {
				$jv_cmt_author	= new WP_User( $jv_cmt_author_id );
				$jv_cmt_author	= $jv_cmt_author->display_name;
			}
		}

		$args						= Array(
			'user_id'				=> $jv_cmt_author_id
			, 'comment_post_ID'		=> $jv_query->get( 'post_id', 0 )
			, 'comment_parent'		=> $comment_id
			, 'comment_author'		=> $jv_cmt_author
			, 'comment_content'		=> $jv_query->get( 'content', '' )
			, 'comment_approved'	=> intVal( $comment_approved )
		);

		if( $comment_id = wp_insert_comment( $args ) )
		{
			$_comment				= get_comment( $comment_id );

			$response['state']		= 'success';
			$response['approve']	= (boolean) $comment_approved;
			$response['data']		= Array(
				'id'				=> "ID=>".$comment_id
				, 'author'			=> $_comment->comment_author
				, 'content'			=> $_comment->comment_content
				, 'date'			=> sprintf(
					__( "%s ago" , 'lvdr-review' )
					, human_time_diff( get_comment_date( 'U', $_comment ), current_time( 'timestamp' ) )
				)
			);
		}

		die( json_encode( $response ) );
	}

	public static function save_rating( $comment_id, $query =null )
	{
		global $jv_tso;

		if( ! $comment_id )
			return;

		$jv_scores					= (Array) $query->get( 'jvfrm_spot_rats', Array() );
		$jv_rating_scores				= Array();

		foreach( $jv_scores as $index => $field )
			$jv_rating_scores[ $field['label'] ] = $field['score'];


		if( $result = self::scoresCalculation( $jv_rating_scores ) )
		{
			update_comment_meta( $comment_id, 'rating_scores'	, $result['scores'] );
			update_comment_meta( $comment_id, 'rating_total'	, $result['total'] );
			update_comment_meta( $comment_id, 'rating_average'	, $result['average'] );
		}

		do_action( 'update_rating', $comment_id );
	}

	public static function listings_review() {
		global $jv_tso;

		// Browser Backspace Key Error
		// check_ajax_referer( 'javo-listings-review', 'nonce' );

		$response = Array();
		$jv_query = new jvfrm_spot_Array( $_POST );
		$post_id = $jv_query->get( 'post_id', 0 );

		if( intVal( $post_id ) == 0 )
			die( json_encode( $response ) );

		$jv_comments_args = Array(
			'post_id' => $post_id,
			'parent' => 0,
			'status' => 'approve',
			'number' => 3,
			'offset' => (int) $jv_query->get( 'offset', 0 ),
		);

		$response[ 'offset' ] = $jv_query->get( 'offset' );

		$objComment =  new WP_Comment_Query;
		$jv_get_comments = $objComment->query( $jv_comments_args );
		$jv_comment = Array();

		foreach( $jv_get_comments as $comment ) {

			$_this_relies_args = Array(
				'post_id' => $post_id
				, 'status' => 'approve'
				, 'parent' => $comment->comment_ID
			);

			$comment_reply = Array();

			if( $_this_replies = get_comments( $_this_relies_args ) )
			{
				foreach( $_this_replies as $reply )
				{
					$comment_reply[]	= Array(
						'author' => $reply->comment_author
						, 'content' => $reply->comment_content
						, 'date' => sprintf(
							__( "%s ago" , 'lvdr-review' )
							, human_time_diff( get_comment_date( 'U', $reply ), current_time('timestamp') )
						)
					);
				}
			}

			$_data_meta = Array(
				'id' => $comment->comment_ID
				, 'author' => $comment->comment_author
				, 'content' => $comment->comment_content
				, 'date' => sprintf(
					__( "%s ago" , 'lvdr-review' )
					, human_time_diff( get_comment_date( 'U', $comment ), current_time('timestamp') )
				)
				, 'reply' => $comment_reply
				, 'is_author' => get_current_user_id() === (int) $comment->user_id && (int) $comment->user_id > 0
			);

			$jv_comment[] = apply_filters( 'jvfrm_spot_review_add_rating_meta', $comment->comment_ID, $_data_meta );
		}

		$intFoundComments = get_comments_number( $post_id ) - intVal( $jv_comments_args[ 'offset' ] );

		if( intVal( $intFoundComments ) <= count( $jv_get_comments ) )
			$response['finish'] = 'finish';

		$response[ 'state' ] = true;
		$response[ 'data' ] = $jv_comment;
		$response[ 'found_count' ] = $intFoundComments;

		die( json_encode( $response ) );
	}

	public static function update_review_contents()
	{
		$response		= Array();
		$jv_query		= new jvfrm_spot_Array( $_POST );

		$comment_id		= $jv_query->get( 'comment', false );

		if( ! $comment = get_comment( $comment_id ) )
			$response[ 'error' ]		= __( "Invalid comment ID.", 'lvdr-review' );

		if( (int)$comment->user_id !== get_current_user_id() )
			$response[ 'error' ]		= __( "Your not the author", 'lvdr-review' );

		if( !isset( $response['error'] ) )
		{
			remove_action( 'edit_comment'	, Array( __CLASS__, 'comment_meta_update' ) );
			$comment_id	= wp_update_comment(
				Array(
					'comment_ID'		=> $comment->comment_ID
					, 'comment_content'	=> $jv_query->get( 'content' )
				)
			);
			add_action( 'edit_comment'		, Array( __CLASS__, 'comment_meta_update' ) );

			if( $comment_id )
				$response[ 'state' ]	= 'OK';
		}

		die( json_encode( $response ) );
	}

	public static function getWriteForm( $lists = false )
	{
		global $post, $jv_tso, $jv_custom_item_label;

		if( (int) $post->ID <= 0 )
			die( -1 );


		if( is_user_logged_in() )
			wp_enqueue_media();

		$post_id			= $post->ID;
		$allow_write_form	= false;

		ob_start();
		?>
		<div class="row jv-rating-form-wrap">
			<div class="col-md-12 col-sm-12 col-xs-12">

			<?php
			if( jvfrm_spot_tso()->get( 'review_only_member', false ) )
				$allow_write_form	= is_user_logged_in();

			else
				$allow_write_form	= is_user_logged_in();
				//$allow_write_form = true;

			if( $allow_write_form ):
				?>
					<div id="javo-review-form-container" class="cursor-pointer">
						<div style="text-align:center;">
							<span class="lv-review-submit">
								<?php _e( "LEAVE A REVIEW", 'lvdr-review' );?>
							</span>
						</div>



						<form class="hidden" role="form">
							<div class="row">
								<div class="jv-rating-wrap">
									<div class="col-md-12 jv-rating-top-inner">
										<?php
										if( (boolean) $jv_rating_fields = jvfrm_spot_tso()->get('rating_field') )
										{
											?>
											<ul>
												<?php
												foreach( $jv_rating_fields as $index => $label )
												{
													?>
													<li class="col-md-6 jv-rating-selector">
														<div class="row">
															<div class="col-md-6 col-sm-6 javo-raintg-form-field-label-wrap">
																<span class="javo-raintg-form-field-label"><?php echo $label;?></span>
															</div>
															<div class="col-md-6 col-sm-6 jvfrm_spot_rat_star-warp">
																<span class="jvfrm_spot_rat_star" data-score="0" data-input-name="jvfrm_spot_rats[<?php echo $index;?>][score]" data-label="<?php echo $label;?>" required></span>
																<input type="hidden" name="jvfrm_spot_rats[<?php echo $index;?>][label]" value="<?php echo $label;?>">
															</div>
														</div><!-- /.row -->
													</li>
													<?php
												} ?>
											</ul>
											<?php
										} ?>
									</div><!-- /.col-md-12 -->
									<div class="col-md-12 jv-rating-bottom-inner">
										<textarea name="comment_content" class="form-control" data-label="<?php _e( "Contents", 'lvdr-review' );?>"></textarea>
									</div><!-- /.col-md-6 -->
								</div><!--jv-rating-wrap-->
							</div><!--/.row -->

							<div class="row jv_comment_image_preview comment_image_preview"></div><!-- /.row -->

							<div class="row jv-rating-submit-wrap">
								<div class="col-md-12 text-center">
										<div class="inline-block">
											<button
												type			= "button"
												class			= "btn javo-fileupload admin-color-setting"
												data-multiple	= "1"
												data-title		= "<?php _e( "Review Thumbnail", 'lvdr-review' );?>"
												data-preview	= ".comment_image_preview"
											>
												<i class="fa fa-picture-o"></i>
												<?php _e( "Upload Image", 'lvdr-review' );?>
											</button>
											<input type="hidden" name="comment_image_id">
											<button type="submit" class="btn admin-color-setting">
												<i class="fa fa-send"></i>
												<?php _e( "Publish Review", 'lvdr-review' );?>
											</button>

										</div><!-- /.inline-block -->
								</div><!-- /.col-md-12 -->
							</div><!--/.row -->

							<fieldset>
								<input type="hidden" name="action" value="add_item_review">
								<input type="hidden" name="post_id" value="<?php echo $post_id;?>">
								<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'javo-write-review' ); ?>">
							</fieldset>
						</form>
					</div><!-- well -->
				<?php
				else:

				endif;
				?>

			</div><!-- /.col-md-12 -->
		</div><!-- /.rating-form-wrap -->

		<?php
		if( $lists )
			self::getReviewLists();
		ob_end_flush();
	}

	public static function getReviewLists()
	{
		global $post;

		if( (int) $post->ID <= 0 )
			die( -1 );

		$post_id				= $post->ID;

		ob_start();
		?>

		<div class="row javo-detail-item-review-wrap">
			<div id="javo-detail-item-review-container"></div>
		</div><!-- /.row -->

		<div class="row lv-review-loadmore">
			<div class="col-md-12 text-center">
				<button type="button" class="btn btn-primary disabled admin-color-setting" id="javo-detail-item-review-loadmore" data-loading-text="<?php echo ucfirst( __( "Loading", 'javo' ) ); ?>...">
					<i class="fa fa-write"></i>
					<?php _e( "Load More", 'lvdr-review' ); ?>
				</button><!-- /#javo-detail-item-review-loadmore -->
			</div><!-- /.col-md-12 -->
		</div><!-- /.row -->

		<fieldset id="javo-detail-item-review-parameter">
			<input type="hidden" name="ajaxurl" value="<?php echo admin_url( 'admin-ajax.php' );?>">
			<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'javo-listings-review' ); ?>">
			<input type="hidden" name="post_id" value="<?php echo $post_id;?>">
			<input type="hidden" name="user_id" value="">
			<input type="hidden" name="reply_register_success"	value="<?php _e( "Successfully Saved", 'lvdr-review' ); ?>">
			<input type="hidden" name="reply_modify_fail"		value="<?php _e( "Save failed", 'lvdr-review' ); ?>">
			<input type="hidden" name="review_register_success"	value="<?php _e( "Successfully Saved", 'lvdr-review' ); ?>">
			<input type="hidden" name="review_register_fail"	value="<?php _e( "Save failed", 'lvdr-review' ); ?>">

		</fieldset>

		<script type="text/html" id="javo-detail-item-review-template">
			<div class="javo-detail-item-review-inner col-md-12 col-sm-12 col-xs-12">
				<!-- Title -->
					<div class="">

						<div class="inline-block col-md-2 col-xs-2 review-left-wrap">
							<div class="javo-thb" style="background-image:url({avatar}); width:100px; height:100px; border-radius:100%;"></div>

						</div><!-- /.inline-block -->

						<div class="inline-block col-md-9 col-xs-10 review-right-wrap">
						<!-- Content -->
							<div class="row">
								<div class="review-subject pull-left">
									<h4>
										<span class="review-author">{author}</span>
										<span class="review-date">{date}</span>
									</h4>
									<!-- Date -->
								</div><!-- /.pull-left -->
								<div class="review-rating pull-right">
							<!-- Ratings -->
									<div class="row">
										<div class="col-md-12">
											<div data-score="{rating_average}"></div> {edit}
										</div><!-- /.col-md-12 -->
									</div><!-- /.row -->
								</div>
								<div class="col-md-12 col-xs-12 javo-detail-item-comment-content-wrap">
									<div class="javo-detail-item-comment-content">
										{content}
									</div><!-- /.javo-detail-item-comment-content -->
								</div><!-- /.col-md-12 -->
							</div><!-- /.row -->
							<!-- Thumbnails -->
							<div class="row review-thumbnails-wrap">
								<div class="col-md-12 col-xs-12">
									{thumbnails}
								</div><!-- /.col-md-12 -->
							</div><!-- /.row -->
							<!-- Reply Listing -->
							<div class="row javo-detail-item-reply-container" data-parent-id="{comment-id}">
								<div class="row">
									<div class="col-md-12 javo-detail-item-inner">
										{reviews}
									</div><!-- /.col-md-12 -->
								</div><!-- /.row -->
							</div><!-- /.javo-detail-item-reply-container -->
							<!-- Reply Field -->
							<div class="row javo-detail-item-reply-textarea">

							<?php if( is_user_logged_in() ) : ?>
								<div class="col-md-3 col-xs-1">
									<span class="hidden-xs reply-author">{author}</span>
								</div><!-- /.col-md-1 -->
								<div class="col-md-9 col-xs-11">
									<div
										class="javo-detail-item-reply"
										data-parent-id="{comment-id}"
										contenteditable="true"
										placeholder="<?php _e('Write a comment.. ( Enter key to submit )', 'lvdr-review');?>"></div>
								</div><!-- /.col-md-11 -->
								<?php endif; ?>
							</div>
						</div><!-- /.inline-block -->
					</div><!-- /.panel-footer -->
			</div>
			<!-- Separator -->
				<hr>

		</script>
		<script type="text/html" id="javo-detail-item-reply-template">


				<div class="row">

					<div class="col-md-12 javo-detail-item-replys">
						<div class="javo-detail-item-reply-inner">
							<span class="col-md-3">{author}</span>
							<h6 class="javo-detail-item-reply-content col-md-9">{content}
								<span class="javo-detail-item-reply-content-date">{date}</span>
							</h6>
						</div>
						<div class="javo-detail-item-reply-meta"></div>
					</div><!-- /.col-md-4 -->

				</div><!-- /.row -->
		</script>

		<script type="text/html" id="javo-detail-item-review-empty">
			<div class="well well-sm dismiss-ready text-center col-xs-12 col-md-12">
				<h4><?php echo strtoupper( __( "Not found review", 'lvdr-review' ) );?></h4>
			</div>
		</script>
		<?php
		ob_get_flush();
	}


	public static function update_rating( $comment_id )
	{
		global $jv_tso;

		if( (int) $comment_id === 0 )
			return $comment_id;

		if( ! $jv_cmt = get_comment( $comment_id ) )
			return $comment_id;

		$jv_cmt_post				= $jv_cmt->comment_post_ID;

		if( self::$instance->post_type !== get_post_type( $jv_cmt_post ) )
			return $comment_id;

		$jv_cmt_args				= Array(
			'status'				=> 'approve'
			, 'parent'				=> 0
			, 'post_id'				=> $jv_cmt_post
		);

		$jv_comments				= get_comments( $jv_cmt_args );
		$jv_rating_sum			= $jv_rating_average = 0;
		$jv_comments_count		= count( $jv_comments );

		foreach( $jv_comments as $cmt ) {
			$jv_rating_sum		+= (float)get_comment_meta( $cmt->comment_ID, 'rating_average', true );
		}
		$jv_rating_average		= 0;

		@$jv_rating_average		= sprintf( "%.1f", ( (float) $jv_rating_sum / $jv_comments_count ) );

		update_post_meta( $jv_cmt_post, 'rating_average', $jv_rating_average );
		update_post_meta(
			$jv_cmt_post
			, 'rating_meta'
			, Array(
				'total'			=> $jv_rating_sum
				, 'count'		=> $jv_comments_count
				, 'average'		=> $jv_rating_average
			)
		);
	}

	public static function get( $key, $default=0 )
	{
		global $post;

		if( (int) $post->ID == 0 )
			return;

		$jv_cmt_meta			= get_post_meta( $post->ID, 'rating_meta', true );

		if( !empty( $jv_cmt_meta[ $key ] ) )
			return $jv_cmt_meta[ $key ];

		else
			return $default;
	}

	public static function fa_get(
		$fill = 'fa-star'
		, $unfill = 'fa-star-o'
	){

		$jv_post_score		= floor( self::get( 'average' ) );
		$jv_ulfill_score	= intVal( abs( self::MAX_SCORE - $jv_post_score ) );
		$jv_el_start		= str_repeat( "<i class=\"fa {$fill}\"></i>", $jv_post_score );
		$jv_el_end			= str_repeat( "<i class=\"fa {$unfill}\"></i>", $jv_ulfill_score );
		return "{$jv_el_start}{$jv_el_end}";
	}

	public static function part_progress()
	{
		global $jv_tso	, $post;

		$jv_rating_fields		= jvfrm_spot_tso()->get( 'rating_field', Array() );

		$jv_cmt_args			= Array(
			'status'			=> 'approve'
			, 'parent'			=> 0
			, 'post_id'			=> $post->ID
		);

		$jv_comments			= get_comments( $jv_cmt_args );
		$jv_comments_count	= count( $jv_comments );

		$jv_score_results		= Array();

		foreach( $jv_comments as $cmt )
		{
			$jv_rating_scores	= get_comment_meta( $cmt->comment_ID, 'rating_scores', true );

			if( !empty( $jv_rating_fields ) ) {
				foreach( $jv_rating_fields as $field ) {
					if( isset( $jv_rating_scores[ $field ] ) )
						$jv_score_results[ $field ][] = floatVal( $jv_rating_scores[ $field ] );
				}
			}
		}

		if( !empty( $jv_rating_fields ) )
		{
			foreach( $jv_rating_fields as $field )
			{

				$jv_part_sum = $jv_part_avg = 0;

				if( !empty( $jv_score_results[ $field] ) )
					$jv_part_sum		= Array_Sum( $jv_score_results[ $field ] );

				if( $jv_part_sum )
					@$jv_part_avg		= (float) $jv_part_sum / $jv_comments_count;

				$jv_part_per			= sprintf( "%.1f", ( ( $jv_part_avg / self::MAX_SCORE ) * 100 ) );
				echo "<div class=\"row review-avg-bar-wrap\">";
					echo "<div class=\"col-md-3 col-sm-3 col-xs-6\"><div class=\"progress-title\">{$field}</div></div>";
					echo "<div class=\"col-md-9 col-sm-9 col-xs-6\"><div class=\"progress\">";
						echo "<div class=\"progress-bar progress-bar-blug\"" . ' ';
						//echo "aria-valuenow=\"{$jv_part_per}\" aria-valuemin=\"0\" aria-valuemax=\"100\"" . ' ';
						echo "style=\"width:{$jv_part_per}%\">";
							echo $jv_part_per. '% ';
						echo "</div>";
					echo "</div></div>";
				echo "</div>";
			}
		}
	}

	public static function get_option( $key_name, $default=false ) {
		if( function_exists( 'jvfrm_spot_tso' ) )
			$default	= jvfrm_spot_tso()->get( $key_name, $default );
		return $default;
	}
}