<?php
function lava_directory_review_script()
{
	wp_localize_script(
		lv_directoryReview()->getClass() . '-' . sanitize_title( 'rating-script.js' ),
		'lavaDirectoryReview',
		Array(
			'url'				=> esc_url( admin_url( 'admin-ajax.php' ) ),
			'urlImage'		=> lv_directoryReview()->image_url,
			'strOK'			=> __( "OK", 'lvdr-review' ),
			'strFillRaty'	=> __( "Please rate on %s.", 'lvdr-review' ),
			'strPending'	=> __( "Your comment is awaiting moderation.", 'lvdr-review' ),
		)
	);

	wp_enqueue_script( lv_directoryReview()->getClass() . '-' . sanitize_title( 'jquery.raty.min.js' ) );
	wp_enqueue_script( lv_directoryReview()->getClass() . '-' . sanitize_title( 'jquery.magnific-popup.min.js' ) );
	wp_enqueue_script( lv_directoryReview()->getClass() . '-' . sanitize_title( 'rating-script.js' ) );
}
function get_lava_directory_review()
{
	add_action( 'wp_footer', 'lava_directory_review_script' );
	?>
	<div class="lv-directory-review-wrap">
		<div class="review-avg-wrap">
			<div class="row review-avg-score-wrap">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="review-avg-score-box admin-color-setting">
						<div class="review-avg-score"><?php echo lv_directoryReview()->core->get( 'average'); ?></div>
						<div class="review-avg-stars"><?php echo lv_directoryReview()->core->fa_get();?></div>
					</div> <!-- review-avg-score-box -->
				</div> <!--.col-md-3 -->
				<div class="col-md-9 col-sm-9 col-xs-12 review-avg-des">
					<?php //echo lv_directoryReview()->core->get_option( 'rating_alert_content' ); ?>
					<?php lv_directoryReview()->core->part_progress(); ?>
				</div> <!--.col-md-9 review-avg-des -->
			</div> <!-- / .row review-avg-score-wrap -->
			<div class="review-avg-bars">
				<?php //lv_directoryReview()->core->part_progress(); ?>
			</div> <!-- review-avg-bars -->
		</div> <!-- /.review-avg-wrap -->
		<?php lv_directoryReview()->core->getWriteForm( true ); ?>
	</div> <!-- /.lvdr-review-wrap -->
	<?php
}