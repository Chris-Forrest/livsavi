<?php
/**
 * Plugin Name: Lava Directory Review Addons
 * Plugin URI: http://lava-code.com/directory/
 * Description: Lava Directory Plugin Review Addon
 * Version: 1.0.4
 * Author: Lavacode
 * Author URI: http://lava-code.com/
 * Text Domain: lvdr-review
 * Domain Path: /languages/
 */
/*
    Copyright Automattic and many other contributors.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

if( ! defined( 'ABSPATH' ) )
	die();

if( ! class_exists( 'Lava_Directory_Review' ) ) :

	class Lava_Directory_Review
	{
		public $core;

		private $path = false;

		private $version = '1.0.4';

		private $scripts = Array(
			'jquery.raty.min.js',
			'jquery.magnific-popup.min.js',
			'rating-script.js',
		);

		private $styles = Array(
			'magnific-popup.css',
		);

		protected $post_type = 'lv_listing';

		// Required Variable $jv_tso
		public $allow_theme = 'javo-spot';

		public $allow_themes = array(
			'javo-spot',
			'javo-directory'
		);

		public $optionGroup = false;
		public $image_url = false;
		public static $instance;

		public function __construct( $file )
		{

			$this->file						= $file;
			$this->folder					= basename( dirname( $this->file ) );
			$this->path					= dirname( $this->file );
			$this->template_path	= trailingslashit( $this->path ) . 'templates';
			$this->assets_url			= esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
			$this->image_url			= esc_url( trailingslashit( $this->assets_url . 'images/' ) );

			$this->theme				= wp_get_theme();
			$this->template			= $this->theme->Name;

			if( $hasParent = $this->theme->Template )
				$this->template		= $hasParent;

			/**
			if( $this->allow_theme !== sanitize_title( $this->template ) )
				return false;
			*/

			if( ! in_array( sanitize_title( $this->template ), $this->allow_themes ) )
				return false;

			$this->load_files();
			$this->register_hooks();
			$this->optionGroup	= 'lava_' . $this->post_type . '_review';

		}

		public function getClass() {
			return sanitize_title( $this->folder );
		}

		public function getVersion() {
			return $this->version;
		}

		public function getPluginDir() {
			return trailingslashit( dirname( dirname( __FILE__ ) ) );
		}

		public function load_files() {
			require_once "includes/class-core.php";
			require_once "includes/class-template.php";
		}

		public function register_hooks()
		{
			add_action( 'wp_enqueue_scripts'		, Array( $this, 'enqueues' ) );

			// add_filter( "lava_{$this->post_type}_admin_tab"	, Array( $this, 'add_addons_tab' ) );
			add_filter( 'jvfrm_spot_theme_setting_pages', Array( $this, 'add_review_page' ) );
			load_plugin_textdomain('lvdr-review', false, $this->folder . '/languages/');

			$this->core = new Lava_Directory_Review_Core;
		}

		public function enqueues()
		{

			if( !empty( $this->styles ) ) : foreach( $this->styles as $cssFile ) {
				wp_enqueue_style(
					sanitize_title( $this->getClass() .'-' . $cssFile ),
					$this->assets_url . '/css/' . $cssFile,
					Array()
				);
			} endif;

			if( !empty( $this->scripts ) ) : foreach( $this->scripts as $jsFile ) {
				wp_register_script(
					sanitize_title( $this->getClass() .'-' . $jsFile ),
					$this->assets_url . '/js/' . $jsFile,
					Array( 'jquery' )
				);
			} endif;

			printf(
				'<link rel="stylesheet/less" type="text/css" href="%1$s">',
				$this->assets_url . 'css/review-style.less'
			);

		}

		public function add_addons_tab( $args )
		{
			return wp_parse_args(
				Array(
					'review'		=> Array(
						'label'		=> __( "Review", 'Lavocode' ),
						'group'	=> $this->optionGroup,
						'file'		=> $this->template_path . '/admin-review.php'
					)
				), $args
			);
		}

		public function add_review_page( $pages ){
			return wp_parse_args(
				Array(
					'review_addon'	=> Array(
						__( "Review", 'lvdr-review' ), false
						, 'priority'		=> 36
						, 'external'		=> $this->template_path . '/admin-review.php'
					)
				)
				, $pages
			);
		}

		public static function get_instance( $file )
		{
			if( null === self::$instance )
				self::$instance = new self( $file );
			return self::$instance;
		}
	}
endif;

if( !function_exists( 'lv_directoryReview' ) ) :
	function lv_directoryReview() {
		return Lava_Directory_Review::get_instance( __FILE__ );
	}
endif;
$GLOBALS[ 'lava_directory_review' ] = lv_directoryReview();