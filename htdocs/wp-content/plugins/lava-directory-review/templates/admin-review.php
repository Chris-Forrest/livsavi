<div class="jvfrm_spot_ts_tab javo-opts-group-tab hidden" tar="review_addon">
	<script type="text/template" id="javo-ts-rating-field-template">
		<div class="jvfrm_spot_rating_item">
			<label><?php _e('Rating field Name', 'lvdr-review');?> : <input name="jvfrm_spot_ts[rating_field][]"></label>
		</div>
	</script>

	<h2> <?php _e("Rating Settings", "lvdr-review"); ?>	</h2>
	<table class="form-table">
	<tr><th>
		<?php _e('Rating', 'lvdr-review');?>
		<span class="description">
			<?php _e('Customize the amount and the names of each rating field.', 'lvdr-review');?>
		</span>
	</th><td>

		<h4><?php _e('Reviews Publish Status', 'lvdr-review');?></h4>
		<fieldset class="inner">
			<div>
				<label>
					<input type="radio" name="jvfrm_spot_ts[approve_review]" value="approve" <?php checked('approve' == jvfrm_spot_tso()->get('approve_review', 'pending'));?>>
					<?php _e("Approve (Immediately)", 'lvdr-review');?>
				</label>
			</div>
			<div>
				<label>
					<input type="radio" name="jvfrm_spot_ts[approve_review]" value="pending" <?php checked('pending' == jvfrm_spot_tso()->get('approve_review', 'pending'));?>>
					<?php _e('Pending ( After admin approval)', 'lvdr-review');?>
				</label>
			</div>
		</fieldset>

		<h4><?php _e('Number of Rating Fields',	'lvdr-review');?></h4>
		<fieldset>
			<select	name="jvfrm_spot_ts[rating_count]" id="jvfrm_spot_rating_count">
				<?php
				for($i = 1;	$i <= 6; $i++){
					printf('<option	name="%s"%s>%s</option>'
						, $i
						, ( count( jvfrm_spot_tso()->get('rating_field') ) == $i ? ' selected' : '' )
						, $i
					);
				} ?>
			</select>
			<input type="button" class="button button-primary jvfrm_spot_rat_apply" value="<?php _e('Apply', 'lvdr-review');?>">
			<h3><?php _e('Items', 'lvdr-review');?></h3>
			<div class="jvfrm_spot_rating_field">
				<?php
				$jv_rating_field = jvfrm_spot_tso()->get('rating_field', null);
				if(!empty($jv_rating_field) && is_array($jv_rating_field)){
					foreach($jv_rating_field as $field){
						printf('<div class="jvfrm_spot_rating_item">
								<label>%s :	<input name="jvfrm_spot_ts[rating_field][]" value="%s"></label>
							</div>'
							, __('Rating Field Name', 'lvdr-review')
							, __($field, 'lvdr-review')
						);
					};
				}else{
					printf('<div class="jvfrm_spot_rating_item">
							<label>%s :	<input name="jvfrm_spot_ts[rating_field][]"></label>
						</div>'
						, __('Rating Field Name', 'lvdr-review')
					);
				};
				?>
			</div>
		</fieldset>
		<script	type="text/javascript">
		jQuery(function($){
			"use strict";

			var lava_directory_review_admin_script = {

				init:function()
				{
					$(document).on('click', '.jvfrm_spot_rat_apply', this.applyField);
				}
				, applyField: function(e)
				{
					e.preventDefault();
					var cur_cnt = $('[name="jvfrm_spot_ts[rating_field][]"]').length;
					var set_cnt = $(this).prev('select').val();

					// Added Field
					if( set_cnt > cur_cnt  )
					{
						for(var i = cur_cnt; set_cnt > i; i++)
						{
							$('.jvfrm_spot_rating_field').append( $('#javo-ts-rating-field-template').html() );
						}
					}
					// Remove Field
					else if( set_cnt < cur_cnt  )
					{
						for(var i = cur_cnt; set_cnt < i; i--)
						{
							$('.jvfrm_spot_rating_field').find('div:last-child').remove();
						}
					}
				}
			}
			lava_directory_review_admin_script.init();
		});
		</script>
		<hr>

		<fieldset>
			<h4><?php _e('Rating Alert Title', 'lvdr-review');?></h4>
			<input type="text" name="jvfrm_spot_ts[rating_alert_header]" class="large-text" value="<?php echo	jvfrm_spot_tso()->get('rating_alert_header', '');?>">
			<h4><?php _e('Rating Alert Content', 'lvdr-review');?></h4>
			<textarea name="jvfrm_spot_ts[rating_alert_content]" class="large-text" rows="10"><?php echo jvfrm_spot_tso()->get('rating_alert_content', '');?></textarea>
		</fieldset>
	</td></tr>
	</table>
</div>