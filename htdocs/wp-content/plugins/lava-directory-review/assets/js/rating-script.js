;( function( $ ) {
	var javo_write_review = function( element ){
		this.el			= $( element );
		this.selector	= element;
		this.args		= lavaDirectoryReview;
		if( ! window.__JAVO_WRITE_REVIEW__ )
		{
			window.__JAVO_WRITE_REVIEW__ = true;
			this.init();
		}
	};

	javo_write_review.prototype = {

		constructor : javo_write_review

		, init : function()
		{
			var
				obj = this,
				el = obj.el,
				form = el.find( 'form' );

			obj.list_offset	= 0;

			form.removeClass( 'hidden' ).hide();

			this
				.setInputRatingFields();

			$( document )

				.on( 'click', this.selector, this.showWriteForm )
				.on( 'click', '#javo-detail-item-review-loadmore', this.getReviewLists() )
				.on( 'submit', form.selector, this.submit_rating() )
				.on( 'keypress', ".javo-detail-item-reply", this.appendReply() )
				.on( 'click', '.javo-detail-item-edit-comment', this.editComment() );

			if( $( "#javo-detail-item-review-container" ).length > 0 )
				$( "#javo-detail-item-review-loadmore" ).trigger( 'click' );

		}

		, showWriteForm : function( e )
		{
			var form = $( this ).find( "form" );
			form.slideDown( 'fast' );

			$( this )
				.removeClass( 'cursor-pointer')
				.off( 'click' )
				.find( '.lv-review-submit')
				.remove();
		}

		, setInputRatingFields : function( reset )
		{
			var element		= $('.jvfrm_spot_rat_star' );

			element.each(
				function(k, v)
				{
					if( ! reset )
					{
						$( this ).raty({
							starOff: lavaDirectoryReview.urlImage + 'star-off.png'
							, starOn: lavaDirectoryReview.urlImage + 'star-on.png'
							, starHalf: lavaDirectoryReview.urlImage + 'star-half.png'
							, half: true
							, width:150
							, scoreName: $(this).data('input-name')
							, score: function() {
								return $(this).attr('data-score');
							}
						});
					}else{
						$( this ).raty( 'reload' );
					}
				}
			);
			return this;
		}

		, setRatings : function()
		{
			var el	= $( "#javo-detail-item-review-container" );

			el
				.find( "[data-score]" )
				.raty({
					starOff		: lavaDirectoryReview.urlImage + 'star-off-s.png'
					, starOn	: lavaDirectoryReview.urlImage + 'star-on-s.png'
					, starHalf	:lavaDirectoryReview.urlImage +  'star-half-s.png'
					, half		: true
					, readOnly	: true
					, scoreName	: $(this).data('input-name')
					, score		: function() {
						return $(this).attr('data-score');
					}
				})
				.css( 'width', 'auto' );
		}

		, empty_field : function( elements )
		{
			var
				obj		= this
				output	= '';


			obj.el.find( "form [required]" ).each(
				function( i, k )
				{
					var value;

					if( typeof $( this ).data( 'input-name' ) != 'undefined'  ) {
						value = $( "[name='" + $( this ).data( 'input-name' ) + "']" ).val();
					}else{
						value = $( this ).val();
					}

					if( ! value )
						output += lavaDirectoryReview.strFillRaty.replace( /%s/g, $( this ).data('label') ) + "<br>";
				}
			);

			if( output ) {
				obj.message( {
					content		: output
					, button	: lavaDirectoryReview.strOK
				} );
				return false;
			}
			return true;
		}

		, submit_rating : function()
		{
			var obj				= this;

			return function( e ){
				e.preventDefault();
				var
					form			= $( this )
					, ajax_url		= obj.args.url
					, el			= $( "#javo-detail-item-review-container" )
					, param			= form.serialize()
					, param_meta	= $( "#javo-detail-item-review-parameter" );

				if( ! obj.empty_field() )
					return false;

				$.post(
					ajax_url
					, param
					, function( response )
					{
						if( response.state )
						{
							if( response.approve )
							{
								obj.message({ content: param_meta.find("[name='review_register_success']").val() });
								response.data.prepend = true;
								obj.list_offset = parseInt( obj.list_offset ) + 1;
								obj.add_listing( response.data );
								el.find( ".dismiss-ready" ).remove();
								obj.setReviewImageSlider();
							}else{
								obj.message({ content: lavaDirectoryReview.strPending });
							}
							obj.reset_review_form();

						}else{
							obj.message({ content: param_meta.find("[name='review_register_fail']").val() });
						}
					}
					, 'json'
				)
				.fail( function( xhr ) {
					console.log( xhr.responseText );
				} );
			}
		}

		, reset_review_form : function()
		{
			var
				obj					= this
				, image_container	= obj.el.find( ".comment_image_preview" );

			obj.el.find( "[name='comment_content']" ).val( null );

			obj.setInputRatingFields( true );
			image_container.html( false );

		}

		, setReviewImageSlider : function()
		{
			var el = $( "#javo-detail-item-review-container .javo-detail-item-review-inner" );
			el.each(
				function(){
					$(this).magnificPopup(
						{
							type		: 'image'
							, fixedContentPos : false
							, delegate	: 'div.javo-thb[href]'
							, gallery	: { enabled: true }
						}
					);
				}
			);
		}

		, add_listing : function( data )
		{
			var obj = this;
			var str	= $( "#javo-detail-item-review-template" ).html();
			var emp	= $( "#javo-detail-item-review-empty" ).html();
			var el	= $( "#javo-detail-item-review-container" );
			var edit_text;

			el.find( ".dismiss-ready" ).remove();

			if( data )
			{
				var
					edit_link			= ''
					, del_link			= ''
					, detail_ratings	= "";


				if( typeof data.is_author != "undefined" && data.is_author )
				{
					edit_link		+= "<a class=\"javo-detail-item-edit-comment\"" + " ";
					edit_link		+= "data-comment-id=\"" + data.id + "\" href=\"javascript:\">";
						edit_link		+= "<i class=\"fa fa-pencil\"></i>" + " ";
						edit_link		+= edit_text || "Edit";
					edit_link		+= "</a>";

					del_link		+= "<a class=\"javo-detail-item-delete-comment\"" + " ";
					del_link		+= "data-comment-id=\"" + data.id + "\" href=\"javascript:\">";
						del_link		+= "<i class=\"fa fa-pencil\"></i>" + " ";
						del_link		+= edit_text || "Delete";
					del_link		+= "</a>";
				}

				str = str.replace( /{comment-id}/g		, data.id || '' );
				str = str.replace( /{author}/g			, data.author || '' );
				str = str.replace( /{avatar}/g			, data.avatar || '' );
				str = str.replace( /{content}/g			, data.content || '' );
				str = str.replace( /{date}/g			, data.date || '' );
				str = str.replace( /{ratings}/g			, data.ratings || '' );
				str = str.replace( /{rating_sum}/g		, data.rating_sum || 0 );
				str = str.replace( /{rating_average}/g	, data.rating_average || 0 );
				str = str.replace( /{thumbnails}/g		, data.thumbnails || '' );
				str = str.replace( /{reviews}/g			, obj.add_listing_reply( data ) );
				str = str.replace( /{edit}/g			, edit_link );
				str = str.replace( /{del}/g				, del_link );

				if( typeof data.ratings == 'object' )
				{
					var dr_score = '';
					$.each(
						data.ratings
						, function( title, score )
						{
							dr_score		+= "<div class=\"\" style=\"width:350px;\">";
								dr_score		+= "<div class=\"inline-block\" style=\"width:150px; vertical-align:middle; word-break:break-all\">";
								dr_score		+= title;
								dr_score		+= "</div>";
								dr_score		+= "<div class=\"inline-block\" style=\"width:200px; vertical-align:middle\">";
								dr_score		+= "<div data-score=\"" + score + "\"></div>";
								dr_score		+= "</div>";
							dr_score		+= "</div>";
						}
					);
					detail_ratings += dr_score;
				}

				if( data.prepend ) {
					// el.prepend( str );
					$( str )
						.prependTo( el )
						.find( '[data-score]' )
						.css( 'cursor', 'pointer' )
						.popover(
							{
								content		: detail_ratings
								, html		: true
								, trigger	: 'hover'
								, placement	: 'top'
							}
						)
						.on(
							'shown.bs.popover'
							, function ()
							{
								$( this )
									.parent()
									.find( '.popover [data-score]' )
									.raty({
										starOff: lavaDirectoryReview.urlImage + 'star-off-s.png'
										, starOn: lavaDirectoryReview.urlImage + 'star-on-s.png'
										, starHalf: lavaDirectoryReview.urlImage + 'star-half-s.png'
										, half: true
										, width:150
										, readOnly	: true
										, score: function() {
											return $(this).attr('data-score');
										}
									});
							}
						);

				}else{

					// el.append( str );
					$( str )
						.appendTo( el )
						.find( '[data-score]' )
						.css( 'cursor', 'pointer' )
						.popover(
							{
								content		: detail_ratings
								, html		: true
								, trigger	: 'hover'
								, placement	: 'top'
							}
						)
						.on(
							'shown.bs.popover'
							, function ()
							{
								$( this )
									.parent()
									.find( '.popover [data-score]' )
									.raty({
										starOff: lavaDirectoryReview.urlImage + 'star-off-s.png'
										, starOn: lavaDirectoryReview.urlImage + 'star-on-s.png'
										, starHalf: lavaDirectoryReview.urlImage + 'star-half-s.png'
										, half: true
										, width:150
										, readOnly	: true
										, score: function() {
											return $(this).attr('data-score');
										}
									});
							}
						);

				}
			}else{
				el.append( emp );
			}

			$('[data-toggle="popover"]').popover();
		}

		, add_listing_reply : function( data )
		{
			var result = '';

			if( typeof data.reply == 'undefined' )
				return '';

			$.each(
				data.reply
				, function( i, k ){
					var str = $("#javo-detail-item-reply-template").html();
					str = str.replace( /{author}/g			, k.author || '' );
					str = str.replace( /{content}/g			, k.content || '' );
					str = str.replace( /{date}/g			, k.date || '' );
					result += str;
				}
			);
			return result;
		}

		, getReviewLists : function()
		{
			var
				obj = this,
				param_el = $( "#javo-detail-item-review-parameter" ),
				ajaxurl		= param_el.find( "[name='ajaxurl']" ).val(),
				param		= {
					action		: 'get_item_review'
					, nonce		: param_el.find( "[name='nonce']" ).val()
					, post_id	: param_el.find( "[name='post_id']").val()
				};

			return function( e )
			{
				e.preventDefault();

				var	el		= $( this );

				param.offset	= obj.list_offset;

				$.ajaxSetup({

					beforeSend : function() {
						el.button( 'loading' );
					}
					, complete : function() {
						el.button( 'reset' );
						obj.setRatings();
					}
				});

				$.post(
					ajaxurl
					, param
					, function( response ) {
						var _finish = response.finish || false;
						if( response.state ) {
							if( response.data.length > 0 ) {
								$.each( response.data, function( i, data ){
									obj.add_listing( data );
								});
								obj.list_offset += response.data.length;
							}
							obj.setReviewImageSlider();
						}else{
							obj.message({ content: 'Error' });
						}
						if( _finish ) {
							el.remove();
						}
					}
					, 'JSON'
				)

				.fail( function( xhr ) {
					console.log( xhr.responseText );
				} );
			}
		}

		, appendReply : function(){

			var obj = this, opt;
			return function( e ) {

				if( e.keyCode == 13 )
				{
					opt = {
						content		: $( this ).html()
						, parent	: $( this ).data( 'parent-id' )
					};
					obj.regsiterReply( this, opt );
				}
			};
		}

		, regsiterReply : function( el, opt )
		{
			var obj				= this;
			var param_el		= $( "#javo-detail-item-review-parameter" );
			var ajaxurl			= param_el.find( "[name='ajaxurl']" ).val();
			var param			= {
				action			: 'add_comment_reply'
				, nonce			: param_el.find( "[name='nonce']" ).val()
				, comment_id	: opt.parent
				, post_id		: param_el.find( "[name='post_id']").val()
				, content		: opt.content
			};

			if( $( el ).hasClass( 'disabled' ) )
				return false;

			$( el )
								.attr( 'contenteditable', false )
								.addClass( 'disabled' );

			$.post(
				ajaxurl
				, param
				, function( xhr )
				{
					var str;
					var data = xhr.data;

					if( xhr.state == 'success' )
					{
						if( xhr.approve ){
							str = $("#javo-detail-item-reply-template").html();
							str = str.replace( /{author}/g		, data.author || '' );
							str = str.replace( /{content}/g		, data.content || '' );
							str = str.replace( /{date}/g		, data.date || '' );
							$( ".javo-detail-item-reply-container[data-parent-id='" + opt.parent + "']" )
								.prepend( str );

							obj.message({ content: param_el.find( "[name='reply_register_success']" ).val() });

						}else{
							obj.message({ content: window.__javo_str_comment_pending });
						}
					}

					$( el )
								.empty()
								.attr( 'contenteditable', true )
								.removeClass( 'disabled' );
				}
				, 'json'
			);
		}

		, editComment : function()
		{
			var obj = this;

			return function( e )
			{
				e.preventDefault();

				var
					el				= $( this )
					, comment_id	= $( this ).data( "comment-id" )
					, parent		= $( this ).closest( ".javo-detail-item-review-inner" )
					, text_el		= parent.find( ".javo-detail-item-comment-content" )
					, param_el		= $( "#javo-detail-item-review-parameter" )
					, ajaxurl		= param_el.find( "[name='ajaxurl']" ).val();

				if( $( this ).hasClass( 'active' ) )
				{

					$.post(
						ajaxurl

						, {
							action		: 'javo_update_review'
							, comment	: comment_id
							, content	: text_el.html()
						}

						, function( response )
						{
							if( typeof response.error === "undefined" )
							{
								if( response.state !== "OK" ) {
									obj.message({ content: param_el.find( "[name='reply_modify_fail']" ).val(), delay : 5000 });
								}else{
									el.removeClass( 'active' );
									text_el
										.attr( "contenteditable" , false )
										.removeClass( "text-field" );
								}

							}else{
								obj.message({ content: response.error, delay : 5000 });
							}
						}
						, 'json'
					)
					.fail( function( xhr ){
						console.log( xhr.responseText );
					} );



				}else{
					$( this ).addClass( 'active' );
					text_el
						.attr( "contenteditable" , true )
						.addClass( "text-field")
						.focus();
				}
			}
		},

		message : function( objMsg, args ){

			if( typeof jQuery.jvfrm_spot_msg == 'function' ){
				if( args ){
					$.jvfrm_spot_msg( objMsg, args );
				}else{
					$.jvfrm_spot_msg( objMsg );
				}
			}else{
				alert( objMsg.content );
			}

		}
	};

	new javo_write_review( "#javo-review-form-container" );

} )( jQuery );