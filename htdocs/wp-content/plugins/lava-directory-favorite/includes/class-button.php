<?php
class lvDirectoryFavorite_button
{
	public $post;

	public $post_id = 0;

	public $save = 'Save';

	public $unsave = 'Unsave';

	public $dashboard = false;

	public $is_allow = false;

	public static $instance;

	public function __construct( $args=Array() ) {
		global $post;

		$arrOptions = wp_parse_args(
			$args,
			Array(
				'post_id' => 0,
				'save' => __( "Save", 'lvdr-favorite' ),
				'unsave' => __( "Unsave", 'lvdr-favorite' ),
				'show_count' => false,
				'show_add_text' => false,
				'class' => Array(),
			)
		);

		foreach( $arrOptions as $strKey => $strValue )
			$this->$strKey = $strValue;

		if( 0 === $this->post_id && isset( $post->ID ) )
			$this->post_id = $post->ID;

		if( lv_directory_favorite()->core->post_verify( $this->post_id ) )
			$this->is_allow = true;

		self::$instance = &$this;
	}

	public function getSaveCount( $post_id=0 ){
		if( !$post_id )
			$post_id = get_the_ID();
		return intVal( get_post_meta( $post_id, '_save_count', true ) );

	}

	public function classes( $classes=false){
		$arrClass	= $classes ? (Array)$classes : Array();
		$arrClass[] = 'favorite';
		$arrClass[] = 'lava_favorite';

		if( $this->dashboard )
			$arrClass[] = 'remove';

		$arrClass = wp_parse_args( $this->class, $arrClass );

		return join( ' ', $arrClass );
	}

	public function render(){
		$is_saved = lv_directory_favorite()->core->hasPostID( $this->post_id, true ) ? 'saved' : false;

		if( $this->show_add_text ) {
			$this->save = $this->show_add_text . $this->save;
			$this->unsave = $this->show_add_text . $this->unsave;
		}

		$strCount = $this->show_count ? sprintf( '(%d)', $this->getSaveCount( $this->post_id ) ) : false;

		$output_html = sprintf(
			'<a href="#" class="%1$s" data-save="%2$s" data-saved="%3$s" data-post-id="%4$s" data-show-count="%7$s">%5$s%6$s</a>',
			$this->classes( $is_saved ),
			$this->save,
			$this->unsave,
			$this->post_id,
			( ! $is_saved ? $this->save : $this->unsave ),
			$strCount,
			( $strCount ? 'true' : 'false' )
		);

		/*
		$output_html		= Array();
		$output_html[]	= "<a";
		$output_html[]	= "href=\"javascript:\"";
		$output_html[]	= 'class="' . $this->classes( $is_saved ) . '"';
		$output_html[]	= "data-save=\"{$this->save}\"";
		$output_html[]	= "data-saved=\"{$this->unsave}\"";
		$output_html[]	= "data-post-id=\"{$this->post_id}\">";
		$output_html[]	= $is_saved ? $this->unsave : $this->save;

		if( $this->show_count )
			$output_html[] = sprintf( '(%d)', $this->getSaveCount( $this->post_id ) );
		$output_html[]			= "</a>";
		$output_html			= join( ' ', $output_html );
		*/
		return $output_html;
	}

	public static function scripts(){
		$output_strings		= Array();
		$output_strings[]	= "<script type=\"text/javascript\">";

		// die( var_dump( self::$instance ) );

		$output_strings[]	= sprintf(
			'var lava_favorite_params = %s;',
			json_encode(
				Array(
					'url' => admin_url( 'admin-ajax.php' ),
					'user' => get_current_user_id(),
					'action' => 'lava_' . lv_directory_favorite()->getSlug(). '_add_favorite',
					'str_nologin' => __( "Please login", 'lvdr-favorite' ),
					'str_error' => __( "Error", 'lvdr-favorite' ),
					'str_fail' => __( "Failed", 'lvdr-favorite' ),
					'is_dashboard' => self::$instance->dashboard,
					'loading' => lv_directory_favorite()->images_url . 'loading.gif',
				)
			)
		);
		$output_strings[]	= "</script>";
		echo @implode( "\n", $output_strings );
		?>

		<script type="text/javascript">
		jQuery( function( $ ) {
			if( typeof jQuery.fn.lava_favorite != 'undefined' ) {
				$( document ).on( 'lava:favorite', function() {
					$( "a.lava_favorite" ).each(
						function(){
							lava_favorite_params = $.extend( true, {}, lava_favorite_params, {
								str_save	: $( this ).data( 'saved' ),
								str_unsave	: $( this ).data( 'save' ),
								before		: function(){
									if( ! lava_favorite_params.user ){
										if( typeof $.fn.modal != 'undefined' || $( '#login_panel' ).length ){
											$( '#login_panel' ).modal();
										}else{
											alert( lava_favorite_params.str_nologin );
										}
										return false;
									};
									return;
								}
							} );
							if( lava_favorite_params.is_dashboard ){
								$( "a.lava_favorite", this ).lava_favorite(
									lava_favorite_params,
									function(){
										if( $( this ).hasClass( 'remove' ) ){
											$( this ).closest( 'tr' ).remove();
										}
									}
								);
							}else{
								$( "a.lava_favorite", this ).lava_favorite( lava_favorite_params );
							}

						}
					);
				} );
				$.ajaxSetup( {
					complete : function() {
						$( document ).trigger( 'lava:favorite' );
					}
				} );
				$( document ).trigger( 'lava:favorite' );
			}
		} );
		</script>
		<?php
	}

	public function output( $_echo=true ){

		if( ! $this->is_allow )
			return false;

		add_action( 'wp_footer', Array( __CLASS__, 'scripts' ) );

		// Output
		if( $_echo )
			echo $this->render();

		return $this->render();
	}
}