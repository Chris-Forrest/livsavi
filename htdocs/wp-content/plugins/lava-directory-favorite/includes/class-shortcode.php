<?php
class Lava_Directory_Favorite_Shortcodes
{
	public $post_type ='';

	public function __construct() {
		$this->post_type = lv_directory_favorite()->getSlug();
		add_shortcode( 'lava_' . $this->post_type . '_favorites', Array( $this, 'favorite_lists' ) );
	}

	public function favorite_lists( $attr, $content='' ) {
		ob_start();
		require_once dirname( __FILE__ ). '/html-favorites.php';
		return ob_get_clean();
	}
}