<table width="100%" cellPadding="0" cellSpacing="0" class="lava-wish-list">
	<thead>
		<tr>
			<th width="8%"></th>
			<th width="50%"><?php _e('Title', 'lvdr-favorite'); ?></th>
			<th width="30%"><?php _e('Posted Date', 'lvdr-favorite'); ?></th>
			<th width="12%"><?php _e('Action', 'lvdr-favorite'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	$lava_this_current_user_favorites = lv_directory_favorite()->core->getFavorites();
	if( !empty($lava_this_current_user_favorites) ){
		foreach( $lava_this_current_user_favorites as $favorite ){
			$lava_this_post = empty( $favorite['post_id'] ) ? null : get_post($favorite['post_id']);
			if( $lava_this_post != null )
			{
				?>
				<tr>
					<td>
						<a href="<?php echo get_permalink($lava_this_post->ID);?>">
							<?php
							if(
								isset( $lava_this_post->ID ) &&
								has_post_thumbnail($lava_this_post->ID) )
							{
								echo get_the_post_thumbnail($lava_this_post->ID, 'lava-tiny', Array('class'=>'img-responsive'));
							}
							else
							{
								//printf('<img src="%s" class="img-cycle" style="width:100%%;">', $lava_tso->get('no_image', JAVO_IMG_DIR.'/no-image.png'));
							} ?>
						</a>
					</td>
					<td valign="middle">
						<a href="<?php echo get_permalink($lava_this_post->ID);?>">
							<?php echo isset( $lava_this_post->post_title ) ? $lava_this_post->post_title : __("No Title", 'lvdr-favorite');?>
						</a>
					</td>
					<td align="center">
						<?php echo date('Y-m-d h:i:s', strtotime($favorite['save_day']));?>
					</td>
					<td class="btn-group btn-group-justified">
						<?php
						$objButton = new lvDirectoryFavorite_button(
							Array(
								'post_id'	=> $lava_this_post->ID,
								'unsave'	=> __( "Delete", 'lvdr-favorite' ),
								'dashboard'	=> true
							)
						);
						$objButton->output( true );?>
					</td>
				</tr>
				<?php
			} // End If
		} // End Foreach
	}else{?>
		<tr><td colspan="4"><?php _e('You have not saved any favorite items. Please press the Save button on the list.', 'lvdr-favorite');?></td></tr>
	<?php };?>
	</tbody>
</table>