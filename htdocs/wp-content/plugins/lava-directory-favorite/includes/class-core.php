<?php
class Lava_directory_favorite_CORE
{
	const FAVORITE_FORMAT = '_favorites';
	const COUNT_FORMAT = '_save_count';
	public $logged = false;
	public $post_type='';

	public function __construct(){

		$this->post_type = lv_directory_favorite()->getSlug();

		/** Ajax Hook */
		add_action( "wp_ajax_lava_{$this->post_type}_add_favorite", Array( $this, 'ajax_favorite_callback' ) );
		add_action( "wp_ajax_nopriv_lava_{$this->post_type}_add_favorite", Array( $this, 'ajax_favorite_callback' ) );

		/** Append Hook */
		add_action( 'lava_single_meta_append_contents'	, Array( $this, 'appned_favorite' ) );

		add_filter( 'lava_multiple_listing_contents'	, Array( $this, 'lava_map_append_favorite' ), 10, 2 );

		/* Variable Initialize */
		$this->logged = is_user_logged_in();
	}

	public function getFavorites( $user_id=0 ){
		if( $user_id === 0 )
			$user_id = get_current_user_id();
		return (Array) get_user_meta( $user_id, self::FAVORITE_FORMAT, true );
	}

	public function setFavorites( $arrFavorites=Array(), $user_id=0 ){
		if( $user_id === 0 )
			$user_id = get_current_user_id();
		return update_user_meta( $user_id, self::FAVORITE_FORMAT, $arrFavorites );
	}

	public function post_verify( $post_id=0 ){
		$objPost = WP_Post::get_instance( $post_id );
		if( $objPost instanceof WP_Post )
			if( $objPost->post_type == lv_directory_favorite()->getSlug() )
				return true;
		return false;
	}

	public function hasPostID( $post_id=0, $is_true=TRUE, $strReturn=FALSE ){
		$arrFavorites = $this->getFavorites();
		if( $this->logged ){
			if( 0 < (int) $post_id ){
				if( !empty( $arrFavorites ) ) : foreach( $arrFavorites as $arrFavorite ) {
					if( isset( $arrFavorite[ 'post_id' ] ) && $arrFavorite[ 'post_id' ] ==  $post_id )
						return $is_true;
				} endif;
			}
		}
		return $strReturn;
	}

	public function del( $post_id ){
		$arrFavorites = $this->getFavorites();
		if( $this->logged ){
			if( 0 < (int) $post_id ){
				if( !empty( $arrFavorites ) ) : foreach( $arrFavorites as $intIndex => $arrFavorite ) {
					if( isset( $arrFavorite[ 'post_id' ] ) && $arrFavorite[ 'post_id' ] ==  $post_id )
						unset( $arrFavorites[ $intIndex ] );
				} endif;
			}
		}
		return $arrFavorites;
	}

	public function savePostCount( $post_id=0 ) {
		$intCount = intVal( get_post_meta( $post_id, self::COUNT_FORMAT, true ) );
		$intCount++;
		update_post_meta( $post_id, self::COUNT_FORMAT, $intCount );
		return $intCount;
	}

	public function unsavePostCount( $post_id=0 ) {
		$intCount = intVal( get_post_meta( $post_id, self::COUNT_FORMAT, true ) );
		if( 0 < $intCount )
			$intCount--;
		update_post_meta( $post_id, self::COUNT_FORMAT, $intCount );
		return $intCount;
	}

	public function ajax_favorite_callback()
	{
		$lava_query = new lava_ARRAY( $_POST );
		$arrFavorites = $this->getFavorites();

		$arrResult = Array(
			'return'		=> 'fail',
		);
		$post_id = (int)$lava_query->get( 'post_id', 0 );

		$reg = $lava_query->get( 'reg', false );

		if( 0 < $post_id ){
			if( $reg ){
				if( !$this->hasPostID( $post_id ) && $this->post_verify( $post_id ) ){
					$arrFavorites[] = Array(
						'post_id'	=> $post_id,
						'save_day'	=> date('Y-m-d h:i:s'),
					);
					$arrResult[ 'count' ] = $this->savePostCount( $post_id );
				}
			}else{
				$arrFavorites = $this->del( $post_id );
				$arrResult[ 'count' ] = $this->unsavePostCount( $post_id );
			};
			$this->setFavorites( $arrFavorites );
			$arrResult[ 'return' ] = 'success';
		}
		die( json_encode( $arrResult ) );
	}

	public function appned_favorite( $post ) {

		if( lv_directory_favorite()->admin->getOption( 'favorite_addon' ) != 'use' )
			return;

		$objButton = new lvDirectoryFavorite_button(
			Array(
				'post_id'	=> $post->ID
			)
		);
		echo "<li>" . $objButton->output( false ) . "</li>";
	}

	public function lava_map_append_favorite( $args, $post_id=false ){

		if( intVal( $post_id ) > 0 )
			$args[ 'favorite']	=  $this->hasPostID( $post_id, ' saved');
		return $args;
	}
}