<?php

/*
 *
 *	post_id	: (int) Post ID
 *	echo	: (Boolean) Print
 *	option	: (Array) Option Parametter
 */
function lava_favorite_button( $options=Array(), $_echo=true )
{
	$objButton = new lvDirectoryFavorite_button( $options );
	$objButton->output( $_echo );
}