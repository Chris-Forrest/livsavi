<?php

if( ! defined( 'ABSPATH' ) )
	die();

class Lava_Directory_Favorite_Admin extends Lava_Directory_Favorite
{
	const GROUPKEY_FORMAT = 'lava_%s_favorite';

	private $parent				= null;
	private $optionGroup		= false;

	public function __construct()
	{
		$this->optionGroup = sprintf( self::GROUPKEY_FORMAT, $this->post_type );
		$this->options = get_option( self::getOptionFieldName() );
		$this->admin_hooks();
	}

	public static function getOptionFieldName(){
		return sprintf( self::GROUPKEY_FORMAT, lv_directory_favorite()->getSlug() ) . '_field';
	}

	public static function getOption( $key, $default=false ) {
		if( array_key_exists( $key, (Array) lv_directory_favorite()->admin->options ) )
			if( $value = lv_directory_favorite()->admin->options[ $key ] )
				$default = $value;
		return $default;
	}

	public function admin_hooks() {
		add_action( 'admin_init', Array( $this, 'register_option' ) );
		add_filter( "lava_{$this->post_type}_admin_tab"	, Array( $this, 'add_addons_tab' ), 20 );
	}

	public function register_option() {
		register_setting( $this->optionGroup, self::getOptionFieldName() );
		/* die(
			var_dump( $this->optionGroup, self::getOptionFieldName() )
		); */
	}

	public function add_addons_tab( $args )
	{
		return wp_parse_args(
			Array(
				'favorite_addon'	=> Array(
					'label'			=> __( "Favorite", 'Lavocode' ),
					'group'			=> $this->optionGroup,
					'file'			=> lv_directory_favorite()->template_path . '/template-admin.php'
				)
			), $args
		);
	}
}