<?php
/**
 * Plugin Name: Lava Directory Favorite Addon
 * Description: Lava Directory Favorite Addon
 * Version: 1.0.2
 * Author: Lava
 * Author URI: http://URI_Of_The_Plugin_Author
 * Text Domain: lvdr-favorite
 * Domain Path: /languages/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: A short license name. Example: GPL2
 */

if( ! defined( 'ABSPATH' ) )
	die();

if( ! class_exists( 'Lava_Directory_Favorite' ) ) :
	class Lava_Directory_Favorite
	{
		protected $post_type = 'lv_listing';

		private static $instance;

		public function __construct( $file )
		{
			$this->file				= $file;
			$this->folder			= basename( dirname( $this->file ) );
			$this->path				= dirname( $this->file );
			$this->template_path	= trailingslashit( $this->path ) . 'templates';
			$this->assets_url		= esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
			$this->images_url		= esc_url( trailingslashit( $this->assets_url ) . 'images/' );

			$this->load_files();
			$this->register_hooks();

			do_action( 'Lava_Directory_Favorite_loaded' );
		}

		public function getSlug(){
			return $this->post_type;
		}

		public function load_files() {
			require_once 'includes/class-admin.php';
			require_once 'includes/class-core.php';
			require_once 'includes/class-shortcode.php';
			require_once 'includes/class-button.php';
			require_once 'includes/function-template.php';
		}

		public function register_hooks() {
			add_action( 'init'					, Array( $this, 'register_shortcodes' ), 30 );
			add_action( 'wp_enqueue_scripts'	, Array( $this, 'load_scripts' ) );
			load_plugin_textdomain('lvdr-favorite', false, $this->folder . '/languages/');
		}

		public function register_shortcodes() {
			$this->admin		= new Lava_Directory_Favorite_Admin;
			$this->shortcode	= new Lava_Directory_Favorite_Shortcodes;
			$this->core			= new Lava_directory_favorite_CORE;
		}

		public function load_scripts()
		{
			$output_less		= Array();
			$output_less[]		= "<link";
			$output_less[]		= "rel=\"stylesheet/less\"";
			$output_less[]		= "type=\"text/css\"";
			$output_less[]		= sprintf( "href=\"%s\"", "{$this->assets_url}css/wishlist.less" );
			$output_less[]		= ">";
			echo @implode( ' ', $output_less );

			wp_register_script( 'lava-wish-list-script', "{$this->assets_url}js/jquery.favorite.js", Array( 'jquery' ) );
			wp_enqueue_script( 'lava-wish-list-script' );
		}

		public static function get_instance( $file )
		{
			if( null === self::$instance )
				self::$instance = new self( $file );

			return self::$instance;
		}
	}
endif;

if( !function_exists( 'lv_directory_favorite' ) ):
	function lv_directory_favorite(){
		return Lava_Directory_Favorite::get_instance( __FILE__ );
	}
	lv_directory_favorite();
endif;