<table class="form-table">
	<tbody>

		<tr valign="top">
			<th scope="row"><?php _e( "Favorites Settings", 'lvdr-favorite' ); ?></th>
			<td>
				<table class="widefat">
					<tbody>
						<tr valign="top">
							<td width="1%"></td>
							<th><?php _e( "Favorites Addons", 'lvdr-favorite' ); ?></th>
							<td>
								<?php
								foreach(
									Array(
										'use'	=> __( "Enable", 'lvdr-favorite' ),
										''		=> __( "Disable", 'lvdr-favorite' )
									)
									as $value => $label
								){
									printf( "
										<label>
											<input type=\"radio\" name=\"%s\" value=\"{$value}\" %s>
											%s
										</label> &nbsp;",
										lv_directory_favorite()->admin->getOptionFieldName() . '[favorite_addon]',
										checked( $value == lv_directory_favorite()->admin->getOption( 'favorite_addon' ), true, false ),
										$label
									);
								} ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">&nbsp;</th>
			<td>
				<table class="widefat">
					<tbody>
						<tr valign="top">
							<td width="1%"></td>
							<th><?php _e( "Favorite Shortcode", 'lvdr-favorite' ); ?></th>
							<td>
								<p>
									<strong><?php _e( "if you want to add as a shortcode.", 'lvdr-favorite' ); ?></strong>
									<div><code>[lava_directory_claim name="<strong>button</strong>" label="<strong>Claim</strong>" icon="<strong>icon-class</strong>"]</code></div><br>
								</p>
								<p>
									<strong><?php _e( "If you want to add in php as a function.", 'lvdr-favorite' ); ?></strong>
									<?php
									echo '<pre>'. join( '<br>',
										Array(
											'&lt;?php',
											'if( function_exists( "lava_directory_claim_button" ) ){',
											"\tlava_directory_claim_button(Array(",
												"\t\t'class'	=> 'btn btn-primary btn-block',",
												"\t\t'label'		=> 'Claim',",
												"\t\t'icon'		=> 'fa fa-send-o'",
											"\t));",
											'}?>',
										)
									) . '</pre>';  ?>
								</p>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
