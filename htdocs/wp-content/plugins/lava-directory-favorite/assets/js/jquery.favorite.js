;( function($){
	var lava_favorite_func = function( data, el, callback )
	{
		var _DEFAULT		= {
			url				: null
			, before		: null
			, user			: 0
			, action		: 'lava_add_favorite'
			, str_nologin	: "Please login"
			, str_save		: "Save"
			, str_unsave	: "UnSave"
			, str_error		: "Server Error!"
			, str_fail		: "favorite regist fail."
			, mypage		: false
			, loading		: false
		};
		this.attr			= $.extend( true, {}, _DEFAULT, data );
		this.callback		= callback;
		this.el				= el;
		this.init();
	}

	lava_favorite_func.prototype = {

		constructor : lava_favorite_func

		, init: function()
		{
			var obj					= this;
			obj.__INSTANCE__		= true;

			// Events
			$( document )		.on( 'click', obj.el, obj.favorite() );
		}
		, action: function( callback )
		{
			if( typeof callback == 'function' )
			{
				if( false == callback() )
				{
					return false;
				}
			}
			return;
		}

		, user_check: function( user_id )
		{
			if( parseInt( user_id ) > 0 )
			{
				return true;
			}
			// $.lava_msg({ content: attr.str_nologin, delay:5000 });

			alert( attr.str_nologin );
			return false;
		}

		, button_block: function( el, onoff )
		{
			if( onoff ) {
				el.addClass( 'disabled' ).prop( 'disabled', true );
			}else{
				el.removeClass( 'disabled' ).prop( 'disabled', false );
			}

			if( this.attr.loading ){
				if( onoff ){
					$( "<img class='lv-favroite-loading-image' src='" + this.attr.loading + "'>" ).appendTo( el );
				}else{
					$( 'img.lv-favroite-loading-image' ).remove();
				}
			}

		}

		, favorite : function()
		{
			var obj			= this;
			return function( e )
			{
				e.preventDefault();

				var $this	= $( this );
				var is_save	= $this.hasClass( 'saved' ) ? null : true;
				var attr	= obj.attr;

				if( false == obj.action( attr.before ) ){ return false; };

				current_user = obj.user_check( attr.user );

				if( ! current_user || $( this ).hasClass( 'disabled' ) ){ return false; }
				obj.button_block( $( this ), true );

				$.post(
					attr.url
					, { post_id: $( this ).data('post-id'), reg: is_save, action: attr.action }
					, function( response )
					{

						if( response.return == 'success' )
						{
							var
								intCount = response.count || 0,
								strCount = '';
							if( $this.data( 'show-count' ).toString() == "true" ) {
								strCount = '(' + intCount + ')';
							}
							if( $this.hasClass("saved")){

								$this.removeClass("saved");

								if( ! $this.data('no-swap') ){
									$this.html(attr.str_unsave + strCount );
								}
								// $.lava_msg({ content: attr.str_unsave, delay:800, close:false});
								//alert( attr.str_unsave );


							}else{

								$this.addClass("saved");

								if( ! $this.data('no-swap') ){
									$this.html(attr.str_save + strCount);
								}
								// $.lava_msg({ content: attr.str_save, delay:800, close:false });
								// alert( attr.str_save );

							}
							if( typeof obj.callback == 'function' ){
								obj.callback.call( $this );
							}
						}else{
							// $.lava_msg({ content: attr.str_fail, delay:5000 });
							alert( attr.str_fail );
						}
					}
					, 'json'
				)
				.fail( function( response )
				{
					console.log( "favorite Error: " + response.responseText );
					// $.lava_msg({ content: attr.str_error, delay:5000 });
					alert( attr.str_error );
				})
				.always( function()
				{
					obj.button_block( $this, false );
				});
			}
		}
	};

	jQuery.fn.lava_favorite = function(o, p) {
		new lava_favorite_func( o, $( this ).selector, p );
	}

})(jQuery);