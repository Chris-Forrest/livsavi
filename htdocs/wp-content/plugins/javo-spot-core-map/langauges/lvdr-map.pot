#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-04-07 07:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: includes/class-admin.php:84 includes/class-admin.php:262
#: includes/class-admin.php:287 templates/template-map-module-selector.php:5
#: templates/template-map-template-settings.php:96
msgid "Map"
msgstr ""

#: includes/class-admin.php:257 includes/class-admin.php:280
msgid "Map / List ?"
msgstr ""

#: includes/class-admin.php:263 includes/class-admin.php:288
msgid "Archive"
msgstr ""

#: templates/template-map-module-selector.php:8
#: templates/template-map-template-settings.php:97
msgid "List"
msgstr ""

#: templates/template-map-module-selector.php:14
msgid "Select Module"
msgstr ""

#: templates/template-map-module-selector.php:30
msgid "Column"
msgid_plural "Columns"
msgstr[0] ""
msgstr[1] ""

#: templates/template-map-module-selector.php:36
msgid "Visible"
msgstr ""

#: templates/template-map-module-selector.php:37
msgid "HIdden"
msgstr ""

#: templates/template-map-module-selector.php:78
msgid "Max Excerpt Length"
msgstr ""

#: templates/template-map-module-selector.php:84
msgid "Blank is unlimited. 0 is hide excerpt."
msgstr ""

#: templates/template-admin-order-selector.php:3
msgid "Display featured listings first"
msgstr ""

#: templates/template-admin-order-selector.php:4
#: templates/template-admin-order-selector.php:10
#: templates/template-map-template-settings.php:76
#: templates/template-map-template-settings.php:86
#: templates/template-map-template-settings.php:306
#: templates/template-map-template-settings.php:335
#: templates/template-map-template-settings.php:353
msgid "Enable"
msgstr ""

#: templates/template-admin-order-selector.php:5
#: templates/template-admin-order-selector.php:11
#: templates/template-map-template-settings.php:77
#: templates/template-map-template-settings.php:87
#: templates/template-map-template-settings.php:285
#: templates/template-map-template-settings.php:305
#: templates/template-map-template-settings.php:354
msgid "Disable"
msgstr ""

#: templates/template-admin-order-selector.php:9
msgid "Random listing display"
msgstr ""

#: templates/template-map-template-settings.php:5
msgid "Panel Position"
msgstr ""

#: templates/template-map-template-settings.php:7
msgid "Right ( Default )"
msgstr ""

#: templates/template-map-template-settings.php:8
msgid "Left"
msgstr ""

#: templates/template-map-template-settings.php:9
msgid "Top-Bottom"
msgstr ""

#: templates/template-map-template-settings.php:19
msgid "Panel Width"
msgstr ""

#: templates/template-map-template-settings.php:21
msgid "Narrow (433px) - 1 col"
msgstr ""

#: templates/template-map-template-settings.php:22
msgid "Middle (700px) - 2 cols"
msgstr ""

#: templates/template-map-template-settings.php:23
msgid "Wide (935px) <br /> - 3 cols"
msgstr ""

#: templates/template-map-template-settings.php:24
msgid "wide (1200px) <br /> - 4 cols"
msgstr ""

#: templates/template-map-template-settings.php:32
msgid "Display Panel or Content"
msgstr ""

#: templates/template-map-template-settings.php:34
msgid "Panel (Default)"
msgstr ""

#: templates/template-map-template-settings.php:35
msgid "Content"
msgstr ""

#: templates/template-map-template-settings.php:42
msgid "Search Filter Position"
msgstr ""

#: templates/template-map-template-settings.php:44
msgid "On Panel (Default)"
msgstr ""

#: templates/template-map-template-settings.php:45
msgid "Upper Map"
msgstr ""

#: templates/template-map-template-settings.php:50
msgid "Filter Type"
msgstr ""

#: templates/template-map-template-settings.php:52
#: templates/template-map-template-settings.php:428
msgid "Default"
msgstr ""

#: templates/template-map-template-settings.php:53
msgid "One-Line Type"
msgstr ""

#: templates/template-map-template-settings.php:64
msgid "Display Search Form"
msgstr ""

#: templates/template-map-template-settings.php:66
msgid "Show (Default)"
msgstr ""

#: templates/template-map-template-settings.php:67
msgid "Hide"
msgstr ""

#: templates/template-map-template-settings.php:74
msgid "Show Map/List Switcher"
msgstr ""

#: templates/template-map-template-settings.php:84
msgid "LIST - Display Filter"
msgstr ""

#: templates/template-map-template-settings.php:94
msgid "Initial display - MAP or LIST first"
msgstr ""

#: templates/template-map-template-settings.php:153
msgid "Template Layout SETTING"
msgstr ""

#: templates/template-map-template-settings.php:157
msgid "Search form background color ( Only One-Line Type )"
msgstr ""

#: templates/template-map-template-settings.php:165
msgid "Markers (Pin / Icon) SETTING"
msgstr ""

#: templates/template-map-template-settings.php:169
msgid "Listing hover effect"
msgstr ""

#: templates/template-map-template-settings.php:176
msgid "Type 1 (Default - No movement) "
msgstr ""

#: templates/template-map-template-settings.php:177
msgid "Type 2 (Auto move to the listing)"
msgstr ""

#: templates/template-map-template-settings.php:178
msgid "Type 3 (More button)"
msgstr ""

#: templates/template-map-template-settings.php:189
msgid "Map Marker (Pin/Icon) Setup"
msgstr ""

#: templates/template-map-template-settings.php:196
msgid "Category Marker Icons"
msgstr ""

#: templates/template-map-template-settings.php:197
msgid "Default Marker Icons"
msgstr ""

#: templates/template-map-template-settings.php:208
msgid ""
"Default map marker<br /><small>(For default marker icons and Type2)</small>"
msgstr ""

#: templates/template-map-template-settings.php:213
#: templates/template-map-template-settings.php:233
msgid "Marker Selector"
msgstr ""

#: templates/template-map-template-settings.php:213
#: templates/template-map-template-settings.php:233
msgid "Select"
msgstr ""

#: templates/template-map-template-settings.php:215
#: templates/template-map-template-settings.php:235
msgid "Marker Select"
msgstr ""

#: templates/template-map-template-settings.php:218
#: templates/template-map-template-settings.php:238
msgid "Delete"
msgstr ""

#: templates/template-map-template-settings.php:220
msgid "Blank is marker image as theme settings"
msgstr ""

#: templates/template-map-template-settings.php:228
msgid ""
"Hover map marker<br /><small>(For Type2 : default marker icons only)</small>"
msgstr ""

#: templates/template-map-template-settings.php:247
msgid ""
"Zoom Level (when mouse over a list)<small>(For Type2 : default marker icons "
"only)</small>"
msgstr ""

#: templates/template-map-template-settings.php:251
#: templates/template-map-template-settings.php:325
msgid "( 0 or Blank is OFF. 1 ~ 24 (Higher is closer) "
msgstr ""

#: templates/template-map-template-settings.php:278
msgid "Hover Marker Animation ( Hover on lists )"
msgstr ""

#: templates/template-map-template-settings.php:286
msgid "Drop"
msgstr ""

#: templates/template-map-template-settings.php:287
msgid "Bounce"
msgstr ""

#: templates/template-map-template-settings.php:293
msgid "( For Type2 : when a listing is hovered )"
msgstr ""

#: templates/template-map-template-settings.php:298
msgid "Initial Marker Animation"
msgstr ""

#: templates/template-map-template-settings.php:312
msgid "( Drop animation after filtering or loading )"
msgstr ""

#: templates/template-map-template-settings.php:317
msgid "Cluster / Zoom SETTING"
msgstr ""

#: templates/template-map-template-settings.php:321
msgid "Initial Map Zoom Level"
msgstr ""

#: templates/template-map-template-settings.php:330
msgid "Turn on 'My Current Location' as default"
msgstr ""

#: templates/template-map-template-settings.php:335
msgid "Disable (Default)"
msgstr ""

#: templates/template-map-template-settings.php:340
msgid ""
"(It will get visitors current location when this page loads. This feature "
"reqires SSL)"
msgstr ""

#: templates/template-map-template-settings.php:346
msgid "Marker Cluster ( Grouping )"
msgstr ""

#: templates/template-map-template-settings.php:361
msgid "For Listing hover effect Type 2, you should disalbe. "
msgstr ""

#: templates/template-map-template-settings.php:366
msgid "Marker Cluster ( Grouping ) Radius"
msgstr ""

#: templates/template-map-template-settings.php:374
msgid "Search Maximum Distance Radius Range"
msgstr ""

#: templates/template-map-template-settings.php:378
msgid "It`s for search distance or geolocation radius. "
msgstr ""

#: templates/template-map-template-settings.php:383
msgid "Number of posts to list ( Number of posts to load )"
msgstr ""

#: templates/template-map-template-settings.php:387
msgid "( 0 or Blank is 10 posts )"
msgstr ""

#: templates/template-map-template-settings.php:393
msgid "Map Primary Color / Background Color"
msgstr ""

#: templates/template-map-template-settings.php:398
msgid "Blank, Clear Color will be back to normal (default) map color"
msgstr ""

#: templates/template-map-template-settings.php:405
msgid "Advanced Google Map Styles"
msgstr ""

#: templates/template-map-template-settings.php:412
#, php-format
msgid ""
"Please <a href=\"%1$s\" target=\"_blank\">click here</a> to create your own "
"stlye and paste json code here."
msgstr ""

#: templates/template-map-template-settings.php:417
msgid "This code will overwritten map color setting"
msgstr ""

#: templates/template-map-template-settings.php:424
msgid "Distance Unit"
msgstr ""

#: templates/template-map-template-settings.php:428
msgid "KM"
msgstr ""

#: templates/template-map-template-settings.php:430
msgid "Mile"
msgstr ""

#: templates/template-map-template-settings.php:437
msgid "Listing Module setting"
msgstr ""

#: templates/template-map-template-settings.php:443
msgid "Module Display Rating Type"
msgstr ""

#: templates/template-map-template-settings.php:450
msgid "Stars ( default )"
msgstr ""

#: templates/template-map-template-settings.php:451
msgid "Numeric"
msgstr ""

#: templates/template-map-template-settings.php:452
msgid "Disabled"
msgstr ""

#: templates/template-part-module8-hover.php:2
msgid "Move"
msgstr ""

#: templates/template-map-preview-panel.php:18
msgid "condition"
msgstr ""

#: templates/template-map-preview-panel.php:21
#: templates/template-map-preview-panel.php:76
msgid "Description"
msgstr ""

#: templates/template-map-preview-panel.php:34
msgid "Job condition"
msgstr ""

#: templates/template-map-preview-panel.php:41
msgid "Category"
msgstr ""

#: templates/template-map-preview-panel.php:45
msgid "Location"
msgstr ""

#: templates/template-map-preview-panel.php:50
msgid "Salary"
msgstr ""

#: templates/template-map-preview-panel.php:57
msgid "Recruit By"
msgstr ""

#: templates/template-map-preview-panel.php:61
msgid "How many"
msgstr ""

#: templates/template-map-preview-panel.php:65
msgid "Contact Type"
msgstr ""

#: templates/template-map-preview-panel.php:136
msgid "See Detail Page"
msgstr ""

#: templates/template-map-preview-panel.php:142
msgid "Save List"
msgstr ""

#: templates/template-admin-position-selector.php:4
msgid "Map position setting"
msgstr ""

#: templates/template-admin-position-selector.php:7
msgid "Initial position of this map"
msgstr ""

#: templates/template-admin-position-selector.php:19
msgid "(Blank or 0 : Auto detect and FitBound from google API)"
msgstr ""

#: templates/template-admin-position-selector.php:20
msgid ""
"If Initial Map Zoom Level is not set, location will be changed but it will "
"be showing whole map."
msgstr ""

#: templates/template-admin-filter-settings.php:4
msgid "Filter Setting"
msgstr ""

#: templates/template-admin-filter-settings.php:8
msgid "Amenities"
msgstr ""

#: templates/template-admin-filter-settings.php:13
msgid "OR (Default)"
msgstr ""

#: templates/template-admin-filter-settings.php:17
msgid "And"
msgstr ""

#. Name of the plugin
msgid "Javo Spot Core Map Addons"
msgstr ""

#. Description of the plugin
msgid ""
"This plugin is requested for javo spot wordpress theme. it loads shortcodes "
"and some custom code for javo spot theme."
msgstr ""

#. Author of the plugin
msgid "Javo Themes"
msgstr ""

#. Author URI of the plugin
msgid "http://javothemes.com/spot/"
msgstr ""
