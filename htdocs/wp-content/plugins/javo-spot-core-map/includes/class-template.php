<?php
class Javo_Spot_Core_Map_Template extends Javo_Spot_Core_Map
{
	public function __construct() {
		$this->register_hooks();
	}

	public function register_hooks() {

		$strPrefix		= 'jvfrm_spot_' . self::$instance->post_type;

		add_filter( 'jvfrm_spot_map_class', Array( $this, 'custom_map_style' ), 10, 2 );
		add_filter( $strPrefix . '_map_list_content_column_class', Array( $this, 'custom_list_content_column' ), 10, 3 );
		add_filter( $strPrefix . '_map_switcher_value', Array( $this, 'arichive_map_switch_value' ) );
		add_filter( $strPrefix . '_custom_map_css_rows', Array( $this, 'custom_map_template_style' ), 10, 2 );

		// Show / Hide List Panel
		add_filter( $strPrefix . '_map_lists_output_visible', Array( $this, 'display_the_content_output' ), 10, 2 );

		// Output The Contents After Map
		add_action( $strPrefix . '_map_container_after', Array( $this, 'map_template_the_content' ) );

		// Map-template.js Params
		add_filter( $strPrefix . '_map_params', Array( $this, 'map_template_params' ), 10, 3 );

		// Preview Panel
		// add_filter( $strPrefix . '_map_wrap_after', Array( $this, 'add_preview_panel' ) );

		add_filter( 'jvfrm_spot_template_map_module_options', array( $this, 'checkParentPage' ), 10, 2 );
		add_action( 'jvfrm_spot_module_hover_content', Array( $this, 'addMarkerMoveButton' ), 10, 2 );

	}

	public function custom_map_style( $classes, $post_id ) {

		if( $thisValue = get_post_meta( $post_id, '_map_type', true ) )
			$classes[]	= $thisValue;

		if( $thisValue = get_post_meta( $post_id, '_map_filter_position', true ) )
			$classes[]	= $thisValue;

		if( $thisValue = get_post_meta( $post_id, '_panel_position', true ) )
			$classes[]	= $thisValue;

		if( $thisValue = get_post_meta( $post_id, '_switch', true ) )
			$classes[]	= 'hide-switcher';

		if( 'hide' == get_post_meta( $post_id, '_map_display_filter', true ) )
			$classes[]	= 'hide-map-filter';

		if( $thisValue = get_post_meta( $post_id, '_listing_filter', true ) )
			$classes[]	= 'hide-listing-filter';

		if( $thisValue = $this->getTemplateOption( $post_id, 'link_type', false ) )
			$classes[]	= 'module-link-' . $thisValue;

		if( $thisValue = get_post_meta( $post_id, '_filter_type', true ) )
			$classes[]	= $thisValue;

		if( $intModuleColumn = self::$instance->admin->getModuleColumns( 'map', $post_id ) )
			$classes[]	= 'column-' . $intModuleColumn;


		return $classes;
	}

	public function getTemplateOption( $post_id=0, $meta_key='', $default='') {

		if( ! $post_id ) {
			$post_id = $GLOBALS[ 'post' ]->ID;
		}

		if( empty( $this->mapTemplateOptions ) )
			$this->mapTemplateOptions = (Array) get_post_meta( $post_id, 'jvfrm_spot_map_page_opt', true );

		if( !empty( $this->mapTemplateOptions[ $meta_key ] ) )
			$default = $this->mapTemplateOptions[ $meta_key ];
		return $default;
	}	

	public function custom_list_content_column( $class_name, $post=null ) {

		if( is_a( $post, 'WP_Post' ) )
			if( get_post_meta( $post->ID, '_listing_filter', true ) )
				$class_name	= 'col-sm-12';
		return $class_name;
	}

	public function arichive_map_switch_value( $value ) {
		global $wp_query;
		$term_id = $wp_query->get_queried_object_id();
		if( $wp_query->is_archive() )
			$value		= get_option( 'lava_listing_category_' . $term_id . '_type_option', 0 );
		return $value;
	}

	public function getMapTemplateContentType( $post_id=0 ) {
		return get_post_meta( $post_id, '_map_display_panel_or_content', true );
	}

	public function display_the_content_output( $old_value=true, $template_id=0 ){
		/**
		 * true : Show Panel
		 * false : Hide Panel
		*/
		return $this->getMapTemplateContentType( $template_id ) != 'map-display-content';
	}

	public function map_template_the_content( $template_id = 0 ) {
		if( $this->getMapTemplateContentType( $template_id ) == 'map-display-content' ){
			echo '<div class="container">';
				the_content();
			echo '</div>';
		}
	}

	public function map_template_params( $params=Array(), $tso=null, $mapOption=null ) {

		if( !is_object( $tso ) && !is_object( $mapOption ) )
			return $params;

		if( !is_array( $params ) )
			$params = Array();

		return wp_parse_args(
			Array(
				'loadmore_amount' => $mapOption->get( 'loadmore_amount', 10 ),
				'map_primary_color' => $mapOption->get( 'map_primary_color' ),
				'map_style_json' => $mapOption->get( 'map_style_json' ),
				'allow_wheel' => $tso->get( 'map_allow_mousewheel', 'a' ),
				'cluster' => $mapOption->get( 'cluster', '' ),
				'cluster_level' => $mapOption->get( 'cluster_level', 100 ),
				'distance_max' => $mapOption->get( 'distance_max', 100 ),
				'distance_unit' => $mapOption->get( 'distance_unit', 'km' ),
				'marker_type' => $mapOption->get( 'marker_type' ),
				'map_zoom' => $mapOption->get( 'init_map_zoom' ),
				'map_marker' => $mapOption->get( 'map_marker', $tso->get( 'map_marker' ) ),
				'map_marker_hover' => $mapOption->get( 'after_map_marker' ),
				'link_type' => $mapOption->get( 'link_type' ),
				'marker_zoom_level' => $mapOption->get( 'marker_zoom_level' ),
				'panel_list_featured_first' => $mapOption->get( 'panel_list_featured_first' ),
				'panel_list_random' => $mapOption->get( 'panel_list_random' ),
			), $params
		);
	}

	public function custom_map_template_style( $rows=Array(), $strPrefix='' ){
		global $post;

		$arrOptions = get_post_meta( $post->ID, 'jvfrm_spot_map_page_opt', true );
		$strColor = isset( $arrOptions[ 'one_line_color' ] )  && $arrOptions[ 'one_line_color' ] != '' ?
			$arrOptions[ 'one_line_color' ] : '#f4f4f4';

		$rows[] = sprintf(
			'%1$s%2$s{ background-color:%3$s; }',
			$strPrefix,
			'.javo-maps-panel-wrap.map-layout-top.jv-map-filter-type-bottom-oneline .javo-maps-panel-wrap-inner',
			$strColor
		);
		return $rows;
	}

	public function add_preview_panel( $page_id=0 ) {
		parent::$instance->load_template(
			'map-preview-panel',
			Array(),
			Array(
				'page_id' => $page_id
			)
		);
	}

	public function checkParentPage( $args, $template_id=0 ) {
		$args[ 'in_map_template' ] = $this->getTemplateOption( $template_id, 'link_type', false ) == 'type3';
		return $args;
	}

	public function addMarkerMoveButton( $module_name, $obj=false ) {

		if( !$obj )
			return false;

		if( ! $obj->in_map_template )
			return false;

		switch( $module_name ) {
			case 'module8': jv_core_map()->load_template( 'part-module8-hover' ); break;
			default: jv_core_map()->load_template( 'part-module-hover' );
		}
	}

}