<?php
$arrModules								= isset( $this->modules ) ? (Array) $this->modules : Array();
$moduleSelector						= Array(
	'_map_module'						=> Array(
		'label'								=> __( "Map", 'lvdr-map' ),
	),
	'_list_module'						=> Array(
		'label'								=> __( "List", 'lvdr-map' ),
	),
);

$arrOutput								= $arrBuffer = Array();
foreach( $moduleSelector as $key => $meta ) {
	$arrModuleOptions				= Array( sprintf( "<option value=''>%s</option>", __( "Select Module", 'lvdr-map' ) ) );
	$arrModuleColumnOptions	= $arrAvatarOption = Array();

	// Module Name
	if( !empty( $arrModules ) ) : foreach( $arrModules as $module ) {
		$arrModuleOptions[]		= sprintf(
			"<option value='{$module}'%s>{$module}</option>",
			selected( $module == get_post_meta( $post->ID, $key, true ), true, false )
		);
	} endif;

	// Column Selector
	for( $intCounter=1; $intCounter <= ( $key == '_map_module' ? 4 : 3 ); $intCounter++ )
		$arrModuleColumnOptions[]	= sprintf(
			"<option value='{$intCounter}'%s>{$intCounter} %s</option>",
			selected( $intCounter == get_post_meta( $post->ID, $key . '_column' , true ), true, false ),
			_n( 'Column', 'Columns', $intCounter, 'lvdr-map' )
		);

	// Display Post Avatar
	foreach(
		Array(
			''			=> __( "Visible", 'lvdr-map' ),
			'hide'		=> __( "HIdden", 'lvdr-map' ),
		) as $optAvatar	=> $labelAvatar
	) $arrAvatarOption[]	=sprintf(
		"<option value='{$optAvatar}'>{$labelAvatar}</option>"
	);

	$arrBuffer[]	= join( "\n", Array(
		'<tr>',
			'<th valign="middle">',
				"<label for=\"javo{$key}_selector\">{$meta[ 'label' ]}</label>",
			'</th>',
			'<td valign="middle">',

				/**
				 *	Module Selector
				 */
				"<select name=\"lava_map_param[{$key}]\" id=\"javo{$key}_selector\">",
					join( false, $arrModuleOptions ),
				"</select>",

				/**
				 *	Column Selector
				 */
				"<select name=\"lava_map_param[{$key}_column]\" id=\"javo{$key}_selector\">",
					join( false, $arrModuleColumnOptions ),
				"</select>",

				/**
				 *	Display Post Avatar
				 */

				 /*
				"<select name=\"lava_map_param[{$key}_avatar]\" id=\"javo{$key}_selector\">",
				join( false, $arrModuleColumnOptions ),
				"</select>",
				*/

				/**
				 *	Post Excerpt Length
				 */
				"<div>",
					"<label>" . __( "Max Excerpt Length", 'lvdr-map' ),
					sprintf(
						"<input type=\"number\" name=\"lava_map_param[{$key}_excerpt_length]\" id=\"javo{$key}_excerpt_length\" value=\"%s\">",
						get_post_meta( $post->ID, $key . '_excerpt_length', true )
					),
					"</label>",
					sprintf( "<small>( %s )</small>", __( "Blank is unlimited. 0 is hide excerpt.", 'lvdr-map' ) ),
				"</div>",
			'</td>',
		'</tr>',
	) );
}

echo join( "\n", Array(
	'<table class="widefat">',
		'<tbody>',
			join( "\n", $arrBuffer ),
		'</tbody>',
	'</table>',
) );
