<table class="widefat">
	<tbody>
		<tr>
			<td class="setting-big-titles" colspan="2"><h4><?php _e('Map position setting', 'lvdr-map'); ?></h4></td>
		</tr>
		<tr>
			<td><label><?php _e( 'Initial position of this map', 'lvdr-map' );?></label></td>
			<td>
				<div class="jv-default-pos-wrap">
					<div class="">
						<?php _e( 'Latitude', ''); ?> :
						<input type="text" name="jvfrm_spot_map_opts[default_pos_lat]" value="<?php echo jv_core_map()->getOption( 'default_pos_lat', 0 ); ?>" class="short-text">
					</div>
					<div class="">
						<?php _e( 'Longitude', ''); ?> :
						<input type="text" name="jvfrm_spot_map_opts[default_pos_lng]" value="<?php echo jv_core_map()->getOption( 'default_pos_lng', 0 ); ?>" class="short-text">
					</div>
					<div class="">
						<div><span class="description"><?php esc_html_e( "(Blank or 0 : Auto detect and FitBound from google API)", 'lvdr-map' ); ?></span></div>
						<div><span class="description"><?php esc_html_e( "If Initial Map Zoom Level is not set, location will be changed but it will be showing whole map.", 'lvdr-map' ); ?></span></div>

					</div>
					<div class="">
						<div class="jv-default-pos-map"></div>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>