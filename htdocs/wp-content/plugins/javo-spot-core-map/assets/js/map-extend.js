;( function( $ ){

	var jvfrm_spot_mapCore = function() {
		this.args = jvfrm_spot_core_map_args;
		this.init();
	}

	jvfrm_spot_mapCore.prototype = {
		constructor : jvfrm_spot_mapCore,
		init : function () {
			var
				container		= $( '#javo-maps-listings-wrap' ),
				map_wrap	= $( '#javo-maps-wrap', container ),
				list_wrap		= $( '#javo-listings-wrap', container ),
				switcher		= $( '#javo-maps-listings-switcher', container ),

				// Map
				map_panel	= $( '.javo-maps-panel-wrap', map_wrap ),
				map_search	= $( '.javo-maps-search-wrap', map_panel ),
				fdAddress	= $( '#javo-map-box-location-ac', map_search ),
				btnGeoLoc	= $( '.javo-my-position', map_search ),

				// Filter
				swMapSearch	= $( '.javo-maps-search-wrap', switcher ),
				swfdAddress	= $( '#javo-map-box-location-ac', swMapSearch ),
				swbtnGeoLoc	= $( '.javo-my-position', swMapSearch );

			if( map_panel.is( '.map-layout-top, .jv-map-filter-type-bottom-oneline' ) ) {
				$( document )
					.on( 'click focus keydown', fdAddress.selector, this.displayDisntanceBar() )
					.on( 'click', btnGeoLoc.selector, this.displayDisntanceBar() )
					.on( 'click', $( '.filter-address .close', map_search ).selector, this.hideDisntanceBar() );
			}
			if( switcher.hasClass( 'map-layout-search-top' ) ) {
				$( document )
					.on( 'click focus keydown', swfdAddress.selector, this.displayDisntanceBar() )
					.on( 'click', swbtnGeoLoc.selector, this.displayDisntanceBar() )
					.on( 'click', $( '.filter-address .close', swMapSearch ).selector, this.hideDisntanceBar() );
			}

			this
				.close_button()
				.markerAnimationApply()
				.filteredMarkerAnimation()
				.defaultMapPosition()
				.autoMyPosition();
		},

		displayDisntanceBar : function() {
			return function( e ){
				var
					container		= $( this ).closest( '.filter-address' ) ,
					myPosition	= $( '.javo-my-position-geoloc', container ),
					layer				= $( '> .row > .col-md-10', myPosition );
				if(! layer.hasClass( 'active') )
					layer.addClass( 'active' );
			}
		},

		hideDisntanceBar : function() {
			return function( e ) {
				$( this ).parent().removeClass( 'active' );

			}
		},

		markerAnimationApply : function(){

			var markerAnimate;

			switch( this.args.marker_animation ){
				case 'bounce':
					markerAnimate = google.maps.Animation.BOUNCE;
					break;

				case 'drop':
					markerAnimate = google.maps.Animation.DROP;
					break;

				default:
					markerAnimate = null;
			}

			if( null == markerAnimate )
				return this;

			$( document ).on( 'javo:marker_restore', function( e, param ){
				param.constructor.el.gmap3({
					get:{ name: 'marker', id:'mid_' + param.module_id, callback : function( marker ) {
						if( !marker )
							return;
						marker.setAnimation( null );
					}}
				});
			} );
			$( document ).on( 'javo:marker_release', function( e, param ){
				param.constructor.el.gmap3({
					get:{ name: 'marker', id:'mid_' + param.module_id, callback : function( marker ) {
						if( !marker )
							return;
						marker.setAnimation( markerAnimate );
					}}
				});
			} );

			return this;
		},

		filteredMarkerAnimation : function(){
			if( !this.args.first_marker_animation )
				return this;

			$( document ).on( 'javo:parse_marker', function( e, constructor ){
				var options = constructor.extend_mapOption || {};
				constructor.extend_mapOption = $.extend( {}, options,
					{
						marker:{
							options:{
								animation: google.maps.Animation.DROP
							}
						}
					}
				);
			} );
			return this;
		},

		close_button : function() {

			var
				obj = this,
				wrapAdvance = $( ".javo-maps-advanced-filter-wrap" ),
				btnClose = $( ".filter-close", wrapAdvance );

			btnClose.on( 'click', function( e ){

				e.preventDefault();
				$( "#javo-map-box-advance-filter" ).trigger( 'click', true );

			} );

			return obj;
		},

		defaultMapPosition : function () {
			var
				obj = this,
				default_pos_lat = parseFloat( obj.args.default_pos_lat || 0 ),
				default_pos_lng = parseFloat( obj.args.default_pos_lng || 0 ),
				default_pos = new google.maps.LatLng( default_pos_lat, default_pos_lng );

			if( default_pos_lat && default_pos_lng ) {
				$( window ).on( 'javo:parse_marker', function( e, constructor ){

					var options = constructor.extend_mapOption || {};
					constructor.extend_mapOption = $.extend( {}, options,
						{
							map:{
								options:{
									center : default_pos
								}
							}
						}
					);

					if( ! obj.default_pos_once ) {
						constructor.disableAutofit = true;
						obj.default_pos_once = true;
					}

				} );
			}
			return obj;
		},

		autoMyPosition : function () {
			var
				obj = this,
				args = obj.args || {},
				is_actived = args.auto_myposition == 'enable';
			if( is_actived ) {
				$( '[name="get_pos_trigger"]' ).val( 'yes' );
			}
			return obj;
		}

	}
	new jvfrm_spot_mapCore;
})( jQuery );