; ( function( $, window, unde ) {
	$('.javo-sortable-container').sortable({
		connectWith	: '.javo-sortable-container'
		, cursor		: 'move'
		, handle		: '.javo-sortable-handle'
		, opacity		: '0.5'
		, placeholder	: 'postbox javo-sortable-place-holder'
	});
} )( jQuery, window );