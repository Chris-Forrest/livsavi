<?php
/**
 * Plugin Name: Lava Directory Custom Field Addon
 * Plugin URI : http://lava-code.com/real-estate/
 * Description: Lava Directory Custom Field Addon
 * Version: 1.0.1
 * Author: lavacode
 * Author URI: http://lava-code.com/
 * Text Domain: Lavacode
 * Domain Path: /languages/
 */
/*
    Copyright Automattic and many other contributors.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

if( ! defined( 'ABSPATH' ) )
	die;

if( ! class_exists( 'Lava_Directory_CustomField' ) ) :

	class Lava_Directory_CustomField
	{

		const DEBUG = false;

		public $post_type = 'lv_listing';

		private $theme_name = 'javo_spot';

		private $allow_themes = array(
			'javo_spot',
			'javo_directory'
		);

		private $theme = null;

		public static $instance;

		public function __construct( $file )
		{
			$this->file							= $file;
			$this->folder						= basename( dirname( $this->file ) );
			$this->path						= dirname( $this->file );
			$this->assets_url				= esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
			$this->include_path			= trailingslashit( $this->path ) . 'includes';
			$this->template_path		= trailingslashit( $this->path ) . 'templates';

			if( $this->theme_check( $this->allow_themes ) ) {
				$this->load_files();
				$this->register_hooks();
			}

			do_action( 'Lava_Directory_CustomField_init' );
		}

		public function theme_check( $themes=Array() )
		{
			$this->theme					= wp_get_theme();
			$this->template				= $this->theme->get( 'Name' );
			if( $this->theme->get( 'Template' ) ) {
				$this->parent				= wp_get_theme(  $this->theme->get( 'Template' ) );
				$this->template			= $this->parent->get( 'Name' );
			}
			$this->template				= str_replace( ' ', '_', strtolower( $this->template ) );
			return in_array( sanitize_key( $this->template ), $themes );
		}

		public function load_template( $template_name, $extension='.php', $prefix='template' )
		{
			global $post;

			$filePath		= self::$instance->template_path . '/' . $prefix . '-' . $template_name . $extension;
			if( file_exists( $filePath ) ) {
				include_once $filePath;
				return true;
			}
			return false;
		}

		public function load_files()
		{
			$arrFIles		= Array();
			$arrFIles[]		= $this->include_path . '/class-core.php';
			$arrFIles[]		= $this->include_path . '/class-template.php';
			$arrFIles[]		= $this->include_path . '/class-admin.php';

			if( !empty( $arrFIles ) ) foreach( $arrFIles as $filename )
				if( file_exists( $filename ) )
					require_once $filename;
		}

		public function register_hooks() {
			add_action( 'init', Array( $this, 'load_core' ), 99 );
			add_action( 'after_setup_theme', Array( $this, 'theme_init' ) );
			add_action( 'admin_enqueue_scripts', Array( $this, 'admin_enqueue' ), 99 );
		}

		public function theme_init() {
			add_action( 'wp_enqueue_scripts', Array( $this, 'enqueue' ), 99 );
			$this->template	= 	new Lava_Directory_CustomField_Template;
		}

		public function load_core() {
			$this->core		= new jvfrm_spot_custom_field;
			$this->admin	= 	new Lava_Directory_CustomField_Admin;
		}

		public function enqueue() {
			wp_enqueue_style(
				'lv-directory-custom-field-frontend'
				, $this->assets_url . 'css/custom-field.css'
			);
		}

		public function admin_enqueue()
		{
			wp_enqueue_style(
				'lv-directory-custom-field-backend'
				, $this->assets_url . 'css/admin-style.css'
			);
			wp_enqueue_script( '	jquery-ui-sortable' );
			wp_enqueue_script(
				'javo-spot-core-map-admin-script'
				, $this->assets_url . 'js/admin-script.js'
				, Array( 'jquery' )
				, false
				, true
			);
		}

		public static function get_instance( $file=null )
		{
			if( null === self::$instance )
				self::$instance = new Lava_Directory_CustomField( $file );

			return self::$instance;
		}
	}
endif;
if( !function_exists( 'lv_directory_customfield' ) ) :
	function lv_directory_customfield(){
		$objInstance	= Lava_Directory_CustomField::get_instance( __FILE__ );
		$GLOBALS[ 'Lava_Directory_CustomField' ] = $objInstance;
		return $objInstance;
	}
	lv_directory_customfield();
endif;