<?php
class Lava_Directory_CustomField_Admin extends Lava_Directory_CustomField
{
	public function __construct()
	{
		add_action( 'jvfrm_spot_theme_setting_pages', Array( $this, 'custom_field_page' ) );
		add_action( "lava_{$this->post_type}_admin_metabox_after", Array( $this, 'append_to_metabox' ) );
	}

	public function custom_field_page( $pages )
	{
		return wp_parse_args(
			Array(
				'custom_field_addons'	=> Array(
					__( "Custom Field", 'javo' ), false
					, 'priority'				=> 36
					, 'external'				=> self::$instance->template_path . '/template-admin-custom-field.php'
				)
			)
			, $pages
		);
	}

	public function append_to_metabox( $post ) {
		$GLOBALS[ 'edit' ] = $post;
		$this->load_template( 'admin-metabox' );
	}
}