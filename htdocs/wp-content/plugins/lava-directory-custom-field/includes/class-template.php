<?php
class Lava_Directory_CustomField_Template extends Lava_Directory_CustomField
{
	public function __construct()
	{
		$post_type	= self::$instance->post_type;
		add_action( "lava_add_{$post_type}_form_after", Array( $this, 'append_to_write_form' ) );
		add_action( "jvfrm_spot_{$post_type}_single_description_before", Array( $this, 'append_to_single_page' ) );
	}

	public function append_to_write_form( $edit )
	{
		global $jvfrm_spot_tso;
		?>
		<div class="">
			<label><?php echo $jvfrm_spot_tso->get( 'field_caption', __( "Additional Information", 'javo' ) );?></label>
			<?php echo jvfrm_spot_custom_field::$instance->form(); ?>
		</div>
		<?php
	}

	public function append_to_single_page() {
		$this->load_template( 'custom-field' );
	}
}