<?php
class jvfrm_spot_custom_field
{
	public static $instance;
	public function __construct()
	{
		self::$instance					= &$this;
		$this->jvfrm_spot_fields				=  Array(
			__('Group', 'javo')			=> 'group'
			, __('Text Field', 'javo')	=> 'text'
			, __('Textarea', 'javo')		=> 'textarea'
			, __('Select Box', 'javo')	=> 'select'
			, __('Radio', 'javo')			=> 'radio'
			, __('Checkbox', 'javo')	=> 'checkbox'
		);

		add_filter( 'jvfrm_spot_custom_field'				, Array( 'jvfrm_spot_custom_field', 'insert_field'), 10, 5 );
		add_action( 'save_post'						, Array( __class__, 'jvfrm_spot_custom_in_post_save_callback') );
		add_action( 'admin_footer'					, Array( __CLASS__, 'scripts_callback' ) );
		add_action( 'wp_ajax_get_custom_field_form'	, Array( $this, 'wp_ajax_get_custom_field_form_callback') );
	}

	public function wp_ajax_get_custom_field_form_callback()
	{
		$jvfrm_spot_get_custom_filed_id = 'id'.md5( strtotime( date( 'YmdHis' ) ).rand(10,1000000) );
		ob_start();?>
		<div class="javo-custom-field-forms ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="meta-box-sortables">
				<div class="postbox">
					<h3 class="ui-widget-header ui-corner-all javo-sortable-handle">
						<?php
						printf(
							"%s <small class=\"required\">%s</small>"
							, __('Field Attributes', 'javo')
							, __('Unsaved', 'javo')
						);?>
					</h3>
					<div class="inside">
						<dl>
							<dt><?php _e('Input Label', 'javo');?></dt>
							<dd>
								<input type="hidden" name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][name]" value="<?php echo $jvfrm_spot_get_custom_filed_id;?>" data-order>
								<input type="hidden" name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][order]">
								<input type="text" name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][label]" value="">
							</dd>
						</dl>
						<dl class="group-hidden">
							<dt><?php _e('Field ID', 'javo');?></dt>
							<dd>
								<input type="text" value="<?php echo $jvfrm_spot_get_custom_filed_id;?>" readonly="readyonly">
							</dd>
						</dl>
						<dl>
							<dt><?php _e('Element Type', 'javo');?></dt>
							<dd>
								<select name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][type]">
									<?php
									echo $this->insert_option( $this->jvfrm_spot_fields );?>
								</select>
							</dd>
						</dl>
						<dl class="group-hidden">
							<dt><?php _e('Values', 'javo');?></dt>
							<dd>
								<div class="description"><small><?php _e('You must use "," as a separator for dropdown, radio, check boxes', 'javo');?></small></div>
								<input name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][value]" value="">

							</dd>
						</dl>
						<dl class="group-hidden">
							<dt><?php _e('CSS Class Name', 'javo');?></dt>
							<dd><input name="jvfrm_spot_ts[custom_field][<?php echo $jvfrm_spot_get_custom_filed_id;?>][css]" value=""></dd>
						</dl>
						<dl>
							<dt><?php _e('Action', 'javo');?></dt>
							<dd>
								<a class="button button-warning javo-remove-custom-field"><?php _e('Remove', 'javo');?></a>
							</dd>
						</dl>
					</div>
				</div><!-- PostBox End -->
			</div><!-- PostBox Sortable End -->
		</div><!-- PostBox Container End -->


		<?php
		$jvfrm_spot_get_this_content = ob_get_clean();
		echo json_encode(Array(
			'output'=> $jvfrm_spot_get_this_content
		));
		exit;
	}

	static function jvfrm_spot_custom_in_post_save_callback($post_id){
		$jvfrm_spot_query = new jvfrm_spot_Array( $_POST );

		if( false !== (boolean)( $fields = $jvfrm_spot_query->get('jvfrm_spot_custom_field', false ) ) )
		{
			if( is_Array( $fields ) )
			{
				foreach( $fields as $key => $fields )
				{
					$jvfrm_spot_value = new jvfrm_spot_Array( $fields );
					update_post_meta( $post_id, $key, $jvfrm_spot_value->get( 'value' ) );
				}
			}

		}


	}

	public function insert_option( $options, $default=NULL){
		$jvfrm_spot_this_output ="";
		foreach( (Array) $options as $key=> $value){
			$jvfrm_spot_this_output .= sprintf('<option value="%s"%s>%s</option>', $value, ($value == $default ? ' selected': ''), $key);
		}
		return $jvfrm_spot_this_output;
	}


	static function gets()
	{
		global
			$post
			, $jvfrm_spot_tso;

		$jvfrm_spot_return				= Array();
		$jvfrm_spot_get_custom_field		= $jvfrm_spot_tso->get('custom_field', null);

		if( ! empty( $jvfrm_spot_get_custom_field ) )
		{
			// Output : Label, Value
			foreach( $jvfrm_spot_get_custom_field as $key => $field )
			{
				$is_group				= !empty( $field['type'] ) && $field['type'] == "group";

				$jvfrm_spot_class_name		= !empty( $field['css'] ) ? $field['css'] : '';

				$jvfrm_spot_return[ $field['name'] ]	= Array(
					'label'				=> $field['label']
					, 'value'			=> $is_group ? "&nbsp;" : get_post_meta( $post->ID, $field['name'], true )
					, 'type'			=> $field['type']
					, 'css'				=> $jvfrm_spot_class_name
				);
			}
		}

		if( empty( $jvfrm_spot_return ) ){ return; };
		return $jvfrm_spot_return;
	}

	static function insert_field($label, $type, $attributes = Array(), $values = NULL, $default_value=NULL)
	{

		$jvfrm_spot_this_output	= Array('attribute' => '', 'values' => '');
		$jvfrm_spot_field_key		= "";
		
		if( !isset( $attributes['class'] ) )
			$attributes['class']	= '';

		$attributes['class'] .= ' form-control';
		foreach( (Array)$attributes as $key => $value){
			if($key == 'name'){
				$jvfrm_spot_this_output['attribute'] .= ' '.$key.'="jvfrm_spot_custom_field['.$value.'][value]"';
				$jvfrm_spot_field_key = $value;
			}else{
				$jvfrm_spot_this_output['attribute'] .= ' '.$key.'="'.$value.'"';
			};
		}
		$jvfrm_spot_this_output['attribute'] .= ">";

		switch( $type ){
			case 'textarea':
				$jvfrm_spot_this_output['before']		= '<textarea';
				$jvfrm_spot_this_output['after']		= '</textarea>';
				$jvfrm_spot_this_output['values']		= $default_value != NULL ? $default_value : $values;
			break;
			case 'select':
				$jvfrm_spot_this_output['before']		= '<select';
				if( !empty( $values ) ){
					$jvfrm_spot_this_values = explode(',', $values);
					foreach($jvfrm_spot_this_values as $value)
					{
						$jvfrm_spot_this_output['values'] .= sprintf('<option value="%s"%s>%s</option>'
							, trim( $value )
							, selected( trim( $value ) == trim( $default_value ), true, false)
							, trim( $value )
						);
					};
				};
				$jvfrm_spot_this_output['after']		= '</select>';
			break;
			case 'radio':
			case 'checkbox':

				$jvfrm_spot_this_output['before']		= '<div ';
				$jvfrm_spot_this_output['attribute']	= 'class="form-control" style="float:none;">';
				$jvfrm_spot_this_output['after']		= '</div>';

				$jvfrm_spot_this_field_array = $type == 'checkbox' ? '[]' : '';

				$jvfrm_spot_this_values = explode( ',', $values );
				foreach( $jvfrm_spot_this_values as $value )
				{
					$jvfrm_spot_this_output['values'] .= sprintf("<label><input type='{$type}' name='jvfrm_spot_custom_field[{$attributes['name']}][value]{$jvfrm_spot_this_field_array}' value='%s'%s>%s</label> &nbsp;"
						, trim( $value )
						, checked( !empty( $default_value ) && in_Array( trim( $value ), (Array)$default_value ), true, false )
						, trim( $value )
					);
				}
			break;
			case 'text':
				$jvfrm_spot_this_output['before']		= '<input type="text" value="'. ( $default_value != NULL ? $default_value : $values ).'"';
				$jvfrm_spot_this_output['after']		= '';
			break;
		};
		ob_start();

		if( $type != "group" ){
			?>
			<div class="form-group type-<?php echo $type;?>">
				<div class="input-group">
					<span class="input-group-addon"><?php echo $label;?></span>
					<?php echo $jvfrm_spot_this_output['before'].$jvfrm_spot_this_output['attribute'].$jvfrm_spot_this_output['values'].$jvfrm_spot_this_output['after'];?>
				</div>
				<input type="hidden" name="jvfrm_spot_custom_field[<?php echo $attributes['name'];?>][label]" value="<?php echo $label;?>">
			</div>
			<?php
		}else{
			?>
			<div class="form-group page-header type-<?php echo $type;?>">
				<?php echo $label;?>
				<input type="hidden" name="jvfrm_spot_custom_field[<?php echo $jvfrm_spot_field_key;?>][value]" value="|">
			</div>
			<?php
		}
		return ob_get_clean();
	}

	public function form(){
		global
			$jvfrm_spot_tso
			, $edit;

		$temp = $edit;

		if( empty( $edit ) )
		{
			$edit		= new stdClass();
			$edit->ID	= 0;

		}

		$jvfrm_spot_get_custom_field = $jvfrm_spot_tso->get('custom_field', null);
		$jvfrm_spot_get_custom_variables = Array();


		ob_start();?>
		<?php
		if( !empty( $jvfrm_spot_get_custom_field ) ){

			foreach( $jvfrm_spot_get_custom_field as $key => $field){
				if( !empty( $jvfrm_spot_get_custom_variables[$field['name']] )){
					$jvfrm_spot_this_form_data = new jvfrm_spot_Array( $jvfrm_spot_get_custom_variables[$field['name']] );
				};
				echo apply_filters(
					'jvfrm_spot_custom_field'
					, $field['label']
					, $field['type']
					, Array(
						'name'			=> $field['name']
					), $field['value']
					, get_post_meta( $edit->ID, "{$field['name']}", true )
				);
			};
		};

		// Repaire Variable
		$edit = $temp;
		return ob_get_clean();
	}
	public function admin()
	{
		global $jvfrm_spot_tso;
		$jvfrm_spot_get_custom_field = $jvfrm_spot_tso->get('custom_field', null);
		ob_start();
		if( !empty($jvfrm_spot_get_custom_field) ){
			foreach($jvfrm_spot_get_custom_field as $key => $field){
				$jvfrm_spot_field_string = new jvfrm_spot_Array($field);
				?>
				<div class="javo-custom-field-forms ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

					<div class="meta-box-sortables">
						<div class="postbox">
							<h3 class="ui-widget-header ui-corner-all javo-sortable-handle">
								<?php
								printf(
									"%s [%s]"
									, __('Field Attributes', 'javo')
									, $jvfrm_spot_field_string->get( 'label' )
								); ?>
							</h3>
							<div class="inside hidden">
								<input type="hidden" name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][name]" value="<?php echo esc_attr( $field['name'] );?>" style="width:500px;">
								<dl>
									<dt><?php _e('Input Label', 'javo');?></dt>
									<dd>
										<input type="hidden" name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][name]" value="<?php echo $field['name'];?>">
										<input type="hidden" name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][order]" data-order>
										<input type="text" name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][label]" value="<?php echo $jvfrm_spot_field_string->get('label');?>">
									</dd>
								</dl>
								<dl class="group-hidden">
									<dt><?php _e('Field ID', 'javo');?></dt>
									<dd>
										<input type="text" value="<?php echo $key;?>" readonly="readyonly">
									</dd>
								</dl>
								<dl>
									<dt><?php _e('Element Type', 'javo');?></dt>
									<dd>
										<select name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][type]">
											<?php
											echo $this->insert_option( $this->jvfrm_spot_fields , $field['type']);?>
										</select>
									</dd>
								</dl>
								<dl class="group-hidden">
									<dt><?php _e('Values', 'javo');?></dt>
									<dd>
										<div class="description"><small><?php _e('You must use "," as a separator for dropdown, raido, check boxes', 'javo');?></small></div>
										<input name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][value]" value="<?php echo $jvfrm_spot_field_string->get('value');?>">
									</dd>
								</dl>
								<dl class="group-hidden">
									<dt><?php _e('CSS Class Name', 'javo');?></dt>
									<dd>
										<input name="jvfrm_spot_ts[custom_field][<?php echo $key;?>][css]" value="<?php echo $jvfrm_spot_field_string->get('css');?>">
									</dd>
								</dl>
								<dl>
									<dt><?php _e('Action', 'javo');?></dt>
									<dd>
										<a class="button button-cancel javo-remove-custom-field"><?php _e('Remove', 'javo');?></a>
									</dd>
								</dl>
							</div>
						</div><!-- PostBox End -->
					</div><!-- PostBox Sortable End -->
				</div>
				<?php
			} // End foreach
		}; // End if
		return ob_get_clean();
	}


	public static function scripts_callback()
	{
		$ajaxurl = admin_url( 'admin-ajax.php' );
		ob_start();
		?>
		<script type="text/javascript">
		( function( $ ) {
			var jvfrm_spot_ts_custom_field = function()
			{
				if( ! window.jvfrm_spot_tcf_instance )
				{
					window.jvfrm_spot_tcf_instance = true;
					this.events();
				}
			}
			jvfrm_spot_ts_custom_field.prototype.el_type	= "select[name^='jvfrm_spot_ts[custom_field]']";
			jvfrm_spot_ts_custom_field.prototype.add_args = function()
			{
				return {
					action: 'get_custom_field_form'
				};
			}

			jvfrm_spot_ts_custom_field.prototype.events = function()
			{
				var obj	= new jvfrm_spot_ts_custom_field;

				$( document )
					.on( 'click'	, '.javo-add-custom-field', this.add() )
					.on( 'click'	, '.javo-remove-custom-field', this.remove )
					.on( 'change'	, obj.el_type, this.setElementGroup )
					.on( 'click'	, 'h3.javo-sortable-handle', this.toggle )

				$( obj.el_type ).trigger( 'change' );
			}

			jvfrm_spot_ts_custom_field.prototype.add = function()
			{
				var obj		= this;
				return function( e ){
					e.preventDefault();

					var el	= $( this );

					if( el.hasClass( 'disabled' ) ) return false;

					el.addClass( 'disabled' );

					$.post(
						'<?php echo $ajaxurl; ?>'
						, obj.add_args()
						, function( response )
						{
							$( response.output ).appendTo('.javo-sortable-container');
							el.removeClass('disabled');
						}
						, 'json'
					)
					.always( function() {
						$( obj.el_type ).trigger( 'change' );
					} );
				}
			}

			jvfrm_spot_ts_custom_field.prototype.remove = function( e )
			{
				e.preventDefault();
				var tar = $( this ).closest( '.javo-custom-field-forms' );
				tar.remove();
			}

			jvfrm_spot_ts_custom_field.prototype.setElementGroup = function( e )
			{
				var parent = $(this).closest( 'div.inside' ).find('dl.group-hidden' );
				parent.removeClass('hidden');
				if( $(this).val() == "group" && parent.hasClass('group-hidden') ){
					parent.addClass("hidden");
				}
			}

			jvfrm_spot_ts_custom_field.prototype.toggle = function( e )
			{
				e.preventDefault();
				$( this ).closest( '.javo-custom-field-forms' ).find( 'div.inside').toggleClass( 'hidden' );
			}
			new jvfrm_spot_ts_custom_field;

		} )( jQuery );
		</script>
		<?php
		ob_end_flush();
	}

}