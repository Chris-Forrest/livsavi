<?php global $jvfrm_spot_tso; ?>
<div class="jvfrm_spot_ts_tab javo-opts-group-tab hidden" tar="custom_field_addons">
	<h2> <?php _e( "Custom Fields Settings", 'javo'); ?> </h2>

	<table class="form-table">
		<tr><th>
			<?php _e('Item Field Title', 'javo');?>
			<span class="description">
				<?php _e('Create an additional title for your item page.', 'javo');?>
			</span>
		</th><td>
			<h4><?php _e('Title Name', 'javo');?></h4>
			<fieldset>
				<input type="text" name="jvfrm_spot_ts[field_caption]" value="<?php echo $jvfrm_spot_tso->get('field_caption', __('Additional Information', 'javo'));?>" class="large-text">
			</fieldset>
		</td></tr><tr><th>
			<?php _e('Customizable Item Fields', 'javo');?>
			<span class="description">
				<?php _e('Add and remove item fields as needed.', 'javo');?>
			</span>
		</th><td bgcolor='#efefef'>
			<div id="dashboard-widgets-wrap">
				<div class="javo-sortable-container">
					<?php echo jvfrm_spot_custom_field::$instance->admin();?>
				</div>
				<a class="button button-primary javo-add-custom-field"><?php _e('Add New Field', 'javo');?></a>
			</div>
		</td></tr>
	</table>
</div>