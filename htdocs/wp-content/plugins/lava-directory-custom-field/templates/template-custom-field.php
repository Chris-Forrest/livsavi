<?php
global $jvfrm_spot_tso;

$jvfrm_spot_integer = 0;
$jvfrm_spot_el_childrens = "";
$jvfrm_spot_custom_field = jvfrm_spot_custom_field::gets();

if( !empty( $jvfrm_spot_custom_field ) ){
	foreach($jvfrm_spot_custom_field as $field){
		$jvfrm_spot_marge_value = '';
		$jvfrm_spot_this_value = !empty( $field['value'] ) ? (Array) $field['value'] : Array();
		if(
			empty( $jvfrm_spot_this_value ) || $jvfrm_spot_this_value == '' &&
			( !empty( $field['type'] ) && $field['type'] != "group" )
		){
			continue;
		}
		if($field['type']!="group"){
			$jvfrm_spot_integer++;
		}
		foreach( $jvfrm_spot_this_value as $value)
		{
			$jvfrm_spot_marge_value .= $value . ', ';
		}

		$jvfrm_spot_marge_value = substr( trim( $jvfrm_spot_marge_value ), 0, -1 );
		if( !empty( $field['type'] ) && $field['type'] == "group" )
		{
			$jvfrm_spot_el_childrens .= "<li><h5>{$field['label']}</h5></li>";

		}else{
			$jvfrm_spot_el_childrens .= "
				<li class=\"{$field['css']}\">
					<div class='row'>
						<div class='col-sm-2'>{$field['label']}</div>
						<div class='col-sm-10'>{$jvfrm_spot_marge_value}</div>
					</div>
				</li>";
		}

	} // End Foreach
}
if( (int)$jvfrm_spot_integer > 0 ){
	?>
	<div class="col-md-12 col-xs-12 item-description" id="javo-item-customfield-section" data-jv-detail-nav>

		<h3 class="page-header"><?php echo $jvfrm_spot_tso->get( 'field_caption', __( "Additional Information", 'javo' ) )?></h3>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="inner-items">
					<ul><?php echo $jvfrm_spot_el_childrens;?></ul>
				</div>
			</div><!--/.panel-body-->
		</div><!--/.panel-->
	</div><!-- /#javo-item-describe-section -->
	<?php
};// End If?>