<table class="form-table jv-spot-core-custom-field-metabox">
	<tr>
		<th><?php _e( "Custom Field", 'Lavacode' ); ?></th>
		<td>
			<?php echo jvfrm_spot_custom_field::$instance->form();?>
		</td>
	</tr>
</table>