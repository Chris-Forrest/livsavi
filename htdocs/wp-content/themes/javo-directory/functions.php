<?php
/**
 *	Javo Themes functions and definitions
 *
 * @package WordPress
 * @subpackage Javo
 * @since Javo Themes 1.0
 */

 // Path Initialize
define( 'JVFRM_SPOT_APP_PATH'		, get_template_directory() );				// Get Theme Folder URL : hosting absolute path
define( 'JVFRM_SPOT_THEME_DIR'		, get_template_directory_uri() );			// Get http URL : ex) http://www.abc.com/
define( 'JVFRM_SPOT_SYS_DIR'		, JVFRM_SPOT_APP_PATH."/library");			// Get Library path
define( 'JVFRM_SPOT_TP_DIR'			, JVFRM_SPOT_APP_PATH."/templates");		// Get Tempate folder
define( 'JVFRM_SPOT_ADM_DIR'		, JVFRM_SPOT_SYS_DIR."/admin");				// Administrator Page
define( 'JVFRM_SPOT_IMG_DIR'		, JVFRM_SPOT_THEME_DIR."/assets/images");	// Images folder
define( 'JVFRM_SPOT_WG_DIR'			, JVFRM_SPOT_SYS_DIR."/widgets");			// Widgets Folder
define( 'JVFRM_SPOT_HDR_DIR'		, JVFRM_SPOT_SYS_DIR."/header");			// Get Headers
define( 'JVFRM_SPOT_CLS_DIR'		, JVFRM_SPOT_SYS_DIR."/classes");			// Classes
define( 'JVFRM_SPOT_DSB_DIR'		, JVFRM_SPOT_SYS_DIR."/dashboard");			// Dash Board
define( 'JVFRM_SPOT_FUC_DIR'		, JVFRM_SPOT_SYS_DIR."/functions");			// Functions
define( 'JVFRM_SPOT_PLG_DIR'		, JVFRM_SPOT_SYS_DIR."/plugins");			// Plugin folder
define( 'JVFRM_SPOT_ADO_DIR'		, JVFRM_SPOT_SYS_DIR . "/addons");			// Addons folder

define( 'JVFRM_SPOT_CUSTOM_HEADER', false );

// Includes : Basic or default functions and included files
require_once JVFRM_SPOT_SYS_DIR	. "/define.php";								// defines
require_once JVFRM_SPOT_SYS_DIR	. "/load.php";									// loading functions, classes, shotcode, widgets
require_once JVFRM_SPOT_SYS_DIR	. "/enqueue.php";								// enqueue js, css
require_once JVFRM_SPOT_SYS_DIR	. "/wp_init.php";								// post-types, taxonomies
require_once JVFRM_SPOT_ADM_DIR	. "/class-admin-theme-settings.php";			// theme options
require_once JVFRM_SPOT_DSB_DIR	. "/class-dashboard.php";						// theme screen options tab.

$jvfrm_spot_core_include	= apply_filters( 'jvfrm_spot_core_function_path', JVFRM_SPOT_APP_PATH .'/includes/class-core.php' );

if( file_exists( $jvfrm_spot_core_include ) ) require_once $jvfrm_spot_core_include;

do_action( 'jvfrm_spot_themes_loaded' );

// update_option( 'wp_less_cached_files', '' );

if( !function_exists( 'wcs_dequeue_quantity' ) ) {
	add_action( 'wp_enqueue_scripts', 'wcs_dequeue_quantity' );
	function wcs_dequeue_quantity() {
		wp_dequeue_style( 'wcqi-css' );
	}
}

if( !function_exists( 'jvfrm_spot_custom_lava_listing_meta' ) ) {
	add_action( 'lava_lv_listing_field_init', 'jvfrm_spot_custom_lava_listing_meta' );
	function jvfrm_spot_custom_lava_listing_meta( $field='', $args=array() ) {
		remove_filter( 'lava_lv_listing_field_output', '__return_false', 99 );
		if( in_array( $field, Array( '_logo' ) ) ) {
			add_filter( 'lava_lv_listing_field_output', '__return_false', 99 );
		}
	}
}


/*
<?php
$intLogoID = get_post_meta( get_the_ID(), '_logo', true );
if( 0 < intVal( $intLogoID ) && get_post_status( $intLogoID ) !== false ) {
printf( '<div class="featured-header-logo-wrap container"><div class="featured-header-logo"><img src="%1$s" class="border-radius"></div></div>', wp_get_attachment_image_url( $intLogoID, 'large' ) );
} ?>

*/