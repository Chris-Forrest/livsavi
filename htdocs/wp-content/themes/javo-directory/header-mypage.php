<?php
/**
 * The Mypage Header template for Javo Theme
 *
 * @package WordPress
 * @subpackage Javo
 * @since Javo Themes 3.2.1
 */
// Get Options
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php echo esc_attr( get_bloginfo( 'pingback_url' ) ); ?>" />
	<?php
		remove_action( 'wp_head', '_wp_render_title_tag', 1 );
		function jv_change_title() {
			printf('<title>%s</title>', get_bloginfo('name'));
		}
		add_action( 'wp_head', 'jv_change_title' );
	?>

	<!--[if lte IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js" type="text/javascript"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/bootstrap.min.css';?>" rel="stylesheet">

	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/bootstrap-custom.css';?>" rel="stylesheet">
	<!-- Menu CSS 
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/sidebar-nav.min.css';?>" rel="stylesheet">-->
	<!-- toast CSS -->
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/jquery.toast.css';?>" rel="stylesheet">
	<!-- morris CSS 

	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/morris.css';?>" rel="stylesheet">-->
	<!-- animation CSS 

	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/animate.css';?>" rel="stylesheet">-->
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/fonts.css';?>" rel="stylesheet">
	<!-- Custom CSS 
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/jv-member/css/style.css';?>" rel="stylesheet">-->
	<!-- color CSS
	<link href="<?php echo get_template_directory_uri().'/assets/css/extend/jv-member/css/colors/default-jv-mypage.css';?>" id="theme" rel="stylesheet"> -->


</head>
<body <?php body_class();?>>

	<div id="page-style">

		<div class="preloader"><div class="loader-thick-cycle"></div></div>

		<div class="jv-my-page jv-dashboard">
			<div class="">
				<?php get_template_part('library/dashboard/'. jvfrm_spot_dashboard()->page_style. '/header'); ?>
				<?php get_template_part('library/dashboard/' . jvfrm_spot_dashboard()->page_style . '/left', 'sidebar'); ?>
				<div id="content-page-wrapper">
					<div class="container-fluid">
						<?php get_template_part('library/dashboard/' . jvfrm_spot_dashboard()->page_style . '/page', 'title'); ?>