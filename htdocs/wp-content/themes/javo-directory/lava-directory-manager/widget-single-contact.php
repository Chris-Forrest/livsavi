<?php
$arrContactInfos	= Array(
	Array(
		'label'		=> esc_html__( "Author", 'javospot' ),
		'value'		=> esc_attr( $post->display_name ),
		'link'		=> 'enable'
	),
	Array(
		'label'		=> esc_html__( "Phone", 'javospot' ),
		'value'		=> esc_attr( get_the_author_meta('phone') != '' ? get_the_author_meta('phone') : $post->_phone1 )
	),
);

$author_url = jvfrm_spot_getUserPage( get_the_author_meta( 'ID' ) );
add_filter( 'wp_footer', 'jvfrm_spot_single_contact_modal_html' );
function jvfrm_spot_single_contact_modal_html(){
	global $lava_contact_shortcode;
	?>
	<div class="modal fade lava_contact_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="<?php esc_attr_e( "Close", 'javospot' ); ?>"><span aria-hidden="true">&times;</span></button>
					<div class="contact-form-widget-wrap">
						<?php echo apply_filters( 'the_content', $lava_contact_shortcode ); ?>
					</div> <!-- contact-form-widget-wrap -->
				</div>
			</div>
		</div>
	</div>
	<?php
} ?>
<?php
add_filter( 'wp_footer', 'jvfrm_spot_single_report_modal_html' );
function jvfrm_spot_single_report_modal_html(){
	global $lava_contact_shortcode;
	?>
	<div class="modal fade lava_report_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="<?php esc_attr_e( "Close", 'javospot' ); ?>"><span aria-hidden="true">&times;</span></button>
					<div class="contact-form-widget-wrap">
						<?php echo apply_filters( 'the_content', jvfrm_spot_get_reportShortcode() );  ?>
					</div> <!-- contact-form-widget-wrap -->
				</div>
			</div>
		</div>
	</div>
	<?php
} ?>

<?php
/** Get Widget Title, Button Labels **/
$widget_data = get_option('widget_lava_contact_single_page');
$lv_wg_title = !empty( $instance['contact_widget_title'] ) ? $instance['contact_widget_title'] : esc_html__( "Contact", 'javospot' );
$lv_wg_contact_btn_label = !empty( $instance['contact_btn_label'] ) ? $instance['contact_btn_label'] : esc_html__( "Contact", 'javospot' );
$lv_wg_report_btn_label = !empty( $instance['report_btn_label'] ) ? $instance['report_btn_label'] : esc_html__( "Report", 'javospot' );
?>
<form class="cart lava-wg-author-contact-form" method="post" enctype='multipart/form-data'>
	<div id="lv-single-contact" class="panel panel-default">
		<div class="panel-heading admin-color-setting">
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo $lv_wg_title; ?></h3>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.panel-heading -->

		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<ul class="list-group">
						<li class="list-group-item">
							<div class="text-style">
							<?php
							/* Portion informations */
							if( ! empty( $arrContactInfos ) ) {
								echo "<ul style=\"position: relative;\">\n";
								foreach( $arrContactInfos as $args ) {
									echo "\t<li class=\"row\">\n";
										echo "\t\t<div class=\"col-lg-4 col-sm-4\">\n";
											echo "\t\t\t{$args['label']}\n";
										echo "\t\t</div>\n";

										if(isset($args['link'])){
											echo "\t\t<div class=\"col-xs-11 col-lg-8 col-sm-8\"><a href=\"{$author_url}\">\n";
												echo "\t\t\t{$args['value']}\n";
											echo "\t\t</a></div>\n";
										}else{
											echo "\t\t<div class=\"col-xs-11 col-lg-8 col-sm-8\">\n";
												echo "\t\t\t<a href=\"tel:{$args['value']}\">{$args['value']}</a>\n";
											echo "\t\t</div>\n";
										}

									echo "\t</li>\n";
								}
								echo "</ul>\n";
							} ?>

							<?php
							$arrSoicalIcons = Array();
							foreach(
								Array(
									'facebook' => 'fa fa-facebook',
									'twitter' => 'fa fa-twitter',
									'instagram' => 'fa fa-instagram',
									'google' => 'fa fa-google',
									'linkedin' => 'fa fa-linkedin',
									'youtube' => 'fa fa-youtube',
								) as $strSocialName => $strSocialIcon
							) {
								$strSocialLink = get_post_meta( get_the_ID(), '_' . $strSocialName . '_link', true );
								if( $strSocialLink ) {
									$arrSoicalIcons[] = sprintf( '<a href="%1$s" target="_blank" class="jvfrm_spot_single_listing_%2$s" title="%2$s"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="%3$s fa-inverse fa-stack-1x" aria-hidden="true"></i></a>', esc_url_raw( $strSocialLink ), $strSocialName, $strSocialIcon );
								}
							}

							if( !empty( $arrSoicalIcons ) ) {
								printf( '<div id="javo-item-social-section" class="row" data-jv-detail-nav><div class="jvfrm_spot_single_listing_social-wrap text-center">%s</div></div>', join( '', $arrSoicalIcons ) );
							} ?>

							</div> <!-- text-style -->

						</li><!--/.list-group-item-->
					</ul>
				</div><!--/.col-md-12-->
			</div><!--/.row-->

		</div><!-- /.panel-body -->

		<div class="panel-body author-contact-button-wrap">

			<!-- Large modal -->
			<button type="button" class="btn btn-primary admin-color-setting lava_contact_modal_button" data-toggle="modal" data-target=".lava_contact_modal"><?php echo $lv_wg_contact_btn_label; ?></button>
			<!-- Add Wish List -->
			<div class="row share">
				<div class="col-md-6 text-center">

					<button type="button" class="btn admin-color-setting-hover lava-Di-share-trigger">
						<?php esc_html_e( "Share", 'javospot' ); ?>
					</button>

				</div><!-- /.col-md-6 -->
				<div class="col-md-6 text-center">

					<button type="button" class="btn btn-primary admin-color-setting-hover lava_report_modal_button" data-toggle="modal" data-target=".lava_report_modal">
						<?php echo $lv_wg_report_btn_label; ?>
					</button>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
			<div class="row">
				<div class="col-md-12">
					<?php
					if( function_exists( 'lava_directory_claim_button' ) )
						lava_directory_claim_button(
							Array(
								'class'	=> 'btn btn-block admin-color-setting-hover',
								'label'		=> esc_html__( "Claim", 'javospot' ),
								'icon'		=> false
							)
						);
					?>
					<?php
					if( function_exists( 'lava_directory_booking_button' ) )
						lava_directory_booking_button(
							Array(
								'class'	=> 'btn btn-block admin-color-setting-hover',
								'label'		=> esc_html__( "Booking", 'javospot' ),
								'icon'		=> false
							)
						);
					?>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.panel-body -->
	</div><!-- /.panel -->
	<fieldset>
		<input type="hidden" name="ajaxurl" value="<?php echo esc_url( admin_url( 'admin-ajax.php' ) );?>">
	</fieldset>
</form>

<!-- Modal -->

<script type="text/html" id="lava-Di-share">
	<div class="row">
		<div class="col-md-12">
			<header class="modal-header">
				<?php esc_html_e( "Share", 'javospot' ); ?>
				<button type="button" class="close">
					<span aria-hidden="true">&times;</span>
				</button>
			</header>
			<div class="row">
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-link"></i>
						</span><!-- /.input-group-addon -->
						<input type="text" value="<?php the_permalink(); ?>" class="form-control" readonly>
					</div>
				</div><!-- /.col-md-9 -->
				<div class="col-md-3">
					<button class="btn btn-primary btn-block" id="lava-wg-url-link-copy" data-clipboard-text="<?php the_permalink(); ?>">
						<i class="fa fa-copy"></i>
						<?php esc_html_e( "Copy URL", 'javospot' );?>
					</button>
				</div><!-- /.col-md-3 -->
			</div><!-- /,row -->
			<p>
				<div class="row">
					<div class="col-md-4 col-xs-4">
						<button class="btn btn-info btn-block javo-share sns-facebook" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
							<?php esc_html_e( "Facebook", 'javospot' );?>
						</button>
					</div><!-- /.col-md-4 -->
					<div class="col-md-4 col-xs-4">
						<button class="btn btn-info btn-block javo-share sns-twitter" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
							<?php esc_html_e( "Twitter", 'javospot' );?>
						</button>
					</div><!-- /.col-md-4 -->
					<div class="col-md-4 col-xs-4">
						<button class="btn btn-info btn-block javo-share sns-google" data-title="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">
							<?php esc_html_e( "Google +", 'javospot' );?>
						</button>
					</div><!-- /.col-md-4 -->
				</div><!-- /,row -->
			</p>
		</div>
	</div>
</script>