// Dashboard 1

jQuery( function( $ ) {

	var javoMyDashboard = function() {
		this.param = spot_mypage_type_c_args;
		this.MORRIS = false;
		this.init();
	}

	javoMyDashboard.prototype = {
		constructor : javoMyDashboard,
		init : function() {
			var obj = this;

			if( obj.MORRIS ) {
				obj.setMorris();
			}

			obj.setChart();
			obj.setEvent();
			obj.setEventUploader();
			obj.setLeftMenu();
			obj.setRightMenu();


		},

		setMorris : function() {
			var
				currentTime = new Date(),
				dateLoop = new Date(),
				CHART_HEIGHT = 340,
				months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				chart_el = $( '#morris-area-chart' ),
				chart_args = {
					parse : {},
					dates : new Array(),
					keys : new Array(),
					color : new Array(),
					label : new Array(),
					data : new Array()
				};

			if( chart_el.length > 0 ) {

				dateLoop.setMonth( dateLoop.getMonth() - 6 );

				while( dateLoop < currentTime ) {
					dateLoop.setMonth( dateLoop.getMonth() + 1 );
					chart_args.dates.push( dateLoop.getFullYear().toString() + ( "0" + ( dateLoop.getMonth() + 1 ) ).slice( -2 ) + ( "0" + ( dateLoop.getDay() + 1 ) ).slice( -2 ) );
				}

				$.each( chart_el.data( 'values' ), function( intPostID, arrChartMeta ) {
					chart_args.keys.push( intPostID );
					chart_args.color.push( arrChartMeta.color );
					chart_args.label.push( arrChartMeta.title );
					$.each( arrChartMeta.values, function( intvalueKey, arrValueMeta ) {
						var output = {};
						if( typeof chart_args.parse[ arrValueMeta.period ] == 'undefined' ) {
							chart_args.parse[ arrValueMeta.period ] = {};
						}
						chart_args.parse[ arrValueMeta.period ][ intPostID ] = parseInt( arrValueMeta.count );
						//chart_args.parse[ arrValueMeta.period ].push( output );
					});
				} );

				$.each( chart_args.dates, function( intDateIndex, strDate ) {
					var output = {};
					output.period = strDate.slice( 0, 4 ) + '-' + strDate.slice( 4, 6 ) + '-' + strDate.slice( 6 );
					$.each( chart_args.keys, function( intKeyIndex, strKey ) {
						var parse = chart_args.parse[ strDate ] || {};
						output[ strKey ] = parse[ strKey ] || 0;
					} );
					chart_args.data.push( output );
				} );

				/**
				Morris.Area({
					element: 'morris-area-chart',
					data: chart_args.data,
					xkey: 'period',
					ykeys: chart_args.keys,
					labels: chart_args.label,
					xLabelFormat: function (x) {
						var IndexToMonth = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
						var month = IndexToMonth[ x.getMonth() ];
						var year = x.getFullYear();
						return month + ' ' + year;
					},
					dateFormat: function (x) {
						var IndexToMonth = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
						var month = IndexToMonth[ new Date(x).getMonth() ];
						var year = new Date(x).getFullYear();
						return month + ' ' + year;
					},
					pointSize: 3,
					fillOpacity: 0.5,
					pointStrokeColors: chart_args.color,
					behaveLikeLine: true,
					gridLineColor: '#e0e0e0',
					lineWidth: 1,
					hideHover: 'auto',
					lineColors: chart_args.color,
					resize: true
				}); */


				/**
				MG.data_graphic({
					title: 'Downloads',
					description: 'This graphics shows Firefox GA downloads for the past six months.',
					data: MG.convert.date( chart_args.data, 'period' ), // an array of objects, such as [{value:100,date:...},...]
					width: 600,
					height: 250,
					target: '#morris-area-chart', // the html element that the graphic is inserted in
					/**
					x_accessor: 'period',  // the key that accesses the x value
					y_accessor: 'value' // the key that accesses the y value * /
				}); */
			}
		},

		getMonthString : function( month ) {
			var months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
			return typeof months[ month ] != 'undefined' ? months[ month ] : null;
		},


		parseChartData : function( data, limit, type ) {

			var
				obj = this,
				current_time = new Date(),
				start_time = new Date(),
				chart_data = {
					months : new Array(),
					color : new Array(),
					data : new Array()
				},
				_limit = parseInt( limit ) || 0,
				_type = parseInt( type ) || 0;

			switch( _type ) {
				case 0:
					start_time.setDate( current_time.getDate() - _limit );
					break;

				case 2:
					start_time.setMonth( current_time.getMonth() - _limit );
					break;
			}


			while( start_time < current_time ) {

				// chart_data.months.push( obj.getMonthString( start_time.getMonth() ) );
				switch( _type ) {

					case 0:
						start_time.setDate( start_time.getDate() + 1 );
						chart_data.months.push(
							start_time.getFullYear().toString() +
							( '0' + (start_time.getMonth() + 1 ).toString() ).split(-2) +
							( '0' + (start_time.getDate() + 1 ).toString() ).split(-2)
						);
						break;

					case 2:
						start_time.setMonth( start_time.getMonth() + 1 );
						chart_data.months.push( start_time.getFullYear().toString() + ( '0' + (start_time.getMonth() + 1 ).toString() ).split(-2) );
						break;

				}
			}

			$.each( data, function( intPostID, arrChartMeta ) {
				var
					count_data = new Array(),
					start_time = new Date();

				switch( _type ) {
					case 0:
						start_time.setDate( current_time.getDate() - _limit );
						break;
					case 2:
						start_time.setMonth( current_time.getMonth() - _limit );
						break;
				}
				while( start_time < current_time ) {
					var _thisItemCount = 0;

					switch( _type ) {
						case 0:
							start_time.setDate( start_time.getDate() + 1 );
							break;
						case 2:
							start_time.setMonth( start_time.getMonth() + 1 );
							break;
					}

					$.each( arrChartMeta.values, function( intvalueKey, arrValueMeta ) {
						var _thisItemTime;
						switch( _type ) {
							case 0:
								_thisItemTime = new Date( arrValueMeta.period.substring( 0, 4 ) + '-' +  arrValueMeta.period.substring( 4,6 ) + '-' + arrValueMeta.period.substring( 6 ) );
								break;
							case 2:
								_thisItemTime = new Date( arrValueMeta.period.substring( 0, 4 ) + '-' +  arrValueMeta.period.substring( 4 ) + '-01' );
								break;
						}

						if( _type == 0 && ( start_time.getFullYear() == _thisItemTime.getFullYear() && start_time.getMonth() == _thisItemTime.getMonth() && start_time.getDate() == _thisItemTime.getDate() ) ) {
							_thisItemCount = arrValueMeta.count;
							return false;
						}

						if( _type == 2 && ( start_time.getFullYear() == _thisItemTime.getFullYear() && start_time.getMonth() == _thisItemTime.getMonth() ) ) {
							_thisItemCount = arrValueMeta.count;
							return false;
						}
					} );
					count_data.push( _thisItemCount );
				}

				chart_data.data.push({
					label : arrChartMeta.title,
					backgroundColor: arrChartMeta.color,
					borderColor: arrChartMeta.color,
					data: count_data,
					fill : '+2',
				});
				chart_data.color.push( arrChartMeta.color );
			} );

			return {
				labels : chart_data.months,
				datasets : chart_data.data

			};
		},

		parseChartData_old : function( data ) {

			var
				obj = this,
				current_time = new Date(),
				start_time = new Date(),
				chart_data = {
					months : new Array(),
					data : new Array()
				};

			start_time.setMonth( current_time.getMonth() - 6 );
			while( start_time < current_time ) {
				start_time.setMonth( start_time.getMonth() + 1 );
				// chart_data.months.push( obj.getMonthString( start_time.getMonth() ) );
				chart_data.months.push( start_time.getFullYear().toString() + ( '0' + (start_time.getMonth() + 1 ).toString() ).split(-2) );
			}

			$.each( data, function( intPostID, arrChartMeta ) {
				var
					count_data = new Array(),
					start_time = new Date();

				start_time.setMonth( current_time.getMonth() - 6 );
				while( start_time < current_time ) {
					var _thisItemCount = 0;
					start_time.setMonth( start_time.getMonth() + 1 );

					$.each( arrChartMeta.values, function( intvalueKey, arrValueMeta ) {
						var _thisItemTime = new Date( arrValueMeta.period.substring( 0, 4 ) + '-' +  arrValueMeta.period.substring( 4 ) + '-01' );
						if( start_time.getFullYear() == _thisItemTime.getFullYear() && ( start_time.getMonth() + 1 ) == _thisItemTime.getMonth() ) {
							_thisItemCount = arrValueMeta.count;
							return false;
						}
					} );
					count_data.push( _thisItemCount );
				}
				chart_data.data.push({
					label : arrChartMeta.title,
					backgroundColor: arrChartMeta.color,
					borderColor: arrChartMeta.color,
					data: count_data,
					fill : false,
				});
			} );

			return {
				labels : chart_data.months,
				datasets : chart_data.data

			};


			/**
			{
						labels: ["January", "February", "March", "April", "May", "June", "July"],
						datasets: [{
							label: "My First dataset",
							backgroundColor: window.chartColors.red,
							borderColor: window.chartColors.red,
							data: [
								7, 6, 5, 4, 3, 2, 1
							],
							fill: false,
						}, {
							label: "My Second dataset",
							fill: false,
							backgroundColor: window.chartColors.blue,
							borderColor: window.chartColors.blue,
							data: [
								1, 2, 3, 4, 5, 6,7
							],
						}]
					},
					*/


		},

		setChart : function() {

			var
				obj = this,
				elements = $( 'canvas.spot-mydahsobard-report-chart' );
			if( ! elements.length  ) {
				return false;
			}

			elements.map( function() {
				var element = $( this );
				new Chart( element.get(0).getContext( '2d' ) , {
					type: element.data( 'graph' ) || 'line',
					data: obj.parseChartData( element.data( 'values' ), element.data( 'limit' ), element.data( 'type' ) ),
					options: {
						responsive: true,
						title: { display:false, text:'Chart.js Line Chart' },
						tooltips: { mode: 'index', intersect: false },
						hover: { mode: 'nearest', intersect: true },
						scales: {
							xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: element.data( 'x' )
								}
							}],
							yAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: element.data( 'y' )
								}
							}]
						}
					}
				} );
			} );
		},

		setEvent : function() {
			var
				obj = this,
				LIST_WRAP = $( '.mypage-my-events-wrap' ),
				FORM_DELETE = LIST_WRAP.parent().find( 'form.delete-event-form' ),
				STR_DELETE = LIST_WRAP.data( 'delete-comment' ),
				BTN_DELETE = $( 'a.remove.action', LIST_WRAP ),
				DATE_PICKER_FIELD = $( 'input[data-date-picker]' );

			BTN_DELETE.on( 'click', function() {
				if( confirm( STR_DELETE ) ) {
					LIST_WRAP.closest( '.card' ).addClass( 'processing' );
					FORM_DELETE.find( '[name="event_id"]' ).val( $( this ).closest( 'li' ).data( 'id' ) );
					FORM_DELETE.submit();
				}
			} );

			DATE_PICKER_FIELD.datepicker( {
				dateFormat: 'yy-mm-dd',
				numberOfMonths: 3,
				changeMonth: true,
				changeYear: true
			} );
		},

		setEventUploader : function() {

			var obj = this;

			 /* Apply jquery ui sortable on gallery items */
			$( "#lava-multi-uploader" ).sortable({
				revert: 100,
				placeholder: "sortable-placeholder",
				cursor: "move"
			});

			/* initialize uploader */
			var uploaderArguments = {
				browse_button: 'select-images',          // this can be an id of a DOM element or the DOM element itself
				file_data_name: 'lava_multi_uploader',
				drop_element: 'lava-multi-uploader-drag-drop',
				url: obj.param.ajaxurl + '?action=' + obj.param.event_hook + 'upload_detail_images',
				filters: {
					mime_types : [
						{ title : 'image', extensions : "jpg,jpeg,gif,png" }
					],
					max_file_size: '10000kb',
					prevent_duplicates: true
				}
			};


			var uploader = new plupload.Uploader( uploaderArguments );
			uploader.init();

			$('#select-images').click(function(event){
				event.preventDefault();
				event.stopPropagation();
				uploader.start();
			});

			/* Run after adding file */
			uploader.bind('FilesAdded', function(up, files) {
				var html = '';
				var galleryThumb = "";
				plupload.each(files, function(file) {
					galleryThumb += '<div id="holder-' + file.id + '" class="gallery-thumb">' + '' + '</div>';
				});
				document.getElementById('lava-multi-uploader').innerHTML += galleryThumb;
				up.refresh();
				uploader.start();
			});


			/* Run during upload */
			uploader.bind('UploadProgress', function(up, file) {
				document.getElementById( "holder-" + file.id ).innerHTML = '<span>' + file.percent + "%</span>";
			});


			/* In case of error */
			uploader.bind('Error', function( up, err ) {
				document.getElementById('errors-log').innerHTML += "<br/>" + "Error #" + err.code + ": " + err.message;
			});


			/* If files are uploaded successfully */
			uploader.bind('FileUploaded', function ( up, file, ajax_response ) {
				var response = $.parseJSON( ajax_response.response );

				if ( response.success ) {

					var galleryThumbHtml = '<img src="' + response.url + '" alt="" />' +
					'<a class="remove-image" data-event-id="' + 0 + '"  data-attachment-id="' + response.attachment_id + '" href="#remove-image" ><i class="fa fa-trash-o"></i></a>' +
					'<a class="mark-featured" data-event-id="' + 0 + '"  data-attachment-id="' + response.attachment_id + '" href="#mark-featured" ><i class="fa fa-star-o"></i></a>' +
					'<input type="hidden" class="gallery-image-id" name="gallery_image_ids[]" value="' + response.attachment_id + '"/>' +
					'<span class="loader"><i class="fa fa-spinner fa-spin"></i></span>';

					document.getElementById( "holder-" + file.id ).innerHTML = galleryThumbHtml;

					bindThumbnailEvents();  // bind click event with newly added gallery thumb
				} else {
					// log response object
					console.log ( response );
				}
			});

			/* Bind thumbnails events with newly added gallery thumbs */
			var bindThumbnailEvents = function () {

				// unbind previous events
				$('a.remove-image').unbind('click');
				$('a.mark-featured').unbind('click');

				// Mark as featured
				$('a.mark-featured').click(function(event){

					event.preventDefault();

					var $this = $( this );
					var starIcon = $this.find( 'i');

					if ( starIcon.hasClass( 'fa-star-o' ) ) {   // if not already featured

						$('.gallery-thumb .featured-img-id').remove();      // remove featured image id field from all the gallery thumbs
						$('.gallery-thumb .mark-featured i').removeClass( 'fa-star').addClass( 'fa-star-o' );   // replace any full star with empty star

						var $this = $( this );
						var input = $this.siblings( '.gallery-image-id' );      //  get the gallery image id field in current gallery thumb
						var featured_input = input.clone().removeClass( 'gallery-image-id' ).addClass( 'featured-img-id' ).attr( 'name', 'featured_image_id' );     // duplicate, remove class, add class and rename to full fill featured image id needs

						$this.closest( '.gallery-thumb' ).append( featured_input );     // append the cloned ( featured image id ) input to current gallery thumb
						starIcon.removeClass( 'fa-star-o' ).addClass( 'fa-star' );      // replace empty star with full star

					}

				}); // end of mark as featured click event


				// Remove gallery images
				$('a.remove-image').click(function(event){

					event.preventDefault();
					var $this = $(this);
					var gallery_thumb = $this.closest('.gallery-thumb');
					var loader = $this.siblings('.loader');

					loader.show();

					var removal_request = $.ajax({
						url: obj.param.ajaxurl,
						type: "POST",
						data: {
							property_id : $this.data('event-id'),
							attachment_id : $this.data('attachment-id'),
							action : obj.param.event_hook + 'remove_detail_images',
						},
						dataType: "html"
					});

					removal_request.done(function( response ) {
						var result = $.parseJSON( response );
						if( result.attachment_removed ){
							gallery_thumb.remove();
						} else {
							document.getElementById('errors-log').innerHTML += "<br/>" + "Error : Failed to remove attachment";
						}
					});

					removal_request.fail(function( jqXHR, textStatus ) {
						alert( "Request failed: " + textStatus );
					});

				});  // end of remove gallery thumb click event

			};  // end of bind thumbnail events

			bindThumbnailEvents(); // run it first time - required for property edit page
		},

		setLeftMenu : function() {
			var
				obj = this,
				INT_COLLAPSE_WIDTH = 60,
				BTN_SIDEBAR_SWITCHER = $( '.dashboard-sidebar-switcher' ),
				HEADER_ELEMENT = $( '.navbar-default.navbar' ),
				SIDEBAR_ELEMENT = $( '.navbar-default.sidebar' ),
				CONTENT_ELEMENT = $( '#content-page-wrapper' ),
				LOGO_ELEMENT = $( '.page-brand', HEADER_ELEMENT ),
				INT_SIDEBAR_WIDTH = SIDEBAR_ELEMENT.width(),
				COOKIE_KEY = 'javospot_mypage_sidebar_switcher_onoff',
				IS_SWITCHER_COLLAPSE = this.getCookie( COOKIE_KEY ) == 'yes',
				getSidebarTop = function() {
					var
						adminbar = $( '#wpadminbar' ).height() || 0,
						headerbar = HEADER_ELEMENT.height() || 0;
					return adminbar;
					// return parseInt( adminbar + headerbar );
				};
			// SIDEBAR_ELEMENT.find( '#side-menu' ).width( INT_SIDEBAR_WIDTH );
			BTN_SIDEBAR_SWITCHER.on( 'click', function( e, _param ) {
				var param = _param || {};
				if( $( window ).width() > 768 ) {
					if( $( this ).hasClass( 'active' ) ) {
						$( 'body' ).removeClass( 'sidebar-closed' );
						SIDEBAR_ELEMENT.width( INT_SIDEBAR_WIDTH );// .find( '.menu-titles, .section-title' ).removeClass( 'hidden' );
						/** LOGO_ELEMENT.width( INT_SIDEBAR_WIDTH );
						HEADER_ELEMENT.find( 'span.hidden-xs' ).removeClass( 'hidden' ); */
						CONTENT_ELEMENT.css( 'margin-left', INT_SIDEBAR_WIDTH );
						obj.setCookie( COOKIE_KEY, '' );
						BTN_SIDEBAR_SWITCHER.removeClass( 'active' );
					}else{
						$( 'body' ).addClass( 'sidebar-closed' );
						SIDEBAR_ELEMENT.width( INT_COLLAPSE_WIDTH ); // .find( '.menu-titles, .section-title' ).addClass( 'hidden' );;
						/** LOGO_ELEMENT.width( INT_COLLAPSE_WIDTH );
						HEADER_ELEMENT.find( 'span.hidden-xs' ).addClass( 'hidden' ); */
						CONTENT_ELEMENT.css( 'margin-left', INT_COLLAPSE_WIDTH );
						obj.setCookie( COOKIE_KEY, 'yes' );
						BTN_SIDEBAR_SWITCHER.addClass( 'active' );
					}
				}

				/**
				BTN_SIDEBAR_SWITCHER.toggleClass( 'active' );

				if( true === param.activeClass ) {
					$( this ).addClass( 'active' );
				} **/
			} );

			if( IS_SWITCHER_COLLAPSE === true ) {
				$( window ).on( 'load', function() {
					BTN_SIDEBAR_SWITCHER.trigger( 'click', { activeClass : true } );
				} );
			}else{

			}

		},

		setRightMenu : function() {
			var
				opener = $( '.overlay-sidebar-opener' ),
				panel = $( '.quick-view' ),
				panel_width = panel.width(),
				body_color = $( 'body' ).css( 'background-color' );

			panel.find( 'ul' ).width( panel_width );
			opener.on( 'click', function() {
				if( opener.hasClass( 'active' ) ) {
					panel.css( 'margin-right', -(panel_width) + 'px' );
					$( '.jv-my-page' ).removeClass( 'overlay' );
				}else{
					panel.css( 'margin-right', 0 );
					$( '.jv-my-page' ).addClass( 'overlay' );
				}
				opener.toggleClass( 'active' );
			} );
		},

		setCookie : function( key, value ) {
			var
				EXPIRE = new Date( new Date().getMonth() + 1 ),
				_COOKIE = key + '=' + escape( value ) + ';expires=0; path=/';
			document.cookie = _COOKIE;
		},

		getCookie : function( key, _default ) {
			var
				REGEXP = new RegExp( key + "=(.*?)(?:;|$)", "g" ),
				QUERY = REGEXP.exec( document.cookie ),
				_RETURN = _default;
			if( QUERY !== null ) {
				_RETURN = QUERY[1];
			}
			return _RETURN;
		}
	}

	new javoMyDashboard;

	$( '.counter' ).counterUp({ dealy: 100, time: 1200 } );

	$( '.javo-mypage-toast' ).each( function() {
		$.toast({
			heading: $( this ).data( 'heading' ),
			text: $( this ).data( 'content' ),
			position: 'top-right',
			loaderBg: '#ff6849',
			icon: 'info',
			hideAfter: 10000,
			stack: 6
		});
	} );

	jQuery(document).ready(function ($) {

		"use strict";

		var body = $("body");

		$(function () {
			$(".preloader").fadeOut();
			$('#side-menu').metisMenu();
		});

		$("body").toggleClass("fix-header");
		$("body").toggleClass("fix-sidebar");

		$(function () {
			var set = function () {
					var topOffset = 60,
						width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width,
						height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
					if (width < 768) {
						$('div.navbar-collapse').addClass('collapse');
						topOffset = 100;
					} else {
						$('div.navbar-collapse').removeClass('collapse');
					}

					if (width < 1170) {
						body.addClass('sidebar-closed');
						$(".open-close i").removeClass('icon-arrow-left-circle');
						$(".sidebar-nav, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
						$(".logo span").hide();
					} else {
						body.removeClass('sidebar-closed');
						$(".open-close i").addClass('icon-arrow-left-circle');
						$(".logo span").show();
					}

					height = height - topOffset;
					if (height < 1) {
						height = 1;
					}
					if (height > topOffset) {
						$("#content-page-wrapper").css("min-height", (height) + "px");
					}
				},
				url = window.location,
				element = $('ul.nav a').filter(function () {
					return this.href === url || url.href.indexOf(this.href) === 0;
				})/*.addClass('active')*/.parent().parent().addClass('in').parent();
			if (element.is('li')) {
				element.addClass('active');
			}
			$(window).ready(set);
			$(window).on("resize", set);
		});

		$(".open-close").on('click', function () {
			if ($("body").hasClass("sidebar-closed")) {
				$("body").trigger("resize");
				$(".sidebar-nav, .slimScrollDiv").css("overflow", "hidden").parent().css("overflow", "visible");
				$("body").removeClass("sidebar-closed");
				$(".open-close i").addClass("icon-arrow-left-circle");
				$(".logo span").show();
			} else {
				$("body").trigger("resize");
				$(".sidebar-nav, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
				$("body").addClass("sidebar-closed");
				$(".open-close i").removeClass("icon-arrow-left-circle");
				$(".logo span").hide();
			}
		});

		(function ($, window, document) {
			var panelSelector = '[data-perform="panel-collapse"]',
				panelRemover = '[data-perform="panel-dismiss"]';
			$(panelSelector).each(function () {
				var collapseOpts = {
						toggle: false
					},
					parent = $(this).closest('.panel'),
					wrapper = parent.find('.panel-wrapper'),
					child = $(this).children('i');
				if (!wrapper.length) {
					wrapper = parent.children('.panel-heading').nextAll().wrapAll('<div/>').parent().addClass('panel-wrapper');
					collapseOpts = {};
				}
				wrapper.collapse(collapseOpts).on('hide.bs.collapse', function () {
					child.removeClass('ti-minus').addClass('ti-plus');
				}).on('show.bs.collapse', function () {
					child.removeClass('ti-plus').addClass('ti-minus');
				});
			});

			$(document).on('click', panelSelector, function (e) {
				e.preventDefault();
				var parent = $(this).closest('.panel'),
					wrapper = parent.find('.panel-wrapper');
				wrapper.collapse('toggle');
			});

			$(document).on('click', panelRemover, function (e) {
				e.preventDefault();
				var removeParent = $(this).closest('.panel');

				function removeElement() {
					var col = removeParent.parent();
					removeParent.remove();
					col.filter(function () {
						return ($(this).is('[class*="col-"]') && $(this).children('*').length === 0);
					}).remove();
				}
				removeElement();
			});
		}(jQuery, window, document));

		$(function () {
			$('[data-toggle="tooltip"]').tooltip();
		});

		$(function () {
			$('[data-toggle="popover"]').popover();
		});

		$(".list-task li label").on("click", function () {
			$(this).toggleClass("task-done");
		});
		$(".settings_box a").on("click", function () {
			$("ul.theme_color").toggleClass("theme_block");
		});

		$(".collapseble").on("click", function () {
			$(".collapseblebox").fadeToggle(350);
		});

		body.trigger("resize");

		$('.visited li a').on("click", function (e) {
			$('.visited li').removeClass('active');
			var $parent = $(this).parent();
			if (!$parent.hasClass('active')) {
				$parent.addClass('active');
			}
			e.preventDefault();
		});

		$('#to-recover').on("click", function () {
			$("#loginform").slideUp();
			$("#recoverform").fadeIn();
		});

		$(".navbar-toggle").on("click", function () {
			$(".navbar-toggle i").toggleClass("ti-menu").addClass("ti-close");
		});

		$('#myTable').DataTable();
		var table = $('#example').DataTable({
			"columnDefs": [{
				"visible": false,
				"targets": 2
			}],
			"order": [
				[2, 'asc']
			],
			"displayLength": 25,
			"drawCallback": function(settings) {
				var api = this.api();
				var rows = api.rows({
					page: 'current'
				}).nodes();
				var last = null;
				api.column(2, {
					page: 'current'
				}).data().each(function(group, i) {
					if (last !== group) {
						$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
						last = group;
					}
				});
			}
		});

		$('#example tbody').on('click', 'tr.group', function() {
			var currentOrder = table.order()[0];
			if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
				table.order([2, 'desc']).draw();
			} else {
				table.order([2, 'asc']).draw();
			}
		});
	});
} );