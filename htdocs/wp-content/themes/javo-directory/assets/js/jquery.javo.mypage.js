( function( $ ){
	"use strict";

	var javoThemes_MypageScript = function () {
		this.args = jvfrm_spot_myage_args;
		this.init();
	}

	javoThemes_MypageScript.prototype = {

		constructor : javoThemes_MypageScript,

		init : function () {
			this.editPage();
		},

		checkEmptyField : function( form ) {
			var
				_form = $( form ),
				_allow = true,
				_nClass = 'isNull';

			$( '[data-required]', _form ).each( function( index, element ) {
				if( $( element ).val()  ) {
					$( element ).removeClass( _nClass );
					_allow = _allow && true;
				}else{
					$( element ).addClass( _nClass );
					_allow = _allow && false;
				}
			} );
			return _allow;

		},

		editPage : function() {
			var obj = this;

			$( 'input[type="button"]#btn_save.btn' ).on( 'click', function() {
				var form = $( this ).closest( 'form' );
				if( obj.checkEmptyField( form ) ) {
					form.submit();
				}else{
					$( 'html, body' ).animate( { scrollTop: 0 }, 500 );
				}
				return false;
			} );
		}

	}

	new javoThemes_MypageScript;

} )( jQuery, window );