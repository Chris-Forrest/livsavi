<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Javo
 * @since Javo Themes 1.0
 */ ?>
							<?php get_template_part('library/dashboard/' . jvfrm_spot_dashboard()->page_style . '/right', 'sidebar'); ?>
							</div><!-- /.container-fluid -->
							<?php get_template_part('library/dashboard/' . jvfrm_spot_dashboard()->page_style . '/footer'); ?>
						</div><!-- content-page-wrapper -->
					</div><!-- /#wrapper ( header.php ) -->
				</div><!--/.container-->
			</div><!--jv-my-page-->
			<?php wp_footer(); ?>
		</div>
	</body>
</html>