<?php
class jvfrm_spot_dashboard {

	Const MYPAGE_ENQUEUE_FORMAT = 'jvfrm_spot_db_enq_%s';

	public $page_style;
	public $is_db_page = false;

	private static $pages = Array();
	public static $instance = false;

	public function __construct() {
		$this->setVariables();
		$this->loadFiles();
		$this->registerHooks();

		$this->eventAjaxHooks();
	}

	public function setVariables() {
		$this->page_style = jvfrm_spot_tso()->get( 'mypage_style', 'type-c' );
	}

	public function registerHooks() {
		add_action( 'init', Array( __class__, 'define_slug' ) );
		add_action( 'init', Array( __class__, 'rewrite' ) );
		add_filter( 'template_include', Array( __class__, 'dashboard_template'), 1, 1 );
	}

	public function loadFiles() {
		require_once( 'functions-dashboard.php' );
	}

	public function getEnqueueHandle( $suffix='' ) {
		return sprintf( self::MYPAGE_ENQUEUE_FORMAT, sanitize_title( $suffix ) );
	}

	public static function define_slug() {
		self::$pages = apply_filters(
			'jvfrm_spot_dashboard_slugs',
			Array(
				'JVFRM_SPOT_MEMBER_SLUG' => 'member',
				'JVFRM_SPOT_PROFILE_SLUG' => 'edit-my-profile',
				'JVFRM_SPOT_CHNGPW_SLUG' => 'change-password',
				'JVFRM_SPOT_MANAGE_SLUG' => 'manage',
			)
		);
		foreach( self::$pages as $key => $value ) {
			define( $key, $value);
		}
	}

	public function is_dashboard_page() { return $this->is_db_page; }

	static function dashboard_template( $template ) {
		global
			$wp_query
			, $jvfrm_spot_current_user;

		$GLOBALS[ 'jvfrm_spot_dashboard_type' ] = $page_style = jvfrm_spot_dashboard()->page_style;		

		if( get_query_var('pn') == 'member' ) {
			$jvfrm_spot_current_user = get_user_by('login', str_replace("%20", " ", get_query_var('user')));
			add_filter( 'body_class', Array(__class__, 'jvfrm_spot_dashboard_bodyclass_callback'));
			add_action( 'wp_enqueue_scripts', Array( __class__, 'wp_media_enqueue_callback' ) );

			if( !empty( $jvfrm_spot_current_user ) ) {
				self::$instance->is_db_page = true;
				// add_filter( 'wp_head', array( __class__, 'loadMypageLess' ) );
				// add_filter( 'body_class', Array(__class__, 'jvfrm_spot_dashboard_bodyclass_callback'));
				if( jvfrm_spot_dashboard()->page_style =='type-b' ){
					add_action( 'wp_enqueue_scripts', Array( __class__, 'mypage_header_apply_type_b' ) );
				}elseif( jvfrm_spot_dashboard()->page_style =='type-c' ) {
					// add_action( 'wp_enqueue_scripts', Array( __class__, 'wp_media_enqueue_callback' ) );
				}else{
					add_action('wp_enqueue_scripts'	, Array( __class__, 'mypage_header_apply' ) );
				}

				$GLOBALS[ 'jvfrm_spot_curUser' ] = $jvfrm_spot_current_user;
				$GLOBALS[ 'manage_mypage' ] = $jvfrm_spot_current_user->ID === get_current_user_id();

				add_filter( 'wp_title', Array( jvfrm_spot_dashboard(), 'dashboardTitle' ), 99, 2 );

				if( in_Array( get_query_var('sub_page'), self::$pages ) ) {

					if( ! apply_filters( 'jvfrm_spot_dashboard_allow_private_page', ( $GLOBALS[ 'manage_mypage' ] || current_user_can( 'manage_options' ) ), get_query_var( 'sub_page' ), $jvfrm_spot_current_user ) ) {
						if( ! in_array( get_query_var( 'sub_page' ), Array( 'favorite', 'contact' ) ) ) {
							return JVFRM_SPOT_DSB_DIR.'/mypage-private-page.php';
						}
					}
					return apply_filters(
						'jvfrm_spot_dashboard_custom_template_url'
						, JVFRM_SPOT_DSB_DIR . '/' . $page_style . '/mypage-' . get_query_var( 'sub_page' ) . '.php'
						, get_query_var( 'sub_page' )
					);
				} else {
					return JVFRM_SPOT_DSB_DIR.'/' . $page_style . '/mypage-member.php';
				}
			} else {
				return JVFRM_SPOT_DSB_DIR.'/mypage-no-user.php';
			}
		}
		return $template;
	}

	public function getDashboardTItle() {
		$objOnwerUser = jvfrm_spot_getDashboardUser();
		$strTitleFormat = esc_html__( '%1$s Profile', 'javospot' );
		if( $this->checkPermission( 'memberOnly' ) ) {
			$strTitleFormat = esc_html__( '%1$s Dashboard', 'javospot' );
		}
		return sprintf( $strTitleFormat, '<b>' . $objOnwerUser->display_name . '</b>' );

		/**
		$strSlugName = get_query_var( 'sub_page' );
		switch( $strSlugName ){
			case JVFRM_SPOT_PROFILE_SLUG:
				$output = esc_html__('Edit My Profile', 'javospot'); break;
			case JVFRM_SPOT_CHNGPW_SLUG:
				$output = esc_html__('Change Password', 'javospot'); break;
			case JVFRM_SPOT_MEMBER_SLUG:
			default:
				if( $this->checkPermission( 'memberOnly' ) ) {
					$output = esc_html__('My Dashboard', 'javospot');
				}else{
					$output = sprintf( esc_html__('%s\'s Profile', 'javospot'), jvfrm_spot_getDashboardUser()->display_name );
				}
		}
		return apply_filters( 'jvfrm_spot_dashboard_title', $output, $strSlugName ); **/
	}

	public function getBreadCrumbs() {

		$strSlugName = get_query_var( 'sub_page' );

		$arrBreadCrumbs = Array();
		$arrBreadCrumbs[] = esc_html__( "Dashboard", 'javospot' );
		if( ! empty( $strSlugName ) ) {
			$arrBreadCrumbs[] = $this->getDashboardTItle();
		}
		$arrBreadCrumbs = apply_filters( 'jvfrm_spot_dashboard_breadcrumbs', $arrBreadCrumbs, $strSlugName );

		$output = null;

		if( !empty( $arrBreadCrumbs ) ) {
			$output .= sprintf( '<ol class="breadcrumb">' );
			foreach( $arrBreadCrumbs as $arrItem ) {
				$output .= sprintf( '<li><a href="javascript:">%s</a></li>', $arrItem );
			}
			$output .= sprintf( '</ol>' );
		}
		return $output;
	}

	public function dashboardTitle( $title, $sep=' | ' ) {
		$arrOutput = Array( $this->getDashboardTItle(), esc_html__( "My Dashboard", 'javospot'), get_bloginfo( 'name' ) );
		return join( $sep, $arrOutput );
	}

	public static function loadMypageLess( $args=Array() ) {
		$arrDirectory = Array( JVFRM_SPOT_THEME_DIR, 'assets/css/extend', 'jv-member/css/colors' );
		wp_enqueue_style( 'jvfrm_spot_mypage_less', join( '/', $arrDirectory ) . '/jv-my-page.less' );
	}

	static function jvfrm_spot_dashboard_bodyclass_callback( $classes ) {

		$classes[] = 'javo-dashboard';
		$classes[] = self::$instance->page_style;
		$classes[] = 'page-dashboard-' . get_query_var( 'sub_page' );

		if( is_admin_bar_showing() ) {
			$classes[] = 'admin-bar';
		}

		if( self::$instance->checkPermission( 'memberOnly' ) ) {
			$classes[] = 'author-own-page';
		}

		return $classes;
	}

	public static function wp_media_enqueue_callback() {
		wp_enqueue_media();

		$arrDBScripts = Array(
			/* Moved to font-script.js
			'tether.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'jv-member/bootstrap/dist/js' ), */
			'bootstrap-extension.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/bootstrap-extension/js' ),
			//'sidebar-nav.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/sidebar-nav/dist' ),
			'metisMenu.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/metisMenu/dist' ),
			'jquery.slimscroll.js' => Array( 'ver' => '0.0.0', 'pos' => 'jv-member/js' ),
			//'waves.js' => Array( 'ver' => '0.0.0', 'pos' => 'jv-member/js' ),
			'jquery.waypoints.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/waypoints/lib' ),
			'jquery.counterup.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/counterup' ),
			//'raphael-min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/raphael' ),
			// 'morris.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/morrisjs' ),
			'mypage.js' => Array( 'ver' => '0.0.0', 'pos' => 'jv-member/js' ),
			//'jquery.sparkline.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/jquery-sparkline' ),
			//'jquery.charts-sparkline.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/jquery-sparkline' ),
			'jquery.toast.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/toast-master/js' ),
			//'jQuery.style.switcher.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/styleswitcher' ),
			'jquery.dataTables.min.js' => Array( 'ver' => '0.0.0', 'pos' => 'plugins/datatables' ),
			/**
			'd3.min.js' => Array( 'ver' => '0.0.0' ),
			'metricsgraphics.min.js' => Array( 'ver' => '0.0.0' ), **/
			'Chart.min.js' => Array( 'ver' => '0.0.0', 'pos' => '../Chart.js' ),
			// 'Chart.bundle.min.js' => Array( 'ver' => '0.0.0', 'pos' => '../Chart.js' ),
			'utils.js' => Array( 'ver' => '0.0.0', 'pos' => '../Chart.js' ),
		);

		$arrDBStyles = Array(
			/**
			'bootstrap.min.css' => array( 'ver' => '0.0.0', 'pos' => 'jv-member/bootstrap/dist/css' ),
			'bootstrap-extension.css' => array( 'ver' => '0.0.0', 'pos' => 'plugins/bootstrap-extension/css' ),
			'sidebar-nav.min.css' => array( 'ver' => '0.0.0', 'pos' => 'plugins/sidebar-nav/dist' ),
			'jquery.toast.css' => array( 'ver' => '0.0.0', 'pos' => 'plugins/toast-master/css' ),
			'morris.css' => array( 'ver' => '0.0.0', 'pos' => 'plugins/morrisjs' ),
			'animate.css' => array( 'ver' => '0.0.0', 'pos' => 'jv-member/css' ),
			'style.css' => array( 'ver' => '0.0.0', 'pos' => 'jv-member/css' ),
			'animate.css' => array( 'ver' => '0.0.0', 'pos' => 'jv-member/css' ),
			'jv-my-page.less' => array( 'ver' => '0.0.0', 'pos' => 'jv-member/css/colors' ),
			*/
			'jquery.dataTables.min.css' => array( 'ver' => '0.0.0' ),
			'jquery-ui.min.css' => array( 'ver' => '0.0.0' ),
			//'metricsgraphics.css' => array( 'ver' => '0.0.0' ),
		);

		if( !empty( $arrDBScripts ) ) {
			foreach( $arrDBScripts as $strFileName => $arrFileMeta ) {

				if($strFileName != 'bootstrap.min.js')
					$arrDirectory = Array( JVFRM_SPOT_THEME_DIR, 'assets/js/extend' );
				else
					$arrDirectory = Array( JVFRM_SPOT_THEME_DIR );

				if( isset( $arrFileMeta[ 'pos' ] ) ) {
					$arrDirectory[] = $arrFileMeta[ 'pos' ];
				}
				$arrDirectory[] = $strFileName;

				wp_register_script(
					jvfrm_spot_dashboard()->getEnqueueHandle( $strFileName ),
					join( '/', $arrDirectory ),
					array( 'jquery' ), $arrFileMeta[ 'ver' ],
					true
				);
				jvfrm_spot_dashboard()->localize_script( $strFileName );
				wp_enqueue_script( jvfrm_spot_dashboard()->getEnqueueHandle( $strFileName ) );
			}
		}

		if( !empty( $arrDBStyles ) ) {
			foreach( $arrDBStyles as $strFileName => $arrFileMeta ) {

				$arrDirectory = Array( JVFRM_SPOT_THEME_DIR, 'assets/css/extend' );
				if( isset( $arrFileMeta[ 'pos' ] ) ) {
					$arrDirectory[] = $arrFileMeta[ 'pos' ];
				}
				$arrDirectory[] = $strFileName;

				wp_register_style(
					jvfrm_spot_dashboard()->getEnqueueHandle( $strFileName ), join( '/', $arrDirectory ), array(), $arrFileMeta[ 'ver' ]
				);
				wp_enqueue_style( jvfrm_spot_dashboard()->getEnqueueHandle( $strFileName ) );
			}
		}

		wp_localize_script(
			'javoThemes-Mypage', 'jvfrm_spot_myage_args', Array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
			)
		);
		wp_enqueue_script( 'javoThemes-Mypage' );

		wp_enqueue_script( 'plupload' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}

	public function localize_script( $js_name='' ) {

		switch( $js_name ) {

			case 'mypage.js' :
				wp_localize_script(
					jvfrm_spot_dashboard()->getEnqueueHandle( $js_name ),
					'spot_mypage_type_c_args',
					Array(
						'ajaxurl' => admin_url( 'admin-ajax.php' ),
						'event_hook' => function_exists( 'Lava_EventConnector' ) ? Lava_EventConnector()->upload->getAjaxHook() : null,
					)
				);
				break;
		}

	}

	public static function mypage_header_apply_type_b() {
		global $jvfrm_spot_current_user;

		$staticImage = true;
		$strBackground = JVFRM_SPOT_IMG_DIR . '/bg/mypage-bg.png';

		if( !empty( $jvfrm_spot_current_user->mypage_header ) ) {
			$mypage_header_meta = wp_get_attachment_image_src( $jvfrm_spot_current_user->mypage_header, 'full' );
			if( !empty( $mypage_header_meta[0] ) ) {
				$strBackground = $mypage_header_meta[0];
				$staticImage	= false;
			}
		}

		$output_html	= Array();
		$output_html[]	= "<style type=\"text/css\">";
		$output_html[]	= sprintf(
			'%s{%s}'
			, 'body.javo-dashboard.type-b', "background-image:url({$strBackground}); background-size:cover; background-attachment:fixed; background-position:center center;"
		);
		if( $staticImage )
			$output_html[]	= "body.javo-dashboard.type-b{ background-size:auto; background-position:-30% bottom; background-repeat:repeat-x; background-color:#C4E2E9 !important; }";
		$output_html[]	= "</style>";
		echo @implode( "\n", $output_html );
	}

	public static function mypage_header_apply() {
		global $jvfrm_spot_current_user;

		$strBackground	= JVFRM_SPOT_IMG_DIR . '/bg/mypage-bg.png';

		if( !empty( $jvfrm_spot_current_user->mypage_header ) ) {
			$mypage_header_meta = wp_get_attachment_image_src( $jvfrm_spot_current_user->mypage_header, 'full' );
			if( !empty( $mypage_header_meta[0] ) )
				$strBackground = $mypage_header_meta[0];
		}

		$output_html	= Array();
		$output_html[]	= "<style type=\"text/css\">";
		$output_html[]	= sprintf(
			'%s{%s}'
			, 'body.javo-dashboard .jv-my-page .top-row', "background-image:url({$strBackground}); background-size:cover; background-attachment:fixed; background-position:center center;"
		);
		$output_html[]	= "</style>";
		echo @implode( "\n", $output_html );
	}

	/**
	Action : admin_init
	rewrite
	**/
	static function rewrite() {
		add_rewrite_tag('%pn%'										, '([^&]+)');
		add_rewrite_tag('%user%'									, '([^&]+)');
		add_rewrite_tag('%sub_page%'								, '([^&]+)');
		add_rewrite_tag('%edit%'									, '([^&]+)');
		add_rewrite_tag('%update%'									, '([^&]+)');
		add_rewrite_rule( 'lava-my-add-form/?$'						, 'index.php?pn=addform', 'top');
		add_rewrite_rule( 'member/([^/]*)/?$'						, 'index.php?pn=member&user=$matches[1]', 'top');
		add_rewrite_rule( 'member/([^/]*)/page/([^/]*)/?$'			, 'index.php?pn=member&user=$matches[1]&paged=$matches[2]', 'top');
		add_rewrite_rule( 'member/([^/]*)/([^/]*)/?$'				, 'index.php?pn=member&user=$matches[1]&sub_page=$matches[2]', 'top');
		add_rewrite_rule( 'member/([^/]*)/([^/]*)/page/([^/]*)/?$'	, 'index.php?pn=member&user=$matches[1]&sub_page=$matches[2]&paged=$matches[3]', 'top');
		add_rewrite_rule( 'member/([^/]*)/([^/]*)/edit/([^/]*)/?$'	, 'index.php?pn=member&user=$matches[1]&sub_page=$matches[2]&edit=$matches[3]', 'top');
		//flush_rewrite_rules();
	}

	public static function getInstance(){
		if( !self::$instance )
			self::$instance = new self;
		return self::$instance;
	}

	public function checkPermission( $allowed='' ) {
		$is_allowed = false;
		switch( $allowed ) {
			case 'logged_user' :
				$is_allowed = is_user_logged_in();
				break;

			case 'member' :
				$is_allowed = is_user_logged_in() && ( current_user_can( 'manage_options' ) || jvfrm_spot_getDashboardUser()->ID == get_current_user_id() );
				break;

			case 'admin' :
				$is_allowed = is_user_logged_in() && current_user_can( 'manage_options' );
				break;

			case 'memberOnly' :
				$is_allowed = is_user_logged_in() && jvfrm_spot_getDashboardUser()->ID == get_current_user_id();
				break;

			case 'all' :
			default:
				$is_allowed = true;
		}
		return $is_allowed;
	}

	public function getOwnerAvatar( $user_id=9, $size='thumbnail', $args=Array() ) {

		$width = is_array( $size ) && isset( $size[0] ) && is_numeric( $size[0] ) ?  $size[0] : 0;
		$height = is_array( $size ) && isset( $size[1] ) && is_numeric( $size[1] ) ?  $size[1] : 0;
		$classes = isset( $args[ 'class' ] ) ? $args[ 'class' ] : null;

		$output = sprintf( '<img src="%s" width="%s" height="%s" class="%s">', jvfrm_spot_tso()->get( 'no_image', '' ), $width, $height, $classes );
		$intAvatarID = intVal( get_user_meta( $user_id, 'avatar', true ) );
		if( 0 < $intAvatarID ) {
			$strAvatar = wp_get_attachment_image( $intAvatarID, $size, false, $args );
			if( ! empty( $strAvatar ) ) {
				$output = $strAvatar;
			}
		}
		return $output;
	}

	public function eventAjaxHooks() {


	}

}
if( !function_exists( 'jvfrm_spot_dashboard' ) ) :
	function jvfrm_spot_dashboard() {
		return jvfrm_spot_dashboard::getInstance();
	}
	jvfrm_spot_dashboard();
endif;
