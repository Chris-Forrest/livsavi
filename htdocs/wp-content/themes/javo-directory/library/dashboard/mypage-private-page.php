<?php get_header( 'mypage' );?>
<div class="container" id="javo-user-404-page-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="error-template">
				<h1><i>"</i><?php esc_html_e("This is a private page", 'javospot') ?></h1>
				<div class="error-details">
					<p><?php esc_html_e("You don't have permission to access.", 'javospot') ?></p>
				</div>
				<div class="error-actions">
					<a href="javascript:history.back(-1);"><span><i class="fa fa-arrow-left"></i><?php esc_html_e('Go Back', 'javospot') ?></span></a>
				</div>
			</div><!--/.error-template-->
		</div><!-- /.col-md-12 -->
	</div><!--/.row -->
</div><!-- /#javo-user-404-page-wrap -->
<?php get_footer( 'mypage' );?>