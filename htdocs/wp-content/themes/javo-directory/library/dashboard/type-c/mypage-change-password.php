<?php
/**
 * Type C - Dashboard
 * My Dashboard > My List - Pending
 *
 */
global
	$jvfrm_spot_curUser,
	$manage_mypage;

require_once( JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php' );
$jvfrm_spot_CPW = jvfrm_spot_dashboard_change_pw();

get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><h4 class="card-title"><?php esc_html_e( "Change your password", 'javospot' ); ?></h4></div><!-- card-header -->
				<div class="card-block">
				<?php

				if( false !== $jvfrm_spot_CPW ) {
					$strCurrentStatus = is_wp_error( $jvfrm_spot_CPW ) ? 'alert-warning' : 'alert-success';
					$strCurrentStatusMessage = is_wp_error( $jvfrm_spot_CPW ) ? $jvfrm_spot_CPW->get_error_message() : $jvfrm_spot_CPW;
					printf( '<div class="alert %1$s">%2$s</div>', $strCurrentStatus, $strCurrentStatusMessage );
				} ?>
				<div class="row">
					<div class="col-md-12">
						<form method="post" name="javo-dashboard-change-pw">
							<div class="form-group row">
								<div class="col-xs-12 col-md-2">
									<label class="col-form-label">
										<?php esc_html_e( "Current Password", 'javospot');?>&nbsp;
									</label>
								</div><!-- col-xs-12 col-md-2 -->
								<div class="col-10">
									<input type="password" name="current_pass" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter your current password', 'javospot');?>">
								</div>
							</div><!-- form-group row -->
							<div class="form-group row">
								<div class="col-xs-12 col-md-2">
									<label class="col-form-label">
										<?php esc_html_e( "New password", 'javospot');?>&nbsp;
									</label>
								</div><!-- col-xs-12 col-md-2 -->
								<div class="col-10">
									<input type="password" name="new_pass" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter new password', 'javospot');?>">
								</div>
							</div><!-- form-group row -->
							<div class="form-group row">
								<div class="col-xs-12 col-md-2">
									<label class="col-form-label">
										<?php esc_html_e( "Retype new password", 'javospot');?>&nbsp;
									</label>
								</div><!-- col-xs-12 col-md-2 -->
								<div class="col-10">
									<input type="password" name="new_pass_confirm" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter new password again', 'javospot');?>">
								</div>
							</div><!-- form-group row -->
							<div class="form-group row">
								<div class="col-md-12 text-center">
									<input id="btn_save" class="btn btn-primary admin-color-setting" value="<?php esc_attr_e( "Save", 'javospot' )?>" type="submit">
								</div>
							</div>
							<?php wp_nonce_field( 'security', 'jvfrm_spot_dashboard_changepw_nonce' ); ?>
						</form>
					</div><!-- col-md-offset-1 -->
				</div><!-- Row -->

			</div> <!-- card-block -->
			</div> <!-- card -->
		</div> <!-- col-md-12 -->
	</div><!--/row-->
	<!-- Content End -->
<?php get_footer( 'mypage' );