<?php
$objCurrentLoginUser = new WP_User( get_current_user_id() );

$strSubmitLink = apply_filters( 'jvfrm_spot_core_submit_page_link', home_url() );
// $jvfrm_spot_user_avatar_meta = wp_get_attachment_image_src(get_user_meta($objCurrentLoginUser->ID, "avatar", true));
$jvfrm_spot_user_avatar_meta = jvfrm_spot_dashboard()->getOwnerAvatar( $objCurrentLoginUser->ID, Array( 36, 36 ), Array(
	'class' => 'img-circle'
) );
$jvfrm_spot_user_background	= wp_get_attachment_image_src(get_user_meta($objCurrentLoginUser->ID, 'mypage_header', true), 'jvfrm-spot-avatar' );
$isEventActivate = function_exists( 'Lava_EventConnector' ) && function_exists( 'tribe_get_events' );
if( jvfrm_spot_dashboard()->checkPermission( 'admin' ) ) {
	printf(
		'<input type="hidden" class="javo-mypage-toast" data-heading="%1$s" data-content="%2$s">',
		esc_html__( "Hello Admin !", 'javospot' ),
		esc_html__( "<p>Only site administrators can see all menus of other users to manage.</p><br><p>( Normar users can see only specific menus )</p>", 'javospot' )
	);
} ?>
<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top">
		<div class="navbar-header">
			<a href="javascript:void(0)" class="dashboard-sidebar-switcher navbar-toggle hidden-sm hidden-md hidden-lg" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="jv-icon3-left"></span>
				<span class="jv-icon3-right"></span>
			</a>
			<div class="page-brand">
				<a class="logo" href="<?php echo jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID ); ?>">
					<span class="logo-img"><?php echo $jvfrm_spot_user_avatar_meta; ?></span>
					<span class="hidden-xs"><?php echo jvfrm_spot_dashboard()->getDashboardTItle(); ?></span>
				</a>
			</div>
			<ul class="nav header-content-wrapper pull-left hidden-xs">
				<li><a href="javascript:void(0)" class="dashboard-sidebar-switcher"><i class="jv-icon3-left"></i></a></li>
			</ul>

			<ul class="nav header-content-wrapper pull-right">
				<?php
				if( jvfrm_spot_dashboard()->checkPermission( 'member' ) ) :
					if( function_exists( 'lv_directory_favorite' ) ) {
						$arrFavorites = lv_directory_favorite()->core->getFavorites( $objCurrentLoginUser->ID ); ?>
						<li class="dropdown favorite">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class=" jv-icon2-heart"></i>
								<?php if( 0 < sizeof( array_filter( $arrFavorites ) ) ) { ?>
									<div class="header-icon-notice"><span class="badge badge-info label-notice-num"><?php echo sizeof( array_filter( $arrFavorites ) ); ?></span></div>
								<?php } ?>
							</a>
							<ul class="dropdown-menu dropdown-favorites fade-inout drop-right triangle-arrow-right">
								<li>
									<div class="card listing-card narrow">
									 <div class="card-header">
										<h4 class="card-title">
											<?php printf( esc_html__( "You have %s favorite listings", 'javospot' ), sizeof( array_filter( $arrFavorites ) ) ); ?>
										</h4>
									  </div>
									<ul class="list-group list-group-flush">

										<?php
										$intCurrentFavoriteIndex =0;
										foreach( $arrFavorites as $arrFavoriteMeta ) {
											$objPost = isset( $arrFavoriteMeta[ 'post_id' ] ) ? get_post( $arrFavoriteMeta[ 'post_id' ] ) : null;
											if( empty( $objPost ) || 4 <= $intCurrentFavoriteIndex ) {
												continue;
											}
											$intCurrentFavoriteIndex++;
											jvfrm_spot_core()->template->load_template(
												'dashboard/part-mypage-widget-favorites',
												'.php',
												array(
													'jvfrm_spot_aricle_args' => (object) Array(
														'post' => $objPost,
														'date' => date( get_option( 'date_format' ), strtotime( $arrFavoriteMeta[ 'save_day' ] ) ),
														'real_date' => $arrFavoriteMeta[ 'save_day' ],
													),
												), false
											);
										} ?>

									</ul>
									<div class="card-footer text-center">
										<a class="text-center" href="<?php echo jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, 'favorite' ); ?>"> <?php esc_html_e( "See all saved listings", 'javospot' ); ?><i class="fa fa-angle-right"></i> </a>
									</div>
								</div>
								</li>
							</ul>
							<!-- /.dropdown-favorites -->
						</li>
						<?php
					} ?>

					<!-- /.dropdown -->

					<li class="dropdown my-list">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="jv-icon3-note"></i>
						</a>
						<ul class="dropdown-menu dropdown-listings drop-right fade-inout triangle-arrow-right">
							<li>
								<div class="card listing-card narrow actived">
									<div class="card-header">
										<h4 class="card-title">
											<?php
											printf(
												esc_html__( 'You have %1$s listings', 'javospot' ),
												jvfrm_spot_core()->getUserListingCount( get_current_user_id(), Array( 'publish', 'pending' ) )
											); ?>
										</h4>
									</div>
									<ul class="list-group list-group-flush">
										<?php
										foreach(
											array(
												array(
													'label' => esc_html__( "Published Listings", 'javospot' ),
													'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, 'item-published' ),
													'current' => intVal( jvfrm_spot_core()->getUserListingCount( get_current_user_id(), Array( 'publish' ) ) ),
												),
												array(
													'label' => esc_html__( "Pending Listings", 'javospot' ),
													'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, 'item-pending' ),
													'current' => intVal( jvfrm_spot_core()->getUserListingCount( get_current_user_id(), Array( 'pending' ) ) ),
												),
												array(
													'label' => esc_html__( "Expired Listings", 'javospot' ),
													'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, 'item-expired' ),
													'current' => intVal( jvfrm_spot_core()->getUserListingCount( get_current_user_id(), Array( 'expire' ) ) ),
												),
											) as $arrArticleArgs
										) {
											jvfrm_spot_layout()->load_template( 'parts/part-mypage-count-item', Array( 'jvfrm_spot_aricle_args' => (object) $arrArticleArgs ) );
										} ?>
									</ul>
									<div class="card-footer text-center">
										<a class="text-center" href="<?php echo jvfrm_spot_getCurrentUserPage( 'item-published' ); ?>"><?php _e('See all my listings','javospot'); ?><i class="fa fa-angle-right"></i> </a>
									</div>
								</div>
							</li>
						</ul>
						<!-- /.dropdown-listings -->
					</li>
				<?php
				endif;
				if( jvfrm_spot_dashboard()->checkPermission( 'logged_user' ) ) :
					?>
					<!-- /.dropdown -->
					<li class="dropdown account">
						<a class="dropdown-toggle header-userinfo" data-toggle="dropdown" href="#" aria-expanded="false"> <?php echo $jvfrm_spot_user_avatar_meta; ?>
						<b class="hidden"><?php printf('%s %s', $objCurrentLoginUser->first_name, $objCurrentLoginUser->last_name);?></b> </a>
						<ul class="dropdown-menu dropdown-userinfo drop-right fade-inout triangle-arrow-right">
							<li>
								<div class="card listing-card narrow ">
									<ul class="list-group list-group-flush">
										<li class="list-group-item">
										<div class="listing-thumb small-img">
											<a href="<?php echo jvfrm_spot_getCurrentUserPage( JVFRM_SPOT_PROFILE_SLUG ); ?>" target="_blank">
												<?php echo $jvfrm_spot_user_avatar_meta; ?>		</a>

										</div>
										<div class="listing-content">
											<h5 class="title">
												<?php echo $objCurrentLoginUser->display_name; ?>
											</h5>
											<span class="author">
												<a href="#">
												<i class="jv-icon3-user" aria-hidden="true"></i><?php echo $objCurrentLoginUser->user_email; ?></a>
											</span>
										</div>
									</li>
									<li class="list-group-item"><a href="<?php echo jvfrm_spot_getCurrentUserPage( JVFRM_SPOT_PROFILE_SLUG ); ?>"><i class="ti-settings"></i> <?php _e('Edit Profile','javospot'); ?></a></li>
									<li class="list-group-item"><a href="<?php echo jvfrm_spot_getCurrentUserPage( JVFRM_SPOT_CHNGPW_SLUG ); ?>"><i class="ti-settings"></i> <?php _e('Change Password','javospot'); ?></a></li>
									<?php /** (Require!!! ) move to 'Includes' folder !!! */ ?>
									<li class="list-group-item"><a href="<?php echo jvfrm_spot_getCurrentUserPage( 'item-published' ); ?>"><i class="ti-settings"></i> <?php _e('My Listings','javospot'); ?></a></li>
									<li class="list-group-item"><a href="<?php echo jvfrm_spot_getCurrentUserPage( 'favorite' ); ?>"><i class="ti-settings"></i> <?php _e('My Favorites','javospot'); ?></a></li>
									<li class="list-group-item"><a href="<?php echo esc_url( wp_logout_url( home_url( '/' ) ) ); ?>"><i class="fa fa-power-off"></i> <?php _e('Logout','javospot'); ?></a></li>
									</ul>
									<div class="card-footer text-center">
										<a class="text-center" href="<?php echo jvfrm_spot_getCurrentUserPage( 'item-published' ); ?>"><?php esc_html_e( "See all saved listings", 'javospot' ); ?><i class="fa fa-angle-right"></i> </a>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<!-- /.dropdown-userinfo -->
				<?php
				endif;
				if( jvfrm_spot_dashboard()->checkPermission( 'member' ) ) : ?>
					<!--------------- Add New Btn------------------>
					<li class="dropdown btn btn-group submit">
                        <button type="button" class="btn btn-primary pull-right btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-plus"></i>&nbsp;
                            <span class=""><?php esc_html_e( "New", 'javospot' ); ?>&nbsp;</span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </button>
						<ul class="dropdown-menu drop-right fade-inout triangle-arrow-right" role="menu">
							<?php if( function_exists( 'lava_directory_get_add_form_page' ) ) { ?>
                            <li><a href="<?php echo lava_directory_get_add_form_page(); ?>" target="_blank"><i class="jvd-icon-basic_sheet_pencil"></i> <?php _e('Submit a listing','javospot'); ?></a></li>
							<?php } ?>
							<?php if( $isEventActivate ) { ?>
								<li><a href="<?php echo jvfrm_spot_getCurrentUserPage( 'add-event' ); ?>"><i class="jvd-icon-calendar"></i> <?php _e('Submit an event','javospot'); ?></a></li>
							<?php } ?>
                            <!--<li class="divider"> </li>-->
                        </ul>
                    </li>
					<!---------------/ Add New Btn ------------------>
				<?php
				endif;
				if( !jvfrm_spot_dashboard()->checkPermission( 'logged_user' ) ) {
					echo '<li><a data-toggle="modal" data-target="#login_panel"><i class="jv-icon2-user2"></i></a></li>';
				} ?>
				<li class="overlay-sidebar-opener"> <a href="javascript:void(0)"><i class="jv-icon2-paragraph"></i></a></li>
			</ul>
		</div>
		<!-- /.navbar-header -->
		<!-- /.header-content-wrapper -->
		<!-- /.navbar-static-side -->
	</nav>