<?php
/**
 * Type C - Dashboard
 * My Dashboard > My List - Pending
 *
 */
global
	$jvfrm_spot_curUser,
	$manage_mypage;

require_once( JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php' );
$jvfrm_spot_CPW = jvfrm_spot_dashboard_change_pw();

get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><h4 class="card-title"><?php esc_html_e( "Change my password", 'javospot' ); ?></h4></div><!-- card-header -->
				<div class="card-block move-up">
				<?php

				if( false !== $jvfrm_spot_CPW ) {
					$strCurrentStatus = is_wp_error( $jvfrm_spot_CPW ) ? 'alert-warning' : 'alert-success';
					$strCurrentStatusMessage = is_wp_error( $jvfrm_spot_CPW ) ? $jvfrm_spot_CPW->get_error_message() : $jvfrm_spot_CPW;
					printf( '<div class="alert %1$s">%2$s</div>', $strCurrentStatus, $strCurrentStatusMessage );
				} ?>

				<div class="row">
					<div class="col-md-12">				
						<form method="post" name="javo-dashboard-change-pw" class="moveup-animation">

						   <div class="group">      
							  <input type="password" name="current_pass" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter your current password', 'javospot');?>" required>
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label><?php esc_attr_e('Current Password', 'javospot');?></label>
							</div>
							  
							<div class="group">      
							  <input type="password" name="new_pass" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter new password', 'javospot');?>" required>
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label><?php esc_attr_e('Current Password', 'javospot');?></label>
							</div>

							<div class="group">      
							  <input type="password" name="new_pass" id="user_login" class="input form-control" value="" size="20" placeholder="<?php esc_attr_e('Please enter new password', 'javospot');?>" required>
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label><?php esc_html_e( "Confirm New Password", 'javospot');?></label>
							</div>

							
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<button type="submit">
										<?php esc_html_e( "Change password", 'javospot' );?>
									</button>
								</div>
							</div>
							<?php wp_nonce_field( 'security', 'jvfrm_spot_dashboard_changepw_nonce' ); ?>
						</form>
					</div><!-- col12 -->
				</div><!-- Row -->

			</div> <!-- card-block -->
			</div> <!-- card -->
		</div> <!-- col-md-12 -->
	</div><!--/row-->
	<!-- Content End -->
<?php get_footer( 'mypage' );