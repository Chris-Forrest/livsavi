<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav sidebar-nav navbar-collapse collapse">
        <ul class="nav" id="side-menu">
			<?php
			$arrJavoDashboardMenuItems = apply_filters(
				'jvfrm_spot_mypage_sidebar_args',
				Array(
					'' => Array(
						'li_class' => 'side-menu home',
						'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, '' ),
						'icon' => 'jv-icon2-home',
						'label' => esc_html__("Dashboard", 'javospot'),
						'visible' => 'all',
					),
				)
			);

			$arrJavoDashboardMenuItems[] = Array(
				'li_class' => 'section-title selection-title m-t-20 m-l-5 p-l-10',
				'label' => esc_html__( "Settings", 'javospot' ),
				'visible' => 'member',
				'separator' => true
			);

			if( function_exists( 'Lava_ViewCounter' ) ) {
				$arrJavoDashboardMenuItems[ 'settings' ] = Array(
					'label' => esc_html__( "Settings", 'javospot' ),
					'li_class' => 'side-menu settings',
					'icon' => 'jv-icon2-setting',
					'visible' => 'member',
					'inner' => Array(
						'report-settings' => Array(
							'li_class' => 'side-menu report-setting',
							'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, 'report-settings' ),
							'label' => esc_html__( "Report Setting", 'javospot' ),
							'visible' => 'member',
						),
					),
				);
			}

			$arrJavoDashboardMenuItems[ 'account-settings' ] = Array(
				'label' => esc_html__( "Account Setting", 'javospot' ),
				'li_class' => 'side-menu account-setting',
				'icon' => 'jv-icon2-setting',
				'visible' => 'member',
				'inner' => Array(
					JVFRM_SPOT_PROFILE_SLUG => Array(
						'li_class' => 'side-menu',
						'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, JVFRM_SPOT_PROFILE_SLUG ),
						'label' => esc_html__( "Edit Profile", 'javospot' ),
						'visible' => 'member',
					),
					JVFRM_SPOT_CHNGPW_SLUG => Array(
						'li_class' => 'side-menu',
						'url' => jvfrm_spot_getUserPage( jvfrm_spot_getDashboardUser()->ID, JVFRM_SPOT_CHNGPW_SLUG ),
						'label' => esc_html__( "Change Password", 'javospot' ),
						'visible' => 'member',
					),
				),
			);

			$arrJavoDashboardMenuItems[ 'logout' ] = Array(
				'li_class' => 'side-menu logout',
				'url' => wp_logout_url( home_url( '/' ) ),
				'icon' => 'jv-icon2-power',
				'label' => esc_html__( "Logout", 'javospot' ),
				'visible' => 'member',
			);

			/**
			<li class="section-divide menu-titles m-t-20"></li>
				<li class="section-title selection-title m-t-20 m-l-5 p-l-10"><span><?php _e('Setting','javospot'); ?></span></li>
				**/

			if( !empty( $arrJavoDashboardMenuItems ) ) {
				$strBedgeTemplate = '<span class="label label-rouded label-maincolor pull-right">%s</span>';
				$is_allowed_read = jvfrm_spot_dashboard()->checkPermission( 'member' );
				foreach( $arrJavoDashboardMenuItems as $strDashboardMenuSlug => $arrDashboardMenu ) {

					if(
						( isset( $arrDashboardMenu[ 'visible' ] ) && $arrDashboardMenu[ 'visible' ] == 'member' && ! $is_allowed_read ) ||
						( isset( $arrDashboardMenu[ 'visible' ] ) && $arrDashboardMenu[ 'visible' ] == 'others' && jvfrm_spot_dashboard()->checkPermission( 'memberOnly' ) )
					) { continue; }

					if( isset( $arrDashboardMenu[ 'separator' ] ) && $arrDashboardMenu[ 'separator' ] ) {
						printf( '<li class="%1$s"><span>%2$s</span></li>', $arrDashboardMenu[ 'li_class' ], $arrDashboardMenu[ 'label' ] );
						continue;
					}

					$is_active = get_query_var( 'sub_page' ) == $strDashboardMenuSlug;
					$strBedge = null;
					if( isset( $arrDashboardMenu[ 'bedge' ] ) ) {
						$strBedge = sprintf( $strBedgeTemplate, $arrDashboardMenu[ 'bedge' ] );
					}
					if( isset( $arrDashboardMenu[ 'inner' ] ) && is_array( $arrDashboardMenu[ 'inner' ] ) ) {
						$is_active = array_key_exists( get_query_var( 'sub_page' ), $arrDashboardMenu[ 'inner' ] );
						printf(
							'<li class="%5$s"><a href="#" class="%2$s" title="%1$s"><i data-icon="v" class="%4$s"></i> <span class="menu-titles">%1$s <span class="fa arrow"></span>%3$s</span></a> <ul class="nav sub-menu-second">', $arrDashboardMenu[ 'label' ], ( $is_active ? ' active' : '' ), $strBedge, $arrDashboardMenu[ 'icon' ], isset($arrDashboardMenu['li_class']) ? $arrDashboardMenu['li_class'] : ''
						);
						foreach( $arrDashboardMenu[ 'inner' ] as $strDashboardSubMenuSlug => $arrSubMenu ) {
							if(
								( isset( $arrSubMenu[ 'visible' ] ) && $arrSubMenu[ 'visible' ] == 'member' && ! $is_allowed_read ) ||
								( isset( $arrSubMenu[ 'visible' ] ) && $arrSubMenu[ 'visible' ] == 'others' && jvfrm_spot_dashboard()->checkPermission( 'memberOnly' ) )
							) { continue; }

							$is_active = get_query_var( 'sub_page' ) == $strDashboardSubMenuSlug;
							$strBedge = null;
							if( isset( $arrSubMenu[ 'bedge' ] ) ) {
								$strBedge = sprintf( $strBedgeTemplate, $arrSubMenu[ 'bedge' ] );
							}
							printf(
								'<li><a href="%1$s" class="%3$s" title="%2$s">%2$s%4$s</a></li>', $arrSubMenu[ 'url' ], $arrSubMenu[ 'label' ],
								( $is_active ? ' active' : '' ), $strBedge
							);
						}
						printf( '</ul> </li>' );
					}else{
						printf(
							'<li class="%5$s"><a href="%1$s" class="%3$s" title="%2$s"><i data-icon="P" class="%4$s"></i> <span class="menu-titles">%2$s</span></a></li>'
							, $arrDashboardMenu[ 'url' ]
							, $arrDashboardMenu[ 'label' ]
							, ( $is_active ? ' active' : '' )
							, $arrDashboardMenu[ 'icon' ]
							, isset($arrDashboardMenu['li_class']) ? $arrDashboardMenu['li_class'] : ''
						);
					}
				}
			}  ?>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->