<?php // Register Custom Navigation Walker
		require_once('bs4w1.php'); ?>
<!-- .quick-view -->
<div class="quick-view">
		<div class="quick-header">
			<a class="navbar-brand" href="<?php echo home_url( '/' ); ?>">
				<?php esc_html_e( "Site Menu", 'javospot' ); ?>
			</a>
			<span class="overlay-sidebar-opener"><i class="jv-icon2-none"></i></span>
		</div>
		<div class="quick-view-body">
			<nav class="navbar navbar-toggleable-md vertical-nav">
				<?php
				wp_nav_menu([
					'menu'            => 'primary',
					'theme_location'  => 'primary',
					'container'       => 'div',
					'container_id'    => 'bs4navbar',
					'container_class' => 'collapse navbar-collapse show',
					'menu_id'         => false,
					'menu_class'      => 'navbar-nav mr-auto flex-column',
					'depth'           => 3,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker()
				]); ?>
			</nav>
		</div>
</div>
<!-- /.quick-view -->