<?php
/**  JV Dashboard
***	Home page
***	V1.0
***/

global $jvfrm_spot_curUser, $manage_mypage;
$is_allowed_read = is_user_logged_in() && ( current_user_can( 'manage_options' ) || jvfrm_spot_getDashboardUser()->ID == get_current_user_id() );

require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';
get_header( 'mypage' );

if( $is_allowed_read ) {
	jvfrm_spot_layout()->load_template( 'parts/part-mypage-user-dashboard' );
}else{
	jvfrm_spot_layout()->load_template( 'parts/part-mypage-user-listings' );
}

get_footer( 'mypage' );