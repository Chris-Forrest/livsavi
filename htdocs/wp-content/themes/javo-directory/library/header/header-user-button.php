<?php if(is_user_logged_in()){ ?> <!-- jvfrm_user_menu -->
	<div class="jvfrm_user_menu dropdown">
		<button id="jvfrm_user_menu_btn" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true">
			<div class="jvfrm_user_menu_avatar"><?php echo get_avatar( get_current_user_id(), 40 ); ?></div>
			<div class="jvfrm_user_menu_username"><?php echo wp_get_current_user()->user_login; ?></div>
		</button>
		<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="jvfrm_user_menu_btn">
			<li class="dashboard">
				<a href="<?php echo esc_url( home_url( JVFRM_SPOT_DEF_LANG.JVFRM_SPOT_MEMBER_SLUG . '/' . wp_get_current_user()->user_login . '/' ) ); ?>">
					<?php esc_html_e('Dashboard', 'javospot'); ?>
				</a>
			</li>
			<li role="separator" class="divider"></li>
			<li class="edit-profile">
				<a href="<?php echo jvfrm_spot_getCurrentUserPage( JVFRM_SPOT_PROFILE_SLUG );?>">
					<?php esc_html_e('Edit Profile', 'javospot'); ?>
				</a>
			</li>
			<li role="separator" class="divider"></li>
			<li class="submit">
				<?php 
					$member_submit_link = esc_url(jvfrm_spot_getCurrentUserPage( JVFRM_SPOT_ADDITEM_SLUG ));
					if( class_exists( 'Lava_Directory_Manager' ) && '' != lava_directory_manager_get_option( "page_add_lv_listing" )){
						$member_submit_link = get_page_link(lava_directory_manager_get_option( "page_add_lv_listing" ));
					}
				?>
				<a href="<?php echo $member_submit_link; ?>">
					<?php esc_html_e('Submit', 'javospot'); ?>
				</a>
			</li>
			<li role="separator" class="divider"></li>
			<li class="logout">
				<a href="<?php echo esc_url( wp_logout_url( home_url( '/' ) ) ); ?>">
					<?php _e('Log out', 'javospot'); ?>
				</a>
			</li>
		</ul>
	</div>
<?php } ?> <!-- jvfrm_user_menu -->