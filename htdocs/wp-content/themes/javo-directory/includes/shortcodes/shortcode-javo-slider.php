<?php

class jvfrm_spot_slider {

	public $shortcode_name = 'jvfrm_spot_slider';

	public static $load_script = false;

	public function __construct() {
		add_filter( 'jvfrm_spot_other_shortcode_array', Array( $this, 'register_shortcode' ) );
		add_action( 'wp_footer', Array( $this,'load_script_func' ) );
		add_action( 'admin_init' , Array( $this, 'register_shortcode_with_vc' ), 11 );
	}

	public function register_shortcode( $shortcode ) {
		return wp_parse_args(
			Array( $this->shortcode_name => Array( $this, 'output' ) ),
			$shortcode
		);
	}

	public function get_post_types( $options=Array(), $excerpt=Array() ) {
		$arrPostTypes = get_post_types( array( 'public'=> true ), 'objects' );
		$arrReturns = Array();
		foreach( $arrPostTypes as $strPostType => $objPostType ) {
			$arrReturns[ $objPostType->label ] = $strPostType;
		}
		return $arrReturns;
	}

	public function register_shortcode_with_vc() {

		if( !function_exists( 'vc_map') )
			return;

		vc_map(
			array(
				'base' => $this->shortcode_name,
				'name' => esc_html__( "Javo Slider", 'javospot' ),
				'category' => esc_html__('Javo', 'javospot'),
				'icon' => 'jv-vc-shortcode-icon shortcode-featured-properties',
				'params' => Array(
					Array(
						'type' =>'dropdown',
						'heading' => esc_html__( "Post Type", 'javospot'),
						'holder' => 'div',
						'param_name'	=> 'post_type',
						'value' => $this->get_post_types( array() ),
					),
					Array(
						'type' =>'checkbox',
						'heading' => esc_html__( "Only Featured Item", 'javospot'),
						'holder' => 'div',
						'param_name'	=> 'featured',
						'value' => Array( esc_html__( "Use", 'javospot' ) => 'true' ),
					),
					Array(
						'type' =>'textfield',
						'heading' => esc_html__( "Custom post ids", 'javohome'),
						'desccription' => esc_html__( "( )", 'javohome'),
						'holder' => 'div',
						'param_name'	=> 'post_ids',
					),
					Array(
						'type' =>'textfield',
						'heading' => esc_html__( "Listing Count", 'javospot'),
						'desccription' => esc_html__( "( Blank = Unlimited )", 'javospot'),
						'holder' => 'div',
						'param_name'	=> 'count',
					),
				)
			)
		);
	}
	public function load_script_func() {
		if( !self::$load_script )
			return;

		wp_enqueue_script(
			lava_directory()->enqueue->getHandleName( 'jquery.flexslider-min.js' )
		);
		echo "
		<script type='text/javascript'>
			jQuery( function($) {
				var jvSliderResize = function( parent ) {
					var
						wW = $( window ).width(),
						wH = $( window ).height();
					
					wH = parseInt( wW ) <= 768 ? ( wH / 2 ) : wH;
					$( 'li.flexslider-item', parent ).css({ height: wH, overflow:'hidden' });
				}
				$( '.shortcode-jvfrm-spot-slider' ).each( function() {
					$( '.jvfrm-spot-slider-main', this ).flexslider({
						animation : 'slide',
						controlNav : true,
						animationLoop : true,
						directionNav : true,
						slideshow : false,
						sync : '.jvfrm-spot-slider-thumbnail',
						start : function( slider ) {
							slider.removeClass( 'hidden' );
						}
					});
					$( '.jvfrm-spot-slider-thumbnail', this ).flexslider({
						animation : 'slide',
						controlNav : false,						
						animationLoop : false,
						slideshow : true,
						itemWidth : 80,
						itemMargin : 5,
						direction: 'vertical',
						asNavFor : '.jvfrm-spot-slider-main'
					});
					jvSliderResize( $( '.jvfrm-spot-slider-main', this ) );
				});

				$( window ).on( 'resize', function(){
					jvSliderResize( $( '.shortcode-jvfrm-spot-slider .jvfrm-spot-slider-main' ) );					
				} ).trigger( 'resize' );
			} );
		</script>";
	}

	public function output( $atts, $content="" ) {
		self::$load_script = true;
		ob_start();
		jvfrm_spot_core()->template->load_template(
			'../shortcodes/html/html-javo-slider',
			'.php',
			Array(
				'jvfrm_spot_slider_param' => $atts,
			)
		);
		return ob_get_clean();
	}
}

new jvfrm_spot_slider;