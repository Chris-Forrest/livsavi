<?php
if( !isset( $jvfrm_spot_slider_param ) ) {
	die;
}

$arrSliderOptions = shortcode_atts(
	Array(
		'post_type' => 'post',
		'featured' => 'false',
		'count' => '-1',
		'post_ids' => '',
	),
	$jvfrm_spot_slider_param
);

$arrGetSlideParams = Array(
	'post_type' => $arrSliderOptions[ 'post_type' ],
	'meta_query' => Array(),
	'posts_per_page' => intVal( $arrSliderOptions[ 'count' ] ),
);

$is_portfolio = $arrGetSlideParams[ 'post_type' ] == 'portfolio';

if( $arrSliderOptions[ 'featured' ] === 'true' ) {
	if( $is_portfolio ) {
		$arrGetSlideParams[ 'meta_query' ][] = Array(
			'key' => '_jvfrm_featured_portfolio',
			'compare' => '=',
			'value' => 'yes',
		);
	}else{
		$arrGetSlideParams[ 'meta_query' ][] = Array(
			'key' => '_featured_item',
			'compare' => '=',
			'value' => '1',
		);
	}
}

if( $arrSliderOptions[ 'post_ids' ] !== '' ) {
	$arrCustomPostIDs = @explode( ',', $arrSliderOptions[ 'post_ids' ] );	
	$arrCustomPostIDs = array_unique( array_map( 'intVal', $arrCustomPostIDs ) );
	$cleanCustomPostIDs = array_filter( $arrCustomPostIDs );
	$arrGetSlideParams[ 'post__in' ] = $cleanCustomPostIDs;
}

$objWPQ = new WP_Query;
$arrSliderPosts = $objWPQ->query( $arrGetSlideParams );
?>

<div class="javo-shortcode shortcode-jvfrm-spot-slider">
	<div class="jvfrm-spot-slider-main">
		<ul class="slides">
			<?php
			if( !empty( $arrSliderPosts ) ) : foreach( $arrSliderPosts as $objPost ) {
				$strThumbnailSize = wp_is_mobile() ? 'medium' : 'full';
				$tagThumbnailFull = wp_get_attachment_image_url( get_post_thumbnail_id( $objPost ), $strThumbnailSize );
				$strSliderContent = $is_portfolio ? get_post_meta( $objPost->ID, '_jvfrm_short-description', true ) : $objPost->post_content;
				printf(
					'<li class="flexslider-item" style="background-image:url(%1$s);">						
						<div class="cover"></div>
						<p class="flex-caption">							
							<strong class="flex-caption-title">
								<a href="%4$s">%2$s</a>
							</strong>
							<br>
							<span>%3$s</span>
						</p>
					</li>',
					$tagThumbnailFull,
					$objPost->post_title,
					wp_trim_words( $strSliderContent, 10, '...' ),
					get_permalink( $objPost )
				);
			} endif; ?>
		</ul>
	</div>
	<div class="jvfrm-spot-slider-thumbnail hidden">
		<ul class="slides">
			<?php
			if( !empty( $arrSliderPosts ) ) : foreach( $arrSliderPosts as $objPost ) {
				$tagThumbnail = get_the_post_thumbnail( $objPost, 'jvfrm-spot-tiny' );
				printf( '<li>%s</li>', $tagThumbnail );
			} endif; ?>
		</ul>
	</div>
</div>