<?php
if( function_exists( 'javo_spot_single' ) ) {
	$strCurrentType = get_transient( 'single_' . get_post_type() . '_' . get_the_ID() . '_preview_type' );
	$arrSingleTypes = javo_spot_single()->core->get_types();
	?>
	<div class="jvfrm-spot-single-preview-layer left">
		<h3 class="title"><?php esc_html_e( "Single Type", 'javospot' ); ?></h3>
		<form method="post">
			<ul class="lava-single-control-button">
				<li class="control-item select">
					<select name="jvfrm_spot_preview_single_type">
						<?php
						foreach( $arrSingleTypes as $strValue => $strLabel ) {
							printf(
								'<option value="%1$s"%3$s>%2$s</option>',
								$strValue, $strLabel,
								selected( $strValue == $strCurrentType, true, false )
							);
						} ?>
					</select>
				</li>
				<li class="control-item apply">
					<button type="submit"><i></i><?php esc_html_e( "Apply", 'javospot' ); ?></button>
				</li>
			</ul>
			<?php wp_nonce_field( 'jvfrm_spot_single_type_preview', 'single_' . get_post_type() . '_action' ); ?>
		</form>
	</div>
<?php } ?>
<div class="jvfrm-spot-single-preview-layer right">
	<h3 class="title"><?php esc_html_e( "Preview", 'javospot' ); ?></h3>
	<?php
	if( function_exists( 'lava_directory' ) && get_post_type() == self::SLUG ) {
		lava_directory()->template->single_control_buttons();
	} ?>
</div>