<div class="row javo-detail-item-content">
	<?php if( jvfrm_spot_has_attach() ) : ?>
		<div class="col-md-12 col-xs-12 item-gallery">
			<?php get_template_part( 'includes/templates/html', 'single-grid-images' ); ?>
		</div><!-- /.col-md-12.item-gallery -->
	<?php endif; ?>

	<?php
	if( empty($single_addon_options['disable_detail_section'])) {
		//Listing meta section
		if( ''!=(get_post_meta( get_the_ID(), '_website', true )) ||
			''!=(get_post_meta( get_the_ID(), '_email', true )) ||
			''!=(get_post_meta( get_the_ID(), '_address', true )) ||
			''!=(get_post_meta( get_the_ID(), '_phone1', true )) ||
			''!=(get_post_meta( get_the_ID(), '_phone2', true )) ||
			 false != lava_directory_terms( get_the_ID(), 'listing_keyword' ) ) {
			get_template_part( 'includes/templates/html', 'single-detail-options' );
		}
	} ?>

	<?php do_action( 'jvfrm_spot_' . get_post_type() . '_single_description_before' );
	if( '' != get_the_content() ){ ?>
	<div class="col-md-12 col-xs-12 item-description" id="javo-item-describe-section" data-jv-detail-nav>

		<h3 class="page-header"><?php esc_html_e( "Description", 'javospot' ); ?></h3>
		<div class="panel panel-default">
			<div class="panel-body">

				<!-- Post Content Container -->
				<div class="jv-custom-post-content">
					<div class="jv-custom-post-content-inner">
						<?php the_content(); ?>
					</div><!-- /.jv-custom-post-content-inner -->
					<div class="jv-custom-post-content-trigger">
						<i class="fa fa-plus"></i>
						<?php esc_html_e( "Read More", 'javospot' ); ?>

					</div><!-- /.jv-custom-post-content-trigger -->
				</div><!-- /.jv-custom-post-content -->

			</div><!--/.panel-body-->
		</div><!--/.panel-->
	</div><!-- /#javo-item-describe-section -->
	<?php }
	lava_directory_amenities(
		get_the_ID(),
		Array(
			'container_before' => sprintf( '
			<div class="col-md-12 col-xs-12 item-amenities" id="javo-item-amenities-section" data-jv-detail-nav>
				<h3 class="page-header">%1$s</h3>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="expandable-content" >',
						esc_html__( "Amenities", 'javospot' )
			),
			'container_after' => '
						</div>
					</div><!-- panel-body -->
				</div>
			</div><!-- /#javo-item-amenities-section -->'
		)
	); ?>

	<?php do_action( 'jvfrm_spot_' . get_post_type() . '_single_description_after' ); ?>

	<?php do_action( 'jvfrm_spot_' . get_post_type() . '_single_booking' ); ?>

	<?php if( function_exists( 'get_lava_directory_review' ) ): ?>
		<div class="col-md-12 col-xs-12 item-description" id="javo-item-review-section" data-jv-detail-nav>

			<h3 class="page-header"><?php esc_html_e( "Review", 'javospot' ); ?></h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<?php get_lava_directory_review(); ?>
				</div><!--/.panel-body-->
			</div><!--/.panel-->
		</div><!-- /#javo-item-describe-section -->
	<?php endif; ?>

	<?php
	if( function_exists( 'tribe_get_events' ) ) {
		jvfrm_spot_core()->template->load_template(
			'dashboard/part-my-events-content', '.php',
			Array(
				'jvfrm_spot_event_list_args' => Array(
					'post_parent' => get_the_ID(),
					'is_single' => true,
					'before' => sprintf(
						'<div class="col-md-12 col-xs-12 item-description" id="javo-item-events-section" data-jv-detail-nav>
							<h3 class="page-header">%s</h3>
							<div class="panel panel-default">
								<div class="panel-body">',
						esc_html__( "Events", 'javospot' )
					),
					'after' => '</div><!--/.panel-body-->
						</div><!--/.panel-->
					</div><!-- /#javo-item-describe-section -->',
				),
			)
		);
	} ?>

	<?php do_action( 'jvfrm_spot_' . get_post_type() . '_single_map_before' ); ?>
	<?php do_action( 'jvfrm_spot_' . get_post_type() . '_single_map_after' ); ?>

</div><!-- /.javo-detail-item-content -->