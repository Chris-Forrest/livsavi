<?php
if( get_post_type() == 'lv_support' && function_exists( 'jvfrm_single_support_navigation' ) )
	$jvfrm_spot_rs_navigation = jvfrm_single_support_navigation();
else
	$jvfrm_spot_rs_navigation = jvfrm_spot_single_navigation();
	$lv_listing_keyword = lava_directory_terms( get_the_ID(), 'listing_keyword' ); 
	if( ''==(get_post_meta( get_the_ID(), '_website', true )) && 
		''==(get_post_meta( get_the_ID(), '_email', true )) && 
		''==(get_post_meta( get_the_ID(), '_address', true )) && 
		''==(get_post_meta( get_the_ID(), '_phone1', true )) && 
		''==(get_post_meta( get_the_ID(), '_phone2', true )) &&
		 empty($lv_listing_keyword ) ) {
		unset($jvfrm_spot_rs_navigation['javo-item-condition-section']);
	}
?>
<div data-spy="affix" id="dot-nav">
	<?php
	if( !empty( $jvfrm_spot_rs_navigation ) )	{
		echo "<ul>";
			foreach( $jvfrm_spot_rs_navigation as $id => $attr )
			if( in_Array( get_post_type(), $attr[ 'type' ] ) )
			echo "<li class=\"awesome-tooltip\" title=\"{$attr['label']}\"><a href=\"#{$id}\"></a></li>";
		echo "</ul>";
	} ?>
</div> <!-- dot-nav -->