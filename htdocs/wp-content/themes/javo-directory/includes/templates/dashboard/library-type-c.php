<?php
/**
***	Edit My Profile Page
***/

require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';

get_header( 'mypage' ); ?>
<style type="text/css">
.media-toolbar-primary {
    display: none;
}
</style>

	<!-- Content Start -->
	<div class="row my-media">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><h4 class="card-title"><?php esc_html_e( "My Media Library", 'javospot' ); ?></h4></div><!-- card-header -->
				<div class="card-block">		
				<div class="my-media-box text-center">
					<a class="btn javo-fileupload" data-title="<?php esc_attr_e('My Media Library', 'javospot');?>" data-input="input[name='avatar']" data-preview=".javo-upload-review">
					<i class="jv-icon3-cloud-upload"></i>
					<br/>

					<h3><?php esc_html_e('See My Files', 'javospot');?></h3>
					<small><?php esc_html_e('You can manage media here. upload images or delete them.', 'javospot');?></small>
					</a>
				</div><!-- my-media -->
				
			</div> <!-- card-block -->
			</div> <!-- card -->
		</div><!-- col -->
	</div><!-- Row End -->
<!-- Content End -->
<?php
get_template_part('library/dashboard/mypage', 'common-script');
get_footer( 'mypage' );