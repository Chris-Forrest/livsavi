<?php
/**
 *
 * My Dashboard > Review > Received
 *
 */
global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';
get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">

		<!-- Starting Content -->
			<div class="card listing-card">
				<div class="card-header">
					<h4 class="card-title"><?php esc_html_e( "Recent Reviews", 'javospot' ); ?></h4>
				</div>
				<ul class="list-group list-group-flush">
					<?php jvfrm_spot_core()->template->load_template( 'dashboard/part-review-received-content', '.php' ); ?>
				</ul>
			</div> <!-- .card -->
		<!-- Content End -->

		</div> <!-- col-md-12 -->
	</div><!--/row-->

<?php get_footer( 'mypage' );