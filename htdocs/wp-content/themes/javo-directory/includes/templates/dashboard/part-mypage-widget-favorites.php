<?php
if( ! isset( $jvfrm_spot_aricle_args ) ) {
	die;
} ?>
<li class="list-group-item">
	<div class="listing-thumb">
		<a href="<?php echo get_permalink( $jvfrm_spot_aricle_args->post->ID ); ?>" target="_blank">
			<?php echo get_the_post_thumbnail( $jvfrm_spot_aricle_args->post, array( 50, 50 ), array( 'class' => 'img-circle' ) ); ?>
		</a>
	</div>
	<div class="listing-content">
		<h5 class="title">
			<a href="<?php echo get_permalink( $jvfrm_spot_aricle_args->post->ID ); ?>" target="_blank"><?php echo $jvfrm_spot_aricle_args->post->post_title; ?></a>
		</h5>
		<span class="author">
			<a href="#"><i class="jv-icon3-user" aria-hidden="true"></i> <?php echo get_user_by( 'ID', $jvfrm_spot_aricle_args->post->post_author )->display_name; ?></a>
		</span>
		<span class="time date"><i class="jv-icon3-clock" aria-hidden="true"></i> <?php echo $jvfrm_spot_aricle_args->date; ?></span>
	</div>
</li>