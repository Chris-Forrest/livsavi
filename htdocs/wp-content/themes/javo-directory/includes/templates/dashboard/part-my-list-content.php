<?php

if( ! isset( $lavaDashBoardArgs ) ) {
	die;
}

wp_localize_script(
	lava_directory()->enqueue->getHandleName( 'lava-dashboard.js' ),
	'lava_dir_dashboard_args',
	Array(
		'nonce' => wp_create_nonce( 'item_delete' ),
		'strings' => array(
			'strDeleteConfirm' => esc_html__( "Do you want to delete this item?", 'Lavacode' ),
		),
	)
);
wp_enqueue_script( lava_directory()->enqueue->getHandleName( 'lava-dashboard.js' ) );

switch( $lavaDashBoardArgs[ 'type' ] ) {
	case 'publish' :
		$strListingStatus = Array( 'publish' );
		break;

	case 'pending' :
		$strListingStatus = Array( 'pending' );
		break;

	case 'all' : default :
		$strListingStatus = Array( 'pending', 'publish' );

}

$lavaGetPostsQuery = Array(
	'post_type' => jvfrm_spot_core()->slug,
	'author' => get_current_user_id(),
	'post_status' => $strListingStatus,
	'posts_per_page' => 10,
	'paged' => max( 1, get_query_var( 'paged' ) ),
);

if( isset( $lavaDashBoardArgs[ 'payment' ] ) && $lavaDashBoardArgs[ 'payment' ]  == 'expire' ) {
	$lavaGetPostsQuery[ 'meta_query' ][] =  Array(
		'type' => 'NUMERIC',
		'key' => 'lv_expire_day',
		'value' => current_time( 'timestamp' ),
		'compare' => '<=',
	);
}

$lava_user_posts = new WP_Query( $lavaGetPostsQuery );

$arrDashboardTabs = apply_filters(
	'lava_' . jvfrm_spot_core()->slug . '_dashboard_tabs',
	Array(
		'title' => Array(
			'label' => esc_html__( "Title", 'javospot' ),
		),
		'posted_date' => Array(
			'label' => esc_html__( "Posted Date", 'javospot' ),
		),
		'post_status' => Array(
			'label' => esc_html__( "Status", 'javospot' ),
		),
	)
); ?>

<?php
global $lava_dashboard_message;

if( !empty( $lava_dashboard_message ) ) {
	echo $lava_dashboard_message;
} ?>

<ul class="list-group list-group-flush lava-dir-shortcode-dashboard-wrap">
<?php
if( $lava_user_posts->have_posts() ) {
	while( $lava_user_posts->have_posts() ) {
		$lava_user_posts->the_post();
		?>
		<li class="list-group-item lava-directory-<?php the_ID(); ?>">

		<?php
		if( !empty( $arrDashboardTabs ) ) { foreach( $arrDashboardTabs as $strKey => $arrMeta ) {
			switch( $strKey ) {
			case 'title' : ?>
			<div class="listing-thumb">
				<a href="<?php the_permalink(); ?>" target="_blank">
					<?php
					if( has_post_thumbnail() ) {
						the_post_thumbnail( Array( 50, 50 ), Array( 'class' => 'img-circle' ) );
					} ?>
				</a>
			</div>

			<div class="listing-content">
				<h5 class="title"><a href="<?php the_permalink(); ?>" class="title" target="_blank"><?php the_title(); ?></a></h5>
				<span class="meta-taxonomies"><i class="icon-folder"></i><?php echo join( ', ', wp_get_object_terms( get_the_ID(), 'listing_category', Array( 'fields' => 'names' ) ) ); ?></span>
					<?php break;
					case 'posted_date' :
						echo '<span class="time date"><i class="icon-calender"></i>'. get_the_date() .'</span>';
						break;
					case 'post_status' :
						$jv_this_post_status = get_post_status();
						echo '<span class="post-status label label-rounded label-info">'.  (  $jv_this_post_status == 'publish' ? esc_html__( 'PUBLISH','javospot' ) : esc_html__( 'PENDING','javospot' ) )  .'</span>';
						break;
				} // switch
				do_action( 'lava_' . jvfrm_spot_core()->slug . '_dashboard_tab_contents', $strKey, get_post());
			} ?>
			</div><!-- listing-content -->

			<div class="listing-action btn-box">
				<?php if( get_current_user_id() == get_the_author_meta( 'ID' ) ) : ?>
					<div class="lava-action text-right">
						<?php
						do_action(
							"lava_" . jvfrm_spot_core()->slug . "_dashboard_actions_before"
							, get_the_ID()
							, lava_directory_manager_get_option( 'page_add_' . jvfrm_spot_core()->slug )
						) ; ?>
						<a href="<?php lava_directory_edit_page(); ?>" class="edit action">
							<?php _e( "Edit", 'javospot' ); ?>
						</a>
						<a href="javascript:" data-action="delete" data-id="<?php echo esc_attr( get_the_ID() ); ?>" class="remove action">
							<?php _e( "Remove", 'javospot' ); ?>
						</a>

						<?php
						do_action(
							"lava_" . jvfrm_spot_core()->slug  . "_dashboard_actions_after"
							, get_the_ID()
							, lava_directory_manager_get_option( 'page_add_' . jvfrm_spot_core()->slug )
						) ; ?>
					</div><!-- lava-action -->
				<?php endif; ?>
			</div><!-- listing-action -->
		</li><!-- listing-block-body -->
		<?php
		}// if empty check
	} //while
}else{
	printf(
		'<li class="list-group-item text-center">%s, <a href="%s">%s</a></li>',
		esc_html__( "There is no data", 'javospot' ),
		lava_directory_get_add_form_page(),
		esc_html__( "Add new Listing?", 'javospot' )
	);
}//if
wp_reset_query(); ?>
	</ul>
 <div class="lava-pagination">
	<?php
	$big						= 999999999;
	echo paginate_links(
		Array(
			'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) )
			, 'format'		=> '?paged=%#%'
			, 'current'		=> max( 1, get_query_var('paged') )
			, 'total'			=> $lava_user_posts->max_num_pages
		)
	); ?>
</div>

<form method="post" id="lava-directory-manager-myapge-form">
	<?php wp_nonce_field( 'security', 'lava_directory_manager_mypage_delete' ); ?>
	<input type="hidden"name="post_id" value="">
</form>

<?php
$lava_output_variable	= Array();
$lava_output_variable[]	= "<script type=\"text/javascript\">";
$lava_output_variable[]	= sprintf( "var strLavaTrashConfirm = '%s'", __( "Do you want to delete this item?", 'javospot') );
$lava_output_variable[]	= "</script>";

echo @implode( "\n", $lava_output_variable ); ?>

<script type="text/javascript">
jQuery( function( $ ){

	var lava_directory_manager_mypage = function( el ) {

		this.el	= el;
		if( typeof this.instance === 'undefined' )
			this.init();
	}

	lava_directory_manager_mypage.prototype = {

		constructor : lava_directory_manager_mypage
		, init : function(){

			var obj			= this;
			obj.instance	= 1;

			$( document )
				.on( 'click', '[data-lava-directory-manager-trash]', obj.trash() );
		}

		, trash : function()
		{
			var obj = this;

			return function( e )
			{
				e.preventDefault();

				var post_id		= $( this ).data( 'lava-directory-manager-trash' );

				if( confirm( strLavaTrashConfirm ) ) {
					obj.el.find( "[name='post_id']").val( post_id );
					obj.el.submit();
				}
			}
		}
	}
	new lava_directory_manager_mypage( $( "#lava-directory-manager-myapge-form" ) );

} );
</script>