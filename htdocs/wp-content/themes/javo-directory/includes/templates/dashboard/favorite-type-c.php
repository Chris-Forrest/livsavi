<?php
/**
 * Type C - Dashboard
 * My Dashboard > Favorites
 *
 */
if( ! function_exists( 'lv_directory_favorite' ) || ! class_exists( 'lvDirectoryFavorite_button' ) ) {
	die;
}

global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';

$arrFavorites = lv_directory_favorite()->core->getFavorites( $jvfrm_spot_curUser->ID );

get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">

<div class="card listing-card">
	<div class="card-header"><h4 class="card-title"><?php esc_html_e( "My Favorite Listings", 'javospot' ); ?></h4></div><!-- card-header -->
	<ul class="list-group list-group-flush">

	<!-- Starting Content -->
	<?php
	$output = null;
	if( !empty( $arrFavorites ) ) {
		foreach( $arrFavorites as $arrFavoriteMeta ) {
			$objPost = isset( $arrFavoriteMeta[ 'post_id' ] ) ? get_post( $arrFavoriteMeta[ 'post_id' ] ) : null;
			if( empty( $objPost ) ) {
				continue;
			}
			$strImage = jvfrm_spot_tso()->get( 'no_image', JVFRM_SPOT_IMG_DIR.'/no-image.png' );
			if( has_post_thumbnail( $objPost ) ) {
				$intFeaturedID = get_post_thumbnail_id( $objPost->ID );
				$strImage = wp_get_attachment_thumb_url( $intFeaturedID );
			}
			$arrFeaturedCategory = wp_get_object_terms( $objPost->ID, 'listing_category', array( 'fields' => 'names' ) );
			$strDate = date_i18n( get_option( 'date_format' ), strtotime( $arrFavoriteMeta[ 'save_day' ] ) );
			$objButton = new lvDirectoryFavorite_button(
				Array(
					'post_id'	=> $objPost->ID,
					'unsave'	=> __( "Remove", 'javospot' ),
					'dashboard'	=> true
				)
			);
			$strButton = $objButton->output( false );
			$output .= sprintf(
			'<li class="list-group-item">
				<div class="listing-thumb">
					<a href="%1$s" target="_blank"><img src="%5$s" class="img-circle" alt="%2$s"></a>
				</div>
				<div class="listing-content">
					<h5 class="title">
						<a href="%1$s" target="_blank">%2$s</a>
					</h5>
					<span class="author"><a href="%1$s"><i class="jv-icon2-user2" aria-hidden="true"></i> %7$s</a>	</span>
					<span class="meta-taxonomies"><i class="jv-icon2-bookmark"></i>%6$s</span>
					<span class="time date"><i class="jv-icon2-calendar" aria-hidden="true"></i> %3$s</span>
				</div><!-- listing-content -->
				<div class="listing-action btn-box">
					<div class="lava-action text-right">%4$s</div><!-- lava-action -->
				</div><!-- listing-action -->
			</li>',
				get_permalink( $objPost ), $objPost->post_title, $strDate, $strButton, $strImage,
				join( ', ', $arrFeaturedCategory ), get_the_author_meta('user_login', $objPost->post_author)
			);
		}
	}else{
		$output = sprintf( '<p>%s</p>', esc_html__( "Not found any data", 'javospot' ) );
	}
	echo $output;
	?>
	</ul>
</div><!-- card -->
</div>
</div>
	<!-- Content End -->
<?php get_footer( 'mypage' );