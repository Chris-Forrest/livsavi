<?php
/**
 * Type C - Dashboard
 * My Dashboard > Events > My Events
 *
 */
global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';
get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">
			<div class="card listing-card">
				<div class="card-header"><h4 class="card-title"><?php esc_html_e( "My Events", 'javospot' ); ?></h4></div><!-- card-header -->
				<?php jvfrm_spot_core()->template->load_template( 'dashboard/part-my-events-content', '.php' ); ?>
			</div><!-- /.card -->
		</div> <!-- col-md-12 -->
	</div><!--/row-->
	<!-- Content End -->
<?php get_footer( 'mypage' );