<?php
/**
 *
 * My Dashboard > Add Item
 *
 */
global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';
get_header( 'mypage' ); ?>
	<!-- Content Start -->
	<div class="row">
		<div class="col-md-12">

			<!-- Starting Content -->
			<div class="section-block">
				<div class="comment-center">
					<?php echo do_shortcode( "[lava_directory_form]" ); ?>
				</div>
			</div><!-- /.section-block -->
			<!-- Content End -->

		</div> <!-- col-md-12 -->
	</div><!--/row-->

<?php get_footer( 'mypage' );