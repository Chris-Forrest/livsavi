<?php
/**
 * Type C - Dashboard
 * My Dashboard > My List - expired
 *
 */
global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';

function jv_contact_pm_form(){
	global $jvfrm_spot_curUser;
	return $jvfrm_spot_curUser->user_email;
}
add_shortcode('JV_CONTACT_PM_FORM', 'jv_contact_pm_form');

get_header( 'mypage' ); ?>
	<!-- Content Start -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header"><h4 class="card-title"><?php esc_html_e( "Contact", 'javospot' ); ?></h4></div><!-- card-header -->
				<div class="card-block">
					<?php
					$pm_form = '';
					$pm_form_id = $jvfrm_spot_tso->get('pm_contact_form_id');
					$pm_form_type = $jvfrm_spot_tso->get('pm_contact_type');
					if($pm_form_type != ''){
						if($pm_form_type == 'contactform'){ // pm type = contact form
							$pm_form = '[contact-form-7 id=%s title="%s"]';
						}else{ // pm type = ninja form
							$pm_form = '[ninja_forms id=%s title="%s"]';
						}
						echo do_shortcode(sprintf($pm_form, $pm_form_id, ''));
					}else{
						esc_html_e( "Not setup pm form", 'javospot' );
					} ?>
				</div><!-- /.card-block -->
			</div><!-- /.card -->
		</div> <!-- col-md-12 -->
	</div><!--/row-->
</div><!-- container -->
	<!-- Content End -->
<?php get_footer( 'mypage' );