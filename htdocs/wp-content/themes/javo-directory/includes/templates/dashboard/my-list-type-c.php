<?php
/**
 * Type C - Dashboard
 * My Dashboard > My List - Publish
 *
 */
global $jvfrm_spot_curUser, $manage_mypage;
require_once JVFRM_SPOT_DSB_DIR . '/mypage-common-header.php';
get_header( 'mypage' ); ?>
	<!-- Content Start -->

	<div class="row">
		<div class="col-md-12">
			<div class="section-block">
				<?php
				$strFavoriteShortcode = 'lava_directory_mypage';
				if( shortcode_exists( $strFavoriteShortcode ) ) {
					echo do_shortcode( '[' . $strFavoriteShortcode . ']' );
				}else{
					printf( "<div class='jv-mypage-not-found-dat'>%s</div>", esc_html__( "Not found any data", 'javospot' ) );
				} ?>
			</div><!-- /.section-block -->
		</div> <!-- col-md-12 -->
	</div><!--/row-->

	<!-- Content End -->
<?php get_footer( 'mypage' );