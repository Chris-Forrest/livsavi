<?php
/** May NOT use in the future due to separated menu and header */

$arrAllowPostTypes		= apply_filters( 'jvfrm_spot_single_post_types_array', Array( 'lv_listing' ) );
if( class_exists( 'lvDirectoryVideo_Render' ) ) {
	$objVideo = new lvDirectoryVideo_Render( get_post(), array(
		'width' => '100',
		'height' => '100',
		'unit' => '%',
	) );
	$is_has_video = $objVideo->hasVideo();
}else{
	$is_has_video = false;
}

// Single page addon option
if( class_exists( 'Javo_Spot_Single_Addon' ) ){
	$single_addon_options = get_single_addon_options(get_the_ID());
	if($single_addon_options['background_transparent'] == 'disable'){
		$block_meta = 'extend-meta-block';
		if($single_addon_options['featured_height'] != '') $featured_height = 'style=height:'.$single_addon_options['featured_height'].'px;';
	}else{
		if($single_addon_options['featured_height'] != '') $block_meta = '"style=height:'.$single_addon_options['featured_height'].'px;min-height:'.$single_addon_options['featured_height'].'px;';
	}
}

$globalFirstDisplayDIV = jvfrm_spot_tso()->get( 'lv_listing_single_first_header', 'featured' );
$firstDisplayDIV = get_post_meta( get_the_ID(), '_header_type', true );
if( $firstDisplayDIV ) {
	$globalFirstDisplayDIV = $firstDisplayDIV;
}

// Right Side Navigation
$jvfrm_spot_rs_navigation = jvfrm_spot_single_navigation();
function jvfrm_spot_custom_single_style($single_addon_options = '')
{
	if ( false === (boolean)( $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ) ) )
		$large_image_url	= '';
	else
		$large_image_url	=  $large_image_url[0];

	$output_style	= Array();
	$output_style[]	= sprintf( "%s:%s;", 'background-image'			, "url({$large_image_url})" );
	$output_style[]	= sprintf( "%s:%s;", 'background-attachment'	, 'fixed' );
	$output_style[]	= sprintf( "%s:%s;", 'background-repeat'		, 'no-repeat' );
	$output_style[]	= sprintf( "%s:%s;", 'background-position'		, 'center center' );
	$output_style[]	= sprintf( "%s:%s;", 'background-size'			, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-webkit-background-size'	, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-moz-background-size'		, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-ms-background-size'		, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-o-background-size'		, 'cover' );

	if($single_addon_options['background_transparent'] != '' && $single_addon_options['featured_height'] != '' )
		$output_style[]	= sprintf( "%s:%s;", 'height'				, $single_addon_options['featured_height']. 'px' );

	$output_style	= apply_filters( 'jvfrm_spot_featured_detail_header'	, $output_style, $large_image_url );
	$output_style	= esc_attr( join( ' ', $output_style ) );

	echo "style=\"{$output_style}\"";
} ?>
<div class="single-item-tab-feature-bg-wrap single-dynamic-header <?php echo sanitize_html_class( jvfrm_spot_tso()->get( 'lv_listing_single_header_cover', null ) ); ?> <?php echo isset($block_meta) ? $block_meta : ''; ?>">
	<div class="single-item-tab-feature-bg <?php echo sanitize_html_class( jvfrm_spot_tso()->get( 'lv_listing_single_header_cover', null ) ); ?>" <?php jvfrm_spot_custom_single_style($single_addon_options); ?> >
		<div class="jv-pallax"></div>
		<div class="single-item-tab-bg-overlay"></div>
		<div class="bg-dot-black"></div> <!-- bg-dot-black -->
	</div> <!-- single-item-tab-feature-bg -->
	<div class="single-item-tab-bg">
		<div class="container captions">
			<div class="header-inner">
				<div class="item-bg-left pull-left text-left">
					<div class="single-header-terms">
						<div class="tax-item-category"><?php lava_directory_featured_terms( 'listing_category' ); ?></div>
						<div class="tax-item-location"><?php lava_directory_featured_terms( 'listing_location' ); ?></div>
					</div>
					<h1 class="uppercase">
						<?php echo apply_filters( 'jvfrm_spot_' . get_post_type() . '_single_title', get_the_title() );?><br>
						<small style="color:white;font-size:40%;"><?php echo get_post_meta( get_the_ID(), '_tagline', true ); ?></small>
						<a href="<?php echo jvfrm_spot_getUserPage( get_the_author_meta( 'ID' ) ); ?>" class="header-avatar"  style="display:none;">
							<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
						</a>
					</h1>
					<div class="listing-des" style="display:none;">
						<?php
						$arrSoicalIcons = Array();
						foreach(
							Array(
								'facebook' => 'fa fa-facebook',
								'twitter' => 'fa fa-twitter',
								'instagram' => 'fa fa-instagram',
								'google' => 'fa fa-google',
								'linkedin' => 'fa fa-linkedin',
								'youtube' => 'fa fa-youtube',
							) as $strSocialName => $strSocialIcon
						) {
							$strSocialLink = get_post_meta( get_the_ID(), '_' . $strSocialName . '_link', true );
							if( $strSocialLink ) {
								$arrSoicalIcons[] = sprintf( '<a href="%1$s" target="_blank" class="jvfrm_spot_single_listing_%2$s" title="%2$s"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="%3$s fa-inverse fa-stack-1x" aria-hidden="true"></i></a>', esc_url_raw( $strSocialLink ), $strSocialName, $strSocialIcon );
							}
						}

						if( !empty( $arrSoicalIcons ) ) {
							printf( '<div id="javo-item-social-section" class="row" data-jv-detail-nav><div class="jvfrm_spot_single_listing_social-wrap text-center">%s</div></div>', join( '', $arrSoicalIcons ) );
						} ?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div> <!-- header-inner -->
			<ul class="javo-core-single-featured-switcher pull-right list-inline">
				<li class='switch-featured'>
					<a class="javo-tooltip" data-original-title="<?php _e('Featured','javospot'); ?>"><i class="jvd-icon-home" aria-hidden="true"></i></a>
				</li>
				<li class='switch-grid'>
					<a class="javo-tooltip" data-original-title="<?php _e('Grid','javospot'); ?>"><i class="jvd-icon-layers" aria-hidden="true"></i></a>
				</li>
				<li class='switch-category'>
					<a class="javo-tooltip" data-original-title="<?php _e('Category','javospot'); ?>"><i class="jvd-icon-folder" aria-hidden="true"></i></a>
				</li>
				<li class='switch-map'>
					<a class="javo-tooltip" data-original-title="<?php _e('Map','javospot'); ?>"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
				</li>
				<li class='switch-streetview'>
					<a class="javo-tooltip" data-original-title="<?php _e('Streeview','javospot'); ?>"><i class="fa fa-street-view" aria-hidden="true"></i></a>
				</li>				
				<?php if( function_exists( 'lava_directory_3DViewer' ) ) : ?>
					<li class='switch-3dview'>
						<a class="javo-tooltip" data-original-title="<?php _e('3DView','javospot'); ?>"><i class="icon-viewer-icon-g"></i></a>
					</li>
				<?php endif; ?>
				<?php if( $is_has_video ) : ?>
					<li class='switch-video'>
						<a class="javo-tooltip" data-original-title="<?php _e('Video','javospot'); ?>"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
					</li>
				<?php endif; ?>
			</ul>
		</div> <!-- container -->
	</div> <!-- single-item-tab-bg -->
	<div class="javo-core-single-featured-container hidden" data-first="<?php echo esc_attr( $globalFirstDisplayDIV ); ?>">
		<?php
		$strCurrentTermURl = '';
		if( function_exists( 'lava_directory' ) ) {
			$intCurrentItemTerms = wp_get_object_terms( get_the_ID(), 'listing_category', array( 'fields' => 'ids' ) );
			$intCurrentItemTermsID = isset( $intCurrentItemTerms[0] ) ? $intCurrentItemTerms[0] : 0;
			$intCurrentTermFeaturedImage = lava_directory()->admin->getTermOption( $intCurrentItemTermsID, 'featured', 'listing_category' );
			$strCurrentTermURl = wp_get_attachment_image_url( $intCurrentTermFeaturedImage, 'full' );
		} ?>
		<div class="container-category-featured" <?php echo isset($featured_height) ? $featured_height : ''; ?> data-background="<?php echo esc_url_raw( $strCurrentTermURl ); ?>"></div>
		<div class="container-featured" <?php echo isset($featured_height) ? $featured_height : ''; ?> data-background="<?php echo esc_url_raw( wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' ) ); ?>"></div>
		<div class="container-map" <?php echo isset($featured_height) ? $featured_height : ''; ?>></div>
		<div class="container-streetview" <?php echo isset($featured_height) ? $featured_height : ''; ?>></div>
		<div class="container-3dview" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
			<?php
			if( function_exists( 'lava_directory_3DViewer' ) )
				lava_directory_3DViewer()->core->append_3DViewer();
			?>
		</div>
		<div class="container-video" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
			<?php
			if( $is_has_video )
				$objVideo->output();
			?>
		</div>
	</div>
</div>