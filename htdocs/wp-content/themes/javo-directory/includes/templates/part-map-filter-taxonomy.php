<?php
$jvfrm_spot_is_amenities_archive = $jvfrm_spot_this_term_id = false;
$jvfrm_spot_taxonomy_object = get_queried_object();
if( $jvfrm_spot_taxonomy_object instanceof WP_Term ) {
	if( 'listing_amenities' == $jvfrm_spot_taxonomy_object->taxonomy ) {
		$jvfrm_spot_is_amenities_archive = true;
		$jvfrm_spot_this_term_id = $jvfrm_spot_taxonomy_object->term_id;
	}
}

$has_request = !empty( $_GET[ 'amenity' ] ) || $jvfrm_spot_is_amenities_archive;
$jvfrm_spot_multi_filters = Array(
	'listing_amenities' => (Array) $post->req_listing_amenities
);
if( !empty( $jvfrm_spot_multi_filters  ) ) : foreach( $jvfrm_spot_multi_filters as $filter => $currentvalue ) {
	?>
	<div class="row text-left javo-map-box-advance-term">
		<div class="col-md-3 jv-advanced-titles javo-map-box-title">
			<?php echo get_taxonomy( $filter )->label; ?>
		</div><!-- /.col-md-3 -->
		<div class="col-md-9 jv-advanced-fields amenities-filter-area" data-has-request="<?php echo $has_request ? 'yes' : 'no'; ?>" data-archive-term-id="<?php echo esc_attr( $jvfrm_spot_this_term_id ); ?>">
			<?php
			if( $jvfrm_spot_this_term_id ) {
				$currentvalue[] = $jvfrm_spot_this_term_id;
			}
			if( !empty( $currentvalue ) ) {
				$obj_terms = get_terms( array( 'taxonomy' => $filter, 'include' => Array_unique( $currentvalue ), 'hide_empty' => false ) );
				if( !empty( $obj_terms ) ) {
					foreach( $obj_terms as $term ) {
						printf( '<div class="col-md-4 col-sm-6 filter-terms"><label><input type="checkbox" name="jvfrm_spot_map_multiple_filter" value="%1$s" data-tax="%3$s" data-title="%2$s" %4$s>%2$s</label></div>', $term->term_id, $term->name, $filter, checked( true, true, false ) );
					}
				}
			}else{
				?>
				<input type="hidden" name="jvfrm_spot_map_multiple_filter" data-tax="<?php echo esc_attr( $filter ); ?>">
				<span><?php esc_html_e( "There is no amenities/features in this category", 'javospot' ); ?></span>
				<?php
			} ?>
		</div><!-- /.col-md-9 -->
	</div>
	<?php
} endif;