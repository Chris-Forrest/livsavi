<?php
$arrAllowPostTypes		= apply_filters( 'jvfrm_spot_single_post_types_array', Array( 'lv_listing' ) );
if( class_exists( 'lvDirectoryVideo_Render' ) ) {
	$objVideo = new lvDirectoryVideo_Render( get_post(), array(
		'width' => '100',
		'height' => '100',
		'unit' => '%',
	) );
	$is_has_video = method_exists( $objVideo, 'hasVideo' ) ? $objVideo->hasVideo() : false;
}else{
	$is_has_video = false;
}
if( class_exists( 'lvDirectory3DViewer_Render' ) ) {
	$obj3DViewer = new lvDirectory3DViewer_Render( get_post() );
	$is_has_3d = $obj3DViewer->viewer;
}else{
	$is_has_3d = false;
}
$single_addon_options = '';
// Single page addon option
if( class_exists( 'Javo_Spot_Single_Addon' ) ){
	$single_addon_options = get_single_addon_options(get_the_ID());
	if($single_addon_options['background_transparent'] == 'disable'){
		$block_meta = 'extend-meta-block-wrap';
		if($single_addon_options['featured_height'] != '') $featured_height = 'style=height:'.$single_addon_options['featured_height'].'px;';
	}else{
		if($single_addon_options['featured_height'] != ''){
			$block_meta = '"style=height:auto;min-height:auto;';
			$featured_height = 'style=height:'.$single_addon_options['featured_height'].'px;';
		}
	}
	$spy_nav_background = $single_addon_options['spy_nav_background'] != '' ? 'style="background:'.$single_addon_options['spy_nav_background'].';"' : '';
	$spy_nav_font_color = $single_addon_options['spy_nav_font_color'] != '' ? 'style="color:'.$single_addon_options['spy_nav_font_color'].';border-color:'.$single_addon_options['spy_nav_font_color'].';"' : '';
	$header_background = $single_addon_options['header_background'] != '' ? 'style="background-color:'.$single_addon_options['header_background'].';height:'.$single_addon_options['featured_height'].'px !important;"' : '';
}

$globalFirstDisplayDIV = jvfrm_spot_tso()->get( 'lv_listing_single_first_header', 'featured' );
$firstDisplayDIV = get_post_meta( get_the_ID(), '_header_type', true );
if( $firstDisplayDIV ) {
	$globalFirstDisplayDIV = $firstDisplayDIV;
}

// Right Side Navigation
$jvfrm_spot_rs_navigation = jvfrm_spot_single_navigation();
function jvfrm_spot_custom_single_style($single_addon_options = '')
{
	if ( false === (boolean)( $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ) ) )
		$large_image_url	= '';
	else
		$large_image_url	=  $large_image_url[0];

	$output_style	= Array();
	$output_style[]	= sprintf( "%s:%s;", 'background-image'			, "url({$large_image_url})" );
	$output_style[]	= sprintf( "%s:%s;", 'background-attachment'	, 'fixed' );
	$output_style[]	= sprintf( "%s:%s;", 'background-repeat'		, 'no-repeat' );
	$output_style[]	= sprintf( "%s:%s;", 'background-position'		, 'center center' );
	$output_style[]	= sprintf( "%s:%s;", 'background-size'			, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-webkit-background-size'	, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-moz-background-size'		, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-ms-background-size'		, 'cover' );
	$output_style[]	= sprintf( "%s:%s;", '-o-background-size'		, 'cover' );

	if( class_exists( 'Javo_Spot_Single_Addon' ) ){
		if( $single_addon_options['featured_height'] != '' )
			$output_style[]	= sprintf( "%s:%s;", 'height'				, $single_addon_options['featured_height']. 'px !important' );
		else
			$output_style[]	= sprintf( "%s:%s;", 'height'				, '550px' );
	}

	$output_style	= apply_filters( 'jvfrm_spot_featured_detail_header'	, $output_style, $large_image_url );
	$output_style	= esc_attr( join( ' ', $output_style ) );

	echo "style=\"{$output_style}\"";
}
$lv_listing_keyword = lava_directory_terms( get_the_ID(), 'listing_keyword' );
if( ''==(get_post_meta( get_the_ID(), '_website', true )) &&
	''==(get_post_meta( get_the_ID(), '_email', true )) &&
	''==(get_post_meta( get_the_ID(), '_address', true )) &&
	''==(get_post_meta( get_the_ID(), '_phone1', true )) &&
	''==(get_post_meta( get_the_ID(), '_phone2', true )) &&
	 empty($lv_listing_keyword ) ) {
	unset($jvfrm_spot_rs_navigation['javo-item-condition-section']);
}
?>

<div class="single-item-tab-feature-bg-wrap single-dynamic-header <?php echo sanitize_html_class( jvfrm_spot_tso()->get( 'lv_listing_single_header_cover', null ) ); ?> <?php echo isset($block_meta) ? $block_meta : ''; ?>">
	<div class="single-item-tab-feature-bg "
		<?php
		echo isset($featured_height) ? $featured_height : '';
		/*
		if( isset( $header_background ) ) {
			if($header_background == '') {
				jvfrm_spot_custom_single_style($single_addon_options);
			}else {
				echo $header_background;
			}
		}else{
			jvfrm_spot_custom_single_style($single_addon_options);
		} */ ?>>

		<?php /*
		<div class="jv-pallax"></div>
		<div class="single-item-tab-bg-overlay"></div>
		<div class="bg-dot-black"></div> <!-- bg-dot-black --> */ ?>
	</div> <!-- single-item-tab-feature-bg -->



	<div class="single-item-tab-bg">
		<div class="container captions">
			<div class="header-inner">
				<div class="item-bg-left pull-left text-left">
					<div class="single-header-terms">
						<div class="tax-item-category"><?php lava_directory_featured_terms( 'listing_category' ); ?></div><span>/</span>
						<div class="tax-item-location"><?php lava_directory_featured_terms( 'listing_location' ); ?></div>
						<?php
						if( function_exists( 'pvc_post_views' ) )
							pvc_post_views( $post_id = 0, $echo = true );
						?>
					</div>
					<h1 class="uppercase">
						<span class="jv-listing-title"><?php echo get_the_title(); ?></span><br>
						<small style="color:white;font-size:40%;"><?php echo get_post_meta( get_the_ID(), '_tagline', true ); ?></small>
						<a href="<?php echo jvfrm_spot_getUserPage( get_the_author_meta( 'ID' ) ); ?>" class="header-avatar" style="display:none;">
							<?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
						</a>
					</h1>
					<div class="jv-addons-meta-wrap">
						<?php echo apply_filters( 'jvfrm_spot_' . get_post_type() . '_single_listing_rating', '' );?>
						<?php if( $this->single_type == 'type-a' || $this->single_type == 'type-b' ||  $this->single_type == 'type-half' || $this->single_type == 'type-top-tab' || $this->single_type == 'type-left-tab') : ?>
						<?php if( class_exists( 'lvDirectoryFavorite_button' ) ) {
							$objFavorite = new lvDirectoryFavorite_button(
								Array(
									'post_id' => get_the_ID(),
									'show_count' => true,
									'show_add_text' => "<span>".__('Add to Favorites','javospot')."</span>",
									'save' => "<i class='fa fa-heart-o'></i>",
									'unsave' => "<i class='fa fa-heart'></i>",
									'class' => Array( 'btn', 'lava-single-page-favorite' ),
								)
							);
							$objFavorite->output();
						} ?>
					<?php endif; ?>
					</div>
					<div class="listing-des" style="display:none;">
						<?php
						$arrSoicalIcons = Array();
						foreach(
							Array(
								'facebook' => 'fa fa-facebook',
								'twitter' => 'fa fa-twitter',
								'instagram' => 'fa fa-instagram',
								'google' => 'fa fa-google',
								'linkedin' => 'fa fa-linkedin',
								'youtube' => 'fa fa-youtube',
							) as $strSocialName => $strSocialIcon
						) {
							$strSocialLink = get_post_meta( get_the_ID(), '_' . $strSocialName . '_link', true );
							if( $strSocialLink ) {
								$arrSoicalIcons[] = sprintf( '<a href="%1$s" target="_blank" class="jvfrm_spot_single_listing_%2$s" title="%2$s"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="%3$s fa-inverse fa-stack-1x" aria-hidden="true"></i></a>', esc_url_raw( $strSocialLink ), $strSocialName, $strSocialIcon );
							}
						}

						if( !empty( $arrSoicalIcons ) ) {
							printf( '<div id="javo-item-social-section" class="row" data-jv-detail-nav><div class="jvfrm_spot_single_listing_social-wrap text-center">%s</div></div>', join( '', $arrSoicalIcons ) );
						} ?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div> <!-- header-inner -->
			<ul class="javo-core-single-featured-switcher pull-right list-inline">
				<li class='switch-featured'>
					<a class="javo-tooltip" data-original-title="<?php _e('Featured','javospot'); ?>"><i class="jvd-icon-home" aria-hidden="true"></i></a>
				</li>
				<li class='switch-grid'>
					<a class="javo-tooltip" data-original-title="<?php _e('Grid','javospot'); ?>"><i class="jvd-icon-layers" aria-hidden="true"></i></a>
				</li>
				<li class='switch-category'>
					<a class="javo-tooltip" data-original-title="<?php _e('Category','javospot'); ?>"><i class="jvd-icon-folder" aria-hidden="true"></i></a>
				</li>
				<li class='switch-map'>
					<a class="javo-tooltip" data-original-title="<?php _e('Map','javospot'); ?>"><i class="jvd-icon-geolocalizator" aria-hidden="true"></i></a>
				</li>
				<?php if( 0 != get_post_meta( get_the_ID(), "lv_listing_street_visible", true ) ) : ?>
					<li class='switch-streetview'>
						<a class="javo-tooltip" data-original-title="<?php _e('StreetView','javospot'); ?>"><i class="jvd-icon-user" aria-hidden="true"></i></a>
					</li>
				<?php endif; ?>
				<?php if( $is_has_3d ) : ?>
					<li class='switch-3dview'>
						<a class="javo-tooltip" data-original-title="<?php _e('3DView','javospot'); ?>"><i class="icon-viewer-icon-g"></i></a>
					</li>
				<?php endif; ?>
				<?php if( $is_has_video ) : ?>
					<li class='switch-video'>
						<a class="javo-tooltip" data-original-title="<?php _e('Video','javospot'); ?>"><i class="jvd-icon-camera-video" aria-hidden="true"></i></a>
					</li>
				<?php endif; ?>
				<?php if( function_exists( 'lava_directory_direction' ) ) : ?>
					<li class='switch-get-direction' data-toggle="modal" data-target="#jvlv-single-get-direction">
						<a class="javo-tooltip" data-original-title="<?php _e('Get Direction','javospot'); ?>"><i class="jvd-icon-train"></i></a>
					</li>
				<?php endif; ?>
			</ul>
		</div> <!-- container -->
	</div> <!-- single-item-tab-bg -->
	<div class="javo-core-single-featured-container hidden" data-first="<?php echo esc_attr( $globalFirstDisplayDIV ); ?>">
		<div class="container-map" <?php echo isset($featured_height) ? $featured_height : ''; ?>></div>
		<div class="container-streetview" <?php echo isset($featured_height) ? $featured_height : ''; ?>></div>
		<div class="container-grid" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/swiper.min.css'; ?>">
			<style>
				.swiper-container{width: 100%;height: 100%;}
				.swiper-slide{text-align: center;font-size: 18px;background:#fff;overflow:hidden;display:-webkit-box;display: -ms-flexbox;display: -webkit-flex;display: flex;-webkit-box-pack: center;-ms-flex-pack: center;-webkit-justify-content:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;-webkit-align-items:center;align-items: center;}
				.swiper-slide img{height:100%;}
				.swiper-button-prev,
				.swiper-button-next{background-image:none;color:#fff;font-size:40px;width:auto;height:auto;max-width:40px;max-height:40px;opacity:0.85;}
				.swiper-button-prev:hover,
				.swiper-button-next:hover{opacity:1;}
				.swiper-container-horizontal>.swiper-pagination-bullets{bottom:5px;}
				.swiper-pagination-bullet{background:#fff;opacity:0.3;}
				.swiper-pagination-bullet-active{background:#fff;opacity:1;}
			</style>

			<div class="swiper-container" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
				<div class="swiper-wrapper">
					<?php
					$arrImages = Array();
					if( is_object( $GLOBALS[ 'post' ] ) && !empty( $GLOBALS[ 'post' ]->attach ) ) {
						$arrImages = $GLOBALS[ 'post' ]->attach;
					}

					if( !empty( $arrImages ) ) : foreach( $arrImages as $intImageID ) {
						if( $strSRC = wp_get_attachment_image_src( $intImageID, 'jvfrm-medium' ) ) {
							printf( '<div class="swiper-slide"><img src="%s"></div>', $strSRC[0] );
						}
					} endif; ?>
				</div> <!-- swiper-wrapper -->
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
				<!-- Arrow -->
				<div class="swiper-button-next"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
				<div class="swiper-button-prev"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></div>
				<div class="jvfrm_spot-single-header-gradient <?php echo sanitize_html_class( jvfrm_spot_tso()->get( 'lv_listing_single_header_cover', null ) ); ?>"></div>
			</div><!-- swiper-container -->
			<!-- Swiper JS -->
			 <script src="<?php echo get_template_directory_uri().'/assets/js/swiper.min.js'; ?>"></script>

			<!-- Initialize Swiper -->
			<script type="text/javascript">
				( function($){
					$( window ).on( 'load', function( event ) {
						var swiper = new Swiper('.swiper-container', {
							pagination: '.swiper-pagination',
							slidesPerView: 3,
							paginationClickable: true,
							spaceBetween: 10,
							nextButton: '.swiper-button-next',
							prevButton: '.swiper-button-prev',
							breakpoints: {
								1024: {
									slidesPerView: 2,
									spaceBetween: 10
								},
								768: {
									slidesPerView: 2,
									spaceBetween: 10
								},
								640: {
									slidesPerView: 2,
									spaceBetween: 5
								},
								380: {
									slidesPerView: 1,
									spaceBetween: 0
								}
							}
					});
				});
			} )( jQuery );
			</script>
		</div>
		<?php
		$strCurrentTermURl = '';
		if( function_exists( 'lava_directory' ) ) {
			$intCurrentItemTerms = wp_get_object_terms( get_the_ID(), 'listing_category', array( 'fields' => 'ids' ) );
			$intCurrentItemTermsID = isset( $intCurrentItemTerms[0] ) ? $intCurrentItemTerms[0] : 0;
			$intCurrentTermFeaturedImage = lava_directory()->admin->getTermOption( $intCurrentItemTermsID, 'featured', 'listing_category' );
			$strCurrentTermURl = wp_get_attachment_image_url( $intCurrentTermFeaturedImage, 'full' );
		} ?>

		<div class="container-category-featured" <?php echo isset($featured_height) ? $featured_height : ''; ?> data-background="<?php echo esc_url_raw( $strCurrentTermURl ); ?>"></div>
		<div class="container-featured" <?php echo isset($featured_height) ? $featured_height : ''; ?> data-background="<?php echo esc_url_raw( wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' ) ); ?>"></div>
		<div class="container-3dview" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
			<?php
			if( $is_has_3d )
				$obj3DViewer->output();
			?>
		</div>
		<div class="container-video" <?php echo isset($featured_height) ? $featured_height : ''; ?>>
			<?php
			if( $is_has_video )
				$objVideo->output();
			?>
		</div>
	</div>
</div>