<?php
/**
 * Template Name: Add Event
 */

get_header();

$objError = false;
$is_process = isset( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'jvfrm_spot_add_event';

$edit = get_post( get_query_var( 'edit' ) );
if( ! $edit ) {
	$edit = new stdClass;
	$edit->ID = 0;
	$edit->post_title = $edit->post_content = null;
}


if( $is_process ) {
	foreach(
		Array(
			'txtTitle' => esc_html__( "Empty Title", 'javospot' ),
			'txtDescription' => esc_html__( "Empty Description", 'javospot' ),
		) as $strKey => $errMsg
	) {
		$objError = empty( $_POST[ $strKey ] ) ? new WP_Error( 'add_event_err', $errMsg ) : false;
		if( is_wp_error( $objError ) ) {
			continue;
		}
	}

	if( 0 < $edit->ID ) {
		$intEventID = wp_update_post(
			Array(
				'ID' => $edit->ID,
				'post_type' => 'tribe_events',
				'post_title' => $_POST[ 'txtTitle' ],
				'post_content' => $_POST[ 'txtDescription' ],
			)
		);
	}else{
		$intEventID = wp_insert_post(
			Array(
				'post_type' => 'tribe_events',
				'post_title' => $_POST[ 'txtTitle' ],
				'post_content' => $_POST[ 'txtDescription' ],
				'post_status' => 'publish',
			)
		);
	}

	if( class_exists( 'Tribe__Events__API' ) && ! is_wp_error( $intEventID ) ) {
		Tribe__Events__API::saveEventMeta( $intEventID, $_POST, get_post( $intEventID ) );
	}

	if( isset( $_POST[ 'featured_image_id' ] ) && $_POST[ 'featured_image_id' ] != '' ) {
		set_post_thumbnail( $intEventID, $_POST[ 'featured_image_id' ] );
	}

	if( isset( $_POST[ 'gallery_image_ids' ] ) && $_POST[ 'gallery_image_ids' ] != '' ) {
		update_post_meta( $intEventID, 'detail_images', $_POST[ 'gallery_image_ids' ] );
	}

}


?>
<div class="container">
	<form method="post">

		<?php
		if( is_wp_error( $objError ) ) {
			printf( '<div class="alert alert-danger">%s</div>', $objError->get_error_message() );
		} ?>

		<div class="form-group">
			<label><?php esc_html_e( "Title", 'javospot' ); ?></label>
			<input type="text" name="txtTitle" class="form-control" value="<?php echo esc_attr( $edit->post_title ); ?>">
		</div>

		<div class="form-group">
			<label><?php esc_html_e( "Description", 'javospot' ); ?></label>
			<input type="text" name="txtDescription" class="form-control" value="<?php echo esc_attr( $edit->post_content ); ?>">
		</div>

		<div class="form-group">
			<label><?php esc_html_e( "StartDate", 'javospot' ); ?></label>
			<input type="text" name="EventStartDate" class="form-control" value="2017-06-10">
		</div>

		<div class="form-group">
			<label><?php esc_html_e( "StartTime", 'javospot' ); ?></label>
			<input type="text" name="EventStartTime" class="form-control" value="00:00:00">
		</div>

		<div class="form-group"><?php esc_html_e( "To", 'javospot' ); ?></div>

		<div class="form-group">
			<label><?php esc_html_e( "EndDate", 'javospot' ); ?></label>
			<input type="text" name="EventEndDate" class="form-control" value="2017-06-10">
		</div>

		<div class="form-group">
			<label><?php esc_html_e( "EndTime", 'javospot' ); ?></label>
			<input type="text" name="EventEndTime" class="form-control" value="00:00:00">
		</div>

		<div class="checkbox">
			<label>
				<input type="checkbox" name="EventAllDay" class="form-control" value="yes">
				<?php esc_html_e( "All day event", 'javospot' ); ?>
			</label>
		</div>

		<div class="checkbox">
			<label>
				<input type="checkbox" name="venue[EventShowMap][]" class="form-control" value="yes">
				<?php esc_html_e( "Show Google Map", 'javospot' ); ?>
			</label>
		</div>

		<div class="checkbox">
			<label>
				<input type="checkbox" name="venue[EventShowMapLink][]" class="form-control" value="yes">
				<?php esc_html_e( "Show Google Map", 'javospot' ); ?>
			</label>
		</div>













		<div class="form-option images-field-wrapper">
	<?php
	// if ( inspiry_is_edit_property() ) {
	if( true ) {
		?>
		<div id="gallery-thumbs-container" class="clearfix">
			<?php
			$thumbnail_size = 'thumbnail';
			$properties_images = array_filter( (array) get_post_meta( $edit->ID, 'detail_images', true ) );  //rwmb_meta( 'REAL_HOMES_property_images', 'type=plupload_image&size='.$thumbnail_size, $target_property->ID );

			$featured_image_id = get_post_thumbnail_id( $edit->ID );
			if( !empty( $properties_images ) ){
				foreach( $properties_images as $prop_image_id ){
					$obj_prop_image = get_post( $prop_image_id );
					$is_featured_image =  ( $featured_image_id == $prop_image_id );
					$featured_icon = ( $is_featured_image ) ? 'fa-star' : 'fa-star-o';
					echo '<div class="gallery-thumb">';
					echo '<img src="'.$obj_prop_image->guid.'" alt="'.$obj_prop_image->post_title.'" />';
					echo '<a class="remove-image" data-property-id="'.$edit->ID.'" data-attachment-id="' . $prop_image_id . '" href="#remove-image" ><i class="fa fa-trash-o"></i></a>';
					echo '<a class="mark-featured" data-property-id="'.$edit->ID.'" data-attachment-id="' . $prop_image_id . '" href="#mark-featured" ><i class="fa '. $featured_icon . '"></i></a>';
					echo '<span class="loader"><i class="fa fa-spinner fa-spin"></i></span>';
					echo '<input type="hidden" class="gallery-image-id" name="gallery_image_ids[]" value="' . $prop_image_id . '"/>';
					if ( $is_featured_image ) {
						echo '<input type="hidden" class="featured-img-id" name="featured_image_id" value="' . $prop_image_id . '"/>';
					}
					echo '</div>';
				}
			}
			?>
		</div>
		<?php
	} else {
		?>
		<div id="gallery-thumbs-container" class="clearfix"></div>
		<?php
	}
	?>

	<div id="drag-and-drop">
		<div class="drag-drop-msg"><i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;<?php _e('Drag and drop images here','framework'); ?></div>
		<div class="drag-or"><?php _e('or','framework'); ?></div>
		<div class="drag-btn">
			<button id="select-images"  class="real-btn">
				<?php _e('Select Images','framework'); ?>
			</button>
		</div>
	</div>
	<div class="field-description">
		<?php _e( '* An image should have minimum width of 770px and minimum height of 386px.', 'framework' ); ?><br/>
		<?php _e( '* You can mark an image as featured by clicking the star icon, Otherwise first image will be considered featured image.', 'framework' ); ?><br/>
	</div>
	<div id="errors-log"></div>
</div>

		<script type="text/javascript">
			jQuery( function( $ ) {
				var AJAXURL = "<?php echo admin_url( 'admin-ajax.php' ); ?>";


				 /* Apply jquery ui sortable on gallery items */
				$( "#gallery-thumbs-container" ).sortable({
					revert: 100,
					placeholder: "sortable-placeholder",
					cursor: "move"
				});

				/* initialize uploader */
				var uploaderArguments = {
					browse_button: 'select-images',          // this can be an id of a DOM element or the DOM element itself
					file_data_name: 'inspiry_upload_file',
					drop_element: 'drag-and-drop',
					url: AJAXURL + '?action=ajax_img_upload',
					filters: {
						mime_types : [
							{ title : 'image', extensions : "jpg,jpeg,gif,png" }
						],
						max_file_size: '10000kb',
						prevent_duplicates: true
					}
				};


				var uploader = new plupload.Uploader( uploaderArguments );
				uploader.init();

				$('#select-images').click(function(event){
					event.preventDefault();
					event.stopPropagation();
					uploader.start();
				});

				/* Run after adding file */
				uploader.bind('FilesAdded', function(up, files) {
					var html = '';
					var galleryThumb = "";
					plupload.each(files, function(file) {
						galleryThumb += '<div id="holder-' + file.id + '" class="gallery-thumb">' + '' + '</div>';
					});
					document.getElementById('gallery-thumbs-container').innerHTML += galleryThumb;
					up.refresh();
					uploader.start();
				});


				/* Run during upload */
				uploader.bind('UploadProgress', function(up, file) {
					document.getElementById( "holder-" + file.id ).innerHTML = '<span>' + file.percent + "%</span>";
				});


				/* In case of error */
				uploader.bind('Error', function( up, err ) {
					document.getElementById('errors-log').innerHTML += "<br/>" + "Error #" + err.code + ": " + err.message;
				});


				/* If files are uploaded successfully */
				uploader.bind('FileUploaded', function ( up, file, ajax_response ) {
					var response = $.parseJSON( ajax_response.response );

					if ( response.success ) {

						var galleryThumbHtml = '<img src="' + response.url + '" alt="" />' +
						'<a class="remove-image" data-property-id="' + 0 + '"  data-attachment-id="' + response.attachment_id + '" href="#remove-image" ><i class="fa fa-trash-o"></i></a>' +
						'<a class="mark-featured" data-property-id="' + 0 + '"  data-attachment-id="' + response.attachment_id + '" href="#mark-featured" ><i class="fa fa-star-o"></i></a>' +
						'<input type="hidden" class="gallery-image-id" name="gallery_image_ids[]" value="' + response.attachment_id + '"/>' +
						'<span class="loader"><i class="fa fa-spinner fa-spin"></i></span>';

						document.getElementById( "holder-" + file.id ).innerHTML = galleryThumbHtml;

						bindThumbnailEvents();  // bind click event with newly added gallery thumb
					} else {
						// log response object
						console.log ( response );
					}
				});

				/* Bind thumbnails events with newly added gallery thumbs */
				var bindThumbnailEvents = function () {

					// unbind previous events
					$('a.remove-image').unbind('click');
					$('a.mark-featured').unbind('click');

					// Mark as featured
					$('a.mark-featured').click(function(event){

						event.preventDefault();

						var $this = $( this );
						var starIcon = $this.find( 'i');

						if ( starIcon.hasClass( 'fa-star-o' ) ) {   // if not already featured

							$('.gallery-thumb .featured-img-id').remove();      // remove featured image id field from all the gallery thumbs
							$('.gallery-thumb .mark-featured i').removeClass( 'fa-star').addClass( 'fa-star-o' );   // replace any full star with empty star

							var $this = $( this );
							var input = $this.siblings( '.gallery-image-id' );      //  get the gallery image id field in current gallery thumb
							var featured_input = input.clone().removeClass( 'gallery-image-id' ).addClass( 'featured-img-id' ).attr( 'name', 'featured_image_id' );     // duplicate, remove class, add class and rename to full fill featured image id needs

							$this.closest( '.gallery-thumb' ).append( featured_input );     // append the cloned ( featured image id ) input to current gallery thumb
							starIcon.removeClass( 'fa-star-o' ).addClass( 'fa-star' );      // replace empty star with full star

						}

					}); // end of mark as featured click event


					// Remove gallery images
					$('a.remove-image').click(function(event){

						event.preventDefault();
						var $this = $(this);
						var gallery_thumb = $this.closest('.gallery-thumb');
						var loader = $this.siblings('.loader');

						loader.show();

						var removal_request = $.ajax({
							url: ajaxURL,
							type: "POST",
							data: {
								property_id : $this.data('property-id'),
								attachment_id : $this.data('attachment-id'),
								action : "remove_gallery_image",
								nonce : uploadNonce
							},
							dataType: "html"
						});

						removal_request.done(function( response ) {
							var result = $.parseJSON( response );
							if( result.attachment_removed ){
								gallery_thumb.remove();
							} else {
								document.getElementById('errors-log').innerHTML += "<br/>" + "Error : Failed to remove attachment";
							}
						});

						removal_request.fail(function( jqXHR, textStatus ) {
							alert( "Request failed: " + textStatus );
						});

					});  // end of remove gallery thumb click event

				};  // end of bind thumbnail events

				bindThumbnailEvents(); // run it first time - required for property edit page


			} );
		</script>







		<button type="submit">
			<i class="fa fa-check"></i>
			<?php esc_html_e( "Save", 'javospot' ); ?>
		</button>

		<input type="hidden" name="action" value="jvfrm_spot_add_event">
		<input type="hidden" name="post_id" value="<?php echo $edit->ID; ?>">

	</form>
</div>
<?php
get_footer();