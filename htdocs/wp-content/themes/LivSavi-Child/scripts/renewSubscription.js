jQuery(document).ready(function($) {
	$("#RenewSubscription").click(function() {
		var checked = document.getElementById("RenewSubscription").checked;

  		var data = {
			'action': 'setSubscriptionRenewal',			
        	checked: checked
		};

		// We can also pass the url value separately from ajaxurl for front end AJAX implementations
		jQuery.post(ajax_object.ajax_url, data, function(response) {});
	});	
});