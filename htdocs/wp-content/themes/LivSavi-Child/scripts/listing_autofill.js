window.addEventListener('load', function() {
	var parameters = parseQueryString(window.location.search.substring(1));

	for (var key in parameters) {
  		InsertValues(key, parameters[key]);
	}
})

function InsertValues(key, val)
{
	key = decodeURIComponent(key); 
	val = cleanVal(decodeURIComponent(val)); 

	if (key.charAt(0) === '_')
	{
		var contentField = document.getElementsByName("lava_additem_meta[" + key + "]")[0];

		if (contentField != null)
		{
			if (contentField.type === "text")
			{
				contentField.value = val;
			}
			else if (contentField.type === "checkbox")
			{ 
				contentField.checked = true;
			}
		}
	}
	else if (!key.startsWith("post_"))
	{
		var contentField = document.getElementsByName(key)[0];

		if (contentField == null)
		{
			contentField = document.getElementsByClassName(key)[0];
			//if it is still null return
			if (contentField == null)
			{
				return;
			}
		}		
		
		if (contentField.type === "text" || contentField.type == "textarea")
		{
			contentField.value = val;
		}
		else if (contentField.type === "checkbox")
		{ 
			contentField.checked = true;
		}
	}
	
}

var parseQueryString = function( queryString ) {
    var params = {}, queries, temp, i, l;
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for ( i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
};

var cleanVal = function( parameters ) {
	if (parameters.indexOf('+') > -1)
	{
	  	return parameters.split("+").join(" ");
	}
	else
	{
		return parameters;
	}    
};