<?php 

add_action( 'wp_enqueue_scripts', 'javo_enqueue_styles');
function javo_enqueue_styles() {
    wp_enqueue_style( 'my-main-style', get_stylesheet_uri() );

    wp_enqueue_script('your-membership', get_stylesheet_directory_uri() . '/scripts/renewSubscription.js', array('jquery'));
    wp_localize_script( 'your-membership', 'ajax_object',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234) );

    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( strtolower ($user_agent), 'gonative') !== false) {
        echo '<style>.javo-main-navbar, .navbar-mobile-wrap, #header, #footer, .header, .jvbpd-page-builder-footer, .ls-app-never {display:none !important}</style> ';
    }
    else {
        echo '<style>.ls-app-only {display:none !important}</style> ';
    }
}

global $pagenow;
if (! empty($pagenow) && ('post-new.php' === $pagenow || 'post.php' === $pagenow ))
    add_action('admin_enqueue_scripts', 'enqueue_my_admin_scripts');

function enqueue_my_admin_scripts() {
    wp_enqueue_script( 'listing_autofill', get_stylesheet_directory_uri() . '/scripts/listing_autofill.js' );
}

add_filter( 'woocommerce_variable_free_price_html',  'hide_free_price_notice' );
 
add_filter( 'woocommerce_free_price_html',           'hide_free_price_notice' );
 
add_filter( 'woocommerce_variation_free_price_html', 'hide_free_price_notice' );
 
function woocommerce_new_pass_redirect( $user ) {
  wp_safe_redirect( '/password-reset-success' );
  exit;
}

add_action( 'woocommerce_customer_reset_password', 'woocommerce_new_pass_redirect' );
 
/**
 * Hides the 'Free!' price notice
 */
function hide_free_price_notice( $price ) {
 
  return '';
}


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_city']);
    return $fields;
}


add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); 

function woo_custom_order_button_text() {
    return __( 'CONFIRM REDEEM', 'woocommerce' ); 
}


/* woocommerce skip cart and redirect to checkout. */
function woocommerce_skip_cart() {
	$checkout_url = WC()->cart->get_checkout_url();
	return $checkout_url;
}
add_filter ('woocommerce_add_to_cart_redirect', 'woocommerce_skip_cart');


// Only one product in cart
add_filter( 'woocommerce_add_cart_item_data', 'only_one_redemption_in_cart' );
function only_one_redemption_in_cart( $cart_item_data ) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
 
    return $cart_item_data;
}



function wc_renaming_order_status( $order_statuses ) {
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-completed' === $key ) {
            $order_statuses['wc-completed'] = _x( 'Claimed', 'Order status', 'woocommerce' );
        }
    }
    return $order_statuses;
}
add_filter( 'wc_order_statuses', 'wc_renaming_order_status' );



add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}



if ( ! function_exists( 'woocommerce_order_again_button' ) ) {
	function woocommerce_order_again_button( $order ) {
		return;
	}
}


// Add a custom user role

$result = add_role( 'freeuser', __('Free User' ),
	array(
		'read' => true, // true allows this capability
		'edit_posts' => false, // User can't edit their own posts
		'edit_pages' => false, // User can't edit pages
		'edit_others_posts' => false, // User can't edit others posts
		'create_posts' => false, // User can't create new posts
		'manage_categories' => false, // User can't manage post categories
		'publish_posts' => false, // User can't publish posts
		'edit_themes' => false, // false denies this capability. User can’t edit your theme
		'install_plugins' => false, // User cant add new plugins
		'update_plugin' => false, // User can’t update any plugins
		'update_core' => false // user cant perform core updates
	) 
);

function grid_Article_Count($atts){
	echo '<p id="count"></p>
		<script>
			document.addEventListener(\'DOMContentLoaded\', function() {
   				var x = document.getElementsByTagName("article").length - 1;
				if( x == 1 ){
					document.getElementById("count").innerHTML = "You have " + x + " coupon available:";
				}
				else{
					document.getElementById("count").innerHTML = "You have " + x + " coupons available:";
				}   			
		  	}, false);
		</script>';
}

add_shortcode('gridArticleCount','grid_Article_Count');

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

//restricted button shortcode
function restricted_button( $atts ){
    $a = shortcode_atts( array(
	'title' => 'something',
        'url' => 'something',
    ), $atts );
    
    $subscription = rcp_get_subscription( get_current_user_id() );
    $status = rcp_get_status();
    if( $subscription === 'LIVSAVI MEMBER' ) {
	if( $status === 'active' || $status === 'Active' ){
	    $str = '[vc_btn title="' . $a['title'] . '" link="url:' . $a['url'] . '|||"]';
	    echo do_shortcode ( $str );
	}
    	else{
		echo do_shortcode ( '[vc_btn title="RENEW SUBSCRIPTION" link="url:/register"]' );
	}
    }
    else{
        if ( $subscription == 'FREE SUBSCRIBER'){
    		echo do_shortcode ( '[vc_btn title="UPGRADE TO REDEEM" link="url:/register"]' );
    	}
    	else{
    		echo do_shortcode ( '[vc_btn title="SIGN UP TO REDEEM" link="url:/register"]' );
    	}
    }
}
add_shortcode('restrictedbutton', 'restricted_button' );

//restricted button shortcode
function restrict_link_to_app( $atts ){
    $a = shortcode_atts( array(
        'link' => '',
        'title' => 'Redeem'
    ), $atts );

    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    //                 ... ie we get a posistion number indication the string matches and we are on the app
    if ($a['link'] != '' && strpos( strtolower ($user_agent), 'gonative') > 0){
        echo '<a href="'.$a['link'].'" class="link_button">'.$a['title'].'</a>';
    }
}
add_shortcode('RestrictLinkToApp', 'restrict_link_to_app' );

//restricted button shortcode
function get_api_code(){
    $url = 'https://livsavi.ticketmates.net/authentication/login';

    $post = [
        'username' => 'livsaviapi',
        'password' => 'eZ7vT4cX5kA2hI2o'
    ];

    $data = '{
      "username": "livsaviapi",
      "password": "eZ7vT4cX5kA2hI2o"
    }';

    $additional_headers = array(
       'accept: application/json',
       'Content-Type: application/x-www-form-urlencoded'
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=livsaviapi&password=eZ7vT4cX5kA2hI2o");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // close the connection, release resources used
    curl_close($ch);
    if ($retcode == 200)
    {
        $data = json_decode($server_output);
        return $data->token;
    } else {
        return $retcode.'^'.$server_output;
    }
}

function get_experienceoz_product( $atts ){
    $a = shortcode_atts( array(
        'url' => ''
    ), $atts );

    $apiKey = get_transient( 'ticketmates_api_key' );
    // Get any existing copy of our transient data
    if ( false === $apiKey ) {
        // It wasn't there, so regenerate the data and save the transient
        $apiKey = get_api_code();
        set_transient( 'ticketmates_api_key', $apiKey, 60 * 9 );
    }
    $url = 'https://livsavi.ticketmates.net/en/api/v3/'.$a['url'];

    $additional_headers = array(
       'accept: application/json',
       'Authorization: Bearer '.$apiKey
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // close the connection, release resources used
    curl_close($ch);
    if ($retcode == 200)
    {
        $data = json_decode($server_output);
        $result = '<div class="experience_oz_product">';
        //$result = $result.'<p class="experience_oz_title">'.$data->name.'</p>';

        if ($data->productSessions > 0)
        {
            $retailPrice = fix_cents( $data->productSessions[0]->productOptions[0]->retailPrice );
            $price = fix_cents( $data->productSessions[0]->productOptions[0]->price );

            $result = $result.'<p class="experience_price">Was <del>$'.$retailPrice.'</del> now $'.$price.'</p>'; 
            if ($data->productSessions > 1)
            {
                $result = $result.'<p class="experience_price_warning"><i>(Prices may vary by session time)</i></p>';
            }
        }

        $result = $result.'<p class="experience_oz_highlights_title">Highlights</p>'; 
        $result = $result.'<ul class="experience_oz_highlights">';
        foreach ($data->highlights as $key => $value) {
            $result = $result.'<li>'.$value.'</li>';
        }
        $result = $result.'</ul>';
        $result = $result.'<p class="experience_oz_description">'.$data->description.'</p>';
        $result = $result.'</div>'; 
        return $result; 
    }
    // else {
    //    return '<p style="word-break: break-all;"> 
    //        <span>'.$apiKey.'</span>
    //        <span>'.$url.'</span>
    //        <span>'.$server_output.'</span>
    //        <span>'.$retcode.'</span>
    //    </p>';
    //}
}
add_shortcode('GetExperienceozProduct', 'get_experienceoz_product' );

function get_experienceoz_product_info( $atts ){
    $a = shortcode_atts( array(
        'url' => ''
    ), $atts );

    $apiKey = get_transient( 'ticketmates_api_key' );
    // Get any existing copy of our transient data
    if ( false === $apiKey ) {
        // It wasn't there, so regenerate the data and save the transient
        $apiKey = get_api_code();
        set_transient( 'ticketmates_api_key', $apiKey, 60 * 9 );
    }
    $url = 'https://livsavi.ticketmates.net/en/api/v3/'.$a['url'];

    $additional_headers = array(
       'accept: application/json',
       'Authorization: Bearer '.$apiKey
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // close the connection, release resources used
    curl_close($ch);
    if ($retcode == 200)
    {
        $data = json_decode($server_output);
        $result = '<div class="experience_oz_product_info">';

        foreach ($data->moreInformation as $key => $value) {
            $expandedContent = explode(PHP_EOL, $value->content);

            $result = $result.'<p class="experience_oz_moreInfo_heading">'.$value->heading.'</p>'; 

            $result = $result.'<ul class="experience_oz_moreInfo_value">';
            foreach ($expandedContent as $key => $value) {
                if (trim(str_replace('*', '', $value)) !== ''){
                    $result = $result.'<li>'.trim(str_replace('*', '', $value)).'</li>';
                }
            }
            $result = $result.'</ul>';
        }
        $result = $result.'</div>'; 
        return $result; 
    }
}
add_shortcode('GetExperienceozProductInfo', 'get_experienceoz_product_info' );

function fix_cents( $price ){
    if (strrpos($price, '.')) 
    {
        return sprintf("%01.2f", $price);
    }

    return $price;
}

//promotion button
function promotion_button( $atts){
    $a = shortcode_atts( array(
        'promotion_level' => 'something',
        'title' => 'something',
        'paid_link' => 'something',
        'free_link' => 'something',
        'paid_claim_url' => 'something',
        'free_claim_url' => 'something',
        ), $atts );

	$subscription = rcp_get_subscription( get_current_user_id() );
    $status = rcp_get_status();
    //if a member
    if( $subscription === 'LIVSAVI MEMBER' ) {
        //the offer is a paid offer
        if ($a['promotion_level'] === "PAID"){
            // and currently paying then full price
            if( $status === 'active' || $status === 'Active' ){
                $str = '[vc_btn title="' . $a['title'] . '" link="url:' . $a['paid_claim_url'] . '|||"]';
                echo do_shortcode ( $str );
            }
            //resign up
            else{
                echo do_shortcode ( '[vc_btn title="RENEW SUBSCRIPTION" link="url:https%3A%2F%2Fwww.livsavi.com%2Fregister%2F|||"]' );
            }
        }
        else{
            $str = '[vc_btn title="CLAIM YOUR MEMBER OFFER"' . ' link="url:' . $a['paid_link'] . '|||"]';
            echo do_shortcode ( $str );
        }
    }
    else{
        if ($a['promotion_level'] === "PAID"){
            $str = '[vc_btn title="GO TO FREE OFFER"' . ' link="url:' . $a['free_link'] . '|||"]';
            echo do_shortcode ( $str );
        }
        else{
            //if a free account then see the full price then see "go to free offer"
            if ( $subscription == 'FREE SUBSCRIBER'){
                $str = '[vc_btn title="CLAIM FREE OFFER"' . ' link="url:' . $a['free_claim_url'] . '|||"]';
                echo do_shortcode ( $str );
            }
            //if not a member the see free offer -> signup to redeem
            else{
                echo do_shortcode ( '[vc_btn title="SIGN UP TO REDEEM" link="url:https%3A%2F%2Fwww.livsavi.com%2Fregister%2F|||"]' );
            }
        }
        
    }
}
add_shortcode('promotionButton', 'promotion_button' );

// Add favorite button above the Description tag
function custom_favorite_button_deals( $atts){
    // If it is the app just return the basic shortcode.
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( strtolower ($user_agent), 'gonative') !== false) {
        echo do_shortcode('[favorite_button]');
    }
    else {
        echo '<style>
            @media screen and (min-width: 677px) {
                .CustomFavoriteButton{
                    position: absolute;
                    margin: calc(-47%) 0 0 calc(52%);
                }
                .CustomFavoriteButton .simplefavorite-button {
                    display: block;
                    margin: 0 auto;
                }
            }
        </style>
        <div class="CustomFavoriteButton">';
        echo do_shortcode('[favorite_button]');
        echo '</div>';
    }
}
add_shortcode('customFavoriteButton', 'custom_favorite_button_deals' );

function custom_favorite_button_deals_plus( $atts){
    // If it is the app just return the basic shortcode.
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    if(strpos( strtolower ($user_agent), 'gonative') !== false) {
        echo do_shortcode('[favorite_button]');
    }
    else {
        echo '<style>
            @media screen and (min-width: 677px) {
                .CustomFavoriteButton{
                    position: absolute;
                    margin: calc(-50%) 0 0 calc(52%);
                }
                .CustomFavoriteButton .simplefavorite-button {
                    display: block;
                    margin: 0 auto;
                }
            }
        </style>
        <div class="CustomFavoriteButton">';
        echo do_shortcode('[favorite_button]');
        echo '</div>';
    }
}
add_shortcode('customFavoriteButtonDealsPlus', 'custom_favorite_button_deals_plus' );

/*
 * wc_remove_related_products
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );



add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;
	
	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'coupons' ), // Don't display products in the coupons category on the shop page
			'operator' => 'NOT IN'
		)));
	
	}

	remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}


add_filter('woocommerce_login_redirect', 'wc_login_redirect');
 
function wc_login_redirect( $redirect_to ) {
     $redirect_to = 'https://www.livsavi.com/apphome/';
     return $redirect_to;
}


add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}


/**
 * @snippet       WooCommerce Add New Tab @ My Account
 */
 
 
// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error (settings -> permalinks)
 
function add_woocomerce_endpoints() {
    add_rewrite_endpoint( 'your-membership', EP_PAGES );
    add_rewrite_endpoint( 'your-favorites', EP_PAGES );
}
 
add_action( 'init', 'add_woocomerce_endpoints' );
 
 
// ------------------
// 3. Insert the new endpoint into the My Account menu
 
function add_woocomerce_menu_items( $items ) {
    $items['your-membership'] = 'Membership Details';
    $items['your-favorites'] = 'Favorites';
    return $items;
}
 
add_filter( 'woocommerce_account_menu_items', 'add_woocomerce_menu_items' );
 
 
// ------------------
// 4. Add content to the new endpoint
 
function my_membership_details_content() {
	if (rcp_is_active() && rcp_get_subscription_id() == 1)
	{
		echo '<p>You are already a full livsavi member, Your membership is set to expire on <b>';
		echo do_shortcode('[user_expiration]');
		echo '</b></p>';

		echo '<br />';
		echo '<p>If you would like to Auto Renew your paid membership ensure the box below is checked. To cancel your paid membership on your expiry date please uncheck the box below</p>';

		if (rcp_is_recurring())
		{
			echo '<input type="checkbox" id="RenewSubscription" value="RenewSubscription" checked>Renew Livsavi Subscription<br/>';
		}
		else 
		{
			echo '<input type="checkbox" id="RenewSubscription" value="RenewSubscription">Renew Livsavi Subscription<br/>';
		}

		echo "<br/>";
		echo "<br/>";

		echo "<p>If you wish to update your credit card details please come back to this page once your subscription has expired and enter your new details.</p>";
	}
	else 
	{
		echo '<p>You are a free LivSavi member, sign up below to enjoy the full livsavi experience.</p>';

	    echo do_shortcode( '[register_form id="1" logged_in_header="Upgrade your subscription"]' );
	}
}
 
add_action( 'woocommerce_account_your-membership_endpoint', 'my_membership_details_content' );

function favorites_details_content() {
    echo '<h1> Favorites </h1> <br/>';
    echo do_shortcode( '[user_favorites]' );
}

add_action( 'woocommerce_account_your-favorites_endpoint', 'favorites_details_content' );

//Reorder the woocomerce my account menu items
 function my_account_menu_order() {
    $menuOrder = array(
        'dashboard'          => __( 'Dashboard', 'woocommerce' ),
        'orders'             => __( 'Orders', 'woocommerce' ),
        'edit-account'      => __( 'Account Details', 'woocommerce' ),
        'your-membership'      => __( 'Membership', 'woocommerce' ),
        'your-favorites'      => __( 'Favorites', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),
    );
    return $menuOrder;
 }
 add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

add_action( 'wp_ajax_setSubscriptionRenewal', 'setSubscriptionRenewal' );

function setSubscriptionRenewal() {
	$member = new RCP_Member( get_current_user_id() );

	if ($_POST['checked'] == "true")
	{
		$member->set_recurring( true );
		echo "Your membership will be renewed on your expiry date. Thank you for continuing to be a LivSavi User!";
	}
	else 
	{
		$member->set_recurring( false );
		echo "Your membership will not be renewed on your expiry date. Thank you for being a LivSavi User.";
	}
    wp_die();
}

add_action( 'wp_ajax_setSubscriptionRenewal', 'setSubscriptionRenewal' );

add_filter( 'woocommerce_get_price_html', function( $price ) {
    if ( is_admin() ) return $price;

    return '';
} );

/**
 * Adding message if the endpoint is empty
 */
function woocommerce_account_orders_endpoint_addon() {
    if(!has_bought())
    {
        echo '<p>There is no Redemption History, redeem a voucher or offer and see it here</p>'; 
    }
}

add_action( 'woocommerce_account_orders_endpoint', 'woocommerce_account_orders_endpoint_addon' );


// Return true is the user has made a purchase
function has_bought( $user_id = 0 ) {
    global $wpdb;
    $customer_id = $user_id == 0 ? get_current_user_id() : $user_id;
    $paid_order_statuses = array_map( 'esc_sql', wc_get_is_paid_statuses() );

    $results = $wpdb->get_col( "
        SELECT p.ID FROM {$wpdb->prefix}posts AS p
        INNER JOIN {$wpdb->prefix}postmeta AS pm ON p.ID = pm.post_id
        WHERE p.post_status IN ( 'wc-" . implode( "','wc-", $paid_order_statuses ) . "' )
        AND p.post_type LIKE 'shop_order'
        AND pm.meta_key = '_customer_user'
        AND pm.meta_value = $customer_id
    " );

    // Count number of orders and return a boolean value depending if higher than 0
    return count( $results ) > 0 ? true : false;
}

// Stop outlining the version of wordpress in a head meta tag.
add_filter( 'the_generator', '__return_empty_string' );
?>